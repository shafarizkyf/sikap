<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListKajur extends Model
{

    protected $fillable = ['id', 'dosen_id', 'jurusan_id'];

    public function dosen(){
        return $this->belongsTo('App\ListDosen');
    }

    public function jurusan(){
        return $this->belongsTo('App\ListJurusan');
    }

}
