<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KuotaDosen extends Model
{

    public $fillable = ['id', 'komisi_id', 'dosen_id', 'jumlah', 'status'];

    public function dosen(){
        return $this->belongsTo('App\ListDosen');
    }

    public function komisi(){
        return $this->belongsTo('App\ListDosen');
    }

}
