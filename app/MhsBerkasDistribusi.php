<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsBerkasDistribusi extends Model
{
    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function getCreatedAtAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }

}
