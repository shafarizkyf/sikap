<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsKp extends Model
{

    public $timestamps = false;

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }


    public function pembimbing(){
        return $this->hasOne('App\MhsPembimbing', 'nim_id', 'nim_id');
    }



    public function tempat(){
        return $this->belongsTo('App\MhsSuratPengantarKp');
    }

    public function getWaktuDaftarAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }

}
