<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsNilai extends Model
{

	public function mhs(){
		return $this->belongsTo('App\ListMahasiswa', 'nim_id');
	}

    public function getCreatedAtAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

}
