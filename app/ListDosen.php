<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListDosen extends Model{
  
  public $fillable = ['id', 'jurusan_id', 'nama', 'nip', 'nidn', 'kuota', 'telp'];
  
  public function jurusan(){
    return $this->belongsTo('App\ListJurusan');
  }
  
  public static function getwda(){
    $wda_user = User::where('status','wda')->first();
    $wda = ListDosen::find($wda_user->user_id);
    return $wda;
  }
  
  public function scopeJurusan($query, $idJurusan){
    return $query->where('jurusan_id', $idJurusan);
  }
  
}
