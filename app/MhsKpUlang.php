<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MhsKpUlang extends Model
{
    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function getWaktuDaftarAttribute($date){
        return Carbon::parse($date);

    }
}
