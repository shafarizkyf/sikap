<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsProgressStatus extends Model
{

    public $timestamps = false;

    protected $fillable = ['nim_id', 'status_berkas', 'status_koreksi', 'status_judul', 'spk_berkas', 'status_spk',
    'waktu_spk_jadi', 'waktu_spk_mulai', 'waktu_spk_selesai', 'status_bimbingan', 'status_distribusi', 'status_kp', 'waktu_kp_selesai'];

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function nilai(){
        return $this->hasOne('App\MhsNilai', 'nim_id', 'nim_id');
    }

    public function spk(){
        return $this->hasOne('App\MhsSpk', 'nim_id', 'nim_id');
    }

    public function getWaktuBerkasLengkapAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

    public function getWaktuJudulLayakAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

    public function getWaktuSpkJadiAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

    public function getWaktuSpkMulaiAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

    public function getWaktuSpkSelesaiAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

    public function getWaktuKpSelesaiAttribute($date){
        Date::setLocale('id');
        return ($date) ? Date::parse($date) : null;
    }

    public function getSpkJalanBulan(){
      return Carbon::now()->diffInMonths($this->waktu_spk_mulai,false);
    }

    public function getSpkJalanHari(){
      return Carbon::now()->diffInDays($this->waktu_spk_mulai,false);
    }

    public function getSisaWaktuBulan(){
      return Carbon::now()->diffInMonths($this->waktu_spk_selesai,false);
    }

    public function getSisaWaktuHari(){
      return Carbon::now()->diffInDays($this->waktu_spk_selesai,false);
    }
}
