<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsSuratPengantarKp extends Model{
  
  public $timestamps = false;
  
  public $fillable = [
    'id', 'nim_id', 'nomor', 'instansi', 'kepada', 'dimana','waktu_ajukan', 
    'periode_dari', 'periode_ke', 'waktu_selesai', 'status'
  ];
  
  public function nim(){
    return $this->belongsTo('App\ListMahasiswa');
  }
  
  public function getWaktuAjukanAttribute($date){
    Date::setLocale('id');
    return Date::parse($date);
  }
  
  public function getPeriodeDariAttribute($date){
    Date::setLocale('id');
    return ($date) ? Date::parse($date) : null;
  }
  
  public function getPeriodeKeAttribute($date){
    Date::setLocale('id');
    return ($date) ? Date::parse($date) : null;
  }
  
  public function getWaktuSelesaiAttribute($date){
    Date::setLocale('id');
    return ($date) ? Date::parse($date) : null;
  }
  
}
