<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'web'], function () {

    /** FRONT END CONTROLLER */
    Route::group(['prefix'=>'/'], function (){
        Route::get('/', 'HomeController@index')->name('home_index');
        Route::get('seminar', 'HomeController@seminar')->name('home_seminar');
        Route::get('surat', 'HomeController@surat')->name('home_surat');
        Route::get('spk', 'HomeController@spk')->name('home_spk');
        Route::get('bantuan', 'HomeController@bantuan')->name('home_bantuan');
        Route::get('judul', 'HomeController@judul')->name('home_judul');

        Route::get('dev', function(){
            die("
                Sistem Ini Dibuat Oleh Shafa' Rizky Fandestika (H1L014046) dan Aji Sulistyo Nugroho (H1L014014) atas
                Inisiatif Wakil Dekan I Dr.Gito Sugiyanto dan Disempurnakan Bersama TIM KOMISI (Swahesti, Mulki, Intang)
                ");
        });
    });

    /** PROSES LOGIN DAN LOGOUT */
    Route::get('login', 'UserLoginController@index')->name('login');
    Route::get('logout', 'UserLoginController@logout')->name('logout');
    Route::post('login', 'UserLoginController@process')->name('login_process');
    Route::post('update/keamanan', 'UserLoginController@gantiPassword');
    Route::post('update/username', 'UserLoginController@gantiUsername');

    /** IMPORT - EXPORT DATA */
    Route::get('import_export', 'ExportImportController@index')->name('import_export');
    Route::get('export', 'ExportImportController@export')->name('export');

    Route::post('export', 'ExportImportController@export');
    Route::post('import-data', 'ExportImportController@import')->name('import_process');


    /** Ganti Dosbing */
    Route::post('gantidosbing/acc/{id}', 'MhsGantiDosbingController@accept');
    Route::post('gantidosbing/reject/{id}', 'MhsGantiDosbingController@reject');

    /** DOWNLOADABLE FILES */
    Route::group(['prefix'=>'download'], function (){
        Route::get('sample/{file}', 'DownloadController@sample')->name('download_sample_import');
        Route::get('berkas/{file}', 'DownloadController@download')->name('download_berkas');
    });

    /** PRINTABLE DOCUMENTS */
    Route::group(['prefix'=>'doc'], function (){
        Route::get('form_surat_pengantar/{nim}', 'CetakDokumenMhsController@formSuratPengantarKp')->name('cetak_form_surat_pengantar');
        Route::get('lembar_permohonan/{nim}', 'CetakDokumenMhsController@lembarPermohonanKp')->name('cetak_lembar_permohonan');
        Route::get('surat_pengantar/{nim}', 'CetakDokumenMhsController@suratPengantarKp')->name('cetak_surat_pengantar');
        Route::get('spk/{nim}', 'CetakDokumenMhsController@spk')->name('cetak_spk');
        Route::get('kartu_kendali/{nim}', 'CetakDokumenMhsController@kartuKendali')->name('cetak_kartu_kendali');
        Route::get('seminar/{nim}', 'CetakDokumenMhsController@seminar')->name('cetak_seminar');
        Route::get('nilai/{nim}', 'CetakDokumenMhsController@nilai')->name('cetak_nilai');

        Route::get('spklama/{nim}', 'CetakDokumenMhsController@spkLama')->name('cetak_spklama');
        Route::get('suratlama/{nim}', 'CetakDokumenMhsController@suratLama')->name('cetak_suratlama');
        Route::get('form_ganti_pembimbing/{nim}', 'CetakDokumenMhsController@formGantiDosbing')->name('cetak_gantidosen');

        Route::get('view/{file}/{nim}', 'CetakDokumenMhsController@viewFile')->name('view_file');
        Route::get('pdf/{file}/{nim}', 'CetakDokumenMhsController@viewPdf')->name('view_pdf');
    });

    /** FILE DISTRIBUSI */
    Route::group(['prefix'=>'dist'], function (){
        Route::post('save/{id}', 'DistribusiController@save');
    });

    /** MODAL FORM */
    Route::group(['prefix'=>'modal'], function (){
        Route::get('surat/{id}', 'ModalController@surat')->name('modal_surat');
        Route::post('surat/set', 'ModalController@setSurat');

        Route::get('koreksi/{id}', 'ModalController@koreksi')->name('modal_koreksi');
        Route::post('koreksi/update/{id}', 'ModalController@simpanKoreksiBerkas');

        Route::get('koreksikomisi/{id}', 'ModalController@koreksiKomisi')->name('modal_koreksi_komisi');
        Route::post('koreksikomisi/update/{id}', 'ModalController@simpanKoreksiBerkasKomisi');

        Route::get('kelayakan/{id}', 'ModalController@kelayakan')->name('modal_kelayakan');
        Route::post('kelayakan/update/{id}', 'ModalController@setKelayakan');

        Route::post('pemlap/update/{id}', 'ModalController@setPemlap');

        Route::get('spk/{id}', 'ModalController@spk')->name('modal_spk');
        Route::post('spk/update/{id}', 'ModalController@setSpk');

        Route::get('bimbingan/{id}', 'ModalController@bimbingan')->name('modal_spk');
        Route::patch('bimbingan/update/{id}', 'ModalController@updateMateriBimbingan');

        Route::get('judul/{nim}', 'ModalController@judul')->name('modal_judul');
        Route::post('judul/update/{id}', 'ModalController@setJudul');

        Route::get('judulkp/{nim}', 'ModalController@judulKp')->name('modal_judulkp');
        Route::post('judulkp/update/{id}', 'ModalController@setJudulKp');

        Route::get('seminar/{nim}', 'ModalController@seminar')->name('modal_seminar');
        Route::post('seminar/update/{id}', 'ModalController@setSeminar');

        Route::get('nilai/{nim}', 'ModalController@nilai')->name('modal_nilai');
        Route::post('nilai/update/{id}', 'ModalController@setNilai');
        Route::patch('nilai/update/{id}/update', 'ModalController@updateNilai');
        Route::patch('nilai/update/{id}/finalisasi', 'ModalController@finalisasiNilai');

        Route::get('kuota/{id}', 'ModalController@kuota')->name('modal_kuota');
        Route::post('kuota/update/{nip}', 'ModalController@setKuota');

        Route::get('addspk/{id}', 'ModalController@addSpk')->name('modal_addspk');
        Route::post('addspk/update/{id}', 'ModalController@setAddSpk');

        Route::get('mhsbimbingan/{id}', 'ModalController@mhsBimbingan')->name('modal_mhsbimbingan');

        Route::get('dosendetail/{id}', 'ModalController@dosenDetail')->name('modal_dosendetail');
        Route::post('dosendetail/update/{id}', 'ModalController@setDosenDetail');

        Route::get('dosenakun/{id}', 'ModalController@dosenAkun')->name('modal_dosendetail');
        Route::post('dosenakun/update/{id}', 'ModalController@setDosenAkun');

        Route::post('save/kajur', 'ModalController@setKajur');

        Route::get('beritaacara/{id}', 'ModalController@beritaAcara')->name('modal_beritaacara');
        Route::post('beritaacara/set', 'ModalController@setBeritaAcara');

        Route::get('dist/{file}/{id}', 'ModalController@distribusi')->name('modal_distribusi');
        Route::post('dist/update/{id}', 'ModalController@updateBerkasDistribusi');

        Route::get('dis/{id}', 'ModalController@berkasDist');
        Route::post('dis/{id}', 'ModalController@accDist');

        Route::get('gantidosbing/{id}', 'ModalController@gantiDosbing');
        Route::post('gantidosbing/{id}', 'ModalController@gantiDosbingAksi');


    });

    /** PENGAJUAN SEMINAR */
    Route::group(['prefix'=>'seminar'], function (){
        Route::get('delete/{id}', 'SeminarController@deleteJadwal')->name('seminar_delete');
        Route::get('change', 'SeminarController@clearSession')->name('seminar_clear_session');

        Route::get('selesai/{id}', 'SeminarController@seminarSelesai')->name('seminar_selesai');
        Route::get('ulang/{id}', 'SeminarController@seminarUlang')->name('seminar_ulang');

        Route::post('req/jadwal', 'SeminarController@reqJadwalSeminar');
        Route::post('save/pengajuan', 'SeminarController@saveBerkasSeminar');
        Route::post('update/pengajuan/{id}', 'SeminarController@updateBerkasSeminar');
        Route::post('save/jenisseminar/{nim}', 'SeminarController@setJenis');

    });

    /** MAHASISWA DAFTAR KP */
    Route::group(['prefix' => 'mahasiswa'], function () {
        Route::get('/', 'MahasiswaDaftarKpController@index')->name('mahasiswa_index');
        Route::get('menu/{menu}', 'MahasiswaDaftarKpController@menu')->name('mahasiswa_menu');
        Route::get('req/surat_pengantar_kp', 'MahasiswaDaftarKpController@reqSuratPengantarKpBaru')->name('surat_pengantar');
        Route::get('finalisasi/bimbingan', 'MahasiswaDaftarKpController@finalisasiBimbingan')->name('finalisasi_bimbingan');
        Route::get('edit/surat_pengantar_kp/{id}', 'MahasiswaDaftarKpController@editSuratPengantarKp')->name('edit_surat');
        Route::get('edit/dosbing/{id}', 'MahasiswaDaftarKpController@editDosbing')->name('mhs_edit_dosbing');

        Route::post('/', 'MahasiswaDaftarKpController@updateMhsData');
        Route::post('u/level', 'MahasiswaDaftarKpController@updateLevel');
        Route::post('pengajuan', 'MahasiswaDaftarKpController@simpanPengajuan');

        Route::post('req/surat_pengantar_kp', 'MahasiswaDaftarKpController@reqSuratPengantarKp');
        Route::post('save/surat_pengantar_kp/{id}', 'MahasiswaDaftarKpController@updateSuratPengantarKp')->name('save_surat');
        Route::post('save/dosbing/{id}', 'MahasiswaDaftarKpController@updateDosbing')->name('mhs_save_dosbing');

        Route::post('revisi/{id}', 'MahasiswaDaftarKpController@revisi');
        Route::post('save/bimbingan', 'MahasiswaDaftarKpController@simpanBimbingan');
        Route::post('ulang/{id}', 'MahasiswaDaftarKpController@ulang');
    });

    /** PROSES PENGAJUAN OLEH BAPENDIK */
    Route::group(['prefix'=>'bapendik'], function (){
        Route::get('/', 'BapendikController@index')->name('bapendik_index');
        Route::get('menu/{menu}', 'BapendikController@menu')->name('bapendik_menu');

        Route::get('kp/surat', 'BapendikController@kpSurat')->name('bapendik_kp_surat');
        Route::get('kp/prasyarat', 'BapendikController@kpPrasyarat')->name('bapendik_kp_prasyarat');
        Route::get('kp/usulan', 'BapendikController@kpUsulan')->name('bapendik_kp_usulan');
        Route::get('kp/usulan/detail/{nim}', 'BapendikController@kpUsulanDetail')->name('bapendik_kp_usulan_detail');
        Route::get('kp/mhs', 'BapendikController@kpMhs')->name('bapendik_kp_mhs');
        Route::get('kp/spklama', 'BapendikController@spkLama')->name('bapendik_kp_spklama');
        Route::get('kp/suratlama', 'BapendikController@suratLama')->name('bapendik_kp_suratlama');

        Route::get('seminar/pengajuan', 'BapendikController@seminarPengajuan')->name('bapendik_seminar_pengajuan');

        Route::get('s/kp/prasyarat/', 'BapendikController@filterKpPrasyarat')->name('bapendik_filer_kp_prasyarat');
        Route::get('s/kp/surat/', 'BapendikController@filterKpSurat')->name('bapendik_filer_kp_surat');
        Route::get('s/kp/usulan/', 'BapendikController@filterKpUsulan')->name('bapendik_filer_kp_usulan');
        Route::get('s/kp/mhs/', 'BapendikController@filterKpMhs')->name('bapendik_filer_kp_mhs');

        Route::get('s/dsn/', 'BapendikController@filterDosenSemua')->name('bapendik_filer_dsn_semua');
        Route::get('s/dosbing/', 'BapendikController@filterDosbing')->name('bapendik_filer_dosbing');
        Route::get('s/seminar/', 'BapendikController@filterPengajuanSeminar')->name('bapendik_filer_seminar');

        Route::post('u/surat/{nim}', 'BapendikController@setStatusSurat');
        Route::post('u/berkas/{nim}', 'BapendikController@setStatusBerkas');

    });

    /** PROSES PENGAJUAN OLEH KOMISI */
    Route::group(['prefix'=>'komisi'], function (){
        Route::get('/', 'KomisiController@index')->name('komisi_index');
        Route::get('menu/{menu}', 'KomisiController@menu')->name('komisi_menu');
        Route::get('kp/usulan', 'KomisiController@kpUsulan')->name('komisi_kp_usulan');
        Route::get('kp/prasyarat', 'KomisiController@kpPrasyarat')->name('komisi_kp_prasyarat');
        Route::get('kp/usulan', 'KomisiController@kpUsulan')->name('komisi_kp_usulan');
        Route::get('kp/progress', 'KomisiController@kpOnGoing')->name('komisi_kp_progress');
        Route::get('kp/ulang', 'KomisiController@kpUlang')->name('komisi_kp_ulang');
        Route::get('kp/bimbingan/{nim}', 'KomisiController@lihatBimbingan')->name('komisi_lihat_bimbingan');
        Route::get('kp/bimbingan/selesai/{nim}', 'KomisiController@bimbinganSelesai')->name('komisi_bimbingan_selesai');
        Route::get('kp/selesai/{nim}', 'KomisiController@kpSelesai')->name('komisi_kp_selesai');

        Route::get('kuota/refresh/{id}', 'KomisiController@kuotaRefresh')->name('komisi_kuota_refresh');

        Route::post('kp/unlock/bimbingan/{id}', 'KomisiController@unlockBimbingan')->name('komisi_unlock_bimbingan');
        Route::get('log/mhs/{id}', 'KomisiController@logMahasiswa')->name('log_mahasiswa');
    });

    /** PROSES BIMBINGAN DENGAN DOSEN */
    Route::group(['prefix'=>'dosen'], function (){
        Route::get('/', 'DosenController@index')->name('dosen_index');
        Route::get('menu/{menu}', 'DosenController@menu')->name('dosen_menu');

        Route::get('kp/bimbingan', 'DosenController@kpBimbingan')->name('dosen_kp_bimbingan');
        Route::get('kp/bimbingan/{nim}', 'DosenController@lihatBimbingan')->name('dosen_lihat_bimbingan');
        Route::get('kp/bimbingan/selesai/{nim}', 'DosenController@bimbinganSelesai')->name('bimbingan_selesai');

        Route::post('kp/unlock/bimbingan/{id}', 'DosenController@unlockBimbingan')->name('dosen_unlock_bimbingan');

    });

    /** PUNYA WAKIL DEKAN AKADEMIK */
    Route::group(['prefix'=>'wda'], function (){
        Route::get('/', 'WdaController@index')->name('wda_index');
        Route::get('menu/{menu}', 'WdaController@menu')->name('wda_menu');
        Route::get('addkuota/{nip}', 'WdaController@addKuota')->name('wda_addkuota');

        Route::get('kp/bimbingan', 'WdaController@kpBimbingan')->name('wda_kp_bimbingan');
        Route::get('kp/bimbingan/{nim}', 'WdaController@lihatBimbingan')->name('wda_lihat_bimbingan');
        Route::get('kp/bimbingan/selesai/{nim}', 'WdaController@bimbinganSelesai')->name('wda_bimbingan_selesai');
        Route::get('kp/selesai/{nim}', 'WdaController@kpSelesai')->name('wda_kp_selesai');

        Route::post('kp/unlock/bimbingan/{id}', 'WdaController@unlockBimbingan')->name('wda_unlock_bimbingan');
        Route::post('settings/update', 'WdaController@updateSettings')->name('wda_update_settings');
    });

    /** ADMIN */
    Route::group(['prefix'=>'admin'], function (){
        Route::get('/', 'AdminController@index')->name('admin_index');
        Route::get('menu/{menu}', 'AdminController@menu')->name('admin_menu');
        Route::get('hapus/kajur/{id}', 'AdminController@hapusKajur')->name('admin_hapus_kajur');

        Route::post('seve/dosenpa', 'AdminController@setDosenpa');
        Route::post('seve/wda', 'AdminController@setWda');
        Route::post('reset/akun/mhs/{id}', 'AdminController@resetAkunMahasiswa');
        Route::post('reset/password/mhs/{id}', 'AdminController@resetPasswordMhs');
    });

});


