<?php

namespace App\Http\Controllers;

use App\ListDosen;
use App\ListMahasiswa;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserLoginController extends Controller
{

    public $_user, $_id, $_isLogin, $_settings;

    public function __construct() {
        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);

        if(Auth::check() && session()->has('who')){
            $who    = explode(' ', session()->get('who'));
            $this->_user    = $who[0];
            $this->_id      = $who[1];
            $this->_isLogin = true;
        }
    }

    public function index(){

        if($this->_isLogin){
            return redirect($this->_user);
        }

        return view('login');
    }

    public function process(Request $request){
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $login =  Auth::attempt([
                    'username'   => $request->username,
                    'password'   => $request->password
                ]);

        if($login){
            $settings = $this->_settings;
            SmsController::sent('Akun Kerja Praktik Anda Dibuka');

            $user       = User::where('username', $request->username)->first();
            $status     = $user->status;
            $who        = $status.' '.$user->user_id;

            //kalau mahasiswa dan belum bisa mengakses halaman
            if($status == 'mahasiswa'){
                $mhs = ListMahasiswa::find($user->user_id);
                if($mhs->sks <= $settings['sks_login']){
                    return view('errors.denied');                    
                }
            }

            session()->put('who', $who);

            if($status == 'mahasiswa'){
                return redirect('mahasiswa');

            }elseif($status == 'dosen'){
                return redirect('dosen');

            }elseif($status == 'komisi'){
                return redirect('komisi');

            }elseif($status == 'bapendik'){
                return redirect('bapendik');

            }elseif($status == 'wda'){
                return redirect('wda');

            }elseif($status == 'admin'){
                return redirect('admin');
            }

        }else{
            session()->flash('login', 'username/password salah');
            return redirect('/login');
        }

    }

    public function logout(){
        Auth::logout();
        session()->flush();
        return redirect('/');
    }

    public static function isLogin($level){
        if(session()->has('who')){
            $user = explode(' ',session()->get('who'));
            if(Auth::check() && $user[0] == $level)
                return $user[1];
        }
        return false;
    }

    public function gantiPassword(Request $request){

        $this->validate($request, [
            'oldpass'   => 'required',
            'newpass'   => 'required',
            'renewpass' => 'required|same:newpass'
        ]);

        $user           = User::where('user_id', $this->_id)->where('status', $this->_user)->first();
        $oldPassCheck   = Hash::check($request->oldpass, $user->password);
        $currPassCheck  = Hash::check($request->newpass, $user->password);

        if($currPassCheck){
            session()->flash('keamanan', 'Password baru tidak boleh sama dengan yang sekarang');
        }elseif($oldPassCheck){
            $user->password = bcrypt($request->newpass);
            $user->save();
            session()->flash('keamanan', 'Password berhasil diperbarui');
        }else{
            session()->flash('keamanan', 'Password lama salah');
        }

        return redirect(route($this->_user.'_menu','keamanan'));

    }

    public function gantiUsername(Request $request){
        $this->validate($request, [
            'oldusername'   => 'required',
            'newusername'   => 'required|unique:users,username',
            'renewusername' => 'required|same:newusername'
        ]);

        $user = User::where('user_id', $this->_id)->where('status', $this->_user)->first();

        if($request->oldusername != $user->username){
            session()->flash('keamanan', 'username lama salah');

        }elseif($request->newusername == $user->username){
            session()->flash('keamanan', 'username baru tidak boleh sama dengan yang sekarang');

        }else{
            $user->update(['username'=>$request->newusername]);
            session()->flash('keamanan', 'username berkasil diganti menjadi '.$request->newusername);
        }

        return Redirect::back();
    }

    public static function getUser($user, $id){
        switch ($user){
            case 'mahasiswa' :
                return ListMahasiswa::find($id);
                break;

            case 'dosen' :
                return ListDosen::find($id);
                break;

            case 'bapendik' :
                return User::where('status', $user)->where('user_id', $id)->first();
                break;

            case 'komisi' :
                return ListDosen::find($id);
                break;

            case 'wda' :
                return ListDosen::find($id);
                break;

            case 'admin' :
                return User::where('status', $user)->where('user_id', $id)->first();
                break;
        }
    }
}
