<?php

namespace App\Http\Controllers;

use App\MhsGantiDosbing;
use App\MhsPembimbing;
use App\MhsSpk;
use App\MhsSpkLama;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class MhsGantiDosbingController extends Controller
{

    public function accept($id){
        //usulan ganti
        $ganti = MhsGantiDosbing::findOrFail($id);
        //spk sekarang
        $spk = MhsSpk::where('nim_id', $ganti->nim_id)->first();

        //ganti usulan
        $dosbing = MhsPembimbing::where('nim_id', $ganti->nim_id)->first();
        $dosbing->dosbinglama_id    = $dosbing->dosbing1_id;
        $dosbing->dosbing1_id       = $ganti->dosbing_id;
        $dosbing->save();

        //simpan spk lama
        $spkLama = new MhsSpkLama();
        $spkLama->nim_id    = $spk->nim_id;
        $spkLama->nomor     = $spk->nomor;
        $spkLama->save();

        //null nomor di spk sekarang
        $spk->update(['nomor'=>null]);

        //ganti status usulan
        $ganti->update(['status'=>'diterima']);

        return Redirect::back();
    }

    public function reject($id){
        MhsGantiDosbing::findOrFail($id)->update(['status'=>'ditolak']);
        return Redirect::back();
    }

}
