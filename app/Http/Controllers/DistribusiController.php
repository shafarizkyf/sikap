<?php

namespace App\Http\Controllers;

use App\ListMahasiswa;
use App\MhsBerkasDistribusi;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class DistribusiController extends Controller
{

    public function save(Request $request, $id){
        $this->validate($request, [
            'formdis'       => 'required|mimes:pdf|max:2000',
            'pengesahan'    => 'required|mimes:pdf|max:2000'
        ]);

        $mhs = ListMahasiswa::find($id);

        //pindah file formdis ke public folder
        $file       = $request->file('formdis');
        $formdis    = $mhs->nim.'_'.time().'_formdis.pdf';
        $file->move("mahasiswa_berkas/".$mhs->jurusan->jurusan."/", $formdis);

        //pindah file formdis ke public folder
        $file       = $request->file('pengesahan');
        $pengesahan = $mhs->nim.'_'.time().'_pengesahan.pdf';
        $file->move("mahasiswa_berkas/".$mhs->jurusan->jurusan."/", $pengesahan);

        $dist = new MhsBerkasDistribusi();
        $dist->nim_id       = $id;
        $dist->form         = $formdis;
        $dist->pengesahan   = $pengesahan;
        $dist->save();

        return Redirect::back();

    }

}
