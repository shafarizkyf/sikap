<?php

namespace App\Http\Controllers;

use App\ListMahasiswa;
use App\MhsBerkasSeminar;
use App\MhsKp;
use App\MhsNilai;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsSpk;
use App\MhsSuratPengantarKp;
use Illuminate\Http\Request;

use App\Http\Requests;

class GetMahasiswaDataController extends Controller
{

    public static $_mhs, $_surat, $_progres, $_mhsKp, $_dosbing;

    public static function getMhsListData($field, $operator = '=', $value){
        return self::$_mhs = ListMahasiswa::where($field, $operator, $value)->first();
    }

    public static function getMhsSurat($field, $operator = '=', $value){
        return self::$_surat = MhsSuratPengantarKp::where($field, $operator, $value)->first();
    }

    public static function getMhsKpProgress($field, $operator = '=', $value){
        return self::$_progres = MhsProgressStatus::where($field, $operator, $value)->first();
    }

    public static function getMhsKp($field, $operator = '=', $value){
        return self::$_mhsKp = MhsKp::where($field, $operator, $value)->first();
    }

    public static function getMhsDosbing($field, $operator = '=', $value){
        return self::$_dosbing = MhsPembimbing::where($field, $operator, $value)->first();
    }

    public static function getTotalDosbing($mhs_id){

        $tdosbing = 0;
        $dosbing  = MhsPembimbing::where('nim_id', $mhs_id)->first();
        if($dosbing){
            ($dosbing->dosbing1_id) ? $tdosbing++ : false;
            ($dosbing->dosbing2_id) ? $tdosbing++ : false;
            ($dosbing->pemlap)      ? $tdosbing++ : false;
        }

        return $tdosbing;
    }

    public static function getMhsStatusBerkas(){
        $mhskp  = MhsKp::orderBy('id', 'desc')->get();
        $status = MhsProgressStatus::all();

        $mhs = null;
        if($mhskp) {
            foreach ($mhskp as $m) {
                foreach ($status as $s) {
                    if ($m->nim_id == $s->nim_id) {
                        $mhs[] = [
                            'nim' => $m->nim->nim,
                            'nama' => $m->nim->nama,
                            'jurusan' => $m->nim->jurusan->jurusan,
                            'waktu' => $m->waktu_daftar->toFormattedDateString(),
                            'status' => ($s->status_berkas) ? $s->status_berkas : 'menunggu',
                            'dikoreksi' => ($s->status_koreksi) ? $s->status_koreksi : '',
                            'status_judul' => ($s->status_judul) ? $s->status_judul : 'menunggu',
                            'status_berkas' => ($s->status_berkas) ? $s->status_berkas : 'menunggu',
                        ];
                    }
                }
            }
        }
        return $mhs;
    }

    public static function getMhsKpDataByNim($nim){

        $mhs    = self::getMhsListData('nim', '=', $nim);
        $mhskp  = self::getMhsKp('nim_id', '=', $mhs->id);
        $surat  = self::getMhsSurat('nim_id', '=', $mhs->id);
        $dosbing= self::getMhsDosbing('nim_id', '=', $mhs->id);
        $progres= self::getMhsKpProgress('nim_id', '=', $mhs->id);

        $data = [
            'nim_id' => $mhs->id,
            'nim' => $mhskp->nim->nim,
            'nama' => $mhs->nama,
            'judul' => $mhskp->judul,
            'instansi' => $surat->instansi,
            'dosbing1' => $dosbing->dosbing1->nama,
            'dosbing2' => ($dosbing->dosbing2) ? $dosbing->dosbing2->nama : '',
            'outline' => $mhskp->outline,
            'surat' => $mhskp->surat,
            'status_berkas' => ($progres) ? $progres->status_berkas : '',
            'status_koreksi' => ($progres) ? $progres->status_koreksi : '',
            'status_judul' => ($progres) ? $progres->status_judul : '',
        ];


        return $data;
    }

    public static function getAllMhsKpByJurusan($jurusan_id){
        $mhs    = ListMahasiswa::where('jurusan_id', $jurusan_id)->get();
        $data   = null;

        foreach ($mhs as $m){
            $mhskp = MhsKp::where('nim_id', $m->id)->first();
            if($mhskp){
                $progress = MhsProgressStatus::where('nim_id', $mhskp->nim_id)->where('status_berkas', 'lengkap')->first();
                if($progress){
                    $surat = MhsSuratPengantarKp::where('nim_id', $mhskp->nim_id)->first();
                    $data[] = [
                        'nim_id'    => $mhskp->nim_id,
                        'nim'       => $mhskp->nim->nim,
                        'nama'      => $mhskp->nim->nama,
                        'judul'     => $mhskp->judul,
                        'instansi'  => $mhskp->tempat->instansi,
                        'waktu'     => $mhskp->waktu_daftar,
                        'status'    => ($progress->status_judul) ? $progress->status_judul : 'baru'
                    ];
                }
            }
        }

        return $data;
    }

    public static function getMhsSpkDataByNim($nim){
        $mhs = ListMahasiswa::where('nim', $nim)->first();
        if(!$mhs)
            abort(404);

    }

    public static function getAllMhsKp(){
        $spk = MhsSpk::where('nomor', '<>', '')->orderBy('id', 'desc')->get();
        $data = null;
        if($spk){
            foreach ($spk as $s){
                $progress   = MhsProgressStatus::where('nim_id', $s->nim_id)->first();
                $nilai      = MhsNilai::where('nim_id', $s->nim_id)->first();
    
                $spkMulai = null;
                if($progress->waktu_spk_mulai){
                    $spkMulai = $progress->waktu_spk_mulai->format('d F Y');
                }

                $spkSelesai = null;
                if($progress->waktu_spk_selesai){
                    $spkSelesai = $progress->waktu_spk_selesai->format('d F Y');
                }
    
                $data[] = [
                    'nim_id'    => $s->nim_id,
                    'nim'       => $s->nim->nim,
                    'nama'      => $s->nim->nama,
                    'jurusan'   => $s->nim->jurusan->jurusan,
                    'awal_spk'  => $spkMulai,
                    'akhir_spk' => $spkSelesai,
                    'nilai'     => ($nilai) ? $nilai->nilai_akhir : '-'
                ];
            }
        }

        return $data;
    }

    public static function getSeminar(){

        $seminar = MhsBerkasSeminar::all();

        $data = null;
        if($seminar){
            foreach ($seminar as $s){
                $jadwal = MhsSeminar::where('nim_id', $s->nim_id)->first();
                if($jadwal){
                    $data[] = [
                        'nim' => $s->nim->nim,
                        'nim_id' => $s->nim_id,
                        'nama' => $s->nim->nama,
                        'tanggal' => ($jadwal->jadwal) ? $jadwal->jadwal->format('d F Y') : 'belum',
                        'jam' => ($jadwal->jadwal) ? $jadwal->jadwal->format('H:i') : 'belum',
                        'status' => ($s->status_berkas) ? $s->status_berkas : 'belum diperiksa'
                    ];
                }
            }
        }
        return $data;
    }

    public static function getMhsKpByJurusan($jurusan_id){
        $spk = MhsSpk::all();

        $data = null;
        foreach ($spk as $s){
            if($s->nim->jurusan->id == $jurusan_id){
                $progress = MhsProgressStatus::where('nim_id', $s->nim_id)->first();
                $mhskp = MhsKp::where('nim_id', $s->nim_id)->first();

                $data[] = [
                    'nim_id'    => $s->nim_id,
                    'nim'       => $s->nim->nim,
                    'nama'      => $s->nim->nama,
                    'jurusan'   => $s->nim->jurusan->jurusan,
                    'judul'     => $mhskp->judul,
                    'instansi'  => $mhskp->tempat->instansi,
                    'bimbingan' => ($progress->status_bimbingan) ? $progress->status_bimbingan : 'menunggu',
                    'awal_spk'  => $progress->waktu_spk_mulai->format('d F Y'),
                    'akhir_spk' => $progress->waktu_spk_selesai->format('d F Y')
                ];
            }
        }

        return $data;

    }

    public static function getMhsSiapSeminarByJurusan($jurusan_id){
        $mhs = ListMahasiswa::where('jurusan_id', $jurusan_id)->get();

        $data = null;
        if($mhs){
            foreach ($mhs as $m){
                $progress = MhsProgressStatus::where('nim_id', $m->id)->first();
                if($progress){
                    $bimbingan  = ($progress->status_bimbingan) ? $progress->status_bimbingan : '';
                    $mhskp = MhsKp::where('nim_id', $m->id)->first();
                    if($bimbingan == 'selesai'){
                        $data[] = [
                            'nama'          => $m->nama,
                            'nim'           => $m->nim,
                            'judul'         => $mhskp->judul,
                            'judul_final'   => ($mhskp->judul_final) ? $mhskp->judul_final : 'belum di tentukan'
                        ];
                    }
                }
            }
        }
        return $data;
    }

    public static function getCurrentLevel($nim){
        $level =  ListMahasiswa::where('nim', $nim)->first()->level;
        return ($level) ? $level : null;
    }

}
