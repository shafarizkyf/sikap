<?php

namespace App\Http\Controllers;

use App\ListDosen;
use App\ListJurusan;
use App\ListKajur;
use App\ListMahasiswa;
use App\MhsKp;
use App\MhsSeminar;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{

    public $_user;

    public function __construct(){
        $userLogin = UserLoginController::isLogin('admin');
        if(!$userLogin) Redirect::to('/')->send();

        $this->_user = User::where('user_id', $userLogin)->where('status', 'admin')->first();
    }

    public function index(){
        return redirect(route('admin_menu', 'dosen'));
    }

    public function menu($menu){
        $menuList = array('dosen', 'mahasiswa', 'kajur', 'keamanan', 'username', 'wda');

        if(in_array($menu, $menuList)) {
            $admin = $this->_user;

            switch ($menu){
                case 'dosen' :

                    $dosen = DB::table('users')
                        ->join('list_dosens', 'users.user_id', '=', 'list_dosens.id')
                        ->join('list_jurusans', 'list_jurusans.id', '=', 'list_dosens.jurusan_id')
                        ->where('users.status', 'dosen')
                        ->orWhere('users.status', 'komisi')
                        ->orWhere('users.status', 'wda')
                        ->orderBy('list_dosens.nama')
                        ->paginate(20);

                    return view('admin.'.$menu, compact('admin', 'dosen'));
                    break;

                case 'kajur' :

                    $kajur      = ListKajur::paginate(20);
                    $dosen      = GetDosenDataController::GetAllDosenListData();
                    $jurusan    = ListJurusan::lists('jurusan', 'id')->all();
                    return view('admin.'.$menu, compact('admin', 'dosen', 'jurusan', 'kajur'));
                    break;

                case 'wda' :
                    $dosen  = GetDosenDataController::GetAllDosenListData();
                    $wda    = User::where('status', 'wda')->first();
                    return view('admin.'.$menu, compact('admin', 'dosen', 'wda'));
                    break;
                    
                case 'mahasiswa' :
                    $dosen = GetDosenDataController::GetAllDosenListData();
                    $mhs = ListMahasiswa::orderBy('nama')->paginate(20);
                    return view('admin.'.$menu, compact('admin','mhs', 'dosen'));

                case 'keamanan' :
                    return view('admin.keamanan', compact('admin'));
                    break;

                case 'username' :
                    return view('admin.username', compact('admin'));
                    break;
            }

        }else{
            abort(404);
        }
    }

    public function setDosenpa(Request $request){

        $this->validate($request, [
            'dari'      => 'required',
            'ke'        => 'required',
            'dosenpa'   => 'required'
        ]);

        $dari   = strtoupper($request->dari);
        $ke     = strtoupper($request->ke);
        $valid  = true;

        $tahunDari  = $dari[4].''.$dari[5];
        $tahunKe    = $ke[4].''.$ke[5];

        $noDari     = $dari[6].''.$dari[7].''.$dari[8];
        $noKe       = $ke[6].''.$ke[7].''.$ke[8];

        // H 1 L 0 1 4 0 4 6

        //nim harus 9 karakter
        if(strlen($dari) != 9){
            session()->flash('nim', 'NIM harus berjumlah 9 karakter');
            $valid = false;
        }

        //nim jurusan harus sama
        if($dari[2] != $ke[2]){
            session()->flash('nim', 'NIM jurusan harus sama');
            $valid = false;
        }

        //nim tahun dari dan ke harus sama
        if($tahunDari != $tahunKe){
            session()->flash('nim', 'NIM tahun harus sama');
            $valid = false;
        }

        //nim dari harus lebih kecil dari nim ke
        if($noDari > $noKe){
            session()->flash('nim', 'NIM dari harus lebih kecil dari nim ke');
            $valid = false;
        }

        if($valid){
            ListMahasiswa::whereBetween('nim', [$dari, $ke])->update([
                'dosenpa_id' => $request->dosenpa
            ]);
        }

        return Redirect::back();
    }

    public function setWda(Request $request){
        //hapus user dengan status wda
        $user = User::where('status', 'wda')->first();
        $user = User::find($user->id)->update(['status'=>'dosen']);
        $user = User::where('user_id', $request->dosen)->where('status', '<>', 'mahasiswa')
                ->where('status', '<>', 'bapendik')
                ->where('status', '<>', 'admin')
                ->update(['status'=>'wda']);

        session()->flash('wda', 'Wakil Dekan Akademik Berhasil Dipilih');
        return Redirect::back();
    }

    public function hapusKajur($id){


        $kajur = ListKajur::find($id);
        if($kajur){
            $kajur->delete();
        }else{
            abort(404);
        }

        return Redirect::back();
    }

    public function resetPasswordMhs(Request $request, $id){
        $mhs            = ListMahasiswa::find($id);
        $dataToUpdate   = ['password'=>bcrypt($mhs->nim)];
        User::where('user_id', $mhs->id)->where('status', 'mahasiswa')->update($dataToUpdate);
        session()->flash('reset','Password Berhasil di Reset');
        return Redirect::back();
    }

    public function resetAkunMahasiswa(Request $request, $id){
        $mhs        = ListMahasiswa::findOrFail($id);
        $mhskp      = MhsKp::where('nim_id', $mhs->id)->first();
        $seminar    = MhsSeminar::where('nim_id', $mhs->id)->first();

        if($mhskp or $seminar){
            session()->flash('reset','Tidak dapat me-reset akun karena dalam proses kp/seminar');
        }else{

            $status = null; $level = null;
            if($mhs->ipk >=2 && $mhs->sks >=120){
                $level = null;
            }elseif ($mhs->ipk >=2 && $mhs->sks >=90){
                $level = 'kp';
            }

            $mhs->update(['level'=>$level]);
        }

        return Redirect::back();
    }
}
