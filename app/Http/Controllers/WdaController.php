<?php

namespace App\Http\Controllers;

use App\KuotaDosen;
use App\ListDosen;
use App\ListMahasiswa;
use App\ListRuangSeminar;
use App\MhsBimbingan;
use App\MhsKp;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;

class WdaController extends Controller
{

    private $_wda;
    private $_settings;

    public function __construct(){

        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);

        $userLogin = UserLoginController::isLogin('wda');
        if(!$userLogin) Redirect::to('/')->send();

        $this->_wda = ListDosen::find($userLogin);
    }

    public function index(){
        return redirect(route('wda_menu', 'mahasiswa'));
    }

    public function menu(Request $request, $menu){

        $menuList = array('mhsa', 'mhsakp', 'dosbing', 'mahasiswa', 'nilai', 'dosen', 'kuota', 'seminar','keamanan', 'username', 'pengaturan', 'laporan');
        if (in_array($menu, $menuList)) {

            $wda = $this->_wda;
            switch ($menu){

                case 'mhsa' :

                    $mhs = ListMahasiswa::where('dosenpa_id', $wda->id);

                    if(isset($request->layakkp)){
                        $layakkp = $request->layakkp == '0' ? 'belum' : 'memenuhi';
                        $mhs = $mhs->where('status_prasyarat', $layakkp);
                    }

                    if(isset($request->kp)){
                        if($request->kp == '1'){
                            $mhs = $mhs->has('kp');                            
                        }elseif($request->kp == '0'){
                            $mhs = $mhs->doesntHave('kp');                                                    
                        }
                    }
                    
                    $mhs = $mhs->orderBy('nama')->paginate(30);

                    return view('wda.mahasiswa', compact('wda', 'mhs'));
                    break;

                case 'mhsakp' :
                    $mhs = DB::table('list_mahasiswas')
                        ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
                        ->where('list_mahasiswas.dosenpa_id', $wda->id)
                        ->orderBy('nama')
                        ->paginate(30);

                    return view('wda.mahasiswa_kp', compact('wda', 'mhs'));
                    break;

                case 'dosbing' :
                    $mhs = DB::table('list_mahasiswas')
                        ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_surat_pengantar_kps', 'mhs_surat_pengantar_kps.id', '=', 'mhs_kps.tempat_id')
                        ->where('mhs_progress_statuses.status_judul', 'layak')
                        ->where('mhs_pembimbings.dosbing1_id', $wda->id)
                        ->orWhere('mhs_pembimbings.dosbing2_id', $wda->id)
                        ->paginate(30);

                    return view('wda.kp_bimbingan', compact('wda', 'mhs'));
                    break;

                case 'mahasiswa' :
                    $mhskp  = MhsProgressStatus::whereNotNull('waktu_spk_mulai')->whereNull('status_kp')
                            ->orderBy('waktu_spk_mulai','asc')->paginate(30);

                    return view('wda.kp_mahasiswa', compact('wda', 'mhskp'));
                    break;

                case 'nilai' :
                    $mhs    = MhsProgressStatus::whereNotNull('status_kp')
                            ->orderBy('waktu_spk_mulai','desc')->paginate(30);

                    return view('wda.nilai', compact('wda', 'mhs'));
                    break;

                case 'dosen' :
                    $dosen = ListDosen::orderBy('nama')->paginate(20);

                    return view('wda.dosen', compact('wda', 'dosen'));
                    break;

                case 'kuota' :

                    $tahunIni   = Carbon::now()->format('Y');
                    $awalTahun  = new Carbon('first day of January '.$tahunIni);
                    $akhirTahun = new Carbon('last day of December '.$tahunIni);

                    $dosen = DB::table('list_dosens')
                        ->join('kuota_dosens', 'list_dosens.id', '=', 'kuota_dosens.dosen_id')
                        ->whereBetween('kuota_dosens.created_at', [$awalTahun, $akhirTahun])
                        ->where('status', 'menunggu')
                        ->paginate(20);

                    return view('wda.kuota', compact('wda', 'dosen'));
                    break;

                case 'seminar' :

                    $dosen = $this->_wda;

                    $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
                    if($ruangan){
                        $listRuangan = ListRuangSeminar::find($ruangan);
                        $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
                    }else{
                        $listRuangan = '1';
                    }

                    $dateMonth  = Carbon::now('Asia/Jakarta');
                    $today      = Carbon::now('Asia/Jakarta');
                    $ruangan    = ListRuangSeminar::lists('ruangan', 'id')->all();

                    $dateToday  = Carbon::create(
                        $today->format('Y'),
                        $today->format('m'),
                        '1'
                    );

                    return view('wda.'.$menu, compact('dateToday', 'dateMonth', 'dosen', 'ruangan','listRuangan'));
                    break;

                case 'keamanan' :
                    return view('wda.'.$menu);
                    break;

                case 'username' :
                    return view('wda.'.$menu);
                    break;

                case 'pengaturan' :
                    $settings = $this->_settings;
                    return view('wda.'.$menu, compact('settings'));
                break;

                case 'laporan' :
                    $dosen = $this->_wda;
                    return view('wda.export', compact('dosen'));
                break;

            }
        }else{
            abort(404);
        }
    }

    public function addKuota($id){

        $kuota = KuotaDosen::find($id);

        if($kuota){
            $dosen      = ListDosen::find($kuota->dosen_id);
            $addKuota   = $dosen->kuota + $kuota->jumlah;

            $dosen->update(['kuota' => $addKuota]);
            $kuota->update(['status' => 'terpenuhi']);

        }else{
            abort(404);
        }

        return redirect(route('wda_menu', 'kuota'));
    }

    public function unlockBimbingan($id){

        $progress = MhsProgressStatus::where('nim_id', $id)->first();
        if($progress && $progress->spk_berkas == 'jadi'){
            $progress->update(['status_bimbingan'=>'aktif']);
        }else{
            session()->flash('bimbingan', 'Tidak bisa di unlock karena SPK belum jadi');
        }

        return Redirect::back();
    }

    public function bimbinganSelesai($nim){

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        MhsProgressStatus::where('nim_id', $mhs->id)->update(['status_bimbingan'=>'selesai']);

        return Redirect::back();
    }

    public function lihatBimbingan($nim){

        $dosen      = $this->_wda;
        $mhs        = ListMahasiswa::where('nim', $nim)->first();
        $bimbingan  = MhsBimbingan::where('nim_id', $mhs->id)->get();
        $progress   = MhsProgressStatus::where('nim_id', $mhs->id)->first();

        $dosbing    = MhsPembimbing::where('nim_id', $mhs->id)
            ->where('dosbing1_id', $dosen->id)
            ->orWhere('dosbing2_id', $dosen->id)
            ->first();

        if(!$dosbing)
            abort(404);

        return view('wda.bimbingan', compact('bimbingan', 'dosen', 'progress'));
    }

    public function kpSelesai($nim){
        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $bimbingan = MhsProgressStatus::where('nim_id', $mhs->id)->where('status_bimbingan', 'selesai')->first();

        if($bimbingan){
            MhsProgressStatus::where('nim_id', $mhs->id)->update([
                'status_kp' => 'selesai'
            ]);
        }else{
            abort(404);
        }

        return Redirect::back();
    }

    public function updateSettings(Request $request){
        $currentSetting = $this->_settings;

        $this->validate($request, [
            'skskp'         => 'required|numeric',
            'skssistem'     => 'required|numeric',
            'bimbinganmin'  => 'required|numeric',
            'ipkkp'         => 'required|numeric',
            'masaspk'       => 'required|numeric|min:1',
        ]);

        $currentSetting['sks_minimum'] = $request->skskp;
        $currentSetting['sks_login'] = $request->skssistem;
        $currentSetting['bimbingan_minimum'] = $request->bimbinganmin;
        $currentSetting['ipk_minimum'] = $request->ipkkp;
        $currentSetting['masa_spk'] = $request->masaspk;

        DB::transaction(function() use ($request){
            ListMahasiswa::where('sks', '>=', $request->skskp)
            ->where('ipk', '>=', $request->ipkkp)
            ->update(['status_prasyarat'=>'memenuhi']);

            ListMahasiswa::where('sks', '<', $request->skskp)
            ->orWhere('ipk', '<', $request->ipkkp)
            ->update(['status_prasyarat'=>'belum']);            
        });


        $save = file_put_contents(storage_path('settings.json'), json_encode($currentSetting));
        return Redirect::back();
    }

}

