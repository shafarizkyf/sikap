<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class NilaiController extends Controller
{

    public static function convertToHuruf($nilai){
        $huruf = null;

        if($nilai >= 80){
            $huruf = 'A';

        }elseif ($nilai < 80 && $nilai >= 75) {
            $huruf = 'AB';

        }elseif ($nilai < 75 && $nilai >= 70) {
            $huruf = 'B';

        }elseif ($nilai < 70 && $nilai >= 65) {
            $huruf = 'BC';

        }elseif ($nilai < 65 && $nilai >= 60) {
            $huruf = 'C';

        }elseif ($nilai < 60 && $nilai >= 56) {
            $huruf = 'CD';

        }elseif ($nilai < 56 && $nilai >= 46) {
            $huruf = 'D';

        }elseif ($nilai < 46 ) {
            $huruf = 'E';
        }


        return $huruf;
    }

}
