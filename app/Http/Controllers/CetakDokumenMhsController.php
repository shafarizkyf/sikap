<?php

namespace App\Http\Controllers;

use App\ListJurusan;
use App\ListKajur;
use App\ListMahasiswa;
use App\MhsBerkasDistribusi;
use App\MhsBerkasSeminar;
use App\MhsBimbingan;
use App\MhsGantiDosbing;
use App\MhsKp;
use App\MhsKpUlang;
use App\MhsNilai;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsSpk;
use App\MhsSpkLama;
use App\MhsSuratLama;
use App\MhsSuratPengantarKp;

use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Faker\Provider\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class CetakDokumenMhsController extends Controller
{

    public function __construct(){
        Carbon::setLocale('id');
        //setlocale(LC_TIME, 'Indonesia');
    }

    public function permission($level){
        $userLogin = UserLoginController::isLogin($level);
        if(!$userLogin)
            Redirect::to('/')->send();
    }

    public function permissions($level){
        
        $hasAccess = false;        
        foreach ($level as $o) {
            $userLogin = UserLoginController::isLogin($o);
            if($userLogin){
                $hasAccess = true;
            }
        }        

        if(!$hasAccess)
            Redirect::to('/')->send();

        return $hasAccess;
    }


    public function checkUser($nim){
        if(session()->has('who')){
            $user = explode(' ',session()->get('who'));
            $mhs = ListMahasiswa::find($user[1]);
            if(!$mhs or $mhs->nim != $nim)
                abort(404);
        }else{
            abort(404);
        }
    }

    public function formSuratPengantarKp($nim){

        $this->permission('mahasiswa');
        $this->checkUser($nim);

        $mhs    = GetMahasiswaDataController::getMhsListData('nim', '=', $nim);
        $surat  = MhsSuratPengantarKp::where('nim_id', $mhs->id)->get();

        return view('cetak.form_surat_pengantar', compact('surat'));
    }

    public function suratPengantarKp($id){

        $this->permission('bapendik');

        $surat  = MhsSuratPengantarKp::find($id);
        return view('cetak.surat_pengantar', compact('surat'));
    }

    public function suratLama($id){

        $this->permission('bapendik');

        $surat = MhsSuratLama::where('nim_id', $id)->first();
        return view('cetak.surat_pengantar', compact('surat'));
    }

    public function lembarPermohonanKp($nim){
        $this->permission('bapendik');

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $mhs = MhsKp::where('nim_id', $mhs->id)->first();

        return view('cetak.lembar_permohonan', compact('mhs'));
    }

    public function formGantiDosbing($nim){
        $this->permission('bapendik');

        $mhs    = ListMahasiswa::where('nim', $nim)->firstOrFail();
        $mhskp  = MhsKp::where('nim_id', $mhs->id)->firstOrFail();
        $curDosbing = MhsPembimbing::where('nim_id', $mhs->id)->firstOrFail();
        $reqDosbing = MhsGantiDosbing::where('nim_id', $mhs->id)->firstOrFail();
        $komisi = GetDosenDataController::getKomisiData($mhs->jurusan->id);

        return view('cetak.form_ganti_dosbing', compact('mhs', 'mhskp', 'curDosbing', 'reqDosbing', 'komisi'));
    }

    public function spk($nim){
        //$this->permission('bapendik');
        if(session()->has('who')){
            $who = explode(' ', session()->get('who'))[0];
            if($who == 'mahasiswa'){
                $this->checkUser($nim);
            }
        }else{
            abort(404);
        }

        $mhs = GetMahasiswaDataController::getMhsListData('nim', '=', $nim);

        if(!$mhs)
            abort(404);

        $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();
        if(!$progress or $progress->status_judul != 'layak')
            abort(404);

        $mhskp      = MhsKp::where('nim_id', $mhs->id)->first();
        $spk        = MhsSpk::where('nim_id', $mhs->id)->first();
        $dosbing    = MhsPembimbing::where('nim_id', $mhs->id)->first();
        $kajur      = ListKajur::where('jurusan_id', $mhs->jurusan->id)->first();

        return view('cetak.spk', compact('mhskp', 'spk', 'progress', 'dosbing', 'kajur'));
    }

    public function spkLama($nimId){
        $progress = MhsProgressStatus::where('nim_id', $nimId)->first();
        if(!$progress or $progress->status_judul != 'layak')
            abort(404);

        $mhskp      = MhsKp::where('nim_id', $nimId)->first();
        $spk        = MhsSpkLama::where('nim_id', $nimId)->first();
        $dosbing    = MhsPembimbing::where('nim_id', $nimId)->first();
        $kajur      = ListKajur::where('jurusan_id', $mhskp->nim->jurusan->id)->first();

        return view('cetak.spklama', compact('mhskp', 'spk', 'progress', 'dosbing', 'kajur'));
    }

    public function kartuKendali($nim){

        $this->permissions(['mahasiswa', 'komisi', 'dosen', 'bapendik']);

        if(session()->has('who')){
            $who = explode(' ', session()->get('who'));
            if($who[0] == 'mahasiswa'){
                $this->checkUser($nim);                            
            }
        }

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $mhskp = MhsKp::where('nim_id', $mhs->id)->first();
        $spk = MhsSpk::where('nim_id', $mhs->id)->first();
        $dosbing = MhsPembimbing::where('nim_id', $mhs->id)->first();
        $bimbingan = MhsBimbingan::where('nim_id', $mhs->id)->get();
        $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();

        return view('cetak.kartu_kendali', compact('mhskp', 'dosbing', 'bimbingan', 'spk', 'progress'));
    }

    public function seminar($nim){

        $mhs = ListMahasiswa::where('nim', $nim)->firstOrFail();
        $mhskp = MhsKp::where('nim_id', $mhs->id)->firstOrFail();
        $dosbing = MhsPembimbing::where('nim_id', $mhs->id)->firstOrFail();
        $seminar = MhsSeminar::where('nim_id', $mhs->id)->firstOrFail();
        $kajur = ListKajur::where('jurusan_id', $mhs->jurusan->id)->first();
        return view('cetak.seminar', compact(
            'mhs', 'mhskp', 'dosbing', 'seminar', 'kajur'
        ));
    }

    public function nilai($nim){

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $mhskp = MhsKp::where('nim_id', $mhs->id)->first();
        $spk = MhsSpk::where('nim_id', $mhs->id)->first();
        $dosbing = MhsPembimbing::where('nim_id', $mhs->id)->first();
        $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();
        $nilai = MhsNilai::where('nim_id', $mhs->id)->first();
        $nilaiHuruf = NilaiController::convertToHuruf($nilai->nilai_akhir);

        return view('cetak.nilai', compact('mhskp', 'dosbing', 'spk', 'progress', 'nilai', 'nilaiHuruf'));
    }

    public function viewFile($file, $nim){

        $fileList = array('surat', 'outline', 'ksm', 'makalah', 'suratselesai', 'suratspk', 'kartuseminar', 'formdist', 'pengesahan');
        if(!in_array($file, $fileList))
            abort(404);

        $mhs    = ListMahasiswa::where('nim', $nim)->first();
        ($mhs) ? true : abort(404);


        return view('cetak.dokumen', compact('file', 'nim'));
    }

    public function viewPDF($file, $nim){

        $mhs    = ListMahasiswa::where('nim', $nim)->first();
        $mhskp  = MhsKp::where('nim_id', $mhs->id)->first();
        $mhss   = MhsBerkasSeminar::where('nim_id', $mhs->id)->first();
        $mhsd   = MhsBerkasDistribusi::where('nim_id', $mhs->id)->first();
        $berkas = null;

        switch ($file){
            case 'surat' :
                if($mhskp){
                    $berkas = $mhskp->surat;
                }else{
                    $mhsUlang = MhsKpUlang::where('nim_id', $mhs->id)->first();
                    $berkas = $mhsUlang->surat;
                }
                break;

            case 'outline' :
                if($mhskp){
                    $berkas = $mhskp->outline;
                }else{
                    $mhsUlang = MhsKpUlang::where('nim_id', $mhs->id)->first();
                    $berkas = $mhsUlang->outline;
                }
                break;

            case 'ksm' :
                $berkas = $mhss->ksm;
                break;

            case 'makalah' :
                $berkas = $mhss->makalah;
                break;

            case 'suratselesai' :
                $berkas = $mhss->surat_selesai;
                break;

            case 'suratspk' :
                $berkas = $mhss->surat_spk;
                break;

            case 'kartuseminar' :
                $berkas = $mhss->kartu_seminar;
                break;

            case 'formdist' :
                $berkas = $mhsd->form;
                break;

            case 'pengesahan' :
                $berkas = $mhsd->pengesahan;
                break;

            default :
                abort(404);
        }

        $file_path = public_path("mahasiswa_berkas/".$mhs->jurusan->jurusan."/".$berkas);

        //if(!File::exists($file_path)){ abort(404); }

        return response()->file($file_path);
    }

}
