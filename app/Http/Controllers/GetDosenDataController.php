<?php

namespace App\Http\Controllers;

use App\User;
use App\ListDosen;
use App\ListJurusan;
use App\MhsPembimbing;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class GetDosenDataController extends Controller{
  
  public static $_dosen;
  
  public static function GetDosenListDataByJurusan($idJurusan){
    $user = User::active()->dosen($idJurusan)->get();
    $userDosen = array();
    foreach($user as $o){
      $userDosen[$o->user_id] = $o->listDosen->nama;
    }
    
    return self::$_dosen = $userDosen;
  }
  
  public static function GetAllDosenListData(){
    $dosen = DB::table('users')
    ->join('list_dosens', 'list_dosens.id', '=', 'users.user_id')
    ->where('users.is_active', '=', 'aktif')
    ->where('status', '=', 'dosen')
    ->orWhere('status', '=', 'komisi')
    ->orWhere('status', '=', 'wda')
    ->select('list_dosens.nama', 'list_dosens.id')
    ->orderBy('nama')
    ->get();
    
    
    $dosen    = new Collection($dosen);
    $dosen    = $dosen->lists('nama','id')->all();
    
    return self::$_dosen = $dosen;
  }
  
  public static function getKomisiData($jurusan_id){
    $komisi = DB::table('list_dosens')
    ->join('users', 'list_dosens.id', '=', 'users.user_id')
    ->where('list_dosens.jurusan_id', $jurusan_id)
    ->where('users.status', 'komisi')
    ->first();
    
    return $komisi;
  }
  
  public static function getDosenData($user_id){
    $dosen = DB::table('list_dosens')
    ->join('users', 'list_dosens.id', '=', 'users.user_id')
    ->where('users.user_id', $user_id)
    ->where('users.status', 'dosen')
    ->first();
    
    return $dosen;
  }
  
  public static function getMhsDosbing($nim_id){
    return MhsPembimbing::where('nim_id', $nim_id)->first();
  }
  
  public static function getTotalBimbinganMhs($dosen_id){
    
    $total  = DB::table('list_mahasiswas')
    ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
    ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'mhs_progress_statuses.nim_id')
    ->where('mhs_progress_statuses.status_judul', '=', 'layak')
    ->where('mhs_progress_statuses.status_kp', '=', null)
    ->where('mhs_pembimbings.dosbing1_id', $dosen_id)
    ->orWhere('mhs_pembimbings.dosbing2_id', $dosen_id)
    ->get();
    
    return $total;
  }
}
