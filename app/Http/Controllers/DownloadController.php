<?php

namespace App\Http\Controllers;

use App\ListMahasiswa;
use Illuminate\Http\Request;

use App\Http\Requests;

class DownloadController extends Controller
{

    //download file surat/outline pengajuan kp mahasiswa
    public function download($fileName) {
        $data    = explode('_', $fileName);
        $nim     = $data[0];

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $jurusan = $mhs->jurusan->jurusan;

        $file_path = public_path("mahasiswa_berkas/$jurusan/".$fileName);
        return response()->download($file_path);
    }


    //download file sample buat import
    public function sample($file){
        if($file == 'import'){
            $file_path = public_path("bapendik_berkas/import.zip");
            return response()->download($file_path);
        }
    }

}
