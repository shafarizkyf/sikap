<?php

namespace App\Http\Controllers;

use App\ListRuangSeminar;
use App\MhsBerkasDistribusi;
use App\MhsBerkasSeminar;
use App\MhsBimbingan;
use App\MhsGantiDosbing;
use App\MhsKp;
use App\MhsKpUlang;
use App\MhsNilai;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsSpk;
use App\MhsSuratLama;
use App\MhsSuratPengantarKp;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\ListMahasiswa;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use SimpleSoftwareIO\SMS\Facades\SMS;

class MahasiswaDaftarKpController extends Controller {

  private $_mhs;
  private $_settings;
  
  public function __construct() {
      $userLogin = UserLoginController::isLogin('mahasiswa');
      if (!$userLogin) Redirect::to('/')->send();
      $this->_mhs = GetMahasiswaDataController::getMhsListData('id', '=', $userLogin);
      $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
  }

  public function index() {
      $mhs = $this->_mhs;
      $dosenpa = GetDosenDataController::GetDosenListDataByJurusan($mhs->jurusan->id);
      $surat = MhsSuratPengantarKp::where('nim_id', $mhs->id)->get();
      $username = explode(' ', $mhs->nama) [0];
      $username = $this->trimString($username, 10);
      $mhsUlang = MhsKpUlang::where('nim_id', $mhs->id)->first();
      $settings = $this->_settings;
      //MELENGKAPI DATA DASAR MAHASISWA KETIKA PERTAMA KALI LOGIN
      if (!$mhs->hasMelengkapiData) {
          return view('mahasiswa.lengkapi_data', compact('dosenpa', 'mhs', 'username'));
      }
      //STEP 1 - MEMILIH PENGGUNAAN SISTEM KP/TA JIKA MEMENUHI SYARAT SKS DAN IPK
      if ($mhs->hasMemenuhiSyarat) {
          //ALTERNATIVE - MENAMPILKAN FORM PEMILIHAN PENGGUNAAN SISTEM KP/TA
          if (!$mhs->level) {
              return view('mahasiswa.mhsta_level', compact('username'));
          }
          //ALTERNATIVE
          if ($mhs->level == 'ta') {
              //MEMILIH JENIS SEMINAR
              if (!session()->has('seminar')) {
                  return view('mahasiswa.mhsta_seminar_jenis', compact('mhs'));
              } else {
                  //MEMILIH JADWA SEMINAR
                  $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
                  if ($ruangan) {
                      $listRuangan = ListRuangSeminar::find($ruangan);
                      $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
                  } else {
                      $listRuangan = '1';
                  }
                  $dateMonth = Carbon::now('Asia/Jakarta');
                  $today = Carbon::now('Asia/Jakarta');
                  $jumlahHari = $this->_settings['jadwal_seminar'];
                  $seminar = new SeminarController();
                  $seminar = $seminar->getJenis(session()->get('seminar'));
                  $seminarSelesai = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', $seminar[1])->where('status', 'selesai')->count();
                  $jumlah = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', $seminar[1])->where('status', null)->count();
                  $ruangan = ListRuangSeminar::lists('ruangan', 'id')->all();
                  $jumlahUlang = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', $seminar[1])->where('status', 'ulang')->count();
                  $seminarUlang = ($jumlahUlang > 0) ? true : false;
                  $seminarSelesai = ($seminarSelesai > 0) ? true : false;
                  return view('mahasiswa.mhsta_seminar', compact('dateMonth', 'today', 'mhs', 'jumlah', 'ruangan', 'listRuangan', 'jumlahUlang', 'seminarUlang', 'seminarSelesai', 'jumlahHari'));
              }
          }
      }
      //STEP 2 - MEMBUAT SURAT PENGANTAR KP UNTUK PERTAMA KALI
      if (!$mhs->suratPengantarKp->count()) {
          return view('mahasiswa.req_surat_pengantar', compact('mhs', 'username'));
      }
      //STEP 3 - PENGAJUAN USUALAN KERJA PRAKTIK
      if ($mhs->level == 'kp') {
          //cek masa spk, cek dia ngulang apa engga
          $progress = MhsProgressStatus::where('nim_id', '=', $mhs->id)->first();
          if ($progress) {
              //klo keputusannya kp nya di ulang
              if ($progress->status_kp == 'ulang') {
                  return view('mahasiswa.kp_ulang', compact('mhs'));
              }
              //klo masa spknya habis
              if ($progress->spk_berkas == 'jadi' & $progress->status_kp != 'selesai' & $progress->waktu_spk_selesai <= Carbon::now('Asia/Jakarta')) {
                  $progress->update(['status_spk' => 'habis']);
                  return view('mahasiswa.kp_ulang', compact('mhs'));
              }
          }
          $mhskp = GetMahasiswaDataController::getMhsKp('nim_id', '=', $mhs->id);
          //cek pengajuan udah ada apa belum
          if ($mhskp) {
              //klo udah ngajuin kp, nampilin progressnya
              $progress = GetMahasiswaDataController::getMhsKpProgress('nim_id', '=', $mhs->id);
              $dosbing = GetMahasiswaDataController::getMhsDosbing('nim_id', '=', $mhs->id);
              $nilai = MhsNilai::where('nim_id', $mhs->id)->first();
              return view('mahasiswa.progress', compact('mhs', 'mhskp', 'progress', 'nilai', 'dosbing', 'mhsUlang'));
          } else {
              //cek surat pengajuannya udah jadi apa belum
              $surat = MhsSuratPengantarKp::where('nim_id', $mhs->id)->where('status', 'selesai')->first();
              if (!$surat) {
                  $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Anda belum bisa melakukan pengajuan kerja praktik', 'keterangan' => 'karena surat pengantar yang Anda buat belum jadi, masih di proses oleh bapendik'];
                  return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg'));
              } else {
                  //nampilin form pengajuan klo status suratnya udah jadi
                  $dosbing = GetDosenDataController::GetDosenListDataByJurusan($mhs->jurusan->id);
                  $surat = $surat->where('nim_id', $mhs->id)->where('status', 'selesai')->lists('instansi', 'id')->all();
                  return view('mahasiswa.pengajuan', compact('mhs', 'dosbing', 'surat', 'ssurat', 'mhsUlang'));
              }
          }
      } else {
          $msg = ['tag' => 'Ups', 'judul' => 'Anda belum bisa melakukan pengajuan kerja praktik', 'keterangan' => 'karena ipk dan sks anda belum menyukupi'];
          return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg'));
      }
  }

  /** menu di sidebar */
  public function menu($menu) {
      $menuList = array('surat_pengantar', 'keamanan', 'bimbingan', 'seminar', 'nilai', 'archives', 'aktivitas');
      if (in_array($menu, $menuList)) {
          $mhs = $this->_mhs;
          $mhsUlang = MhsKpUlang::where('nim_id', $mhs->id)->first();
          $username = explode(' ', $mhs->nama) [0];
          $username = $this->trimString($username, 10);
          switch ($menu) {
              case 'surat_pengantar':
                  $mhskp = GetMahasiswaDataController::getMhsKp('nim_id', '=', $mhs->id);
                  $surats = MhsSuratPengantarKp::where('nim_id', $mhs->id);
                  $surat = $surats->get();
                  $jSurat = $surats->count();
                  return view('mahasiswa.' . $menu, compact('mhs', 'surat', 'jSurat', 'mhskp', 'mhsUlang'));
              break;
              case 'keamanan':
                  if ($mhs->level == 'ta') return view('mahasiswa.mhsta_keamanan', compact('mhs'));
                  else return view('mahasiswa.' . $menu, compact('mhs', 'mhsUlang'));
                  break;
              case 'bimbingan':
                  $mhskp = MhsProgressStatus::where('nim_id', $mhs->id)->first();
                  $settings = $this->_settings;
                  if (!$mhskp or !$mhskp->status_bimbingan) {
                      $msg = ['tag' => 'Ups', 'judul' => 'Anda belum bisa melakukan bimbingan', 'keterangan' => 'karena proses pengajuan Anda belum selesai atau dosen pembimbing belum meng-unlock halaman ini'];
                      return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg'));
                  } else {
                      $bimbingan = MhsBimbingan::where('nim_id', $mhs->id)->get();
                      $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();
                      $status = null;
                      if (count($bimbingan) > 0) {
                          $status = $bimbingan->first()->status;
                      }
                      return view('mahasiswa.' . $menu, compact('mhskp', 'bimbingan', 'progress', 'status', 'mhsUlang', 'settings'));
                  }
                  break;
              case 'seminar':
                  $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
                  if ($ruangan) {
                      $listRuangan = ListRuangSeminar::find($ruangan);
                      $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
                  } else {
                      $listRuangan = '1';
                  }
                  $username = explode(' ', $mhs->nama) [0];
                  $judul = MhsKp::where('nim_id', $mhs->id)->first();
                  $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();
                  //udah sampai tahap seminar atau belum
                  if (!$judul) {
                      $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Anda belum bisa melakukan pengajuan seminar', 'keterangan' => 'karena Anda belum sampai tahap ini.'];
                      return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg'));
                  } else {
                      //cek udah ada judul final atau belum
                      $jproposal = ($judul->judul_final) ? $judul->judul_final : null;
                      if (!$jproposal) {
                          //belum ada judul final
                          $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Anda belum bisa mengajukan jadwal seminar', 'keterangan' => 'karena Anda belum menentuka judul final kerja praktik'];
                          return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg', 'mhsUlang'));
                      }
                      if (!$progress->status_judul_final) {
                          //judul harus udah di accc
                          $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Anda belum bisa mengajukan jadwal seminar', 'keterangan' => 'sebelum dosen pembimbing menyetujui judul final kerja praktik'];
                          return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg', 'mhsUlang'));
                      }
                  }
                  //STEP 2 - milih jenis seminar
                  if (!session()->has('seminar')) {
                      return view('mahasiswa.seminar_jenis', compact('mhs', 'mhsUlang'));
                  }
                  //STEP 3 - cek berkas seminar mahasiswa, judul harus dah fix
                  $berkas = MhsBerkasSeminar::where('nim_id', $mhs->id)->first();
                  if (!$berkas) {
                      //klo belum ada berkas seminar yang belum di upload
                      return view('mahasiswa.pengajuan_seminar', compact('mhs', 'berkas', 'mhsUlang'));
                  } else {
                      if (!$berkas->status_berkas && !$berkas->status_koreksi) {
                          return view('mahasiswa.berkas_seminar', compact('mhs', 'berkas', 'mhsUlang'));
                          //klo berkasnya masih perlu dikoreksi - form koreksi
                          
                      } elseif ($berkas->status_berkas == 'koreksi' && $berkas->status_koreksi == 'belum') {
                          return view('mahasiswa.pengajuan_seminar', compact('mhs', 'berkas', 'mhsUlang'));
                          //klo berkasnya udah di koreksi
                          
                      } elseif ($berkas->status_berkas == 'koreksi' && $berkas->status_koreksi == 'sudah') {
                          return view('mahasiswa.berkas_seminar', compact('mhs', 'berkas', 'mhsUlang'));
                      } elseif ($berkas->status_berkas == 'lengkap') {
                          $dateMonth = Carbon::now('Asia/Jakarta')->addDays(14);
                          $today = Carbon::now('Asia/Jakarta');
                          $seminar = new SeminarController();
                          $seminar = $seminar->getJenis(session()->get('seminar'));
                          $seminarSelesai = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', $seminar[1])->where('status', 'selesai')->count();
                          $jumlah = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', $seminar[1])->where('status', null)->count();
                          $ruangan = ListRuangSeminar::lists('ruangan', 'id')->all();
                          $jumlahUlang = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', $seminar[1])->where('status', 'ulang')->count();
                          $seminarUlang = ($jumlahUlang > 0) ? true : false;
                          $seminarSelesai = ($seminarSelesai > 0) ? true : false;
                          $jumlahHari = $this->_settings['jadwal_seminar'];
                          return view('mahasiswa.' . $menu, compact('dateMonth', 'today', 'mhs', 'jumlah', 'ruangan', 'listRuangan', 'jumlahUlang', 'seminarUlang', 'seminarSelesai', 'mhsUlang', 'jumlahHari'));
                      }
                  }
                  break;
              case 'distribusi':
                  $username = explode(' ', $mhs->nama) [0];
                  $dist = MhsBerkasDistribusi::where('nim_id', $mhs->id)->first();
                  if (!$dist) {
                      $seminar = MhsSeminar::where('nim_id', $mhs->id)->where('jenis', 'kp')->where('status', 'selesai')->first();
                      if (!$seminar) {
                          $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Belum saatnya distribusi', 'keterangan' => 'karena Anda belum sampai tahap ini atau jika Anda sudah seminar pastikan dosen pembimbing telah memberikan status seminar Anda (Lulus/Ulang).'];
                          return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg'));
                      } else {
                          return view('mahasiswa.' . $menu, compact('mhs', 'mhsUlang'));
                      }
                  } else {
                      return view('mahasiswa.berkas_distribusi', compact('mhs', 'mhsUlang', 'dist'));
                  }
                  break;
              case 'archives':
                  return view('mahasiswa.' . $menu, compact('mhs', 'mhsUlang'));
                  break;
              case 'nilai':
                  $nilai = MhsNilai::where('nim_id', $mhs->id)->first();
                  $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();
                  if (!$progress) {
                      $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Nilai belum keluar', 'keterangan' => 'karena kerja praktik anda belum sampai tahap ini'];
                  } else {
                      if (!$nilai) {
                          $msg = ['tag' => 'Hai, ' . $username, 'judul' => 'Nilai belum keluar', 'keterangan' => 'karena dosen pembimbing belum memasukan nilai anda'];
                      } else {
                          $nilaiHuruf = NilaiController::convertToHuruf($nilai->nilai_akhir);
                          $msg = ['tag' => 'Hai, ' . $username, 'judul' => $nilai->nilai_akhir . ' / ' . $nilaiHuruf, 'keterangan' => 'selamat kerja praktik anda telah selesai'];
                      }
                  }
                  return view('mahasiswa.pengajuan_disable', compact('mhs', 'msg'));
              case 'aktivitas':
                  $mhs = ListMahasiswa::with(['kp', 'progress', 'distribusi', 'nilai', 'berkasSeminar', 'berkasDistribusi', 'seminar', 'surat'])->find($mhs->id);
                  return view('mahasiswa.log', compact('mhs'));
                  break;
              }
          } else {
              abort(404);
          }
  }

  /** update data mahasiswa ketika baru pertama kali masuk */
  public function updateMhsData(Request $request) {
      $this->validate($request, ['email' => 'required|email|unique:list_mahasiswas,email', 'dosenpa' => 'required', 'telp' => 'required|numeric|digits_between:11,13|unique:list_mahasiswas,telp']);
      $msg = 'Selamat data Anda telah lengkap, semoga kerja praktik Anda sukses!';
      SmsController::sent($msg);
      $mhs = ListMahasiswa::find($this->_mhs->id);
      $mhs->email = $request->email;
      $mhs->dosenpa_id = $request->dosenpa;
      $mhs->telp = $request->telp;
      $mhs->save();
      return redirect('mahasiswa');
  }

  /** update level mahasiswa, kp/ta */
  public function updateLevel(Request $request) {
      $this->validate($request, ['level' => 'required']);
      $mhs = $this->_mhs;
      ListMahasiswa::find($mhs->id)->update(['level' => $request->level]);
      return Redirect::back();
  }

  /** pengajuan surat pengantar kp pertama kali */
  public function reqSuratPengantarKp(Request $request) {
      $this->validate($request, ['instansi' => 'required', 'kepada' => 'required', 'dimana' => 'required', 'kepada_custom' => 'required_if:kepada,lainnya', 'periode_dari' => 'required_if:periode,on', 'periode_ke' => 'required_if:periode,on']);
      $msg = 'Anda berhasil mengajukan surat untuk instansi ' . $request->instansi;
      $msg = $msg . ' surat akan dibuat oleh Bapendik. Silahkan cek sistem secara berkala untuk progresnya.';
      SmsController::sent($msg);
      $surat = new MhsSuratPengantarKp();
      $surat->nim_id = $this->_mhs->id;
      $surat->instansi = $request->instansi;
      $surat->kepada = ($request->kepada == 'lainnya') ? $request->kepada_custom : $request->kepada;
      $surat->dimana = $request->dimana;
      $surat->waktu_ajukan = Carbon::now('Asia/Jakarta');
      $surat->periode_dari = ($request->periode_dari) ? $request->periode_dari : null;
      $surat->periode_ke = ($request->periode_ke) ? $request->periode_ke : null;
      $surat->save();
      return redirect('mahasiswa');
  }

  /** edit surat pengantar */
  public function editSuratPengantarKp($id) {
      $mhsSurat = MhsSuratPengantarKp::findOrFail($id);
      $mhs = $this->_mhs;
      $username = explode(' ', $mhs->nama) [0];
      $username = $this->trimString($username, 10);
      if ($mhsSurat->nim_id == $mhs->id) {
          return view('mahasiswa.surat_pengantar_edit', compact('mhs', 'mhsSurat', 'username'));
      } else {
          abort(404);
      }
  }

  /** edit dosbing */
  public function editDosbing($id) {
      $dosbing = MhsPembimbing::findOrFail($id);
      $mhs = $this->_mhs;
      $username = explode(' ', $mhs->nama) [0];
      $username = $this->trimString($username, 10);
      $dosen = GetDosenDataController::GetDosenListDataByJurusan($mhs->jurusan->id);
      if ($dosbing->nim_id == $mhs->id) {
          return view('mahasiswa.dosbing_edit', compact('username', 'dosbing', 'dosen'));
      }
  }
  
  /** update surat pengantar */
  public function updateSuratPengantarKp(Request $request, $id) {
      $this->validate($request, ['instansi' => 'required', 'kepada' => 'required', 'dimana' => 'required', 'kepada_custom' => 'required_if:kepada,lainnya']);
      //backup surat lama
      $curSurat = MhsSuratPengantarKp::find($id);
      $bSurat = new MhsSuratLama();
      $bSurat->nim_id = $this->_mhs->id;
      $bSurat->nomor = $curSurat->nomor;
      $bSurat->instansi = $curSurat->instansi;
      $bSurat->kepada = $curSurat->kepada;
      $bSurat->dimana = $curSurat->dimana;
      $bSurat->waktu_ajukan = $curSurat->waktu_ajukan;
      $bSurat->waktu_selesai = $curSurat->waktu_selesai;
      $bSurat->save();
      //save new update
      MhsSuratPengantarKp::find($id)->update(['nim_id' => $this->_mhs->id, 'nomor' => null, 'instansi' => $request->instansi, 'kepada' => ($request->kepada == 'lainnya') ? $request->kepada_custom : $request->kepada, 'dimana' => $request->dimana, 'waktu_ajukan' => Carbon::now('Asia/Jakarta'), 'waktu_selesai' => null, 'status' => 'menunggu']);
      return redirect(route('mahasiswa_menu', 'surat_pengantar'));
  }

  /** update dosbing */
  public function updateDosbing(Request $request, $id) {
      $this->validate($request, ['dosen' => 'required', 'dosen2' => 'different:dosen', 'alasan' => 'required']);
      $dosbing = new MhsGantiDosbing();
      $dosbing->nim_id = $id;
      $dosbing->dosbing_id = $request->dosen;
      $dosbing->dosbing2_id = $request->dosen2;
      $dosbing->alasan = $request->alasan;
      $dosbing->save();
      return redirect(route('mahasiswa_index'));
  }

  /** pengajuan surat pengantar baru */
  public function reqSuratPengantarKpBaru() {
      $mhs = $this->_mhs;
      $surat = MhsSuratPengantarKp::where('nim_id', $mhs->id)->count();
      //gabisa buat baru kalau suratnya udah bikin dua
      if ($surat > 1) return redirect('mahasiswa');
      return view('mahasiswa.req_surat_pengantar_baru', compact('mhs'));
  }

  /** simpan pengajuan kp */
  public function simpanPengajuan(Request $request) {
      $this->validate($request, ['instansi' => 'required', 'judul' => 'required', 'dosbing1' => 'required|different:dosbing2', 'dosbing2' => 'different:dosbing1', 'outline' => 'required|mimes:pdf|max:2000', 'surat' => 'required|mimes:pdf|max:2000']);
      $msg = 'Anda berhasil mengajukan usulan kerja praktik dengan isi \n';
      $msg = $msg . 'instansi : ' . $request->instansi . ' \n';
      $msg = $msg . 'judul : ' . $request->instansi . ' \n';
      $msg = $msg . 'dosbing1 : ' . $request->instansi . ' \n';
      $msg = $msg . 'dosbing2 : ' . $request->instansi . ' \n';
      $msg = $msg . 'pengajuan akan diproses oleh Bapendik. Silahkan cek sistem secara berkala untuk progresnya.';
      SmsController::sent($msg);
      $nim = $this->_mhs->nim;
      $jurusan = $this->_mhs->jurusan->jurusan;
      //pindah file outline ke public folder
      $file = $request->file('outline');
      $outline = $nim . '_' . time() . '_outline.pdf';
      $file->move("mahasiswa_berkas/$jurusan/", $outline);
      //pindah file surat ke public folder
      $file = $request->file('surat');
      $surat = $nim . '_' . time() . '_surat.pdf';
      $file->move("mahasiswa_berkas/$jurusan/", $surat);
      //insert table mhskp
      $mhs = new MhsKp();
      $mhs->nim_id = $this->_mhs->id;
      $mhs->tempat_id = $request->instansi;
      $mhs->surat = $surat;
      $mhs->outline = $outline;
      $mhs->judul = $request->judul;
      $mhs->waktu_daftar = Carbon::now('Asia/Jakarta');
      $mhs->save();
      //insert table pembimbing
      $dosbing = new MhsPembimbing();
      $dosbing->nim_id = $this->_mhs->id;
      $dosbing->dosbing1_id = $request->dosbing1;
      $dosbing->dosbing2_id = $request->dosbing2;
      $dosbing->save();
      //insert table progress
      $mhsProgress = new MhsProgressStatus();
      $mhsProgress->nim_id = $this->_mhs->id;
      $mhsProgress->save();
      return redirect('mahasiswa');
  }

  /** hapus semua data pengajuan */
  public function revisi($id) {
      $mhs = GetMahasiswaDataController::getMhsKp('nim_id', '=', $id);
      //current file
      $pathSurat = public_path() . '/mahasiswa_berkas/' . $mhs->nim->jurusan->jurusan . '/' . $mhs->surat;
      $pathOutline = public_path() . '/mahasiswa_berkas/' . $mhs->nim->jurusan->jurusan . '/' . $mhs->outline;
      //delete current file
      if (File::exists($pathSurat) && File::exists($pathOutline)) {
          File::delete($pathSurat);
          File::delete($pathOutline);
      }
      //delete rows
      MhsSuratPengantarKp::where('nim_id', $id)->delete();
      MhsKp::where('nim_id', $id)->delete();
      MhsPembimbing::where('nim_id', $id)->delete();
      MhsProgressStatus::where('nim_id', $id)->delete();
      return redirect('mahasiswa');
  }
  /** catat materi bimbingan */
  public function simpanBimbingan(Request $request) {
      $this->validate($request, ['tanggal' => 'required|date', 'bimbingan' => 'required']);
      $bimbingan = new MhsBimbingan();
      $bimbingan->nim_id = $this->_mhs->id;
      $bimbingan->bimbingan = $request->bimbingan;
      $bimbingan->waktu_bimbingan = $request->tanggal;
      $bimbingan->save();
      return redirect(route('mahasiswa_menu', 'bimbingan'));
  }

  /** finalisasi bimbingan */
  public function finalisasiBimbingan() {
      $mhs = $this->_mhs;
      MhsBimbingan::where('nim_id', $mhs->id)->update(['status' => 'finalisasi']);
      return redirect(route('mahasiswa_menu', 'bimbingan'));
  }
  
  /** kp ulang */
  public function ulang($id) {
      if (session()->has('who')) {
          $who = explode(' ', session()->get('who'));
          $nimId = $who[1];
          if ($nimId == $id) {
              //mindah data dari tabel mhs_kps ke mhs_kp_ulangs
              $mhskp = MhsKp::where('nim_id', $nimId)->first();
              $dist = MhsBerkasDistribusi::where('nim_id', $nimId)->first();
              $ulang = new MhsKpUlang();
              $ulang->nim_id = $mhskp->nim_id;
              $ulang->tempat = $mhskp->tempat->instansi;
              $ulang->surat = $mhskp->surat;
              $ulang->outline = $mhskp->outline;
              $ulang->judul = $mhskp->judul;
              $ulang->judul_final = ($mhskp->judul_final) ? $mhskp->judul_final : '';
              $ulang->waktu_daftar = $mhskp->waktu_daftar;
              $ulang->save();
              $seminar = MhsBerkasSeminar::where('nim_id', $nimId)->first();
              //berkas seminar
              if ($seminar) {
                  $pathKsm = public_path() . '/mahasiswa_berkas/' . $mhskp->nim->jurusan->jurusan . '/' . $seminar->ksm;
                  $pathMakalah = public_path() . '/mahasiswa_berkas/' . $mhskp->nim->jurusan->jurusan . '/' . $seminar->makalah;
                  $pathSuratSelesai = public_path() . '/mahasiswa_berkas/' . $mhskp->nim->jurusan->jurusan . '/' . $seminar->surat_selesai;
                  $pathSuratSpk = public_path() . '/mahasiswa_berkas/' . $mhskp->nim->jurusan->jurusan . '/' . $seminar->surat_spk;
                  $pathKartuSeminar = public_path() . '/mahasiswa_berkas/' . $mhskp->nim->jurusan->jurusan . '/' . $seminar->kartu_seminar;
                  if (File::exists($pathKsm)) {
                      File::delete($pathKsm);
                  }
                  if (File::exists($pathMakalah)) {
                      File::delete($pathMakalah);
                  }
                  if (File::exists($pathSuratSelesai)) {
                      File::delete($pathSuratSelesai);
                  }
                  if (File::exists($pathSuratSpk)) {
                      File::delete($pathSuratSpk);
                  }
                  if (File::exists($pathKartuSeminar)) {
                      File::delete($pathKartuSeminar);
                  }
                  $seminar->delete();
              }
              //berkas distribusi
              if ($dist) {
                  $pathForm = public_path() . '/mahasiswa_berkas/' . $dist->nim->jurusan->jurusan . '/' . $dist->form;
                  $pathPengesahan = public_path() . '/mahasiswa_berkas/' . $dist->nim->jurusan->jurusan . '/' . $dist->pengesahan;
                  if (File::exists($pathForm)) {
                      File::delete($pathForm);
                  }
                  if (File::exists($pathPengesahan)) {
                      File::delete($pathPengesahan);
                  }
              }
              MhsBimbingan::where('nim_id', $nimId)->delete();
              MhsPembimbing::where('nim_id', $nimId)->delete();
              MhsProgressStatus::where('nim_id', $nimId)->delete();
              MhsSeminar::where('nim_id', $nimId)->delete();
              MhsSpk::where('nim_id', $nimId)->delete();
              MhsSuratPengantarKp::where('nim_id', $nimId)->delete();
              MhsBerkasDistribusi::where('nim_id', $nimId)->delete();
              $mhskp->delete();
          } else {
              abort(404);
          }
      } else {
          abort(404);
      }
      return Redirect::back();
  }

  /**************** FUNCTIONS ********************/
  public function trimString($kata, $max) {
      $kata = strip_tags($kata);
      if (strlen($kata) > $max) {
          $kata_cut = substr($kata, 0, $max);
          $kata = $kata_cut . "... ";
      }
      return $kata;
  }
}
