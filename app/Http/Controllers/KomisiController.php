<?php

namespace App\Http\Controllers;

use App\KuotaDosen;
use App\ListDosen;
use App\ListMahasiswa;
use App\ListRuangSeminar;
use App\MhsBimbingan;
use App\MhsGantiDosbing;
use App\MhsKp;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsNilai;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class KomisiController extends Controller
{

    private $_komisi, $_settings;

    public function __construct(){

        $userLogin = UserLoginController::isLogin('komisi');
        if(!$userLogin) Redirect::to('/')->send();

        $this->_komisi = ListDosen::find($userLogin);
        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
    }

    public function index(){
        return redirect(route('komisi_kp_usulan'));
    }

    public function menu(Request $request, $menu){
        $menuList = array('seminar', 'mhsa', 'mhsakp', 'dosbing', 'judul', 'keamanan', 'username', 'dosen', 'gantidosbing', 'laporan', 'arsip');

        if(in_array($menu, $menuList)) {
            $komisi = $this->_komisi;

            switch ($menu){

                case 'seminar' :

                    $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
                    if($ruangan){
                        $listRuangan = ListRuangSeminar::find($ruangan);
                        $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
                    }else{
                        $listRuangan = '1';
                    }

                    $dateMonth  = Carbon::now('Asia/Jakarta');
                    $today      = Carbon::now('Asia/Jakarta');
                    $ruangan    = ListRuangSeminar::lists('ruangan', 'id')->all();

                    $dateToday  = Carbon::create(
                        $today->format('Y'),
                        $today->format('m'),
                        '1'
                    );

                    $jumlahHari = $this->_settings['jadwal_seminar'];

                    return view('komisi.'.$menu, compact('dateToday', 'dateMonth', 'komisi', 'ruangan','listRuangan', 'jumlahHari'));
                    break;

                case 'judul' :
                    $mhs = DB::table('list_mahasiswas')
                        ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_surat_pengantar_kps', 'mhs_surat_pengantar_kps.id', '=', 'mhs_kps.tempat_id')
                        ->where('list_mahasiswas.jurusan_id', $komisi->jurusan_id)
                        ->orderBy('mhs_progress_statuses.id', 'desc')
                        ->paginate(30);

                    return view('komisi.'.$menu, compact('mhs', 'komisi'));
                    break;

                case 'mhsa' :


                    $mhs = ListMahasiswa::where('dosenpa_id', $komisi->id);

                    if(isset($request->layakkp)){
                        $layakkp = $request->layakkp == '0' ? 'belum' : 'memenuhi';
                        $mhs = $mhs->where('status_prasyarat', $layakkp);
                    }

                    if(isset($request->kp)){
                        if($request->kp == '1'){
                            $mhs = $mhs->has('kp');                            
                        }elseif($request->kp == '0'){
                            $mhs = $mhs->doesntHave('kp');                                                    
                        }
                    }

                    $mhs = $mhs->orderBy('nama')->paginate(30);

                    return view('komisi.mahasiswa', compact('komisi', 'mhs'));
                    break;

                case 'mhsakp' :
                    $mhs = DB::table('list_mahasiswas')
                        ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
                        ->where('list_mahasiswas.dosenpa_id', $komisi->id)
                        ->orderBy('list_mahasiswas.nama')
                        ->paginate(30);

                    return view('komisi.mahasiswa_kp', compact('komisi', 'mhs'));
                    break;

                case 'dosbing' :
                    $mhs = DB::table('list_mahasiswas')
                        ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
                        ->join('mhs_surat_pengantar_kps', 'mhs_surat_pengantar_kps.id', '=', 'mhs_kps.tempat_id')
                        ->where('mhs_progress_statuses.status_judul', 'layak')
                        ->where('mhs_pembimbings.dosbing1_id', $komisi->id)
                        ->orWhere('mhs_pembimbings.dosbing2_id', $komisi->id)
                        ->orderBy('list_mahasiswas.nama')
                        ->paginate(30);

                    return view('komisi.kp_bimbingan', compact('komisi', 'mhs'));
                    break;

                case 'dosen' :

                    $dosen = ListDosen::where('jurusan_id', $komisi->jurusan_id)->orderBy('nama')->paginate(25);
                    return view('komisi.'.$menu, compact('komisi', 'dosen', 'mhs'));
                    break;

                case 'gantidosbing' :
                    $ganti = MhsGantiDosbing::orderBy('id')->paginate(30);
                    return view('komisi.gantidosbing', compact('komisi', 'ganti'));

                case 'username' :

                    return view('komisi.'.$menu, compact('komisi'));
                    break;

                case 'keamanan' :

                    return view('komisi.'.$menu, compact('komisi'));
                    break;

                case 'laporan' :
                    $dosen = $this->_komisi;
                    return view('komisi.export', compact('dosen'));
                break;
            
                case 'arsip':

                    $mhs = MhsNilai::whereHas('mhs', function($q) use (&$komisi, $request){
                        $q->where('jurusan_id', $komisi->jurusan_id);
                        $q->where(function($q) use ($request){
                            $q->where('nama', 'like', "%{$request->cari}%");
                            $q->orWhere('nim', 'like', "%{$request->cari}%");
                        });
                    })->paginate(20);
                
                    return view('komisi.arsip', compact('komisi', 'mhs'));
                break;
            }
        }else{
            abort(404);
        }
    }

    public function kpPrasyarat(){
        $komisi = $this->_komisi;
        $mhs    = ListMahasiswa::where('jurusan_id', $komisi->jurusan->id)->orderBy('nim')->paginate(50);

        return view('komisi.kp_prasyarat', compact('komisi', 'mhs'));
    }

    public function kpUsulan(Request $request){

        $komisi = $this->_komisi;
        $mhskp = DB::table('list_mahasiswas')
            ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_surat_pengantar_kps', 'mhs_surat_pengantar_kps.id', '=', 'mhs_kps.tempat_id')
            ->where('list_mahasiswas.jurusan_id', $komisi->jurusan_id)
            ->where('mhs_progress_statuses.status_berkas', 'lengkap');

        if($request->cari){
            $mhskp = $mhskp->where(function($q) use ($request){
                $q->where('list_mahasiswas.nama', 'like', "%{$request->cari}%");
                $q->orWhere('list_mahasiswas.nim', 'like', "%{$request->cari}%");
            });
        }
        
        $mhskp = $mhskp->orderBy('mhs_progress_statuses.id', 'desc')->paginate(30);

        return view('komisi.kp_usulan', compact('komisi', 'mhskp'));
    }

    public function unlockBimbingan($id){

        $progress = MhsProgressStatus::where('nim_id', $id)->first();
        if($progress && $progress->spk_berkas == 'jadi'){
            $progress->update(['status_bimbingan'=>'aktif']);
        }else{
            session()->flash('bimbingan', 'Tidak bisa di unlock karena SPK belum jadi');
        }

        return redirect(route('komisi_menu', 'dosbing'));
    }

    public function bimbinganSelesai($nim){

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        MhsProgressStatus::where('nim_id', $mhs->id)->update(['status_bimbingan'=>'selesai']);

        return redirect(route('komisi_menu', 'dosbing'));
    }

    public function lihatBimbingan($nim){

        $dosen      = $this->_komisi;
        $mhs        = ListMahasiswa::where('nim', $nim)->first();
        $bimbingan  = MhsBimbingan::where('nim_id', $mhs->id)->get();
        $progress   = MhsProgressStatus::where('nim_id', $mhs->id)->first();

        $dosbing    = MhsPembimbing::where('nim_id', $mhs->id)
            ->where('dosbing1_id', $dosen->id)
            ->orWhere('dosbing2_id', $dosen->id)
            ->first();

        if(!$dosbing)
            abort(404);

        return view('komisi.bimbingan', compact('bimbingan', 'dosen', 'progress'));
    }

    public function kpSelesai($nim){
        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $bimbingan = MhsProgressStatus::where('nim_id', $mhs->id)->where('status_bimbingan', 'selesai')->first();

        if($bimbingan){
            MhsProgressStatus::where('nim_id', $mhs->id)->update([
                'status_kp' => 'selesai'
            ]);
        }else{
            abort(404);
        }

        return redirect(route('komisi_menu', 'dosbing'));
    }

    public function kuotaRefresh($id){

        KuotaDosen::where('dosen_id', $id)->delete();
        ListDosen::find($id)->update(['kuota'=>'10']);

        return Redirect::back();
    }

    public function kpOnGoing(Request $request){
        $komisi     = $this->_komisi;
        $mhs        = ListMahasiswa::where('jurusan_id',$komisi->jurusan->id)->pluck('id')->toArray();
        $mhskp      = MhsProgressStatus::whereIn('nim_id',$mhs)->whereNotNull('waktu_spk_mulai')->whereNull('status_kp');

        if($request->cari != ''){
            $mhskp = $mhskp->whereHas('nim', function($q) use ($request){
                $q->where('nama', 'like', "%{$request->cari}%");
                $q->orWhere('nim', 'like', "%{$request->cari}%");
            });
        }

        if($request->spk_mulai){
            $mhskp = $mhskp->where('waktu_spk_mulai', '>=', $request->spk_mulai);
        }

        if($request->spk_selesai){
            $mhskp = $mhskp->where('waktu_spk_selesai', '<=', $request->spk_selesai);
        }

        $mhskp = $mhskp->orderBy('waktu_spk_mulai','asc')->paginate(20);
        return view('komisi.kp_ongoing',compact('mhskp','komisi'));
    }

    public function kpUlang(){
        $komisi     = $this->_komisi;
        $mhs        = ListMahasiswa::where('jurusan_id',$komisi->jurusan->id)->pluck('id')->toArray();
        $mhskp      = MhsProgressStatus::whereIn('nim_id',$mhs)->where('status_kp', 'ulang')->paginate(20);

        return view('komisi.kp_ulang',compact('mhskp','komisi'));
    }

    public function logMahasiswa($nim){

        $komisi = $this->_komisi;

        $mhs = ListMahasiswa::with([ 
            'kp', 
            'progress', 
            'distribusi', 
            'nilai', 
            'berkasSeminar',
            'berkasDistribusi', 
            'seminar', 
            'surat'
        ])->where('nim', $nim)->first();

        return view('komisi.log', compact('mhs', 'komisi'));

    }

    /**************** FUNCTIONS ********************/
    public function trimString($kata, $max){
        $kata = strip_tags($kata);
        if(strlen($kata) > $max){
            $kata_cut	= substr($kata,0, $max);
            $kata 	 	= $kata_cut."... ";
        }

        return $kata;
    }

}
