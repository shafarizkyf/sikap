<?php

namespace App\Http\Controllers;

use App\ListMahasiswa;
use App\MhsBerkasSeminar;
use App\MhsProgressStatus;
use App\MhsSeminar;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class SeminarController extends Controller
{

    public $_sessionSeminar;

    public function saveBerkasSeminar(Request $request){

        $this->validate($request, [
            'ksm'       => 'required|mimes:pdf|max:2000',
            'makalah'   => 'required|mimes:pdf|max:2000',
            'surat'     => 'required|mimes:pdf|max:2000',
            'kartu'     => 'required|mimes:pdf|max:2000',
            'spk'       => 'required|mimes:pdf|max:2000'
        ]);


        $user = explode(' ',session()->get('who'));
        $id   = $user[1];
        $mhs  = ListMahasiswa::find($id);
        $jur  = $mhs->jurusan->jurusan;

        //pindah file ksm ke public folder
        $file       = $request->file('ksm');
        $ksm        = $mhs->nim.'_'.time().'_ksm.pdf';
        $file->move("mahasiswa_berkas/$jur/", $ksm);

        //pindah file makalah ke public folder
        $file       = $request->file('makalah');
        $makalah    = $mhs->nim.'_'.time().'_makalahacc.pdf';
        $file->move("mahasiswa_berkas/$jur/", $makalah);

        //pindah file makalah ke public folder
        $file     = $request->file('surat');
        $surat    = $mhs->nim.'_'.time().'_suratselesai.pdf';
        $file->move("mahasiswa_berkas/$jur/", $surat);

        //pindah file makalah ke public folder
        $file     = $request->file('kartu');
        $kartu    = $mhs->nim.'_'.time().'_kartuseminar.pdf';
        $file->move("mahasiswa_berkas/$jur/", $kartu);

        //pindah file makalah ke public folder
        $file   = $request->file('spk');
        $spk    = $mhs->nim.'_'.time().'_spk.pdf';
        $file->move("mahasiswa_berkas/$jur/", $spk);

        $seminar = new MhsBerkasSeminar();
        $seminar->nim_id        = $id;
        $seminar->ksm           = $ksm;
        $seminar->makalah       = $makalah;
        $seminar->surat_spk     = $spk;
        $seminar->surat_selesai = $surat;
        $seminar->kartu_seminar = $kartu;
        $seminar->save();

        $jadwal = new MhsSeminar();
        $jadwal->nim_id = $id;
        $jadwal->save();

        return Redirect::back();
    }

    public function updateBerkasSeminar(Request $request, $id){

        $this->validate($request, [
            'ksm'       => 'mimes:pdf|max:2000',
            'makalah'   => 'mimes:pdf|max:2000',
            'surat'     => 'mimes:pdf|max:2000',
            'kartu'     => 'mimes:pdf|max:2000',
            'spk'       => 'mimes:pdf|max:2000'
        ]);

        $mhs = MhsBerkasSeminar::where('nim_id', $id)->first();

        //current file
        $pathKsm        = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->ksm;
        $pathMakalah    = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->makalah;
        $pathSurat      = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->surat_selesai;
        $pathSpk        = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->surat_spk;
        $pathKartu      = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->kartu_seminar;


        //update ksm
        if($request->file('ksm')){

            if(File::exists($pathKsm)){
                File::delete($pathKsm);
            }

            $file        = $request->file('ksm');
            $fileName    = $mhs->nim->nim.'_'.time().'_ksm.pdf';
            $file->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$fileName);

            MhsBerkasSeminar::where('nim_id', $id)->update([
                'ksm' => $fileName
            ]);
        }

        //update makalah
        if($request->file('makalah')){

            if(File::exists($pathMakalah)){
                File::delete($pathMakalah);
            }

            $file        = $request->file('makalah');
            $fileName    = $mhs->nim->nim.'_'.time().'_makalahacc.pdf';
            $file->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$fileName);

            MhsBerkasSeminar::where('nim_id', $id)->update([
                'makalah' => $fileName
            ]);
        }

        //update surat selesai
        if($request->file('surat')){

            if(File::exists($pathSurat)){
                File::delete($pathSurat);
            }

            $file        = $request->file('surat');
            $fileName    = $mhs->nim->nim.'_'.time().'_suratselesai.pdf';
            $file->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$fileName);

            MhsBerkasSeminar::where('nim_id', $id)->update([
                'surat_selesai' => $fileName
            ]);
        }

        //update spk
        if($request->file('spk')){

            if(File::exists($pathSpk)){
                File::delete($pathSpk);
            }

            $file        = $request->file('spk');
            $fileName    = $mhs->nim->nim.'_'.time().'_spk.pdf';
            $file->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$fileName);

            MhsBerkasSeminar::where('nim_id', $id)->update([
                'surat_spk' => $fileName
            ]);
        }

        //update kartu seminar
        if($request->file('kartu')){

            if(File::exists($pathKartu)){
                File::delete($pathKartu);
            }

            $file        = $request->file('kartu');
            $fileName    = $mhs->nim->nim.'_'.time().'_kartu.pdf';
            $file->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$fileName);

            MhsBerkasSeminar::where('nim_id', $id)->update([
                'kartu_seminar' => $fileName
            ]);
        }

        MhsBerkasSeminar::where('nim_id', $id)->update([
            'status_koreksi' => 'sudah'
        ]);

        return Redirect::back();
    }

    public function shift($idShift){

        $shift = $this->shiftList();

        if(!array_has($shift, $idShift))
            return '';
        else
            return $shift[$idShift];
    }

    public function setJenis(Request $request, $nim){
        $this->validate($request, [
            'jenis' => 'required'
        ]);

        $mhs = ListMahasiswa::where('nim', $nim)->firstOrFail();
        $seminar = MhsSeminar::where('nim_id', $mhs->id)->where('ruangan_id', '<>', '');
        

        //cek udah pernah seminar atau belum
        if($seminar->first()){
            //cek jenis seminar yang dipilih sama yang udah ada
            $currSeminar = $seminar->orderBy('id', 'desc')->first();

            //kalo udah selese gabisa seminar jenis itu lagi
            if($currSeminar->jenis == $request->jenis && $currSeminar->status == 'selesai'){
                return redirect()->back()->with('errseminar', 'Anda sudah manjalani seminar tersebut');
            }else{
                $jenis  = $nim.'/'.$request->jenis;
                session()->put('seminar', $jenis);
            }

        }else{

            if($mhs->level == 'kp' && $request->jenis != 'kp'){
                session()->flash('errseminar', 'Saat ini Anda hanya bisa memilih Seminar Kerja Praktik');
                return Redirect::back();

            }elseif ($mhs->level == 'ta' && $request->jenis != 'kolokium' && $mhs->jurusan->jurusan == 'Teknik Geologi'){
                session()->flash('errseminar', 'Anda hanya bisa memilih Seminar Kolokium');
                return Redirect::back();
            }

            if($request->jenis == 'kolokium' && $mhs->jurusan->jurusan != 'Teknik Geologi'){
                session()->flash('errseminar', 'Seminar Kolokium hanya untuk Teknik Geologi');
                return Redirect::back();
            }

            $jenis  = $nim.'/'.$request->jenis;
            session()->put('seminar', $jenis);
        }

        return Redirect::back();
    }

    public function reqJadwalSeminar(Request $request){

        //jadwal baru yang diajukan
        $jadwal     = explode('/', $request->jadwal);
        $tanggal    = $jadwal[0];
        $shift      = $jadwal[1];
        $nimId      = $jadwal[2];
        $ruang      = $jadwal[3];
        $jam        = $this->shift($shift);
        $level      = ListMahasiswa::find($nimId);
        $page       = '';
        $jenis      = $this->getJenis(session()->get('seminar'));

        //set redirect, bisa diganti Redirect::back
        if($level->level == 'kp'){
            $page = redirect('mahasiswa/menu/seminar?ruangan='.$ruang);
        }elseif ($level->level == 'ta'){
            $page = redirect('mahasiswa?ruangan='.$ruang);
        }

        //cek jumlah status seminar yang sifatnya baru
        $mhs = MhsSeminar::where('nim_id', $nimId)->where('jenis', $jenis[1])
            ->where('status',null)->get();

        //cek jumlah status seminar yang sifatnya ulang
        $jumlahUlang = MhsSeminar::where('nim_id', $nimId) ->where('jenis', $jenis[1])
            ->where('status', 'ulang')->count();

        //hapus pengajuan dummy
        MhsSeminar::where('nim_id', $nimId)->where('ruangan_id', 0)->delete();

        //set status seminar, diulang atau engga
        $seminarUlang  = ($jumlahUlang > 0) ? true : false;

        //cek ada yg udah pake belum
        $currSeminar = MhsSeminar::where('jadwal', $tanggal.' '.$jam)->where('ruangan_id', $ruang)->first();
        if($currSeminar){
            session()->flash('jadwal', 'Maaf jadwal yang ada pilih sudah dipakai');
            return $page;
        }

        //hanya bisa 2 shift saja
        if($mhs->count() > 1 and !$seminarUlang){
            session()->flash('jadwal', 'Kamu hanya bisa mengajukan 2 shift saja');
            return $page;
        }


        //hanya beda 1 shift saja di hari yang sama
        if($mhs->count() > 0){

            //jadwal yang udah di ajuin
            $mhsJadwal  = explode(' ', $mhs->first()->jadwal);
            $mhsTanggal = $mhsJadwal[0];
            $mhsJam     = $mhsJadwal[1];
            $mhsShift   = $mhs->first()->shift;

            //shift harus selisih 1 jam
            $diffShift = abs($mhsShift - $shift);

            if(($tanggal != $mhsTanggal or $diffShift > 1)){
                session()->flash('jadwal', 'Kamu hanya bisa mengajuan 2 shift seminar di hari yang sama dengan beda 1 shift saja');
                return $page;
            }

            //hanya boleh beda 1 shift saja
            if($mhs->first()->ruangan_id != $ruang){
                session()->flash('jadwal', 'Kamu hanya bisa mengajukan seminar di ruang yang sama');
                return $page;
            }

            $seminar = new MhsSeminar();
            $seminar->nim_id = $nimId;
            $seminar->ruangan_id = $ruang;
            $seminar->jadwal = $tanggal.' '.$jam;
            $seminar->shift  = $shift;
            $seminar->jenis  = $jenis[1];
            $seminar->save();

            return $page;
        }

        //klo belum ada pengajuan seminar
        $seminar = new MhsSeminar();
        $seminar->nim_id        = $nimId;
        $seminar->ruangan_id    = $ruang;
        $seminar->jadwal        = $tanggal.' '.$jam;
        $seminar->shift         = $shift;
        $seminar->jenis         = $jenis[1];
        $seminar->save();

        return $page;
    }

    public function deleteJadwal($id){
        $who        = explode(' ', session()->get('who'));
        $user       = $who[0];
        $nimId      = $who[1];
        $seminar    = MhsSeminar::findOrFail($id);

        if($user == 'bapendik' || $nimId == $seminar->nim_id){
            $seminar->delete();
        }else{
            abort(404);
        }

        return Redirect::back();
    }

    public function clearSession(){
        $id     = explode(' ', session()->get('who'))[1];
        $mhs    = ListMahasiswa::find($id);
        $page   = null;

        if($mhs->level == 'kp'){
            $page = route('mahasiswa_menu', 'seminar');
        }elseif($mhs->level == 'ta'){
            $page = route('mahasiswa_index');
        }
        session()->forget('seminar');
        return redirect($page);
    }

    public function seminarSelesai($id){
        $this->setStatusSeminar($id, 'selesai');
        return Redirect::back();
    }

    public function seminarUlang($id){
        $this->setStatusSeminar($id, 'ulang');
        return Redirect::back();
    }


    //************ FUNCTION WITH NO ROUTE ***************/
    public function getJenis($seminarSession){
        return $this->_sessionSeminar = explode('/', $seminarSession);
    }

    public function descSession($seminarSession){
        $jenis = $this->getJenis($seminarSession)[1];
        $desc  = null;

        switch ($jenis){
            case 'kp' :
                $desc = 'Seminar Kerja Praktik';
                break;
            case 'semprop' :
                $desc = 'Seminar Proposal';
                break;
            case 'semhas' :
                $desc = 'Seminar Hasil Tugas Akhir';
                break;
            case 'kolokium' :
                $desc = 'Seminar Kolokium';
                break;
        }

        return $desc;
    }

    public function trimString($kata, $max){
        $kata = strip_tags($kata);
        if(strlen($kata) > $max){
            $kata_cut	= substr($kata,0, $max);
            $kata 	 	= $kata_cut."... ";
        }

        return $kata;
    }

    public function shiftList(){
        $shift = array(
            '1'  => '07:00',
            '2'  => '08:00',
            '3'  => '09:00',
            '4'  => '10:00',
            '5'  => '11:00',
            '6'  => '12:00',
            '7'  => '13:00',
            '8'  => '14:00',
            '9'  => '15:00',
            '10' => '16:00'
        );

        return $shift;
    }

    public function setStatusSeminar($id, $status){
        $seminar = MhsSeminar::find($id);
        if($seminar){
            $this->updateStatusSeminar($seminar, $status);
            return Redirect::back();
        }else{
            abort(404);
        }
    }

    public function updateStatusSeminar($seminar, $status){
        $jadwal = $seminar->jadwal;
        $mhsId  = $seminar->nim_id;

        $sc         = new SeminarController();
        $shiftList  = $sc->shiftList();

        $firstShift = array_slice($shiftList,0,1)[0];
        $lastShift  = array_slice($shiftList, -1, 1)[0];

        $jadwal = date('Y-m-d', strtotime($jadwal));
        return MhsSeminar::where('nim_id', $mhsId)->whereBetween('jadwal', [$jadwal.' '.$firstShift, $jadwal.' '.$lastShift])
        ->where('status', null)->update(['status'=>$status]);
    }

}
