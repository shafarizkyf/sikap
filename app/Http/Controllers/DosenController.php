<?php

namespace App\Http\Controllers;

use App\ListDosen;
use App\ListMahasiswa;
use App\ListRuangSeminar;
use App\MhsBimbingan;
use App\MhsKp;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsNilai;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class DosenController extends Controller
{

    private $_dosen, $_settings;

    public function __construct(){

        $userLogin = UserLoginController::isLogin('dosen');
        if(!$userLogin) Redirect::to('/')->send();

        $this->_dosen = ListDosen::find($userLogin);
        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
    }

    public function index(){
        return redirect(route('dosen_kp_bimbingan'));
    }

    public function menu(Request $request, $menu){
        //nama route sama blade harus sama
        $menuList = array('mhsa','mhsakp','seminar', 'arsip', 'layakkp', 'keamanan', 'username');
        if(in_array($menu, $menuList)) {

            $dosen = $this->_dosen;

            switch ($menu){
                case 'mhsa' :

                    $mhs = ListMahasiswa::where('dosenpa_id', $dosen->id);

                    if(isset($request->layakkp)){
                        $layakkp = $request->layakkp == '0' ? 'belum' : 'memenuhi';
                        $mhs = $mhs->where('status_prasyarat', $layakkp);
                    }

                    if(isset($request->kp)){
                        if($request->kp == '1'){
                            $mhs = $mhs->has('kp');                            
                        }elseif($request->kp == '0'){
                            $mhs = $mhs->doesntHave('kp');                                                    
                        }
                    }

                    $mhs = $mhs->orderBy('nama')->paginate(30);

                    return view('dosen.mahasiswa', compact('dosen', 'mhs'));
                    break;

                case 'mhsakp' :
                    $mhs = DB::table('list_mahasiswas')
                        ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
                        ->where('list_mahasiswas.dosenpa_id', $dosen->id)
                        ->orderBy('list_mahasiswas.nama')
                        ->paginate(30);

                    return view('dosen.mahasiswa_kp', compact('dosen', 'mhs'));
                    break;

                case 'seminar' :

                    $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
                    if($ruangan){
                        $listRuangan = ListRuangSeminar::find($ruangan);
                        $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
                    }else{
                        $listRuangan = '1';
                    }

                    $dateMonth  = Carbon::now('Asia/Jakarta');
                    $today      = Carbon::now('Asia/Jakarta');
                    $ruangan    = ListRuangSeminar::lists('ruangan', 'id')->all();

                    $dateToday  = Carbon::create(
                        $today->format('Y'),
                        $today->format('m'),
                        '1'
                    );

                    $jumlahHari = $this->_settings['jadwal_seminar'];
                    return view('dosen.'.$menu, compact('dateToday', 'dateMonth', 'dosen', 'ruangan','listRuangan', 'jumlahHari'));
                    break;

                case 'keamanan' :

                    return view('dosen.'.$menu, compact('dosen'));
                    break;

                case 'username' :
                    return view('dosen.'.$menu, compact('dosen'));
                    break;

                case 'arsip':

                    $mhs = MhsNilai::whereHas('mhs.pembimbing', function($q) use (&$dosen){
                        $q->where('dosbing1_id', $dosen->id);
                        $q->orWhere('dosbing2_id', $dosen->id);
                    })->paginate(20);
                
                    return view('dosen.arsip', compact('dosen', 'mhs'));
                    break;

                case 'layakkp' :

                    $mhs = ListMahasiswa::where('status_prasyarat', 'memenuhi')
                    ->where('dosenpa_id', $dosen->id)
                    ->orderBy('nama', 'asc')
                    ->paginate(20);
                    return view('dosen.layakkp', compact('mhs', 'dosen'));

                break;

            }
        }
    }

    public function kpBimbingan(){

        $dosen = $this->_dosen;

        $mhskp = DB::table('list_mahasiswas')
            ->join('mhs_kps', 'mhs_kps.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_surat_pengantar_kps', 'mhs_surat_pengantar_kps.id', '=', 'mhs_kps.tempat_id')
            ->where('mhs_progress_statuses.status_judul', 'layak')
            ->where('mhs_pembimbings.dosbing1_id', $dosen->id)
            ->orWhere('mhs_pembimbings.dosbing2_id', $dosen->id)
            ->orderBy('list_mahasiswas.nama')
            ->paginate(30);


        return view('dosen.kp_bimbingan', compact('mhskp', 'dosen'));
    }

    public function unlockBimbingan($id){

        $progress = MhsProgressStatus::where('nim_id', $id)->first();
        if($progress && $progress->spk_berkas == 'jadi'){
            $progress->update(['status_bimbingan'=>'aktif']);
        }else{
            session()->flash('bimbingan', 'Tidak bisa di unlock karena SPK belum jadi');
        }

        return redirect(route('dosen_kp_bimbingan'));
    }

    public function lihatBimbingan($nim){

        $dosen = $this->_dosen;
        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $bimbingan = MhsBimbingan::where('nim_id', $mhs->id)->get();
        $progress = MhsProgressStatus::where('nim_id', $mhs->id)->first();

        return view('dosen.bimbingan', compact('bimbingan', 'dosen', 'progress'));
    }

    public function bimbinganSelesai($nim){

        $mhs = ListMahasiswa::where('nim', $nim)->first();
        MhsProgressStatus::where('nim_id', $mhs->id)->update(['status_bimbingan'=>'selesai']);

        return redirect(route('dosen_kp_bimbingan'));
    }

    public function jadwalSeminar(){
        return 'disini';
    }

    /**************** FUNCTIONS ********************/
    public function trimString($kata, $max){
        $kata = strip_tags($kata);
        if(strlen($kata) > $max){
            $kata_cut	= substr($kata,0, $max);
            $kata 	 	= $kata_cut."... ";
        }

        return $kata;
    }

}
