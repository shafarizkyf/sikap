<?php

namespace App\Http\Controllers;

use App\BobotNilaiKp;
use App\KuotaDosen;
use App\ListDosen;
use App\ListJurusan;
use App\ListKajur;
use App\ListMahasiswa;
use App\MhsBerkasDistribusi;
use App\MhsBerkasSeminar;
use App\MhsBimbingan;
use App\MhsGantiDosbing;
use App\MhsKp;
use App\MhsNilai;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsSpk;
use App\MhsSpkLama;
use App\MhsSuratPengantarKp;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Symfony\Component\Console\Helper\ProgressBar;

class ModalController extends Controller
{

    public $_settings;

    public function __construct(){
        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
    }
    
    public function surat($id){

        $surat  = MhsSuratPengantarKp::find($id);
        $mhs    = GetMahasiswaDataController::getMhsListData('nim', '=', $surat->nim->nim);
        $nama   = explode(' ',$mhs->nama)[0];

        return view('modal.surat', compact('nama', 'surat'));
    }

    public function setSurat(Request $request){
        $this->validate($request, [
            'id'        => 'required',
            'nomor'     => 'required',
            'selesai'   => 'required|date'
        ]);

        $surat  = MhsSuratPengantarKp::find($request->id);
        $nim    = $surat->nim->nim;
        $surat->update([
            'nomor'         => $request->nomor,
            'waktu_selesai' => $request->selesai,
            'status'        => 'selesai'
        ]);


        return redirect(route('bapendik_kp_surat'));

    }

    public function koreksi($id){

        $status = MhsProgressStatus::find($id);
        $mhskp  = MhsKp::where('nim_id', $status->nim_id)->first();

        return view('modal.koreksi', compact('status', 'mhskp'));
    }

    public function simpanKoreksiBerkas(Request $request, $id){

        $mhs = GetMahasiswaDataController::getMhsKp('nim_id', '=', $id);

        //current file
        $pathSurat      = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->surat;
        $pathOutline    = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->outline;

        //update surat
        if($request->file('surat')){

            if(File::exists($pathSurat)){
                File::delete($pathSurat);
            }

            $fileSurat       = $request->file('surat');
            $suratFileName   = $mhs->nim->nim.'_'.time().'_surat.pdf';
            $fileSurat->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$suratFileName);

            MhsKp::where('nim_id', $request->id)->update(['surat' => $suratFileName]);
            MhsProgressStatus::where('nim_id', $request->id)->update(['status_koreksi'=>'sudah']);
        }

        //update outline
        if($request->file('outline')){
            if(File::exists($pathOutline)){
                File::delete($pathOutline);
            }

            $fileOutline        = $request->file('outline');
            $outlineFileName    = $mhs->nim->nim.'_'.time().'_outline.pdf';
            $fileOutline->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$outlineFileName);

            MhsKp::where('nim_id', $request->id)->update(['outline' => $outlineFileName]);
            MhsProgressStatus::where('nim_id', $request->id)->update(['status_koreksi'=>'sudah']);

        }

        return redirect(route('mahasiswa_index'));
    }

    public function koreksiKomisi($id){

        $status = MhsProgressStatus::find($id);
        $mhskp  = MhsKp::where('nim_id', $status->nim_id)->first();

        return view('modal.koreksi_komisi', compact('status', 'mhskp'));
    }

    public function simpanKoreksiBerkasKomisi(Request $request, $id){

        $mhs = GetMahasiswaDataController::getMhsKp('nim_id', '=', $id);

        //current file
        $pathSurat      = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->surat;
        $pathOutline    = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->outline;

        //update surat
        if($request->file('surat')){

            if(File::exists($pathSurat)){
                File::delete($pathSurat);
            }

            $fileSurat       = $request->file('surat');
            $suratFileName   = $mhs->nim->nim.'_'.time().'_surat.pdf';
            $fileSurat->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$suratFileName);

            MhsKp::where('nim_id', $request->id)->update(['surat' => $suratFileName, 'revisi'=>null]);
            MhsProgressStatus::where('nim_id', $request->id)->update(['status_judul'=>null]);
        }

        //update outline
        if($request->file('outline')){
            if(File::exists($pathOutline)){
                File::delete($pathOutline);
            }

            $fileOutline        = $request->file('outline');
            $outlineFileName    = $mhs->nim->nim.'_'.time().'_outline.pdf';
            $fileOutline->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$outlineFileName);

            MhsKp::where('nim_id', $request->id)->update(['outline' => $outlineFileName, 'revisi'=>null]);
            MhsProgressStatus::where('nim_id', $request->id)->update(['status_judul'=>null]);

        }

        return redirect(route('mahasiswa_index'));

    }

    public function kelayakan($id){
        $mhs    = ListMahasiswa::find($id);
        $mhskp  = GetMahasiswaDataController::getMhsKpDataByNim($mhs->nim);

        $dosbing = GetDosenDataController::GetDosenListDataByJurusan($mhs->jurusan->id);

        return view('modal.kelayakan', compact('mhskp', 'dosbing'));
    }

    public function setKelayakan(Request $request, $id){
        $this->validate($request, [
            'aksi'      => 'required',
            'dosbing1'  => 'required_if:aksi,layak',
            'dosbing2'  => 'different:dosbing1',
            'revisi'    => 'required_if:aksi,revisi|required_if:aksi,tolak'
        ]);

        $mhs = ListMahasiswa::find($id);

        if($request->aksi == 'layak'){
            MhsProgressStatus::where('nim_id', $mhs->id)->update([
                'status_judul'      => $request->aksi,
                'waktu_judul_layak' => ($request->aksi == 'layak') ? Carbon::now('Asia/Jakarta') : null
            ]);

            MhsPembimbing::where('nim_id', $mhs->id)->update([
                'dosbing1_id'   => $request->dosbing1,
                'dosbing2_id'   => ($request->dosbing2) ? $request->dosbing2 : null
            ]);

            $spk = new MhsSpk();
            $spk->nim_id = $mhs->id;
            $spk->save();

        }elseif($request->aksi == 'revisi' || $request->aksi == 'tolak'){
            MhsProgressStatus::where('nim_id', $mhs->id)->update([
                'status_judul'      => $request->aksi,
            ]);

            MhsKp::where('nim_id', $mhs->id)->update([
                'revisi'      => $request->revisi,
            ]);

        }
        return redirect(route('komisi_index'));
    }

    public function setPemlap(Request $request, $id){
        $this->validate($request, [
            'nama' => 'required',
            'telp' => 'required|numeric|digits_between:11,13'
        ]);

        MhsPembimbing::where('nim_id',$id)->update([
            'pemlap'        => $request->nama,
            'pemlap_telp'   => $request->telp
        ]);

        return redirect(route('mahasiswa_index'));
    }

    public function spk($id){
        $mhs    = ListMahasiswa::where('nim', $id)->first();
        $mhskp  = MhsKp::where('nim_id', $mhs->id)->first();

        return view('modal.spk', compact('mhskp'));
    }

    public function setSpk(Request $request, $id){

        $progress = MhsProgressStatus::where('nim_id', $id)->first();

        $this->validate($request, [
            'nomor'     => 'required',
            'spk_mulai' => 'required_if:type,new',
            'spk_jadi'  => 'required|date'
        ]);


        $tanggal = Carbon::create(
                        date('Y', strtotime($request->spk_mulai)),
                        date('m', strtotime($request->spk_mulai)),
                        date('d', strtotime($request->spk_mulai)));

                        
        $nomorSpk = MhsSpk::where('nim_id', $id)->first();
        $nomorSpk->nomor = $request->nomor;
        $nomorSpk->save();

        $spkSelesai = $progress->waktu_spk_selesai ? $progress->waktu_spk_selesai : $tanggal->addDays($this->_settings['masa_spk']);

        MhsProgressStatus::where('nim_id', $id)->update([
            'waktu_spk_mulai'   => $request->spk_mulai ? $request->spk_mulai : $progress->waktu_spk_mulai,
            'waktu_spk_selesai' => $spkSelesai,
            'waktu_spk_jadi'    => $request->spk_jadi,
            'spk_berkas'        => 'jadi',
            'status_spk'        => 'aktif'
        ]);

        return Redirect::back();

    }

    public function bimbingan($id){

        $bimbingan = MhsBimbingan::find($id);

        return view('modal.bimbingan_edit', compact('bimbingan'));
    }

    public function updateMateriBimbingan(Request $request, $id){

        $this->validate($request, [
            'bimbingan' => 'required'
        ]);

        MhsBimbingan::find($id)->update(['bimbingan' => $request->bimbingan]);

        return redirect(route('mahasiswa_menu','bimbingan'));

        return '';
    }

    public function judul($nim){
        $mhs    = ListMahasiswa::where('nim', $nim)->first();
        if($mhs){
            $mhskp = MhsKp::where('nim_id', $mhs->id)->first();
            if($mhskp){
                return view('modal.judul', compact('mhskp'));
            }
        }
    }

    public function setJudul(Request $request, $id){
        $this->validate($request, [
            'judul' => 'required'
        ]);

        MhsKp::where('nim_id', $id)->update(['judul_final' => $request->judul]);
        MhsBimbingan::where('nim_id', $id)->update(['status' => 'finalisasi']);

        return Redirect::back();
    }

    public function judulKp($nim){
        $mhs    = ListMahasiswa::where('nim', $nim)->first();
        if($mhs){
            $mhskp = MhsKp::where('nim_id', $mhs->id)->first();
            if($mhskp){
                return view('modal.judulkp', compact('mhskp'));
            }
        }
    }

    public function setJudulKp(Request $request, $id){
        $this->validate($request, [
            'aksi' => 'required',
            'judul' => 'required_if:aksi,revisi'
        ]);

        if($request->aksi == 'setuju'){
            MhsProgressStatus::where('nim_id', $id)->update(['status_bimbingan' => 'selesai', 'status_judul_final' => 'ok']);
        }elseif($request->aksi == 'revisi'){
            MhsKp::where('nim_id', $id)->update(['judul_final' => $request->judul]);
            MhsProgressStatus::where('nim_id', $id)->update(['status_bimbingan' => 'selesai', 'status_judul_final' => 'ok']);
        }

        return Redirect::back();
    }

    public function seminar($nim){

        $mhs    = ListMahasiswa::where('nim', $nim)->first();
        if($mhs){
            $berkas = MhsBerkasSeminar::where('nim_id', $mhs->id)->first();
            if($berkas){
                return view('modal.berkas_seminar', compact('mhs', 'berkas'));
            }else{
                abort(404);
            }
        }else{
            abort(404);
        }

    }

    public function setSeminar(Request $request, $id){
        $this->validate($request, [
            'aksi'      => 'required',
            'koreksi'   => 'required_if:aksi,koreksi'
        ]);


        MhsBerkasSeminar::where('nim_id',$id)->update([
            'status_berkas' => $request->aksi,
            'kesimpulan'    => ($request->koreksi) ? $request->koreksi : null,
            'status_koreksi'=> ($request->aksi == 'koreksi') ? 'belum' : null,
        ]);

        return redirect(route('bapendik_seminar_pengajuan'));
    }

    public function nilai($nim){

        $mhs = ListMahasiswa::where('nim', $nim)->first();

        if($mhs){

            $tdosbing   = GetMahasiswaDataController::getTotalDosbing($mhs->id);
            $nilai      = MhsNilai::where('nim_id', $mhs->id)->first();

            if($nilai){

                $nilai1 = ($nilai->nilai1 && $nilai->bobot1 > 0) ? ($nilai->nilai1 * 100 / $nilai->bobot1) : 0;
                $nilai1 = number_format($nilai1,2,'.',',');

                $nilai2 = ($nilai->nilai2 && $nilai->bobot2 > 0) ? ($nilai->nilai2 * 100 / $nilai->bobot2) : 0;
                $nilai2 = number_format($nilai2,2,'.',',');

                $nilai3 = ($nilai->nilai3 && $nilai->bobot3 > 0) ? ($nilai->nilai3 * 100 / $nilai->bobot3) : 0;
                $nilai3 = number_format($nilai3,2,'.',',');

                $huruf  = NilaiController::convertToHuruf($nilai->nilai_akhir);

                $nilai1 = ($nilai1) ? "{$nilai1} * {$nilai->bobot1} = {$nilai->nilai1}" : '-'; 
                $nilai2 = ($nilai2) ? "{$nilai2} * {$nilai->bobot2} = {$nilai->nilai2}" : '-'; 
                $nilai3 = ($nilai3) ? "{$nilai3} * {$nilai->bobot3} = {$nilai->nilai3}" : '-'; 

                $nilais = array (
                    'nilai1' => $nilai1,
                    'nilai2' => $nilai2,
                    'nilai3' => $nilai3,
                    'nilai4' => ($nilai->nilai_akhir) ? $nilai->nilai_akhir : 0,
                    'huruf'  => $huruf
                );
            }

            return view('modal.nilai', compact('mhs', 'tdosbing', 'nilai', 'nilais'));
        }else{
            abort(404);
        }
    }

    public function setNilai(Request $request, $id){


        $required2 = ($request->has('nilai2') && $request->has('bobot2')) ? 'required_if:status, selesai' : '';
        $required3 = ($request->has('nilai3') && $request->has('bobot3')) ? 'required_if:status, selesai' : '';

        $this->validate($request, [
            'status' => 'required',
            'nilai1' => 'required_if:status, selesai',
            'bobot1' => 'required_if:status, selesai',
            'nilai2' => $required2,
            'bobot2' => $required2,
            'nilai3' => $required3,
            'bobot3' => $required3
        ]);

        //find mahasiswa
        $mhs        = ListMahasiswa::find($id);
        $tdosbing   = GetMahasiswaDataController::getTotalDosbing($mhs->id);

        $seminar = MhsSeminar::where('nim_id', $id)->where('nomor', '<>', '')->get()->last();
        
        if(!$seminar){
            return Redirect::back()->with('warning', 'Tidak dapat memberi nilai karena mahasiswa belum memiliki berita acara seminar');
        }

        (new SeminarController())->updateStatusSeminar($seminar, 'selesai');

        //hitung nilai
        $nilai1 = 0; $nilai2 = 0; $nilai3 = 0; $total = 0; $waktu = null;
        if($tdosbing == 3){
            $nilai1 = ($request->nilai1 * $request->bobot1)/100;
            $nilai2 = ($request->nilai2 * $request->bobot2)/100;
            $nilai3 = ($request->nilai3 * $request->bobot3)/100;
            $total  = ($nilai1 + $nilai2 + $nilai3);

        }elseif ($tdosbing == 2){
            $nilai1 = ($request->nilai1 * $request->bobot1)/100;
            $nilai2 = ($request->nilai2 * $request->bobot2)/100;
            $total  = ($nilai1 + $nilai2);

        }elseif ($tdosbing == 1){
            $nilai1 = ($request->nilai1 * $request->bobot1)/100;
            $total  = $nilai1;
        }

        //set nilai kalo status kp nya selesai
        if($request->status == 'selesai'){
            $mhsNilai = new MhsNilai();
            $mhsNilai->nim_id = $mhs->id;
            $mhsNilai->nilai1 = $nilai1;
            $mhsNilai->nilai2 = $nilai2;
            $mhsNilai->nilai3 = $nilai3;
            $mhsNilai->bobot1 = $request->bobot1;
            $mhsNilai->bobot2 = ($request->bobot2) ? $request->bobot2 : 0;
            $mhsNilai->bobot3 = ($request->bobot3) ? $request->bobot3 : 0;
            $mhsNilai->nilai_akhir = $total;
            $mhsNilai->nilai_huruf = NilaiController::convertToHuruf($total);
            $mhsNilai->save();

            $waktu = Carbon::now('Asia/Jakarta');

        }

        //update status kp
        MhsProgressStatus::where('nim_id', $mhs->id)->update([
            'status_kp' => $request->status,
            'waktu_kp_selesai' => $waktu
        ]);

        return Redirect::back();
    }

    public function updateNilai(Request $request, $id){

        $required2 = ($request->has('nilai2') && $request->has('bobot2')) ? 'required' : '';
        $required3 = ($request->has('nilai3') && $request->has('bobot3')) ? 'required' : '';

        $this->validate($request, [
            'nilai1' => 'required',
            'bobot1' => 'required',
            'nilai2' => $required2,
            'bobot2' => $required2,
            'nilai3' => $required3,
            'bobot3' => $required3
        ]);

        //find mahasiswa
        $mhs        = ListMahasiswa::find($id);
        $tdosbing   = GetMahasiswaDataController::getTotalDosbing($mhs->id);

        //hitung nilai
        $nilai1 = 0; $nilai2 = 0; $nilai3 = 0; $total = 0; $waktu = null;
        if($tdosbing == 3){
            $nilai1 = ($request->nilai1 * $request->bobot1)/100;
            $nilai2 = ($request->nilai2 * $request->bobot2)/100;
            $nilai3 = ($request->nilai3 * $request->bobot3)/100;
            $total  = ($nilai1 + $nilai2 + $nilai3);

        }elseif ($tdosbing == 2){
            $nilai1 = ($request->nilai1 * $request->bobot1)/100;
            $nilai2 = ($request->nilai2 * $request->bobot2)/100;
            $total  = ($nilai1 + $nilai2);

        }elseif ($tdosbing == 1){
            $nilai1 = ($request->nilai1 * $request->bobot1)/100;
            $total  = $nilai1;
        }

        $mhsNilai = MhsNilai::where('nim_id',$mhs->id)->first();
        $mhsNilai->nim_id = $mhs->id;
        $mhsNilai->nilai1 = $nilai1;
        $mhsNilai->nilai2 = $nilai2;
        $mhsNilai->nilai3 = $nilai3;
        $mhsNilai->bobot1 = $request->bobot1;
        $mhsNilai->bobot2 = ($request->bobot2) ? $request->bobot2 : 0;
        $mhsNilai->bobot3 = ($request->bobot3) ? $request->bobot3 : 0;
        $mhsNilai->nilai_akhir = $total;
        $mhsNilai->nilai_huruf = NilaiController::convertToHuruf($total);
        $mhsNilai->save();

        $waktu = Carbon::now('Asia/Jakarta');

        //update status kp
        MhsProgressStatus::where('nim_id', $mhs->id)->update([
            'status_kp' => $request->status,
            'waktu_kp_selesai' => $waktu
        ]);
        return Redirect::back();
    }

    public function finalisasiNilai(Request $request, $id){
        $this->validate($request, [
            'finalisasi' => 'required'
        ]);
        $mhs        = ListMahasiswa::find($id);
        $mhsNilai = MhsNilai::where('nim_id',$mhs->id)->first();
        if($request->finalisasi==1){
            $mhsNilai->status_finalisasi = $request->finalisasi;
            $mhsNilai->save();
        }
        return Redirect::back();
    }

    public function kuota($nip){

        $dosen = ListDosen::where('nip', $nip)->first();

        return view('modal.kuota', compact('dosen'));
    }

    public function setKuota(Request $request, $nip){
        $this->validate($request, [
            'kuota' => 'required|numeric|min:1|max:10'
        ]);

        $who    = explode(' ', session()->get('who'))[1];
        $dosen  = ListDosen::where('nip', $nip)->first();
        $komisi = ListDosen::find($who);

        $kuota = new KuotaDosen();
        $kuota->komisi_id = $komisi->id;
        $kuota->dosen_id  = $dosen->id;
        $kuota->jumlah    = $request->kuota;
        $kuota->status    = 'menunggu';
        $kuota->save();

        return redirect(route('komisi_menu', 'dosen'));
    }

    public function addSpk($nim){
        $mhs = ListMahasiswa::where('nim', $nim)->first();
        $nama = explode(' ', $mhs->nama)[0];

        return view('modal.addspk', compact('mhs', 'nama'));
    }

    public function setAddSpk(Request $request, $id){
        $this->validate($request, [
            'hari' => 'required|numeric|min:1|max:180'
        ]);

        $spk = MhsProgressStatus::where('nim_id', $id)->first();
        $spk = $spk->waktu_spk_selesai;

        $newSpk = $spk->addDays($request->hari);
        $statusSpk = $newSpk > Carbon::now('Asia/Jakarta');

        MhsProgressStatus::where('nim_id', $id)->update([
            'waktu_spk_selesai' => $newSpk,
            'status_spk' => $statusSpk ? 'aktif' : 'habis'
        ]);

        return redirect(route('wda_menu', 'mahasiswa'));
    }

    public function mhsBimbingan($nip){

        $dosen = ListDosen::where('nip', $nip)->first();

        $mhs = DB::table('list_mahasiswas')
            ->join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'list_mahasiswas.id')
            ->join('mhs_pembimbings', 'mhs_pembimbings.nim_id', '=', 'list_mahasiswas.id')
            ->where('mhs_progress_statuses.status_judul', '=', 'layak')
            ->where('mhs_progress_statuses.status_kp', '=', null)
            ->where('mhs_pembimbings.dosbing1_id', '=', $dosen->id)
            ->orWhere('mhs_pembimbings.dosbing2_id', '=', $dosen->id)
            ->get();

        return view('modal.dosbing_bimbingan', compact('dosen', 'mhs'));
    }

    public function dosenDetail($nip){

        $dosen = ListDosen::where('nip', $nip)->first();
        $jurusan = ListJurusan::lists('jurusan', 'id')->all();
        return view('modal.dosen_detail', compact('dosen', 'jurusan'));
    }

    public function setDosenDetail(Request $request, $id){
        $dosen = ListDosen::find($id);

        if($dosen){
            $dosen->update([
                'nama'          => $request->nama,
                'nip'           => $request->nip,
                'nidn'          => $request->nidn,
                'jurusan_id'    => $request->jurusan
            ]);

        }else{
            abort(404);
        }

        return Redirect::back();
    }

    public function dosenAkun($nip){
        $dosen  = ListDosen::where('nip', $nip)->first();
        $user   = User::where('username', $nip)->first();
        return view('modal.dosen_akun', compact('dosen', 'user'));
    }

    public function setDosenAkun(Request $request, $id){
        $this->validate($request, [
            'level' => 'required',
            'akun'  => 'required'
        ]);

        User::where('username', $id)->update([
            'status'        => $request->level,
            'is_active'     => $request->akun
        ]);

        return Redirect::back();
    }

    public function setKajur(Request $request){

        $this->validate($request, [
            'kajur'     => 'required',
            'jurusan'   => 'required'
        ]);

        $jKajur = ListKajur::where('jurusan_id', $request->jurusan)->count();

        if($jKajur > 0){
            session()->flash('kajur', 'Hanya boleh 1 Ketua Jurusan saja');

        }elseif($jKajur == 0){
            $kajur = new ListKajur();
            $kajur->dosen_id    = $request->kajur;
            $kajur->jurusan_id  = $request->jurusan;
            $kajur->save();
        }

        return Redirect::back();
    }

    public function beritaAcara($id){

        $seminar = MhsSeminar::findOrFail($id);
        $mhs    = GetMahasiswaDataController::getMhsListData('id', '=', $seminar->nim->id);
        $nama   = explode(' ',$mhs->nama)[0];

        return view('modal.beritaacara', compact('nama', 'mhs', 'seminar'));
    }

    public function setBeritaAcara(Request $request){
        $this->validate($request, [
            'id'        => 'required',
            'nomor'     => 'required',
        ]);

        $id = explode('/', $request->id);

        //get selected jadwal
        $seminar = MhsSeminar::findOrFail($id[1]);
        $jadwal = $seminar->jadwal;

        //cari klo ada jadwal yang berkaitan
        $sc         = new SeminarController();
        $shiftList  = $sc->shiftList();
        $firstShift = array_slice($shiftList,0,1)[0];
        $lastShift  = array_slice($shiftList, -1, 1)[0];

        $jadwal = date('Y-m-d', strtotime($jadwal));
        MhsSeminar::where('nim_id', $id[0])
            ->whereBetween('jadwal', [$jadwal.' '.$firstShift, $jadwal.' '.$lastShift])
            ->update(['nomor'=>$request->nomor]);

        return Redirect::back();

    }

    public function distribusi($file, $id){
        $mhs = ListMahasiswa::findOrFail($id);
        return view('modal.distribusi', compact('mhs', 'file'));
    }

    public function updateBerkasDistribusi(Request $request, $id){
        $mhs = MhsBerkasDistribusi::where('nim_id', $id)->first();

        //current file
        $pathForm       = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->form;
        $pathPengesahan = public_path().'/mahasiswa_berkas/'.$mhs->nim->jurusan->jurusan.'/'.$mhs->pengesahan;

        //update form
        if($request->file('formdis')){

            $this->validate($request, ['formdis'=>'mimes:pdf|max:2000']);

            if(File::exists($pathForm)){
                File::delete($pathForm);
            }

            $fileForm       = $request->file('formdis');
            $formFileName   = $mhs->nim->nim.'_'.time().'_formdis.pdf';
            $fileForm->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$formFileName);

            MhsBerkasDistribusi::where('nim_id', $id)->update(['form'=>$formFileName]);
        }

        //update form
        if($request->file('pengesahan')){

            $this->validate($request, ['pengesahan'=>'mimes:pdf|max:2000']);

            if(File::exists($pathPengesahan)){
                File::delete($pathPengesahan);
            }

            $filePengesahan = $request->file('pengesahan');
            $pengFileName   = $mhs->nim->nim.'_'.time().'_pengesahan.pdf';
            $filePengesahan->move("mahasiswa_berkas/".$mhs->nim->jurusan->jurusan."/",$pengFileName);

            MhsBerkasDistribusi::where('nim_id', $id)->update(['pengesahan'=>$pengFileName]);
        }

        return Redirect::back();

    }

    public function berkasDist($id){
        $mhs = ListMahasiswa::findOrFail($id);
        $dist = MhsBerkasDistribusi::where('nim_id', $mhs->id)->firstOrFail();
        $status = MhsProgressStatus::where('nim_id', $mhs->id)->firstOrFail();
        return view('modal.distribusi_cek', compact('mhs', 'dist', 'status'));

    }

    public function accDist($id){
        MhsBerkasDistribusi::where('nim_id', $id)->firstOrFail();
        MhsProgressStatus::where('nim_id', $id)->update(['status_distribusi'=>'ok']);
        return Redirect::back();
    }

    public function gantiDosbing($id){
        $dosbing = MhsGantiDosbing::findOrFail($id);
        $mhs     = ListMahasiswa::findOrFail($dosbing->nim_id);
        $dosen   = GetDosenDataController::GetDosenListDataByJurusan($mhs->jurusan->id);
        return view('modal.gantidosbing', compact('dosen', 'mhs', 'dosbing'));
    }

    public function gantiDosbingAksi(Request $request, $id){
        $this->validate($request, [
            'aksi'      => 'required',
            'dosen'     => 'required_if:aksi,diterima',
            'dosen2'    => 'different:dosen'
        ]);

        switch ($request->aksi){
            case 'diterima' :
                //usulan ganti
                $ganti = MhsGantiDosbing::findOrFail($id);
                //spk sekarang
                $spk = MhsSpk::where('nim_id', $ganti->nim_id)->first();

                //ganti usulan
                $dosbing = MhsPembimbing::where('nim_id', $ganti->nim_id)->first();
                $dosbing->dosbinglama_id    = $dosbing->dosbing1_id;
                $dosbing->dosbinglama2_id   = $dosbing->dosbing2_id;
                $dosbing->dosbing1_id       = $request->dosen;
                $dosbing->dosbing2_id       = $request->dosen2;
                $dosbing->save();

                //simpan spk lama
                $spkLama = new MhsSpkLama();
                $spkLama->nim_id    = $spk->nim_id;
                $spkLama->nomor     = $spk->nomor;
                $spkLama->save();

                //null nomor di spk sekarang
                $spk->update(['nomor'=>null]);

                //ganti status usulan
                $ganti->update(['status'=>'diterima']);
                break;

            case 'ditolak' :
                MhsGantiDosbing::findOrFail($id)->update(['status'=>'ditolak']);
                break;
        }

        return Redirect::back();
    }
}
