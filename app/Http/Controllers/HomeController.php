<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\ListRuangSeminar;
use App\MhsProgressStatus;
use App\MhsSuratPengantarKp;
use App\MhsKp;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller {

    private $_user, $_id, $_username, $_settings;

    public function __construct(){

        $user = null; $id = null;
        if(session()->has('who')){
            $who    = session()->get('who');
            $who    = explode(' ', $who);
            $user   = $who[0];
            $id     = $who[1];
            $user   = UserLoginController::getUser($user, $id);
        }

        $this->_id          = $id;
        $this->_user        = $user;
        $this->_username    = ($user) ? explode(' ', $user->nama)[0] : 'login';
        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
    }

    public function index(){

        return redirect(route('home_seminar'));
    }

    public function seminar(){

        $user       = $this->_user;
        $username   = $this->_username;

        $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
        if($ruangan){
            $listRuangan = ListRuangSeminar::find($ruangan);
            $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
        }else{
            $listRuangan = '1';
        }

        $ruangan    = ListRuangSeminar::lists('ruangan', 'id')->all();
        $today      = Carbon::now('Asia/Jakarta');
        $week       = Carbon::now('Asia/Jakarta')->addDay($this->_settings['jadwal_seminar']);

        return view('front.seminar', compact('user','username', 'today', 'week', 'ruangan','listRuangan'));
    }

    public function surat(){
        $user = $this->_user;
        $username= $this->_username;
        $surat = MhsSuratPengantarKp::orderBy('id', 'desc')->paginate(20);

        return view('front.surat', compact('user', 'username', 'surat'));
    }

    public function spk(){
        $user = $this->_user;
        $username= $this->_username;
        $spk  = MhsProgressStatus::orderBy('id', 'desc')->where('spk_berkas', 'jadi')->paginate(10);

        return view('front.spk', compact('user', 'username', 'spk'));
    }

    public function bantuan(){
        $user = $this->_user;
        $username= $this->_username;
        return view('front.bantuan', compact('user', 'username'));
    }

    public function judul(Request $request){

        $user = $this->_user;
        $username= $this->_username;

        $mhskp = MhsKp::where('judul_final', '<>', '')->latest('id')->paginate(20);
        
        $cari = $request->cari;
        if($cari){
            $mhskp = MhsKp::where(function($q) use (&$cari){
                $q->where('judul_final', 'like', "%{$cari}%")->orWhereHas('tempat', function($query) use (&$cari){
                    $query->where('instansi', 'like', "%{$cari}%");
                })->orWhereHas('nim', function($query) use (&$cari){
                    $query->where('nama', 'like', "%{$cari}%");
                    $query->orWhere('nim', 'like', "%{$cari}%");
                });
            })->whereNotNull('judul_final')->paginate(20);
        }


        return view('front.judul', compact('mhskp', 'username', 'user'));
    }


    /**************** FUNCTIONS ********************/
    public function trimString($kata, $max){
        $kata = strip_tags($kata);
        if(strlen($kata) > $max){
            $kata_cut	= substr($kata,0, $max);
            $kata 	 	= $kata_cut."... ";
        }

        return $kata;
    }
}
