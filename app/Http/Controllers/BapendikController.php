<?php

namespace App\Http\Controllers;

use App\ListDosen;
use App\ListJurusan;
use App\ListMahasiswa;
use App\ListRuangSeminar;
use App\MhsBerkasDistribusi;
use App\MhsBerkasSeminar;
use App\MhsKp;
use App\MhsPembimbing;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsSpk;
use App\MhsSpkLama;
use App\MhsSuratLama;
use App\MhsSuratPengantarKp;
use App\User;
use App\MhsNilai;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class BapendikController extends Controller
{

    public $_user, $_id, $_settings;

    public function __construct(){
        $userLogin = UserLoginController::isLogin('bapendik');
        if(!$userLogin) Redirect::to('/')->send();

        $this->_user = User::where('user_id', $userLogin)->where('status', 'bapendik')->first();
        $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
    }

    public function index(){
        return redirect(route('bapendik_kp_surat'));
    }

    //menu di sidebar
    public function menu($menu){
        $user = $this->_user;
        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();
        //nama route sama blade harus sama
        $menuList = array('keamanan', 'username', 'seminar', 'dosen', 'dosbing', 'arsip');
        if(in_array($menu, $menuList)){

            switch ($menu){
                case 'keamanan' :
                    return view('bapendik.'.$menu, compact('user'));
                    break;

                case 'username' :
                    return view('bapendik.'.$menu, compact('user'));

                case 'seminar' :

                    $ruangan = (Input::has('ruangan')) ? Input::get('ruangan') : null;
                    if($ruangan){
                        $listRuangan = ListRuangSeminar::find($ruangan);
                        $listRuangan = ($listRuangan) ? $listRuangan->id : abort(404);
                    }else{
                        $listRuangan = '1';
                    }

                    $dateMonth  = Carbon::now('Asia/Jakarta');
                    $today      = Carbon::now('Asia/Jakarta');
                    $ruangan    = ListRuangSeminar::lists('ruangan', 'id')->all();

                    $dateToday  = Carbon::create(
                        $today->format('Y'),
                        $today->format('m'),
                        '1'
                    );
                    $jumlahHari = $this->_settings['jadwal_seminar'];
                    return view('bapendik.'.$menu, compact('dateToday', 'dateMonth', 'user', 'ruangan','listRuangan', 'jumlahHari'));
                    break;

                case 'dosen' :
                    $dosen  = ListDosen::orderBy('nama')->paginate(30);

                    return view('bapendik.dosen_semua', compact('user', 'dosen', 'listJurusan'));
                    break;

                case 'dosbing' :
                    $dosbing = MhsPembimbing::distinct()->select('dosbing1_id')->paginate(30);

                    return view('bapendik.dosen_dosbing', compact('user', 'listJurusan', 'dosbing'));

                case 'distribusi' :
                    $dist = MhsBerkasDistribusi::orderBy('id','desc')->paginate(30);
                    return view('bapendik.'.$menu, compact('user', 'dist'));
                    break;

                case 'arsip':

                    $mhs    = MhsNilai::paginate(20);
                    $user   = $this->_user;
                    return view('bapendik.arsip', compact('mhs', 'user'));
                    
                break;

            }


        }else{
            abort(404);
        }
    }

    //halaman list status prasyrat mahasiswa
    public function kpPrasyarat(){

        $user           = $this->_user;
        $listJurusan    = ListJurusan::lists('jurusan', 'id')->all();
        $listMhs        = ListMahasiswa::orderBy('nama')->paginate(30);

        return view('bapendik.kp_prasyarat', compact('listJurusan', 'listMhs', 'user'));
    }

    //halaman list pengajuan surat pengantar kp
    public function kpSurat(Request $request){

        $user = $this->_user;
        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();
        $mhs = MhsSuratPengantarKp::orderBy('id', 'desc');

        $idJurusan = $request->jurusan == 'semua' ? null : $request->jurusan;
        $status = $request->status == 'semua' ? null : $request->status;

        if($request->cari || $idJurusan){
            $mhs = $mhs->whereHas('nim', function($q) use ($request, $idJurusan){
                if($request->cari){
                    $q->where('nim', 'like', "%{$request->cari}%");
                    $q->orWhere('nama', 'like', "%{$request->cari}%");
                }

                if($idJurusan){
                    $q->where('jurusan_id', $idJurusan);
                }
            });
        }

        if($status){
            $mhs = $mhs->where('status', $status);
        }

        $mhs = $mhs->paginate(10);

        return view('bapendik.kp_surat', compact('mhs', 'listJurusan', 'user'));
    }

    //halaman list pengajuan kp mahasiswa
    public function kpUsulan(Request $request){
        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();
        $spk = MhsProgressStatus::with(['spk'])->latest('id');

        if($request->cari){
            $spk = $spk->whereHas('nim', function($q) use ($request){
                $q->where('nim', 'like', "%{$request->cari}%");
                $q->orWhere('nama', 'like', "%{$request->cari}%");
            });
        }

        if(is_numeric($request->jurusan)){
            $spk = $spk->whereHas('nim', function($q) use ($request){
                $q->where('jurusan_id', $request->jurusan);
            });
        }

        $status = ['lengkap', 'koreksi'];
        if(in_array($request->status, $status)){
            $spk = $spk->whereHas('nim.progress', function($q) use ($request){
                $q->where('status_berkas', $request->status);
            });
        }

        $spk = $spk->paginate(20);
        return view('bapendik.kp_usulan', compact('spk', 'listJurusan'));
    }

    //halaman list mahasiswa yang kp
    public function kpMhs(Request $request){

        $user = $this->_user;
        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();
        $mhs = MhsSpk::where('nomor', '<>', '')->latest('id');

        if($request->cari){
            $mhs = $mhs->whereHas('nim', function($q) use ($request){
                $q->where('nim', 'like', "%{$request->cari}%");
                $q->orWhere('nama', 'like', "%{$request->cari}%");
            });
        }

        if(is_numeric($request->jurusan)){
            $mhs = $mhs->whereHas('nim', function($q) use ($request){
                $q->where('jurusan_id', $request->jurusan);
            });
        }

        $mhs = $mhs->paginate(30);
        return view('bapendik.kp_mhs', compact('mhs', 'listJurusan', 'user'));

    }

    //halaman detail pengajuan - pengecekan berkas lengkap/koreksi NANTI DIGANTI MODAL
    public function kpUsulanDetail($nim){

        $mhskp  = GetMahasiswaDataController::getMhsKpDataByNim($nim);

        return view('bapendik.kp_usulan_detail', compact('mhskp'));
    }

    //set status surat - cetak, proses, selesai
    public function setStatusSurat(Request $request, $nim){

        $this->validate($request, [
            'aksi' => 'required'
        ]);

        if($request->aksi == 'cetak'){
            return redirect(route('cetak_surat_pengantar', $nim));
        }elseif($request->aksi == 'proses' || $request->aksi == 'selesai'){
            $mhs = GetMahasiswaDataController::getMhsListData('nim', '=', $nim);
            $waktu = ($request->aksi == 'selesai') ? Carbon::now('Asia/Jakarta') : null;

            MhsSuratPengantarKp::where('nim_id', $mhs->id)->update([
                'status'        =>$request->aksi,
                'waktu_selesai' => $waktu
            ]);
            return redirect(route('bapendik_kp_usulan'));
        }

    }

    //set status berkas - lengkap, korekso
    public function setStatusBerkas(Request $request, $nim){

        $this->validate($request, [
            'aksi'      => 'required',
            'koreksi'   => 'required_if:aksi,koreksi'
        ]);

        $koreksi    = ($request->koreksi) ? $request->koreksi : null;
        $mhs        = GetMahasiswaDataController::getMhsListData('nim', '=', $nim);

        //update progress
        MhsProgressStatus::where('nim_id', $mhs->id)->update([
            'status_berkas'         => $request->aksi,
            'status_koreksi'        => 'belum',
            'waktu_berkas_lengkap'  => ($request->aksi == 'lengkap') ? Carbon::now('Asia/Jakarta') : null
        ]);

        //set koreksi
        if($koreksi){
            MhsKp::where('nim_id', $mhs->id)->update([
                'koreksi'=> $koreksi
            ]);
        }

        return redirect(route('bapendik_kp_usulan'));
    }

    //view pengajuan seminar
    public function seminarPengajuan(){

        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();
        $user = $this->_user;
        //$berkas = MhsBerkasSeminar::orderBy('id', 'desc')->paginate(30);
        $seminar = MhsSeminar::orderBy('id', 'desc')->paginate(30);
        return view('bapendik.seminar_pengajuan', compact('seminar', 'user', 'listJurusan'));
    }

    //view spk lama
    public function spkLama(){
        $spk = MhsSpkLama::orderBy('id', 'desc')->paginate(30);
        return view('bapendik.kp_spklama', compact('spk'));
    }

    //view surat lama
    public function suratLama(){
        $surat = MhsSuratLama::orderBy('id', 'desc')->paginate(30);
        return view('bapendik.kp_suratlama', compact('surat'));
    }

    /******************* FILTER STUFF **********/
    public function filterKpPrasyarat(){

        $user           = $this->_user;
        $listJurusan    = ListJurusan::lists('jurusan', 'id')->all();

        $cari       = (Input::get('cari')) ? Input::get('cari') : '';
        $jurusan    = (Input::get('jurusan') == 'semua') ? '%' : Input::get('jurusan');
        $status     = (Input::get('status') == 'semua') ? '%' : Input::get('status');

        $query = "SELECT * FROM list_mahasiswas 
                  WHERE (nama LIKE '%$cari%' OR nim LIKE '%$cari%') AND 
                  jurusan_id LIKE '$jurusan' AND status_prasyarat LIKE '$status' ORDER BY  status_prasyarat DESC ,nim DESC";
        $listMhs = DB::select($query);


        return view('bapendik.filter_kp_prasyarat', compact('listJurusan', 'listMhs', 'user'));
    }

    public function filterKpUsulan(){
        $user           = $this->_user;
        $listJurusan    = ListJurusan::lists('jurusan', 'id')->all();

        $cari       = (Input::get('cari')) ? Input::get('cari') : '';
        $jurusan    = (Input::get('jurusan') == "semua") ? null : Input::get('jurusan');
        $status     = (Input::get('status') == "semua") ? null : Input::get('status');

        $jurusan    = ($jurusan) ? "AND a.jurusan_id = '$jurusan' " : $jurusan;
        $status     = ($status) ? "AND b.status_berkas = '$status' " : $status;

        $query = "SELECT * FROM list_mahasiswas a, mhs_progress_statuses b, mhs_kps c
                  WHERE a.id = b.nim_id AND a.id = c.nim_id AND
                  (a.nama LIKE '%$cari%' OR a.nim LIKE '%$cari%') $jurusan $status ORDER BY  b.status_berkas DESC ,nim DESC";
        $listMhs = DB::select($query);
        $spk            = MhsSpk::all();

       return view('bapendik.filter_kp_usulan', compact('listJurusan', 'listMhs', 'user', 'spk'));
    }

    public function filterKpMhs(){
        $user           = $this->_user;
        $listJurusan    = ListJurusan::lists('jurusan', 'id')->all();

        $cari       = (Input::get('cari')) ? Input::get('cari') : '';
        $jurusan    = (Input::get('jurusan') == 'semua') ? null : Input::get('jurusan');

        $listMhs = ListMahasiswa::where('nama', 'like', "%{$cari}%");
        
        if($jurusan){
            $listMhs = $listMhs->whereHas('jurusan', function($q) use ($jurusan){
                $q->where('id', $jurusan);
            });
        }

        $listMhs = $listMhs->has('progress')->get();
        return view('bapendik.filter_kp_mhs', compact('listJurusan', 'listMhs', 'user'));
    }

    public function filterDosenSemua(){
        $user       = $this->_user;
        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();

        $jurusan    = (Input::has('jurusan')) ? Input::get('jurusan') : null;

        if(!$jurusan){
            return redirect(route('bapendik_menu', 'dosen'));
        }

        $query = "SELECT * FROM list_dosens a
                  WHERE a.jurusan_id = '$jurusan' ORDER BY a.nama DESC";
        $listDosen = DB::select($query);

        return view('bapendik.filter_dosen_semua', compact('listDosen', 'user', 'listJurusan'));
    }

    public function filterDosbing(){
        $user       = $this->_user;
        $listJurusan = ListJurusan::lists('jurusan', 'id')->all();

        $jurusan    = (Input::get('jurusan') == 'semua') ? null : Input::get('jurusan');

        if(!$jurusan){
            return redirect(route('bapendik_menu', 'dosbing'));
        }

        $query = "SELECT DISTINCT(b.dosbing1_id) FROM list_dosens a, mhs_pembimbings b
                  WHERE a.id = b.dosbing1_id AND
                  a.jurusan_id = '$jurusan' ORDER BY a.nama DESC";
        $listDosen = DB::select($query);


        return view('bapendik.filter_dosen_dosbing', compact('listDosen', 'user', 'listJurusan'));
    }

    public function filterPengajuanSeminar(){

        $user           = $this->_user;
        $listJurusan    = ListJurusan::lists('jurusan', 'id')->all();

        $cari       = (Input::get('cari')) ? Input::get('cari') : '';
        $jurusan    = (Input::get('jurusan') == 'semua') ? '%' : Input::get('jurusan');
        $status     = (Input::get('status') == 'semua') ? '%' : Input::get('status');

        $query = "SELECT *, MIN(b.jadwal) AS jadwal_awal, MAX(b.jadwal) AS jadwal_akhir 
                  FROM mhs_seminars b right join mhs_berkas_seminars c on b.nim_id = c.nim_id join list_mahasiswas a on a.id = c.nim_id
                  AND (a.nama LIKE '%$cari%' OR a.nim LIKE '%$cari%') AND a.jurusan_id LIKE '$jurusan' AND c.status_berkas LIKE '$status'
                  GROUP BY b.nim_id ORDER BY  c.id ";
        $listMhs = DB::select($query);

        return view('bapendik.filter_seminar', compact('listDosen', 'user', 'listJurusan', 'listMhs'));
    }

    /**************** FUNCTIONS ********************/
    public function trimString($kata, $max){
        $kata = strip_tags($kata);
        if(strlen($kata) > $max){
            $kata_cut	= substr($kata,0, $max);
            $kata 	 	= $kata_cut."... ";
        }

        return $kata;
    }
}
