<?php

namespace App\Http\Controllers;

use App\ListDosen;
use App\ListJurusan;
use App\ListMahasiswa;
use App\ListRuangSeminar;
use App\MhsNilai;
use App\MhsKp;
use App\User;
use App\MhsProgressStatus;
use App\MhsSeminar;
use App\MhsPembimbing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ExportImportController extends Controller
{

	public $_user, $_id;

	public function __construct(){
	  set_time_limit(0);      
	  $data = User::where('status', 'bapendik')->get();
	  //klo belum ada data halamannya ga di protect
	  if(count($data) > 0){
		  $userLogin = UserLoginController::isLogin('bapendik');
		  
		  if(!$userLogin)
			  $userLogin = UserLoginController::isLogin('komisi');

		  if(!$userLogin)
			  $userLogin = UserLoginController::isLogin('wda');

		  if(!$userLogin) Redirect::to('/')->send();

		  $this->_user = User::where('user_id', $userLogin)->where('status', 'bapendik')->first();
	  }
	  $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
	}

	public function index(){
		$user = $this->_user;
		return view('bapendik.import_export', compact('user'));
	}

	public function export(Request $request){

		$user = $this->_user;

		if(!$request->data){
			return view('bapendik.export', compact('user'));
		}else{
			$this->validate($request, [
				'data' => 'required',
			]);

			$dataToExport = $request->data;

			switch ($dataToExport){
				case 'selesai_kp' :

					//mahasiswa yang kpnya sudah selesai
					$mhsKpSelesai = MhsProgressStatus::select('nim_id')->where('status_kp', 'selesai');

					if($request->tanggal_dari)
					  $mhsKpSelesai = $mhsKpSelesai->where('waktu_kp_selesai', '>=', $request->tanggal_dari);
					
					if($request->tanggal_ke)
					  $mhsKpSelesai = $mhsKpSelesai->where('waktu_kp_selesai', '<=', $request->tanggal_ke);

					//jika tidak ada data yang ditemukan
					if(!count($mhsKpSelesai->get())){
					  session()->flash('import', 'Data Tidak Ditemukan');
					  return Redirect::back();
					}

					//simpan id mahasiswa yang sudah selesai kp
					$nimIds = array();
					foreach ($mhsKpSelesai->get() as $o) {
					  array_push($nimIds, $o->nim_id);
					}

					//ndapatein nama mahasiswa tersebut
					$mhs = ListMahasiswa::whereIn('id', $nimIds)->get();
					$dataMhsSelesaiKp = array();
					
					foreach ($mhs as $o) {

						$seminar = MhsSeminar::where('nim_id', $o->id)->get()->last();

					  	$durasiHari 	= $o->timeDiffTillSeminar();
						if($durasiHari){
							$durasiBulan 	= (int) ($durasiHari / 30);

							$dataMhsSelesaiKp[] = [
								'NIM'             			=> $o->nim,
								'NAMA'            			=> $o->nama,
								'JURUSAN'         			=> $o->jurusan->jurusan,
								'NOMOR SPK'					=> $o->spk->nomor,
								'TANGGAL SEMINAR'			=> $seminar ? $seminar->jadwal->format('d F Y') : '-',
								'JUDUL KP'        			=> $o->kp->judul_final,
								'PEMBIMBING 1'    			=> $o->pembimbing->dosbing1->nama,
								'PEMBIMBING 2'    			=> ($o->pembimbing->dosbing2) ? $o->pembimbing->dosbing2->nama : null,
								'PEMBIMBING 3'    			=> ($o->pembimbing->pemlap) ? $o->pembimbing->pemlap : null,
								'NILAI ANGKA'     			=> $o->nilai->nilai_akhir,
								'NILAI HURUF'     			=> $o->nilai->nilai_huruf,
								'WAKTU SELESAI'   			=> $o->progress->waktu_kp_selesai->format('d-m-y'),
								'DURASI SELESAI'  			=> "{$durasiBulan} BULAN ({$durasiHari} HARI)",
							];

						}
					}

					$data = json_decode(json_encode($dataMhsSelesaiKp), true);
					break;

				case 'progress_kp' :
					$mhskp = MhsProgressStatus::where('status_kp', null);
					
					if($request->tanggal_dari)
						$mhskp = $mhskp->where('created_at', '>=', $request->tanggal_dari);

				  	if($request->tanggal_ke)
						$mhskp = $mhskp->where('created_at', '<=', $request->tanggal_ke);

					//jika tidak ada data yang ditemukan
					if(!count($mhskp->get())){
					  session()->flash('import', 'Data Tidak Ditemukan');
					  return Redirect::back();
					}

					$dataMhsKp = array();
					foreach ($mhskp->get() as $o) {

						$progress = null;
						if(!$o->status_berkas){
							$progress = 'Berkas Sedang Diproses Oleh Bapendik';
						}elseif(!$o->status_judul){
							$progress = 'Berkas Sedang Di Proses Oleh Komisi';
						}elseif(!$o->spk_berkas){
							$progress = 'SPK KP Belum Jadi';
						}elseif(!$o->status_bimbingan){
							$progress = 'Belum Bertemu Dengan Dosen Pembimbing';							
						}elseif($o->status_bimbingan){
							$progress = 'Dalam Bimbingan KP';
						}elseif($o->status_judul_final){
							$progress = 'Menuju Seminar KP';
						}

						$spkTerlewati = null;
						if($o->waktu_spk_mulai){
							$spkTerlewati = "{$o->waktu_spk_mulai->diffInMonths()} BULAN ({$o->waktu_spk_mulai->diffInDays()} HARI)";							
						}

						$spkAktif = null;
						if($o->waktu_spk_selesai){
							$spkAktif = "{$o->getSisaWaktuBulan()} BULAN ({$o->getSisaWaktuHari()} HARI)";
						}


						$dataMhsKp[] = [
							'NIM'				=> $o->nim->nim,
							'NAMA'				=> $o->nim->nama,
							'JURUSAN'			=> $o->nim->jurusan->jurusan,
							'JUDUL'				=> $o->nim->kp ? $o->nim->kp->judul : '-',
							'SPK MULAI' 		=> ($o->waktu_spk_mulai) ? $o->waktu_spk_mulai->format('d-m-y') : '-',
							'SPK HABIS'			=> ($o->waktu_spk_mulai) ? $o->waktu_spk_selesai->format('d-m-y') : '-',
							'SPK TERLEWATI'		=> $spkTerlewati,
							'SPK AKTIF' 		=> $spkAktif,
							'WAKTU DAFTAR'		=> $o->nim->kp ? $o->nim->kp->waktu_daftar->format('d-m-y') : '-',
							'PEMBIMBING 1'		=> ($o->nim->pembimbing->dosbing1) ? $o->nim->pembimbing->dosbing1->nama : '-',
							'PEMBIMBING 2'		=> ($o->nim->pembimbing->dosbing2) ? $o->nim->pembimbing->dosbing2->nama : '-',
							'PEMBIMBING 3'		=> ($o->nim->pembimbing->pemlap) ? $o->nim->pembimbing->pemlap : '-', 
							'PROGRESS'			=> $progress
						];
					}					

					$data = json_decode(json_encode($dataMhsKp), true);
					break;

				case 'rataan_kp_seminar' :

					$mhsSeminar = MhsSeminar::where('status', 'selesai')->where('jenis', 'kp')->groupBy('nim_id');

					if($request->tanggal_dari)
						$mhsSeminar = $mhsSeminar->where('created_at', '>=', $request->tanggal_dari);

				  	if($request->tanggal_ke)
						$mhsSeminar = $mhsSeminar->where('created_at', '<=', $request->tanggal_ke);

					//jika tidak ada data yang ditemukan
					if(!count($mhsSeminar->get())){
					  session()->flash('import', 'Data Tidak Ditemukan');
					  return Redirect::back();
					}

					$waktuSeminarByJurusan = array();
					foreach ($mhsSeminar->get() as $o) {
						$waktuSeminarByJurusan[$o->nim->jurusan->jurusan][] = $o->nim->timeDiffTillSeminar();
					}

					$rataanWaktuByJurusan = array();
					foreach ($waktuSeminarByJurusan as $key => $value) {
						$totalWaktu=0;
						foreach ($value as $o) {
							$totalWaktu += $o;
						}
						$rataanWaktuByJurusan[$key] = $totalWaktu / count($value);
					}

					//format untuk excel
					$dataRataanSeminar = array();
					foreach ($rataanWaktuByJurusan as $key => $value) {
						$valueHari 	= number_format($value,2,'.',',');
						$valueBulan = $value/30;
						$valueBulan = number_format($valueBulan,2,'.',',');
						$dataRataanSeminar[] = [
							'JURUSAN' => strtoupper($key),
							'RATAAN WAKTU (HARI) KP HINGGA SEMINAR' => $valueHari.' HARI',
							'RATAAN WAKTU (BULAN) KP HINGGA SEMINAR' => $valueBulan.' BULAN' 
						];
					}

					$data = json_decode(json_encode($dataRataanSeminar), true);
					break;

				case 'rataan_bimbingan' :

					$meanDosbing1 = DB::table('mhs_pembimbings')
						->join('mhs_bimbingans', 'mhs_pembimbings.nim_id', '=' ,'mhs_bimbingans.nim_id')
						->join('list_dosens', 'list_dosens.id', '=' ,'mhs_pembimbings.dosbing1_id')
						->join('list_jurusans', 'list_jurusans.id', '=' ,'list_dosens.jurusan_id');

					if($request->tanggal_dari)
						$meanDosbing1 = $meanDosbing1->where('mhs_bimbingans.created_at', '>=', $request->tanggal_dari);

				  	if($request->tanggal_ke)
						$meanDosbing1 = $meanDosbing1->where('mhs_bimbingans.created_at', '<=', $request->tanggal_ke);

					$meanDosbing1 = $meanDosbing1
						->select(DB::raw('count(*) as total, mhs_pembimbings.dosbing1_id, list_dosens.nama, list_jurusans.jurusan'))
						->groupBy('mhs_pembimbings.dosbing1_id');

					//jika tidak ada data yang ditemukan
					if(!count($meanDosbing1->get())){
					  session()->flash('import', 'Data Tidak Ditemukan');
					  return Redirect::back();
					}

					$bimbinganPerDosen = array();
					foreach ($meanDosbing1->get() as $o) {
						$bimbinganPerDosen[] = [
							'NAMA'				=> $o->nama,
							'JURUSAN'			=> $o->jurusan,
							'RATAAN BIMBINGAN' 	=> $o->total
						];
					}

				   $data = json_decode(json_encode($bimbinganPerDosen), true);
				break;

				case 'kuota_dosen' :

					$kuota = MhsPembimbing::join('mhs_progress_statuses', 'mhs_progress_statuses.nim_id', '=', 'mhs_pembimbings.nim_id')
						->join('list_dosens', 'list_dosens.id', '=' ,'mhs_pembimbings.dosbing1_id')
						->join('list_jurusans', 'list_jurusans.id', '=' ,'list_dosens.jurusan_id');

					if($request->tanggal_dari)
						$kuota = $kuota->where('mhs_progress_statuses.waktu_spk_mulai', '>=', $request->tanggal_dari);

				  	if($request->tanggal_ke)
						$kuota = $kuota->where('mhs_progress_statuses.waktu_spk_mulai', '<=', $request->tanggal_ke);

					$kuota = $kuota->select(DB::raw('count(*) as total, list_dosens.nama, list_jurusans.jurusan'))
							->groupBy('mhs_pembimbings.dosbing1_id');

					//jika tidak ada data yang ditemukan
					if(!count($kuota->get())){
					  session()->flash('import', 'Data Tidak Ditemukan');
					  return Redirect::back();
					}

					$dataKuota = array();
					foreach ($kuota->get() as $o) {
						$dataKuota[] = [
							'NAMA'					=> $o->nama,
							'JURUSAN'				=> $o->jurusan,
							'BEBAN BIMBINGAN'		=> $o->total
						];
					}

					$data = json_decode(json_encode($dataKuota), true);
					break;

				case 'seminar' :

					$seminar = MhsSeminar::where('ruangan_id', '!=', 0);

					if($request->tanggal_dari)
						$seminar = $seminar->where('jadwal', '>=', $request->tanggal_dari);

				  	if($request->tanggal_ke)
						$seminar = $seminar->where('jadwal', '<=', $request->tanggal_ke);

					//jika tidak ada data yang ditemukan
					if(!count($seminar->get())){
					  session()->flash('import', 'Data Tidak Ditemukan');
					  return Redirect::back();
					}

					$dataSeminar = array();
					foreach ($seminar->get() as $o) {
						$dataSeminar[] = [
							'NOMOR'		=> $o->nomor,
							'NIM'		=> $o->nim->nim,
							'NAMA'		=> $o->nim->nama,
							'JURUSAN'	=> $o->nim->jurusan->jurusan,
							'RUANGAN'	=> $o->ruangan->ruangan,
							'TANGGAL'	=> $o->jadwal->format('d-m-y'),
							'JAM'		=> $o->jadwal->format('H:i'),
							'JENIS'		=> $o->jenis,
							'STATUS'	=> $o->status
						];
					}

					$data = json_decode(json_encode($dataSeminar), true);

					break;
			}

			return Excel::create($dataToExport, function ($excel) use ($data) {
				$excel->sheet('Sheet', function ($sheet) use ($data) {
					$sheet->fromArray($data);
				});
			})->download('xlsx');

		}
	}

	public function import(Request $request){

		$this->validate($request, [
			'dataExport'    => 'required',
			'fileExcel'     => 'required|mimes:xls,xlsx,csv'
		]);

		$dataToImport = $request->dataExport;
		$excelRealPath = $request->fileExcel;

		if ($dataToImport == 'mahasiswa') {
			$this->importMahasiswa($excelRealPath);

		} elseif ($dataToImport == 'dosen') {
			$this->importDosen($excelRealPath);

		} elseif ($dataToImport == 'jurusan') {
			$this->importJurusan($excelRealPath);

		} elseif ($dataToImport == 'ruangan') {
			$this->importRuangan($excelRealPath);

		} elseif ($dataToImport == 'bapendik') {
			$this->importBapendik($excelRealPath);

		} elseif ($dataToImport == 'wda'){
			$this->importWda($excelRealPath);
		}

		return redirect('import_export');
	}

	public function importMahasiswa($excelRealPath){
		$settings 	= $this->_settings;
		$path   	= $excelRealPath;
		$data   	= Excel::load($path, function ($reader){
		})->get();

	   if(!empty($data) && $data->count()){
		   foreach ($data as $key => $value) {

			   $mhsList    = ListMahasiswa::where('nim', $value->nim)->first();
			   $mhs        = (array)$mhsList;

			   if(empty($mhs)){

				   $status = null; $level = null;
				   if($value->ipk >= 2 && $value->totalsks >= 120){
					   $status = 'memenuhi';
					   $level  = null;

				   }elseif($value->ipk >= $settings['ipk_minimum'] && $value->totalsks >= $settings['sks_minimum']){
					   $status = 'memenuhi';
					   $level  = 'kp';

				   }else{
					   $status = 'belum';
				   }

				   $mhsInsert                  = new ListMahasiswa();
				   $mhsInsert->nim             = $value->nim;
				   $mhsInsert->jurusan_id      = $this->getJurusanId($value->jurusan);
				   $mhsInsert->nama            = $value->namamhs;
				   $mhsInsert->kelamin         = $this->getKelaminName($value->jeniskelamin);
				   $mhsInsert->sks             = $value->totalsks;
				   $mhsInsert->ipk             = $value->ipk;
				   $mhsInsert->tahun_ajaran    = $value->tahunakademik;
				   $mhsInsert->status_prasyarat= $status;
				   $mhsInsert->level           = $level;
				   $mhsInsert->save();


				   User::create([
					   'user_id'       => $mhsInsert->id,
					   'username'      => $value->nim,
					   'password'      => bcrypt($value->nim),
					   'status'        => 'mahasiswa',
					   'is_active'     => 'aktif'
				   ]);


				   Session::flash('import', 'Import data mahasiswa sukses');

			   }else{


				   $level = GetMahasiswaDataController::getCurrentLevel($value->nim);

				   $status = null;
				   if($value->ipk >= $settings['ipk_minimum'] && $value->totalsks >= $settings['sks_minimum']){
						  $status = 'memenuhi';
						  $level  = ($level) ? $level : null;
				   }elseif($value->ipk >= 2 && $value->totalsks >= 90){
						  $status = 'memenuhi';
						  $level  = 'kp';
				   }else{
						  $status = 'belum';
				   }

				   ListMahasiswa::where('nim', $value->nim)->update([
						  'sks'               => $value->totalsks,
						  'ipk'               => $value->ipk,
						  'status_prasyarat'  => $status,
						  'level'             => $level,
						  'tahun_ajaran'      => $value->tahunakademik
				   ]);

				   Session::flash('import', 'Data mahasiswa berhasil diperbarui');
			   }
		   }
	   }
	}

	public function importDosen($excelRealPath){
		$path   = $excelRealPath;
		$data   = Excel::load($path, function ($reader){
		})->get();

		if(!empty($data) && $data->count()){
			foreach ($data as $key => $value) {

				$dosenList  = ListDosen::find($value->id);
				$dosen      = (array)$dosenList;

				if(empty($dosen)){
					$dosen              = new ListDosen();
					$dosen->id          = $value->id;
					$dosen->jurusan_id  = $value->jurusan_id;
					$dosen->nip         = $value->nip;
					//$dosen->nidn        = $value->nidn;
					$dosen->nama        = $value->nama;
					$dosen->kuota       = $value->kuota;
					$dosen->save();

					User::create([
						'user_id'       => $dosen->id,
						'username'      => ($value->nidn) ? $value->nidn : $value->nip,
						'password'      => bcrypt($value->password),
						'status'        => $value->level,
						'is_active'     => 'aktif'
					]);

					Session::flash('import', 'Import data dosen sukses');

				}else{
					$dosen              = ListDosen::find($value->id);
					$dosen->id          = $value->id;
					$dosen->jurusan_id  = $value->jurusan_id;
					$dosen->nip         = $value->nip;
					//$dosen->nidn        = $value->nidn;
					$dosen->nama        = $value->nama;
					$dosen->kuota       = $value->kuota;
					$dosen->save();


					Session::flash('import', 'Data dosen berhasil diperbarui');
				}
			}
		}
	}

	public function importJurusan($excelRealPath){

		$path   = $excelRealPath;
		$data   = Excel::load($path, function ($reader){
		})->get();

		if(!empty($data) && $data->count()){
			foreach ($data as $key => $value) {

				$jurusanList = ListJurusan::find($value->id);
				$jurusan     = (array)$jurusanList;

				if(empty($jurusan)){
					$jurusan            = new ListJurusan();
					$jurusan->id        = $value->id;
					$jurusan->jurusan   = $value->jurusan;
					$jurusan->save();

					Session::flash('import', 'Import jurusan data sukses');
				}else{
					$jurusan            = ListJurusan::find($value->id);
					$jurusan->jurusan   = $value->jurusan;
					$jurusan->save();

					Session::flash('import', 'Data jurusan berhasil diperbarui');
				}
			}
		}
	}

	public function importRuangan($excelRealPath){
		$path   = $excelRealPath;
		$data   = Excel::load($path, function ($reader){
		})->get();


		if(!empty($data) && $data->count()){
			foreach ($data as $key => $value) {

				$ruangList = ListRuangSeminar::find($value->id);
				$ruang      = (array)$ruangList;

				if(empty($ruang)){
					$ruang              = new ListRuangSeminar();
					$ruang->id          = $value->id;
					$ruang->ruangan     = $value->ruangan;
					$ruang->kapasitas   = $value->kapasitas;
					$ruang->save();

					Session::flash('import', 'Import data ruangan seminar sukses');
				}else{
					$ruang              = ListRuangSeminar::find($value->id);
					$ruang->ruangan     = $value->ruangan;
					$ruang->kapasitas   = $value->kapasitas;
					$ruang->save();

					Session::flash('import', 'Data ruangan seminar berhasil diperbarui');
				}

			}
		}
	}

	public function importBapendik($excelRealPath){
		$path   = $excelRealPath;
		$data   = Excel::load($path, function ($reader){
		})->get();


		if(!empty($data) && $data->count()){
			foreach ($data as $key => $value) {
				$bapendikList   = User::where('user_id', $value->id)->where('status', 'bapendik')->first();
				$bapendik       = (array)$bapendikList;

				if(empty($bapendik)){

					User::create([
						'user_id'       => $value->id,
						'username'      => $value->username,
						'password'      => bcrypt($value->password),
						'status'        => $value->status,
						'is_active'     => 'aktif'
					]);

					Session::flash('import', 'Import data bapendik sukses');

				}else{

					Session::flash('import', 'Data bapendik sudah ada');
				}
			}
		}
	}

	public function getJurusanId($jurusan){
		if($jurusan == 'Teknik Informatika'){
			return 1;
		}elseif($jurusan == 'Teknik Elektro'){
			return 2;
		}elseif($jurusan == 'Teknik Sipil'){
			return 3;
		}elseif($jurusan == 'Teknik Industri'){
			return 4;
		}elseif($jurusan == 'Teknik Geologi'){
			return 5;
		}
	}

	public function getKelaminName($id){
		if($id == '1'){
			return 'laki-laki';
		}elseif($id =='2'){
			return 'perempuan';
		}
	}

}
