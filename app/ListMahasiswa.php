<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListMahasiswa extends Model{
  
  protected $fillable = [
    'nim', 'jurusan_id', 'dosenpa_id', 'nama', 'kelamin', 'sks', 'ipk', 
    'email', 'telp','tahun_ajaran', 'status_prasyarat', 'level'
  ];
  
  private $_settings;
  
  function __construct(){
    $this->_settings = json_decode(file_get_contents(storage_path('settings.json')), true);
  }
  
  public function getHasMemenuhiSksAttribute(){
    return $this->sks >= $this->_settings['sks_minimum'];
  }
  
  public function getHasMemenuhiIpkAttribute(){
    return $this->ipk >= $this->_settings['ipk_minimum'];
  }

  public function getHasMemenuhiSyaratAttribute(){
    return $this->hasMemenuhiSks && $this->hasMemenuhiIpk;
  }
  
  public function getHasMelengkapiDataAttribute(){
    return $this->telp && $this->email;
  }
  
  public function jurusan(){
    return $this->belongsTo('App\ListJurusan');
  }
  
  public function dosenpa(){
    return $this->belongsTo('App\ListDosen');
  }
  
  //lama
  public function surat(){
    return $this->hasOne('App\MhsSuratPengantarKp', 'nim_id');
  }
  
  //baru
  public function suratPengantarKp(){
    return $this->hasMany('App\MhsSuratPengantarKp', 'nim_id');
  }

  public function kp(){
    return $this->hasOne('App\MhsKp', 'nim_id');
  }
  
  public function distribusi(){
    return $this->hasMany('App\MhsBerkasDistribusi', 'nim_id');
  }
  
  public function spk(){
    return $this->hasOne('App\MhsSpk', 'nim_id');
  }
  
  public function pembimbing(){
    return $this->hasOne('App\MhsPembimbing', 'nim_id');
  }
  
  public function nilai(){
    return $this->hasOne('App\MhsNilai', 'nim_id');
  }
  
  public function progress(){
    return $this->hasOne('App\MhsProgressStatus', 'nim_id');
  }
  
  public function seminar(){
    return $this->hasOne('App\MhsSeminar', 'nim_id');
  }
  
  public function berkasSeminar(){
    return $this->hasOne('App\MhsBerkasSeminar', 'nim_id');
  }
  
  public function berkasDistribusi(){
    return $this->hasOne('App\MhsBerkasDistribusi', 'nim_id');
  }
  
  public function timeDiffTillSeminar(){
    $mulai    = $this->progress->waktu_spk_mulai;
    $seminar  = MhsSeminar::where(['nim_id'=>$this->id,'jenis'=>'kp'])->get()->last();
    if(!$seminar){
      return 0;
    }
    return $mulai->diffInDays($seminar->jadwal,false);
  }
  
  public function cekSeminar(){
    $seminar  = MhsSeminar::where(['nim_id'=>$this->id,'jenis'=>'kp','status'=>'selesai'])->get()->last();
    return $seminar;
  }
  
}
