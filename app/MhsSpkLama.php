<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsSpkLama extends Model
{

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function getCreatedAtAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
        //return Carbon::parse($date);
    }

}
