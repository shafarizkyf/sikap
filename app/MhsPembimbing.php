<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MhsPembimbing extends Model
{

    public $timestamps = false;

    public function mhskp(){
        return $this->belongsTo('App\mhs_kp');
    }

    public function dosbing1(){
        return $this->belongsTo('App\ListDosen');
    }

    public function dosbing2(){
        return $this->belongsTo('App\ListDosen');
    }

    public function dosbinglama(){
        return $this->belongsTo('App\ListDosen');
    }

    public function dosbinglama2(){
        return $this->belongsTo('App\ListDosen');
    }

}
