<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsSuratLama extends Model
{
    public $fillable = ['id', 'nim_id', 'nomor', 'instansi', 'kepada', 'dimana','waktu_ajukan', 'waktu_selesai'];

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function getWaktuAjukanAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }

    public function getWaktuSelesaiAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }

}
