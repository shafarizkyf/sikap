<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsBimbingan extends Model
{

    public $fillable = ['bimbingan', 'waktu_bimbingan', 'status'];

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function getWaktuBimbinganAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
        //return Carbon::parse($date);
    }

}
