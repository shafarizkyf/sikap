<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsGantiDosbing extends Model
{

    public $fillable = ['nim_id', 'dosbing_id', 'alasan', 'status'];

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function dosbing(){
        return $this->belongsTo('App\ListDosen');
    }

    public function dosbing2(){
        return $this->belongsTo('App\ListDosen');
    }


    public function getCreatedAtAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }
}
