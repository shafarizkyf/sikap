<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class MhsSeminar extends Model
{

    protected $fillable = ['id', 'nim_id', 'jadwal', 'ruangan_id', 'shift', 'jenis', 'status'];

    public function nim(){
        return $this->belongsTo('App\ListMahasiswa');
    }

    public function ruangan(){
        return $this->belongsTo('App\ListRuangSeminar');
    }

    public function getJadwalAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }

    public function getCreatedAtAttribute($date){
        Date::setLocale('id');
        return Date::parse($date);
    }

}
