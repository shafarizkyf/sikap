<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
  
  protected $fillable = ['user_id', 'username', 'password', 'status', 'is_active'];
  protected $hidden = ['password', 'remember_token'];

  public function listDosen(){
    return $this->belongsTo('App\ListDosen', 'user_id');
  }

  public function scopeActive($query){
    return $query->where('is_active', 'aktif');
  }
  
  public function scopeBapendik($query){
    return $query->where('status', 'bapendik');    
  }
  
  public function scopeDosen($query, $idJurusan = null){

    $user = $query->where(function($q){
      $q->where('status', 'dosen');
      $q->orWhere('status','komisi');
      $q->orWhere('status','wda');
    });

    if($idJurusan){
      $dosenIds = ListDosen::where('jurusan_id', $idJurusan)->select('id')->get()->toArray();
      $user = $user->whereIn('user_id', $dosenIds);
    }

    return $user;
  }
  
  public function scopeMahasiswa($query){
    return $query->where('status', 'mahasiswa');    
  }
  
  public function scopeWda($query){
    return $query->where('status', 'wda');
  }
}
