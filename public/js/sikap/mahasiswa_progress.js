$(document).ready(function() {
    $('#coba-btn').click(function () {
        var nim= this.getAttribute('aria-nim');
        getDetailMhs(nim);
    });

    $('#koreksi-komisi').click(function () {
        var nim= this.getAttribute('aria-nim');
        getFormKoreksiKomisi(nim);
    });

    function getDetailMhs(nim) {
        $.ajax({
            type: 'get',
            url: '/modal/koreksi/'+nim,
            success: function (data) {
                $('#ex1').html(data);
            }
        });
    }

    function getFormKoreksiKomisi(nim) {
        $.ajax({
            type: 'get',
            url: '/modal/koreksikomisi/'+nim,
            success: function (data) {
                $('#ex1').html(data);
            }
        });
    }
});