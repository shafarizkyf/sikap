$(document).ready(function() {
    $(function () {
        $('#tanggal').datepicker();
    });

    $('#bimbingan').click(function () {
        var nim = this.getAttribute('aria-nim');
        getDetailMhs(nim);
    });

    $('#finalisasi').click(function () {
        var nim = this.getAttribute('aria-nim');
        setJudulFinal(nim);
    });

    $('.materi').click(function () {
        var nim = this.getAttribute('aria-nim');
        getDetailMhs(nim);
    });

    function getDetailMhs(nim) {
        $.ajax({
            type: 'get',
            url: '/modal/bimbingan/' + nim,
            success: function (data) {
                $('#ex2').html(data);
            }
        });
    }

    function setJudulFinal(nim) {
        $.ajax({
            type: 'get',
            url: '/modal/judul/' + nim,
            success: function (data) {
                $('#ex2').html(data);
            }
        });
    }
});