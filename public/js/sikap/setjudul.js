$(document).ready(function() {
    $('#finalisasi').click(function () {
        var nim = this.getAttribute('aria-nim');
        getJudulFinal(nim);
    });

    function getJudulFinal(nim) {
        $.ajax({
            type: 'get',
            url: '/modal/judulkp/' + nim,
            success: function (data) {
                $('#ex2').html(data);
            }
        });
    }
});