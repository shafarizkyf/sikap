$(document).ready(function() {
    $('.detail-btn').click(function () {
        var nip= this.getAttribute('aria-nip');
        getDetailDosen(nip);
    });

    $('.akun-btn').click(function () {
        var nip= this.getAttribute('aria-nip');
        getDosenAkun(nip);
    });

    function getDetailDosen(nip) {
        $.ajax({
            type: 'get',
            url: '/modal/dosendetail/'+nip,
            success: function (data) {
                $('#ex1').html(data);
            }
        });
    }

    function getDosenAkun(nip) {
        $.ajax({
            type: 'get',
            url: '/modal/dosenakun/'+nip,
            success: function (data) {
                $('#ex2').html(data);
            }
        });
    }
});