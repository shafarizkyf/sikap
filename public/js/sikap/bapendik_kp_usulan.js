$(document).ready(function() {
    $('.detail-btn').click(function () {
        var nim= this.getAttribute('aria-nim');
        getDetailMhs(nim);
    });

    $('.set-spk').click(function () {
        var nim= this.getAttribute('aria-nim');
        setSpk(nim);
    });

    function getDetailMhs(nim) {
        $.ajax({
            type: 'get',
            url: "<?php \Illuminate\Support\Facades\URL::to('/') ?>" + '/usulan/detail/'+nim,
            success: function (data) {
                $('#ex1').html(data);
            }
        });
    }

    function setSpk(nim) {
        $.ajax({
            type: 'get',
            url: '/modal/spk/'+nim,
            success: function (data) {
                $('#ex2').html(data);
            }
        });
    }

});