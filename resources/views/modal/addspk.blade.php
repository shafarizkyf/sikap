<div class="tag">
    <div class="tag-content">penambahan masa spk untuk {{ $nama }}</div>
</div>
<div class="modal-content">
    {!! Form::open(['method'=>'post', 'action'=>['ModalController@setAddSpk', $mhs->id]]) !!}
    <div class="field">
        {!! Form::label('hari', null, ['class' => 'ion-email']) !!}
        {!! Form::text('hari', null, ['id' => 'hari', 'placeholder' => 'Jumlah hari (max. 180 hari)']) !!}
    </div>
    <div class="field noborder center">
        {!! Form::submit('Tambah', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>