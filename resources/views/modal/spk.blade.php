<div class="tag">
    <div class="tag-content">Set SPK - {{ $mhskp->nim->nama }}</div>
</div>
<div class="modal-content">
    {!! Form::open(['method'=>'post', 'action'=>['ModalController@setSpk', $mhskp->nim_id]]) !!}
    <div class="field">
        {!! Form::label('nomor', null, ['class' => 'ion-email']) !!}
        {!! Form::text('nomor', null, ['id' => 'nomor', 'placeholder' => 'Nomor SPK']) !!}
    </div>
    @if($errors->first('nomor'))
    <div class="field noborder error">
        {{ $errors->first('nomor') }}
    </div>
    @endif
    @if(!$mhskp->pembimbing->dosbinglama_id)
    {!! Form::hidden('type', 'new') !!}
    <div class="field">
        {!! Form::label('spk_mulai', null, ['class' => 'ion-android-calendar']) !!}
        {!! Form::text('spk_mulai', null, ['id' => 'spk_mulai', 'placeholder' => 'Tanggal Mulai SPK']) !!}
    </div>
    @if($errors->first('spk_mulai'))
        <div class="field noborder error">
            {{ $errors->first('spk_mulai') }}
        </div>
    @endif
    @endif
    <div class="field">
        {!! Form::label('spk_jadi', null, ['class' => 'ion-android-calendar pickdate']) !!}
        {!! Form::text('spk_jadi', null, ['id' => 'spk_jadi', 'placeholder' => 'Kapan SPK Diambil']) !!}
    </div>
    @if($errors->first('spk_jadi'))
        <div class="field noborder error">
            {{ $errors->first('spk_jadi') }}
        </div>
    @endif
    <div class="field noborder center">
        {!! Form::submit('Buat SPK', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>

<script>
    $(document).ready(function() {
        $(function () {
            $('#spk_mulai').datepicker();
            $('#spk_jadi').datepicker();
        });
    });
</script>