<div class="tag">
    <div class="tag-content">Berita Acara untuk {{ $nama }}</div>
</div>
<div class="modal-content">
    {!! Form::open(['method'=>'post', 'action'=>'ModalController@setBeritaAcara']) !!}
    {!! Form::hidden('id', $mhs->id.'/'.$seminar->id) !!}
    <div class="field">
        {!! Form::label('nomor', null, ['class' => 'ion-email']) !!}
        {!! Form::text('nomor', null, ['id' => 'nomor', 'placeholder' => 'Nomor Surat']) !!}
    </div>
    <div class="field noborder center">
        {!! Form::submit('Buat Berkas', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>