<div class="tag">
    <div class="tag-content">surat pengantar untuk {{ $nama }}</div>
</div>
<div class="modal-content">
    {!! Form::open(['method'=>'post', 'action'=>'ModalController@setSurat']) !!}
    {!! Form::hidden('id', $surat->id) !!}
    <div class="field">
        {!! Form::label('nomor', null, ['class' => 'ion-email']) !!}
        {!! Form::text('nomor', null, ['id' => 'nomor', 'placeholder' => 'Nomor Surat']) !!}
    </div>
    <div class="field">
        {!! Form::label('selesai', null, ['class' => 'ion-android-calendar']) !!}
        {!! Form::text('selesai', null, ['id' => 'selesai', 'placeholder' => 'Tanggal pengambilan surat']) !!}
    </div>
    <div class="field noborder center">
        {!! Form::submit('Buat Surat', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>

<script>
    $(document).ready(function() {
        $(function () {
            $('#selesai').datepicker();
        });
    });
</script>