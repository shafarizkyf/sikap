@if($mhskp)
    <div class="tag">
        <div class="tag-content">judul - {{ $mhskp->nim->nama }}</div>
    </div>
    <div class="modal-content">
        <div class="list">
            <label for="tempatkp" class="ion-ios-paper-outline"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Topik Kerja Praktik</h2>
                <p class="desc">
                    {{ $mhskp->judul }}
                </p>
            </div>
        </div>
        @if($mhskp->judul_final)
            <div class="list">
                <label for="tempatkp" class="ion-ios-paper-outline"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Judul Kerja Praktik</h2>
                    <p class="desc">
                        {{ $mhskp->judul_final }}
                    </p>
                </div>
            </div>
        @endif
        {!! Form::open(['method'=>'post ', 'action'=>['ModalController@setJudulKp', $mhskp->nim_id]]) !!}
        <div class="field">
            {!! Form::label("aksi", null, ['class' => 'ion-ios-browsers']) !!}
            <div class="styled-select">
                {!! Form::select('aksi', [''=>'Pilih Aksi', 'setuju'=>'Setujui', 'revisi'=>'Revisi Judul'], null, ['id'=>'aksi']) !!}
            </div>
        </div>
        <div class="field judul">
            {!! Form::label('judul', null, ['class' => 'ion-ios-lightbulb']) !!}
            {!! Form::text('judul', null, ['placeholder' => 'Judul Final Proposal Kerja Praktik']) !!}
        </div>
        <div class="field noborder center">
            {{ Form::submit('Simpan', ['class'=>'button']) }}
        </div>
    </div>
@endif

<script>
    $(document).ready(function() {
        $('.judul').hide();

        $('#aksi').change(function(){
            var status 	= $('#aksi').val();
            if(status == 'revisi'){
                $('.judul').fadeIn('normal');
            }else{
                $('.judul').fadeOut('normal');
            }
        });
    });
</script>