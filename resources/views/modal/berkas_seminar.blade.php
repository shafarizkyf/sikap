<div class="tag">
    <div class="tag-content">Pengajuan Seminar {{ $mhs->nama }}</div>
</div>
<div class="modal-content">
    {!! Form::open(['method'=>'post ', 'action'=>['ModalController@setSeminar', $berkas->nim_id], 'files'=>true]) !!}
    <div class="list">
        <label for="tempatkp" class="ion-location"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Kartu Studi Mahasiswa</h2>
            <p class="desc">
                KSM : <a href="{{ route('download_berkas', $berkas->ksm) }}">Download</a> |
                <a href="{{ route('view_file', array('ksm', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    <div class="list">
        <label for="tempatkp" class="ion-location"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Scan TTD Dosen pada Draft Laporan</h2>
            <p class="desc">
                Scan TTD : <a href="{{ route('download_berkas', $berkas->makalah) }}">Download</a> |
                <a href="{{ route('view_file', array('makalah', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    <div class="list">
        <label for="tempatkp" class="ion-location"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Surat Selesai Kerja Praktik</h2>
            <p class="desc">
                Surat Selesai Kerja Praktik : <a href="{{ route('download_berkas', $berkas->surat_selesai) }}">Download</a> |
                <a href="{{ route('view_file', array('suratselesai', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    <div class="list">
        <label for="tempatkp" class="ion-location"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Lembar Presensi Lapangan Kerja Praktik</h2>
            <p class="desc">
                Lembar Presensi : <a href="{{ route('download_berkas', $berkas->surat_spk) }}">Download</a> |
                <a href="{{ route('view_file', array('suratspk', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    <div class="list">
        <label for="tempatkp" class="ion-location"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Kartu Seminar</h2>
            <p class="desc">
                Kartu Seminar : <a href="{{ route('download_berkas', $berkas->kartu_seminar) }}">Download</a> |
                <a href="{{ route('view_file', array('kartuseminar', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    @if((!$berkas->status_koreksi or $berkas->status_koreksi == 'sudah') and $berkas->status_berkas != 'lengkap')
    <div class="field">
        <label for="aksi" class="ion-flag"></label>
        <div class="styled-select">
            {!! Form::select('aksi', ['lengkap'   => 'Berkas Lengkap', 'koreksi'   => 'Koreksi Berkas'], null, ['placeholder'=>'Status Berkas', 'id'=>'aksi']) !!}
        </div>
    </div>
    @if($errors->first('aksi'))
        <div class="field noborder error">
            {{ $errors->first('aksi') }}
        </div>
    @endif
    <div class="koreksi-wrapper">
    <div class="field textarea">
        <label for="koreksi" class="ion-ios-lightbulb"></label>
        {!! Form::textarea('koreksi', null, ['id'=>'koreksi','placeholder'=>'Koreksi yang diperlukan' ]) !!}
    </div>
    </div>
    <div class="field noborder center">
        {!! Form::submit('Simpan', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
    @endif
</div>

<script>
    $(document).ready(function() {
        $('.koreksi-wrapper').hide();

        $('#aksi').change(function(){
            var status 	= $('#aksi').val();
            if(status == 'koreksi'){
                $('.koreksi-wrapper').fadeIn('normal');
            }else{
                $('.koreksi-wrapper').fadeOut('normal');
            }
        });
    });
</script>