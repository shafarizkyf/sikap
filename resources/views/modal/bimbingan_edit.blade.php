<div class="tag">
    <div class="tag-content">surat pengantar untuk</div>
</div>
<div class="modal-content">
    {!! Form::model($bimbingan, ['method'=>'patch', 'action'=>['ModalController@updateMateriBimbingan', $bimbingan->id]]) !!}
    <div class="field textarea">
        <label for="bimbingan" class="ion-ios-lightbulb"></label>
        {!! Form::textarea('bimbingan', null, ['id'=>'bimbingan','placeholder'=>'Materi Bimbingan' ]) !!}
    </div>
    <div class="field noborder center">
        {!! Form::submit('Simpan', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>