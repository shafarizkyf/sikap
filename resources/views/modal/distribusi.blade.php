@if($file == 'dist')
    <div class="tag">
        <div class="tag-content">Update Berkas Distribusi - {{ $mhs->nama }}</div>
    </div>
    <div class="modal-content">
        {!! Form::open(['method'=>'post', 'action'=>['ModalController@updateBerkasDistribusi', $mhs->id], 'files'=>true]) !!}
        <div class="field">
            <label for="formdis" class="filefield">
                <span class="ion-paperclip icon"></span>
                Form Distribusi (.pdf)
            </label>
            {!! Form::file('formdis', ['id'=>'formdis']) !!}
        </div>
        @if($errors->first('formdis'))
            <div class="field noborder error">
                {{ $errors->first('formdis') }}
            </div>
        @endif
        <div class="field noborder center">
            {{ Form::submit('Simpan', ['class'=>'button']) }}
        </div>
        {!! Form::close() !!}
    </div>
@elseif($file == 'pengesahan')
    <div class="tag">
        <div class="tag-content">Update Berkas Distribusi - {{ $mhs->nama }}</div>
    </div>
    <div class="modal-content">
        {!! Form::open(['method'=>'post', 'action'=>['ModalController@updateBerkasDistribusi', $mhs->id], 'files'=>true]) !!}
        <div class="field">
            <label for="pengesahan" class="filefield">
                <span class="ion-paperclip icon"></span>
                Pengesahan Laporan KP (.pdf)
            </label>
            {!! Form::file('pengesahan', ['id'=>'pengesahan']) !!}
        </div>
        @if($errors->first('pengesahan'))
            <div class="field noborder error">
                {{ $errors->first('pengesahan') }}
            </div>
        @endif
        <div class="field noborder center">
            {{ Form::submit('Simpan', ['class'=>'button']) }}
        </div>
        {!! Form::close() !!}
    </div>
@endif

