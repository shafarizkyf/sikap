@if($mhs)
    <div class="tag">
        <div class="tag-content">KP - {{ $mhs->nama }}</div>
    </div>
    <div class="modal-content">
    @if(!$nilai)
        {!! Form::open(['method'=>'post','id' => 'form_nilai', 'action'=>['ModalController@setNilai', $mhs->id]]) !!}
            <div class="field">
                {!! Form::label("status", null, ['class' => 'ion-person']) !!}
                <div class="styled-select">
                    {!! Form::select('status', ['selesai'=>'Selesai', 'ulang'=>'Ulang'], null, ['placeholder'=>'Status Kerja Praktik']) !!}
                </div>
            </div>
            <div class="nilai-wrapper">
            @for($i=1; $i<=$tdosbing; $i++)
                <?php
                    if($tdosbing == 3){
                        $placeholder = ($i==1 or $i==2) ? 'Nilai Dosen Pembimbing '.$i : null;
                        $placeholder = ($i==3) ? 'Nilai Pembimbing Lapangan' : $placeholder;
                    }elseif($tdosbing == 2){
                        $placeholder = ($i==1) ? 'Nilai Dosen Pembimbing '.$i : null;
                        $placeholder = ($i==2) ? 'Nilai Pembimbing Lapangan' : $placeholder;
                    }elseif($tdosbing == 1){
                        $placeholder = 'Nilai Dosen Pembimbing ';
                    }
                ?>
                <div class="field field-inline">
                    <div class="input-left">
                        {!! Form::label('nilai'.$i, null, ['class' => 'ion-ribbon-a']) !!}
                        {!! Form::text('nilai'.$i, null, ['placeholder' => $placeholder, 'class' => 'nilai_kp']) !!}
                    </div>
                    <div class="input-right">
                        <label for="{{ 'bobot'.$i }}">%</label>
                        {!! Form::text('bobot'.$i, null, ['placeholder' => 'Bobot','class' => 'bobot_kp']) !!}
                    </div>
                </div>
            @endfor
            </div>
            <div class="field noborder center">
                {{ Form::submit('Simpan', ['class'=>'button','id'=>'nilai_submit']) }}
            </div>
        {!! Form::close() !!}
    @else
        @if($tdosbing == 3)
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Nilai Dosen Pembimbing 1</h2>
                    <p class="desc">{{ $nilais['nilai1'] }}</p>
                </div>
            </div>
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Nilai Dosen Pembimbing 2</h2>
                    <p class="desc">{{ $nilais['nilai2'] }}</p>
                </div>
            </div>
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Pembimbing Lapangan</h2>
                    <p class="desc">{{ $nilais['nilai3'] }}</p>
                </div>
            </div>
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Nilai Akhir</h2>
                    <p class="desc">{{ $nilais['nilai4'] }}</p>
                </div>
            </div>
        @else
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Nilai Dosen Pembimbing 1</h2>
                    <p class="desc">{{ $nilais['nilai1'] }}</p>
                </div>
            </div>
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Pembimbing Lapangan</h2>
                    <p class="desc">{{ $nilais['nilai2'] }}</p>
                </div>
            </div>
            <div class="list">
                <label for="tempatkp" class="ion-ribbon-a"></label>
                <div class="item" id="tempatkp">
                    <h2 class="title">Nilai Akhir</h2>
                    <p class="desc">{{ $nilais['nilai4'] }} / {{ $nilais['huruf'] }}</p>
                </div>
            </div>
        @endif
            <div class="field noborder center" style="margin-bottom: 20px;">
                @if(!$nilai->status_finalisasi)
                    {!! Form::open(['method'=>'patch','id' => 'finalisasi_form','action'=>['ModalController@finalisasiNilai', $mhs->id]]) !!}
                        {!! Form::hidden('finalisasi','1') !!}
                        <a class="button" style="margin-right: 10px;" id="nilai-ubah">ubah</a>
                        {!! Form::submit('finalisasi', ['class'=>'button']) !!}
                    {!! Form::close() !!}
                @endif
            </div>
        @if(!$nilai->status_finalisasi)
            {!! Form::open(['method'=>'patch','id' => 'ubah_nilai_form','style' => 'display:none;', 'action'=>['ModalController@updateNilai', $mhs->id]]) !!}
            {!! Form::hidden('type', 'update') !!}
            @for($i=1; $i<=$tdosbing; $i++)
                <?php
                    if($tdosbing == 3){
                        $placeholder = ($i==1 or $i==2) ? 'Nilai Dosen Pembimbing '.$i : null;
                        $placeholder = ($i==3) ? 'Nilai Pembimbing Lapangan' : $placeholder;
                    }elseif($tdosbing == 2){
                        $placeholder = ($i==1) ? 'Nilai Dosen Pembimbing '.$i : null;
                        $placeholder = ($i==2) ? 'Nilai Pembimbing Lapangan' : $placeholder;
                    }elseif($tdosbing == 1){
                        $placeholder = 'Nilai Dosen Pembimbing ';
                    }
                ?>
                <div class="field field-inline">
                    <div class="input-left">
                        {!! Form::label('nilai'.$i, null, ['class' => 'ion-ribbon-a']) !!}
                        {!! Form::text('nilai'.$i, null, ['placeholder' => $placeholder, 'class' => 'nilai_kp']) !!}
                    </div>
                    <div class="input-right">
                        <label for="{{ 'bobot'.$i }}">%</label>
                        {!! Form::text('bobot'.$i, null, ['placeholder' => 'Bobot','class' => 'bobot_kp']) !!}
                    </div>
                </div>
            @endfor
                <div class="field noborder center">
                    {{ Form::submit('Simpan', ['class'=>'button','id'=>'nilai_ubah_submit']) }}
                </div>
            {!! Form::close() !!}
        @endif
    @endif
    </div>
@endif

<script>
    $(document).ready(function() {
        $('#nilai-ubah').click(function(){
            $('#ubah_nilai_form').show();
            $('#finalisasi_form').hide();
        });
        $('.nilai-wrapper').hide();

        $('#status').change(function(){
            var status 	= $('#status').val();
            if(status == 'selesai'){
                $('.nilai-wrapper').fadeIn('normal');
            }else{
                $('.nilai-wrapper').fadeOut('normal');
            }
        });
        var nilai_fields = $('input.nilai_kp');
        var bobot_fields = $('input.bobot_kp');
        nilai_fields.blur(function(e){
            let nilai = parseInt($(this).val());
            $(this).val(nilai);
            if(nilai<0 || nilai>100 || isNaN(nilai)){
                alert("Nilai adalah bilangan bulat 0 - 100");
                $(this).val('0');
                $(this).focus();
            }
        });
        bobot_fields.blur(function(e){
            let bobot = parseInt($(this).val());
            $(this).val(bobot);
            if(bobot<0 || bobot>100 || isNaN(bobot)){
                alert("Bobot adalah bilangan bulat 0 - 100");
                $(this).val('0');
                $(this).focus();
            }
        });
        $('#nilai_submit').click(function(e){
            e.preventDefault();
            var bobot = 0;
            $.each(bobot_fields, function(i, item){
                bobot = bobot+parseInt($(item).val());
            });
            if(bobot != 100){
                alert("Total bobot harus 100");
            }else{
                $('#form_nilai').submit();
            }
        });

        $('#nilai_ubah_submit').click(function(e){
            e.preventDefault();
            var bobot = 0;
            $.each(bobot_fields, function(i, item){
                bobot = bobot+parseInt($(item).val());
            });
            if(bobot != 100){
                alert("Total bobot harus 100");
            }else{
                $('#ubah_nilai_form').submit();
            }
        });
    });
</script>