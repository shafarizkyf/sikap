<div class="tag">
    <div class="tag-content">Distribusi - {{ $mhs->nama }}</div>
</div>
<div class="modal-content">
    <div class="list">
        <label for="tempatkp" class="ion-android-document"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Form Distribusi</h2>
            <p class="desc">
                Form Distribusi : <a href="{{ route('download_berkas', $dist->form) }}">Download</a> |
                <a href="{{ route('view_file', array('formdist', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    <div class="list">
        <label for="tempatkp" class="ion-android-document"></label>
        <div class="item" id="tempatkp">
            <h2 class="title">Pengesahan Laporan KP</h2>
            <p class="desc">
                Pengesahan Laporan KP : <a href="{{ route('download_berkas', $dist->pengesahan) }}">Download</a> |
                <a href="{{ route('view_file', array('pengesahan', $mhs->nim)) }}" target="_blank">Lihat</a>
            </p>
        </div>
    </div>
    @if(!$status->status_distribusi)
    {!! Form::open(['method'=>'post ', 'action'=>['ModalController@accDist', $dist->nim_id]]) !!}
    <div class="field noborder center">
        {{ Form::submit('Terima', ['class'=>'button']) }}
    </div>
    @endif
</div>