@if($dosen)
    <div class="tag">
        <div class="tag-content">Kuota - {{ $dosen->nama}}</div>
    </div>
    <div class="modal-content">
        {!! Form::open(['method'=>'post ', 'action'=>['ModalController@setKuota', $dosen->nip]]) !!}
        <div class="field">
            {!! Form::label('kuota', null, ['class' => 'ion-ios-lightbulb']) !!}
            {!! Form::text('kuota', null, ['placeholder' => 'Kuota tambahan (max.10)']) !!}
        </div>
        <div class="field noborder center">
            {{ Form::submit('Ajukan', ['class'=>'button']) }}
        </div>
    </div>
@endif