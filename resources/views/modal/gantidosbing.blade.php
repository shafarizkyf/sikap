@if($mhs)
    <div class="tag">
        <div class="tag-content">penggantian dosen- {{ $mhs->nama }}</div>
    </div>
    <div class="modal-content">
        {!! Form::open(['method'=>'post', 'action'=>['ModalController@gantiDosbingAksi', $dosbing->id]]) !!}
        <div class="field">
            {!! Form::label('aksi', null, ['class' => 'ion-flag']) !!}
            <div class="styled-select">
                {!! Form::select('aksi', ['diterima'=>'Terima', 'ditolak'=>'Tolak'], null, ['placeholder'=>'Pilih Aksi', 'id'=>'aksi']) !!}
            </div>
        </div>
        @if($errors->first('aksi'))
            <div class="field noborder error">
                {{ $errors->first('aksi') }}
            </div>
        @endif
        <div class="field dosen">
            {!! Form::label('dosen', null, ['class' => 'ion-person']) !!}
            <div class="styled-select">
                {!! Form::select('dosen', $dosen, null, ['placeholder'=>'Pilih Dosen Pembimbing Utama']) !!}
            </div>
        </div>
        @if($errors->first('dosen'))
            <div class="field noborder error">
                {{ $errors->first('dosen') }}
            </div>
        @endif
        <div class="field dosen">
            {!! Form::label('dosen2', null, ['class' => 'ion-person']) !!}
            <div class="styled-select">
                {!! Form::select('dosen2', $dosen, null, ['placeholder'=>'Pilih Dosen Pembimbing Anggota']) !!}
            </div>
        </div>
        @if($errors->first('dosen2'))
            <div class="field noborder error">
                {{ $errors->first('dosen2') }}
            </div>
        @endif
        <div class="field noborder center">
            {!! Form::submit('OK', ['class' => 'button']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endif

<script>
    $(document).ready(function() {
        $('.dosen').hide();

        $('#aksi').change(function(){
            var status 	= $('#aksi').val();
            if(status == 'diterima'){
                $('.dosen').fadeIn('normal');
            }else{
                $('.dosen').fadeOut('normal');
            }
        });
    });
</script>
