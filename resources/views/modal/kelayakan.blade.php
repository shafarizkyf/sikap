@if($mhskp)
    <div class="tag">
        <div class="tag-content">DETAIL PENGAJUAN KP - {{ $mhskp['nim'] }}</div>
    </div>
    <div class="modal-content">
        <div class="list">
            <label for="tempatkp" class="ion-location"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Tempat Kerja Praktik</h2>
                <p class="desc">
                    {{ $mhskp['instansi'] }}
                </p>
            </div>
        </div>
        <div class="list">
            <label for="tempatkp" class="ion-ios-lightbulb"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Topik Proposal Kerja Praktik</h2>
                <p class="desc">
                    {{ $mhskp['judul'] }}
                </p>
            </div>
        </div>
        <div class="list">
            <label for="tempatkp" class="ion-person"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Usulan Dosen Pembimbing 1</h2>
                <p class="desc">
                    {{ $mhskp['dosbing1'] }}
                </p>
            </div>
        </div>
        @if($mhskp['dosbing2'])
        <div class="list">
            <label for="tempatkp" class="ion-person"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Usulan Dosen Pembimbing 2</h2>
                <p class="desc">
                    {{ $mhskp['dosbing2'] }}
                </p>
            </div>
        </div>
        @endif
        <div class="list">
            <label for="tempatkp" class="ion-paperclip"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Outline Proposal Kerja Praktik</h2>
                <p class="desc">
                    Outline Proposal : <a href="{{ route('download_berkas', $mhskp['outline']) }}">Download</a> |
                    <a href="{{ route('view_file', array('outline', $mhskp['nim'])) }}" target="_blank">Lihat</a>
                </p>
            </div>
        </div>
        <div class="list">
            <label for="tempatkp" class="ion-paperclip"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Surat Penerimaan Lokasi KP</h2>
                <p class="desc">
                    Surat Penerimaan : <a href="{{ route('download_berkas', $mhskp['surat']) }}">Download</a> |
                    <a href="{{ route('view_file', array('surat', $mhskp['nim'])) }}" target="_blank">Lihat</a>
                </p>
            </div>
        </div>
        <div class="kp-status">
            <h3 class="title">status berkas : {{ ($mhskp['status_judul']) ? $mhskp['status_judul'] : 'Menunggu Keputusan Komisi' }}</h3>
        </div>
        @if(!$mhskp['status_judul'])
            {!! Form::open(['method'=>'post', 'action'=>['ModalController@setKelayakan', $mhskp['nim_id'] ]]) !!}
            <div class="field">
                <label for="aksi" class="ion-flag"></label>
                <div class="styled-select">
                    {!! Form::select('aksi', ['layak'   => 'Layak Kerja Praktik', 'revisi'   => 'Revisi Konten', 'tolak'=>'Tolak Usulan'], null, ['placeholder'=>'Pilih Aksi', 'id'=>'aksi']) !!}
                </div>
            </div>
            @if($errors->first('aksi'))
                <div class="field noborder error">
                    {{ $errors->first('aksi') }}
                </div>
            @endif
            <div class="dosbing-wrapper">
            <div class="field">
                <label for="dosbing1" class="ion-person"></label>
                <div class="styled-select">
                    {!! Form::select('dosbing1', [] + $dosbing, null, ['placeholder'=>'Pilih Dosen Pembimbing Utama']) !!}
                </div>
            </div>
            @if($errors->first('dosbing1'))
                <div class="field noborder error">
                    {{ $errors->first('dosbing1') }}
                </div>
            @endif
            <div class="field">
                <label for="dosbing2" class="ion-person"></label>
                <div class="styled-select">
                    {!! Form::select('dosbing2', [] + $dosbing, null, ['placeholder'=>'Pilih Dosen Pembimbing KP Anggota']) !!}
                </div>
            </div>
            @if($errors->first('dosbing2'))
                <div class="field noborder error">
                    {{ $errors->first('dosbing2') }}
                </div>
            @endif
            </div>
            <div class="revisi-wrapper">
            <div class="field textarea">
                <label for="revisi" class="ion-ios-lightbulb"></label>
                {!! Form::textarea('revisi', null, ['id'=>'revisi','placeholder'=>'Diisi ketika status revisi/tolak' ]) !!}
            </div>
            </div>
            <div class="field noborder center">
                {!! Form::submit('Simpan', ['class'=>'button']) !!}
            </div>
            {!! Form::close() !!}
        @endif
    </div>
@endif
<script>
    $(document).ready(function() {
        $('.dosbing-wrapper').hide();
        $('.revisi-wrapper').hide();

        $('#aksi').change(function(){
            var status 	= $('#aksi').val();
            if(status == 'layak'){
                $('.dosbing-wrapper').fadeIn('normal');
                $('.revisi-wrapper').hide();
            }else{
                $('.revisi-wrapper').fadeIn('normal');
                $('.dosbing-wrapper').hide();
            }
        });
    });
</script>