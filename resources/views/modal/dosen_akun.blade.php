@if($dosen)
    <div class="tag">
        <div class="tag-content">Detail - {{ $dosen->nama }}</div>
    </div>
    <div class="modal-content">
        {!! Form::open(['method'=>'post ', 'action'=>['ModalController@setDosenAkun', $dosen->nip]]) !!}
        <div class="field">
            <label for="level" class="ion-person"></label>
            <div class="styled-select">
                {!! Form::select('level', ['dosen'=>'Dosen', 'komisi'=>'Komisi'], $user->status) !!}
            </div>
        </div>
        <div class="field">
            <label for="akun" class="ion-power"></label>
            <div class="styled-select">
                {!! Form::select('akun', ['aktif'=>'Aktif', 'deaktif'=>'Deaktif'], $user->is_active) !!}
            </div>
        </div>
        <div class="field noborder center">
            {{ Form::submit('Simpan', ['class'=>'button']) }}
        </div>
    </div>
@endif