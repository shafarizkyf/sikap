<div class="tag">
    <div class="tag-content">Koreksi Berkas</div>
</div>
<div class="modal-content">
    <div class="kp-status">
        <p class="desc">{{ $mhskp->revisi }}</p>
    </div>
    {!! Form::open(['method'=>'post ', 'action'=>['ModalController@simpanKoreksiBerkasKomisi', $mhskp->nim_id], 'files'=>true]) !!}
    <div class="field">
        <label for="outline" class="filefield">
            <span class="ion-paperclip icon"></span>
            Upload Outline Proposal (.pdf)
        </label>
        {!! Form::file('outline', ['id'=>'outline']) !!}
    </div>
    @if($errors->first('outline'))
        <div class="field noborder error">
            {{ $errors->first('outline') }}
        </div>
    @endif
    <div class="field">
        <label for="surat" class="filefield">
            <span class="ion-paperclip icon"></span>
            Upload Surat Penerimaan Lokasi KP (.pdf)
        </label>
        {!! Form::file('surat', ['id'=>'surat']) !!}
    </div>
    @if($errors->first('surat'))
        <div class="field noborder error">
            {{ $errors->first('surat') }}
        </div>
    @endif
    <div class="field noborder center">
        {!! Form::submit('Koreksi', ['class'=>'button']) !!}
    </div>
    {!! Form::close() !!}
</div>