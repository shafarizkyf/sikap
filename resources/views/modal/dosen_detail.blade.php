@if($dosen)
    <div class="tag">
        <div class="tag-content">Detail - {{ $dosen->nama }}</div>
    </div>
    <div class="modal-content">
        {!! Form::model($dosen, ['method'=>'post ', 'action'=>['ModalController@setDosenDetail', $dosen->id]]) !!}
        <div class="field">
            {!! Form::label('nama', null, ['class' => 'ion-person']) !!}
            {!! Form::text('nama', null, ['placeholder' => 'Nama Dosen']) !!}
        </div>
        <div class="field">
            {!! Form::label('nip', null, ['class' => 'ion-card']) !!}
            {!! Form::text('nip', null, ['placeholder' => 'NIP']) !!}
        </div>
        <div class="field">
            {!! Form::label('nidn', null, ['class' => 'ion-card']) !!}
            {!! Form::text('nidn', null, ['placeholder' => 'NIDN']) !!}
        </div>
        <div class="field">
            <label for="jurusan" class="ion-ios-flag"></label>
            <div class="styled-select">
                {!! Form::select('jurusan', [] + $jurusan, $dosen->jurusan->id) !!}
            </div>
        </div>
        <div class="field noborder center">
            {{ Form::submit('Simpan', ['class'=>'button']) }}
        </div>
    </div>
@endif