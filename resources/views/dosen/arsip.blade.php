@extends('layouts.footer')

@section('bar-username')
    {{ $dosen->nama }}
@endsection

@section('bar-page_title')
    mahasiswa lulus kp
@endsection

@section('bar-page_subtitle')
    mahasiswa {{ $dosen->jurusan->jurusan }} lulus kp
@endsection

@section('sidebar-link_arsip')
    active
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Mahasiswa lulus kp
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Judul</td>
                <td>Tanggal Dinilai</td>
                <td>Nilai</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @foreach($mhs as $o)
                <tr>
                    <td class="fit">{{ $no++ }}</td>
                    <td class="fit">{{ $o->mhs->nama }}</td>
                    <td class="fit">{{ $o->mhs->nim }}</td>
                    <td>{{ $o->mhs->kp->judul_final }}</td>
                    <td>{{ $o->created_at->format('d M Y') }}</td>
                    <td>{{ $o->nilai_huruf }}</td>
                    <td class="fit">
                        <div class="buttonwrapper">
                            <a href="#ex1" aria-nim="{{ $o->nim_id }}" rel="modal:open" class="button small">
                                <span class="ion-eye icon"></span>
                                Detail
                            </a>
                            <a href="{{ route('cetak_spk', $o->mhs->nim) }}" class="button small" target="_blank">
                                <span class="ion-ios-paper icon"></span>
                                SPK
                            </a>
                            <a href="{{ route('cetak_kartu_kendali', $o->mhs->nim) }}" class="button small" target="_blank">
                                <span class="ion-android-people icon"></span>
                                Kartu Kendali
                            </a>                                
                        </div>
                    </td>                    
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.dosen.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.button').click(function () {
                var nim= this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/kelayakan/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')