@extends('layouts.footer')

@section('bar-username')
    {{ $dosen->nama }}
@endsection

@section('bar-page_title')
    mahasiswa bimbingan akademik
@endsection

@section('bar-page_subtitle')
    informasi mahasiswa dalam bimbingan akademik
@endsection

@section('sidebar-link_mhsa')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('dosen_menu', 'mhsa') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('dosen_menu', 'mhsakp') }}">dalam proses kp</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('dosen_menu', 'layakkp') }}">layak kp</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Daftar Mahasiswa
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td class="fit">IPK</td>
                <td class="fit">SKS</td>
                <td class="fit">Prasyarat KP</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td class="fit">{{ $m->ipk }}</td>
                        <td class="fit">{{ $m->sks }}</td>
                        <td class="fit">{{ $m->status_prasyarat }}</td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.dosen.sidebar_content')

@extends('layouts.header')