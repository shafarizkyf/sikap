<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="description" content="Example code">
<meta name="author" content="Aji Sulistyo Nugroho">
<link rel="stylesheet/less" type="text/css" href="{{ asset('less/style.less') }}" />
<script src="{{ asset('js/less/less.min.js') }}"></script>
<title>Sikap | Error 404</title>
</head>
<body>
<div class="content">
    <div class="panel">
        <div class="tag">
            <div class="tag-content">
                <span class="ion-settings"></span> OOOPS
            </div>
        </div>
        <div class="login-header">
            <div class="text">
                <span class="ion-sad-outline icon"></span>

                <br>
                ERROR CODE 404
                <p class="secondary">

                    Sepertinya anda tersesat ?
                </p>
            </div>
        </div>
        <div class="field noborder center">
            <div class="button-wrapper">
                <a href="/" class="button">
                <span class="ion-arrow-left-a"></span> Kembali</a>
            </div>
        </div>
        <div class="footer"></div>
    </div>
</div>
</body>
</html>