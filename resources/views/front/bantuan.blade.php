@extends('layouts.footer')

@section('bar-username')
    @if(!$user)
        Sistem
    @else
        {{ ($user->nama) ? $user->nama : $user->username }}
    @endif
@endsection


@section('login_username')
    @if(!$username)
        login
    @else
        {{ $username }}
    @endif
@endsection

@section('bar-page_title')
    Bantuan
@endsection

@section('bar-page_subtitle')
    pertanyaan yang sering diajukan
@endsection

@section('sidebar-link_bantuan')
    active
@endsection

@section('bar-content')
    <div class="content tableinside">
        <table>
            <thead>
            <tr>
                <td>Pertanyaan</td>
                <td>Jawaban</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur eveniet laborum, dolor vitae aspernatur nihil recusandae, quod necessitatibus suscipit nulla velit earum amet repudiandae nostrum ullam. Velit enim, commodi. Amet.</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, fugit tenetur earum necessitatibus! Provident ipsa error voluptatum non pariatur earum blanditiis voluptates, dolores, fugit quaerat maxime amet laudantium doloremque. Eaque.</td>
            </tr>
            <tr>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur eveniet laborum, dolor vitae aspernatur nihil recusandae, quod necessitatibus suscipit nulla velit earum amet repudiandae nostrum ullam. Velit enim, commodi. Amet.</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, fugit tenetur earum necessitatibus! Provident ipsa error voluptatum non pariatur earum blanditiis voluptates, dolores, fugit quaerat maxime amet laudantium doloremque. Eaque.</td>
            </tr>
            <tr>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur eveniet laborum, dolor vitae aspernatur nihil recusandae, quod necessitatibus suscipit nulla velit earum amet repudiandae nostrum ullam. Velit enim, commodi. Amet.</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, fugit tenetur earum necessitatibus! Provident ipsa error voluptatum non pariatur earum blanditiis voluptates, dolores, fugit quaerat maxime amet laudantium doloremque. Eaque.</td>
            </tr>
            <tr>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur eveniet laborum, dolor vitae aspernatur nihil recusandae, quod necessitatibus suscipit nulla velit earum amet repudiandae nostrum ullam. Velit enim, commodi. Amet.</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, fugit tenetur earum necessitatibus! Provident ipsa error voluptatum non pariatur earum blanditiis voluptates, dolores, fugit quaerat maxime amet laudantium doloremque. Eaque.</td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7"></td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.front.sidebar_content')

@extends('layouts.header')