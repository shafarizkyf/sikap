@extends('layouts.footer')

@section('bar-username')
    @if(!$user)
        Sistem
    @else
        {{ ($user->nama) ? $user->nama : $user->username }}
    @endif
@endsection


@section('login_username')
    @if(!$username)
        login
    @else
        {{ $username }}
    @endif
@endsection

@section('bar-page_title')
    Judul Kerja Praktik
@endsection

@section('sidebar-link_judul')
    active
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Judul kerja praktik
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                    {!! Form::open(['method'=>'get', 'action'=>'HomeController@judul']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'Cari Sesuatu ...']) !!}
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>            
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Instansi Tujuan</td>
                <td>Judul</td>
            </tr>
            </thead>
            <tbody>
            @if($mhskp)
                <?php $no = 1; ?>
                @foreach($mhskp as $o)
                <tr>
                    <td class="fit">{{ $no++ }}</td>
                    <td>{{ $o->nim->nama }}</td>
                    <td class="fit">{{ $o->nim->nim }}</td>
                    <td>{{ $o->tempat->instansi }}</td>
                    <td>{{ $o->judul }}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhskp->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.front.sidebar_content')

@extends('layouts.header')