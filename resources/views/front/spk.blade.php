@extends('layouts.footer')

@section('bar-username')
    @if(!$user)
        Sistem
    @else
        {{ ($user->nama) ? $user->nama : $user->username }}
    @endif
@endsection


@section('login_username')
    @if(!$username)
        login
    @else
        {{ $username }}
    @endif
@endsection

@section('bar-page_title')
    pengambilan berkas SPK
@endsection

@section('bar-page_subtitle')
    berkas spk mahasiswa berikut sudah dapat diambil di bapendik
@endsection

@section('sidebar-link_surat')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('home_surat') }}">Surat Pengantar</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('home_spk') }}">SPK</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Masa SPK</td>
            </tr>
            </thead>
            <tbody>
            @if($spk)
                <?php $no = 1; ?>
                @foreach($spk as $s)
                    <tr>
                        <td class="fit">{{ $no++ }}</td>
                        <td>{{ $s->nim->nama }}</td>
                        <td class="fit">{{ $s->nim->nim }}</td>
                        <td class="fit">{{ $s->nim->jurusan->jurusan }}</td>
                        <td class="fit">{{ $s->waktu_spk_mulai->format('d M Y') }} s.d {{ $s->waktu_spk_selesai->format('d M Y') }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $spk->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.front.sidebar_content')

@extends('layouts.header')