@extends('layouts.footer')

@section('bar-username')
    @if(!$user)
        Sistem
    @else
        {{ ($user->nama) ? $user->nama : $user->username }}
    @endif
@endsection


@section('login_username')
    @if(!$username)
        login
    @else
        {{ $username }}
    @endif
@endsection

@section('bar-page_title')
    pengambilan surat pengantar kp
@endsection

@section('bar-page_subtitle')
    waktu pengambilan surat pengantar kp
@endsection

@section('sidebar-link_surat')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('home_surat') }}">Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('home_spk') }}">SPK</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Instansi Tujuan</td>
                <td class="fit">Pengajuan</td>
                <td>Pengambilan</td>
            </tr>
            </thead>
            <tbody>
            @if($surat)
                <?php $no = 1; ?>
                @foreach($surat as $s)
                <?php 
                    $selesai = "Proses";
                    if($s->waktu_selesai){
                        $selesai = ($s->waktu_selesai == Carbon\Carbon::now() ? 'Proses' : $s->waktu_selesai->format('d F Y'));
                    }
                ?>
                <tr>
                    <td class="fit">{{ $no++ }}</td>
                    <td>{{ $s->nim->nama }}</td>
                    <td class="fit">{{ $s->nim->nim }}</td>
                    <td>{{ $s->instansi }}</td>
                    <td class="fit">{{ $s->waktu_ajukan->format('d F Y') }}</td>
                    <td class="fit">{{ $selesai }}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $surat->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.front.sidebar_content')

@extends('layouts.header')