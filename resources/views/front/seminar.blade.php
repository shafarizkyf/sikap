@extends('layouts.footer')

@section('bar-username')
    @if(!$user)
        Sistem
    @else
        {{ ($user->nama) ? $user->nama : $user->username }}
    @endif
@endsection

@section('login_username')
    @if(!$username)
        login
    @else
        {{ $username }}
    @endif
@endsection

@section('bar-page_title')
    jadwal seminar
@endsection

@section('bar-page_subtitle')
    jadwal seminar mahasiswa teknik
@endsection

@section('sidebar-link_seminar')
    active
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table class="seminar">
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    Jadwal Seminar
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="11">
                    {!! Form::open(['method'=>'get', 'action'=>['HomeController@seminar']]) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('ruangan', []+$ruangan, $listRuangan) !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="center">Tanggal</td>
                <td class="center">SHIFT 1<span>(JAM 7-8)</span></td>
                <td class="center">SHIFT 2<span>(JAM 8-9)</span></td>
                <td class="center">SHIFT 3<span>(JAM 9-10)</span></td>
                <td class="center">SHIFT 4<span>(JAM 10-11)</span></td>
                <td class="center">SHIFT 5<span>(JAM 11-12)</span></td>
                <td class="center">SHIFT 6<span>(JAM 12-13)</span></td>
                <td class="center">SHIFT 7<span>(JAM 13-14)</span></td>
                <td class="center">SHIFT 8<span>(JAM 14-15)</span></td>
                <td class="center">SHIFT 9<span>(JAM 15-16)</span></td>
                <td class="center">SHIFT 10<span>(JAM 16-17)</span></td>
            </tr>
            </thead>
            <tbody>
            @for(; $today <= $week; $today->addDay())
                <tr>
                    @if($today->isSunday())
                        @continue
                    @endif

                    @for($i=0; $i<=10; $i++)
                        <?php
                            $sc         = new \App\Http\Controllers\SeminarController();
                            $shift      = $sc->shift($i);
                            $j          = $today->format('Y-m-d').' '.$shift;
                            $jadwal     = \App\MhsSeminar::where('jadwal', $j)->where('ruangan_id', $listRuangan)->first();

                            if($jadwal){
                                $mhsj   = \App\ListMahasiswa::find($jadwal->nim_id);
                                $nama   = new \App\Http\Controllers\HomeController();
                                $nama  = $nama->trimString($jadwal->nim->nama, 8);
                            }
                        ?>

                        @if($i==0)
                            <td class="center caption">
                                {{ $today->format('l') }}
                                <span class="big">{{ $today->format('d') }}</span>
                                {{ $today->format('F Y') }}
                            </td>
                        @endif

                        @if($i != 0 and $jadwal)
                            <td class="center">
                                @if(!$jadwal->status)
                                    <span class="mark ion-help-circled"></span>
                                @else
                                    <span class="mark ion-checkmark-circled "></span>
                                @endif
                                {{ strtoupper($jadwal->jenis) }}<br>
                                {{ $jadwal->nim->nim }}<br>
                                {{ $nama }}
                                <span>{{ $jadwal->nim->jurusan->jurusan }}</span>
                            </td>

                        @elseif($i != 0 and !$jadwal)
                            <td class="center"></td>
                        @endif

                    @endfor
                </tr>
            @endfor
            </tbody>
        </table>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.front.sidebar_content')

@extends('layouts.header')