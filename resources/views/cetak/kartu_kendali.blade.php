<!DOCTYPE html>
<html>
<head>
    <title>SPK Kerja Praktik</title>
    <link rel="stylesheet" media="screen" href="{{ asset('css/spkv2.css') }}">
    <link rel="stylesheet" media="print" href="{{ asset('css/spkv2.css') }}">
</head>
<body>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase sans size11">Kementerian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase sans size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase sans size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="sans size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="sans size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="sans size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="font uppercase bold center">
        Kartu kendali Bimbingan</p>
    <p class="font uppercase bold center nomargin-left">kerja praktik</p>
    <table class="nomargin-left">
        <tr>
            <td class="font">Nama</td>
            <td class="colon">:</td>
            <td class="font ">{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td class="font">NIM</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td class="font">Jurusan / Program Studi</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td class="font">Judul Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->judul_final }}</td>
        </tr>
        <tr>
            <td class="font">No SPK</td>
            <td class="colon">:</td>
            <td class="font">{{ $spk->nomor }}</td>
        </tr>
        <tr>
            <td class="font">Terhitung sejak</td>
            <td class="colon">:</td>
            <td class="font">{{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_selesai->format('d-m-Y') }}</td>
        </tr>
        <tr>
            <td class="font">Tempat Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->tempat->instansi }} {{ $mhskp->tempat->dimana }}</td>
        </tr>
        <tr>
            <td class="font">Dosen Pembimbing I</td>
            <td class="colon">:</td>
            <td class="font size11">{{ $dosbing->dosbing1->nama }}</td>
        </tr>
        @if($dosbing->dosbing2)
        <tr>
            <td class="font">Dosen Pembimbing II</td>
            <td class="colon">:</td>
            <td class="font size11">{{ $dosbing->dosbing2->nama }}</td>
        </tr>
        @endif
    </table>
    <table border="1" cellspacing="0" class="kendali nomargin-left">
        <tr class="kendali-head">
            <td>No</td>
            <td class="two-line">Tanggal Kehadiran</td>
            <td>Uraian</td>
            <td class="two-line">Tanda Tangan</td>
        </tr>
        @if($bimbingan)
            <?php $x=1 ?>
            @foreach($bimbingan as $b)
                <tr>
                    <td>{{ $x++ }}</td>
                    <td class="two-line">{{ $b->waktu_bimbingan->format('d F Y') }}</td>
                    <td style="text-align:justify;">{{ $b->bimbingan }}</td>
                    <td class="two-line"></td>
                </tr>
            @endforeach
        @endif
    </table>
    <table class="ttd">
        <tr>
            <td>........................, ................................ </td>
        </tr>
        <tr>
            <td>Dosen Pembimbing</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ $dosbing->dosbing1->nama }}</td>
        </tr>
    </table>
    <p class="tembusan">*) Kartu kendali dapat diperbanyak sendiri</p>
</div>

</body>
</html>