<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="print" href="{{ asset('css/seminar.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('css/seminar.css') }}">
    <title>Seminar Kerja Praktik</title>
</head>
<body>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">Kementrian Riset teknologi dan pendidikan tinggi</td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
            <td rowspan="3" class="bold size14" style="border:1px solid black;padding:0.2cm">FS-KP13</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
            <td></td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <table class="no-surat">
        <tr>
            <td>Nomor</td>
            <td>:</td>
            <td class="bold">{{ $seminar->nomor }}</td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>:</td>
            <td>3 (tiga) lembar</td>
        </tr>
        <tr>
            <td>Perihal</td>
            <td>:</td>
            <td>Seminar/Ujian Kerja Praktik</td>
        </tr>
    </table>
    <br>
    <br>
    <p class="justify">
        Yth. Bapak/Ibu<br>
        <b class="bold">{{ $dosbing->dosbing1->nama }}</b><br>
        ditempat<br><br>
        Berdasarkan verifikasi dari Administrasi Bidang Pendidikan Fakultas Teknik Unsoed maka mahasiswa berikut ini :
        <br>
    </p>
    <table class="table-inside">
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td>:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">Telah memenuhi syarat dengan menunjukkan berkas-berkas berikut ini :</td>
        </tr>
        <tr>
            <td colspan="3">1. Surat Perintah Kerja Praktik (Asli)</td>
        </tr>
        <tr>
            <td colspan="3">2. Pengantar telah selesai bimbingan Kerja Praktik dari Pembimbing </td>
        </tr>
        <tr>
            <td colspan="3">3. Lembar Asistensi dari Dosen Pembimbing  </td>
        </tr>
        <tr>
            <td colspan="3">4. Surat Selesai Kerja Praktik dari Perusahaan/proyek </td>
        </tr>
        <tr>
            <td colspan="3">5. Satu eksemplar laporan Kerja Praktik (belum dijilid)</td>
        </tr>
    </table>
    <br>
    <p class="justify">
        Sehingga yang bersangkutan layak untuk melakukan Seminar/Ujian Kerja Praktik dalam rangka mendapatkan nilai mata kuliah Kerja Praktik tahun ajaran {{ $mhs->tahun_ajaran }}.<br>
        Mahasiswa tersebut harus telah seminar/Ujian Kerja Praktik paling lambat Hari/tanggal {{ $seminar->jadwal->format('l d F Y') }} Jam {{ $seminar->jadwal->format('H') }}.00 s/d selesai Wib di {{ $seminar->ruangan->ruangan }} Jurusan Teknik FT dan menyerahkan Laporan Kerja Praktik (telah disahkan dan dijilid) rangkap 3 (tiga) yaitu untuk  :<br>
    </p>
    <table class="table-inside">
        <tr>
            <td>1. Dosen Pembimbing I</td>
            <td>(1 eksemplar)</td>
        </tr>
        <tr>
            <td>2. Dosen Pembimbing II </td>
            <td>(1 eksemplar)</td>
        </tr>
        <tr>
            <td>3. Pusat Informasi Ilmiah  Fakultas Teknik</td>
            <td>(1 eksemplar, khusus Teknik Industri)</td>
        </tr>
        <tr>
            <td>4. Yang bersangkutan ( mahasiswa )</td>
            <td>(1 eksemplar)</td>
        </tr>
    </table>
    <table class="ttd">
        <tr>
            <td>Purbalingga, {{ $seminar->created_at->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Ketua Jurusan {{ ($kajur) ? $kajur->dosen->jurusan->jurusan : '(belum di set)' }}</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ ($kajur) ? $kajur->dosen->nama : '(belum di set)' }}</td>
        </tr>
        <tr>
            <td>NIP.{{ ($kajur) ? $kajur->dosen->nip : '(belum di set)' }}</td>
        </tr>
    </table>
    <p class="tembusan">[Rangkap 3 : Dosen Pembimbing I, Pusat Informasi Ilmiah FT, Mahasiswa]</p>
</div>
@if($dosbing->dosbing2)
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">Kementrian Riset teknologi dan pendidikan tinggi</td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
            <td rowspan="3" class="bold size14" style="border:1px solid black;padding:0.2cm">FS-KP13</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
            <td></td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <table class="no-surat">
        <tr>
            <td>Nomor</td>
            <td>:</td>
            <td class="bold">{{ $seminar->nomor }}</td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>:</td>
            <td>3 (tiga) lembar</td>
        </tr>
        <tr>
            <td>Perihal</td>
            <td>:</td>
            <td>Seminar/Ujian Kerja Praktik</td>
        </tr>
    </table>
    <br>
    <br>
    <p class="justify">
        Yth. Bapak/Ibu<br>
        <b class="bold">{{ $dosbing->dosbing2->nama }}</b><br>
        ditempat<br><br>
        Berdasarkan verifikasi dari Administrasi Bidang Pendidikan Fakultas Teknik Unsoed maka mahasiswa berikut ini :
        <br>
    </p>
    <table class="table-inside">
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td>:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">Telah memenuhi syarat dengan menunjukkan berkas-berkas berikut ini :</td>
        </tr>
        <tr>
            <td colspan="3">1. Surat Perintah Kerja Praktik (Asli)</td>
        </tr>
        <tr>
            <td colspan="3">2. Pengantar telah selesai bimbingan Kerja Praktik dari Pembimbing </td>
        </tr>
        <tr>
            <td colspan="3">3. Lembar Asistensi dari Dosen Pembimbing  </td>
        </tr>
        <tr>
            <td colspan="3">4. Surat Selesai Kerja Praktik dari Perusahaan/proyek </td>
        </tr>
        <tr>
            <td colspan="3">5. Satu eksemplar laporan Kerja Praktik (belum dijilid)</td>
        </tr>
    </table>
    <br>
    <p class="justify">
        Sehingga yang bersangkutan layak untuk melakukan Seminar/Ujian Kerja Praktik dalam rangka mendapatkan nilai mata kuliah Kerja Praktik tahun ajaran {{ $mhs->tahun_ajaran }}.<br>
        Mahasiswa tersebut harus telah seminar/Ujian Kerja Praktik paling lambat Hari/tanggal {{ $seminar->jadwal->format('l d F Y') }} Jam {{ $seminar->jadwal->format('H') }}.00 s/d selesai Wib di {{ $seminar->ruangan->ruangan }} Jurusan Teknik FT dan menyerahkan Laporan Kerja Praktik (telah disahkan dan dijilid) rangkap 3 (tiga) yaitu untuk  :<br>
    </p>
    <table class="table-inside">
        <tr>
            <td>1. Dosen Pembimbing I</td>
            <td>(1 eksemplar)</td>
        </tr>
        <tr>
            <td>2. Dosen Pembimbing II </td>
            <td>(1 eksemplar)</td>
        </tr>
        <tr>
            <td>3. Pusat Informasi Ilmiah  Fakultas Teknik</td>
            <td>(1 eksemplar, khusus Teknik Industri)</td>
        </tr>
        <tr>
            <td>4. Yang bersangkutan ( mahasiswa )</td>
            <td>(1 eksemplar)</td>
        </tr>
    </table>
    <table class="ttd">
        <tr>
            <td>Purbalingga, {{ $seminar->created_at->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Ketua Jurusan {{ ($kajur) ? $kajur->dosen->jurusan->jurusan : '(belum di set)' }}</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ ($kajur) ? $kajur->dosen->nama : '(belum di set)' }}</td>
        </tr>
        <tr>
            <td>NIP.{{ ($kajur) ? $kajur->dosen->nip : '(belum di set)' }}</td>
        </tr>
    </table>
    <p class="tembusan">[Rangkap 3 : Dosen Pembimbing I, Pusat Informasi Ilmiah FT, Mahasiswa]</p>
</div>
@endif
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">Kementrian Riset teknologi dan pendidikan tinggi</td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
            <td rowspan="3" class="bold size14" style="border:1px solid black;padding:0.2cm">FS-KP13L</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
            <td></td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center">
        <br>
        <b>LAMPIRAN BERITA ACARA SEMINAR/UJIAN KP</b>
        <br>
        <br>
        Tentang
        <br>
        <br>
        {{ ($mhskp->judul_final) ? $mhskp->judul_final : '(belum ada judul final)' }}
        <br>
        <br>
    </p>
    <p>Untuk memenuhi persyaratan mendapatkan nilai matakuliah Kerja Praktik,</p><br>
    <table class="table-inside">
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td>:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
    </table>
    <br>
    <p>Diharuskan untuk memperbaiki Laporan Kerja Praktik : (*)</p>
    <table class="checkbox" cellspacing="2">
        <tr>
            <td></td>
            <td>Pembimbing Utama</td>
            <td>:</td>
            <td>{{ $dosbing->dosbing1->nama }}</td>
        </tr>
        @if($dosbing->dosbing2)
        <tr>
            <td></td>
            <td>Pembimbing 2</td>
            <td>:</td>
            <td>{{ $dosbing->dosbing2->nama }}</td>
        </tr>
        @endif
        <tr>
            <td></td>
            <td>Pembimbing Lapangan</td>
            <td>:</td>
            <td>........................................</td>
        </tr>
    </table>
    <br>
    <p>Dan perbaikan harus dianggap sah setelah disetujui oleh  : (*)</p>
    <table class="checkbox" cellspacing="2">
        <tr>
            <td></td>
            <td>Pembimbing Utama</td>
            <td>:</td>
            <td>{{ $dosbing->dosbing1->nama }}</td>
        </tr>
        @if($dosbing->dosbing2)
        <tr>
            <td></td>
            <td>Pembimbing 2</td>
            <td>:</td>
            <td>{{ $dosbing->dosbing2->nama }}</td>
        </tr>
        @endif
        <tr>
            <td></td>
            <td>Pembimbing Lapangan</td>
            <td>:</td>
            <td>........................................</td>
        </tr>
    </table>
    <br>
    <p>Hal-hal yang harus diperhatikan dalam perbaikan Laporan Kerja Praktik adalah : (**)</p>
    <table class="dots">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br>
    <table class="ttd-berita-acara">
        <tr>
            <td>Pembimbing I</td>
            <td>: {{ $dosbing->dosbing1->nama }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td>: {{ $dosbing->dosbing1->nip }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        @if($dosbing->dosbing2)
        <tr>
            <td>Pembimbing II</td>
            <td>: {{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nama : null }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td>: {{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nip : null }}</td>
        </tr>
        @endif
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Mahasiswa</td>
            <td>: {{ $mhskp->nim->nama }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>: {{ $mhskp->nim->nim }}</td>
        </tr>
    </table>
    <p class="tembusan">(*)   Beri tanda pada kotak sesuai.<br>
        (**) Jika diperlukan, dapat diberi tambahan halaman.
    </p>
</div>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">Kementrian Riset teknologi dan pendidikan tinggi</td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
            <td rowspan="3" class="bold size14" style="border:1px solid black;padding:0.2cm">FS-KP14</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
            <td></td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center">
        <br>
        <b>LAMPIRAN DAFTAR HADIR SEMINAR KP</b>
        <br>
        <br>
        Tentang
        <br>
        <br>
        {{ ($mhskp->judul_final) ? $mhskp->judul_final : '(belum ada judul final)' }}
        <br>
        <br>
    </p>
    <p>
        Telah dilakukan seminar Kerja Praktik atas nama mahasiswa sebagai berikut,
        <br>
    </p>
    <table class="mhs-daftarhadir">
        <tr class="v-top">
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td class="v-top">:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr class="v-top">
            <td>Hari/Tanggal Seminar</td>
            <td>:</td>
            <td>{{ $seminar->jadwal->format('l') }}, {{ $seminar->jadwal->format('d F Y') }} Jam {{ $seminar->jadwal->format('H') }}.00 WIB s/d Selesai<br>
                di {{ $seminar->ruangan->ruangan }} Fakultas Teknik
            </td>
        </tr>
    </table>
    <p>Dengan perincian peserta sebagai berikut :<br></p>
    <table class="daftarhadir" border="1" cellspacing="0">
        <tr>
            <td>No</td>
            <td>NIM</td>
            <td>Nama</td>
            <td>Tanda tangan</td>
        </tr>
        <tr>
            <td>1</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>2</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>3</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>4</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>5</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>6</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>7</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>8</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>9</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>10</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br>
    <p>
        Ditetapkan di Purbalingga, <br>
        {{ $seminar->jadwal->format('l') }}, {{ $seminar->jadwal->format('d F Y') }} Jam {{ $seminar->jadwal->format('H') }}.00 WIB s/d Selesai <br>
        di {{ $seminar->ruangan->ruangan }} Fakultas Teknik <br>
    </p>
    <table class="ttd-berita-acara">
        <tr>
            <td>Pembimbing I</td>
            <td>: {{ $dosbing->dosbing1->nama }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td>: {{ $dosbing->dosbing1->nip }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        @if($dosbing->dosbing2)
        <tr>
            <td>Pembimbing II</td>
            <td>: {{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nama : null }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td>: {{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nip : null }}</td>
        </tr>
        @endif
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <p class="tembusan">[Rangkap 3:dosen pembimbing, ketua komisi, bapendik]
    </p>
</div>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">Kementrian Riset teknologi dan pendidikan tinggi</td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
            <td rowspan="3" class="bold size14" style="border:1px solid black;padding:0.2cm">FS-KP15<br><p class="size7">Dosen Pembimbing</p></td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
            <td></td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center">
        <br>
        <b>LAMPIRAN PENILAIAN SEMINAR DAN UJIAN KP</b>
        <br>
        <br>
        Tentang
        <br>
        <br>
        {{ ($mhskp->judul_final) ? $mhskp->judul_final : '(belum ada judul final)' }}
        <br>
        <br>
    </p>
    <p>
        Telah dilakukan seminar Kerja Praktik atas nama mahasiswa sebagai berikut,
        <br>
    </p>
    <table class="mhs-daftarhadir">
        <tr class="v-top">
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td class="v-top">:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr class="v-top">
            <td>Hari/Tanggal Seminar</td>
            <td>:</td>
            <td>{{ $seminar->jadwal->format('l') }}, {{ $seminar->jadwal->format('d F Y') }} Jam {{ $seminar->jadwal->format('H') }}.00 WIB s/d Selesai <br>
                di {{ $seminar->ruangan->ruangan }} Fakultas Teknik
            </td>
        </tr>
    </table>
    <p>Dengan perincian sebagai berikut :<br></p>
    <table class="penilaian" border="1" cellspacing="0">
        <tr>
            <td>No</td>
            <td>Komponen Penilaian</td>
            <td colspan="5">Nilai</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Sistematika penulisan laporan (0-100)</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Tatabahasa, penulisan laporan (0-100);</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Sistematika penjelasan materi seminar (0-100)</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Kecocokan isi laporan dengan materi kerja praktik (0-100)</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Materi kerja  praktik (0-100)</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Penguasaan terhadap permasalahan KP</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>7</td>
            <td>Diskusi</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>8</td>
            <td>Ujian</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="total">
            <td>&nbsp;</td>
            <td>TOTAL NILAI (Dosen Pembimbing) 70 % </td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="total">
            <td>&nbsp;</td>
            <td>TOTAL NILAI (Pembimbing Teknis Lapangan) 30 %</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="total">
            <td>&nbsp;</td>
            <td>JUMLAH NILAI</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="total">
            <td>&nbsp;</td>
            <td>HURUF MUTU</td>
            <td colspan="5"></td>
        </tr>

    </table>
    <p>Keterangan</p>
    <table class="keterangan-nilai" border="0" cellspacing="0">
        <td>
            A. 80 &le; Nilai<br>
            AB. 79.99 &le; Nilai &lt; 75<br>
            B. 74.99 &le; Nilai &lt; 70<br>
        </td>
        <td>
            BC. 69.99 &le; Nilai &lt; 65<br>
            C. 64.99 &le; Nilai &lt; 60<br>
            CD. 59.99 &le; Nilai &lt; 56<br>
        </td>
        <td>
            D. 55.99 &le; Nilai &lt; 46<br>
            E. Nilai &lt; 46<br>
        </td>
    </table>
    <p>
        Nilai di atas akan sah sebagai nilai matakuliah jika yang bersangkutan telah melengkapi kekurangannya  yang tercantum dalam "Berita Acara Seminar Kerja Praktik" beserta menyerahkan laporan akhir Kerja Praktik yang telah dijilid dan disahkan.<br>
        Ditetapkan di Purbalingga, <br>
        {{ $seminar->jadwal->format('l') }}, {{ $seminar->jadwal->format('d F Y') }} Jam {{ $seminar->jadwal->format('H') }}.00 WIB s/d Selesai <br>
        di {{ $seminar->ruangan->ruangan }} Fakultas Teknik <br>
    </p>
    <table class="ttd-berita-acara">
        <tr>
            <td>Pembimbing I</td>
            <td>: {{ $dosbing->dosbing1->nama }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td>: {{ $dosbing->dosbing1->nip }}</td>
        </tr>
        <tr>
            <td>&nbsp</td>
        </tr>
        @if($dosbing->dosbing2)
        <tr>
            <td>Pembimbing II</td>
            <td>: {{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nama : null }}</td>
            <td rowspan="2" class="ttd-area">ttd...........................</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td>: {{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nip : null }}</td>
        </tr>
        @endif
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">Kementrian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <table class="no-surat">
        <tr>
            <td>Nomor</td>
            <td>:</td>
            <td class="bold">. . . . .</td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>:</td>
            <td>3 (tiga) lembar</td>
        </tr>
        <tr>
            <td>Perihal</td>
            <td>:</td>
            <td>Seminar/Ujian Kerja Praktik</td>
        </tr>
    </table>
    <br>
    <br>
    <p class="justify">
        Kepada<br>
        Yth.
    </p>
    <table class="table-inside">
        <tr>
            <td>1. Bapak/Ibu <b>{{ $dosbing->dosbing1->nama }}</b></td>
        </tr>
        <tr>
            <td>2. <b>Seluruh Mahasiswa</b> ditempat</td>
        </tr>
    </table>
    <br>
    <br>
    <p>Berdasarkan ajuan dari mahasiswa berikut ini :</p>
    <table class="mhs-undangan">
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td>:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td>Judul KP</td>
            <td>:</td>
            <td>{{ ($mhskp->judul_final) ? $mhskp->judul_final : '(belum ada judul final)'}}</td>
        </tr>
    </table>
    <br>
    <p class="justify">
        Akan melakukan seminar Kerja Praktik pada :
    </p>
    <table class="mhs-undangan">
        <tr>
            <td>Hari/Tanggal</td>
            <td>:</td>
            <td>{{ $seminar->jadwal->format('l') }}, {{ $seminar->jadwal->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Jam</td>
            <td>:</td>
            <td>Jam {{ $seminar->jadwal->format('H') }}.00 WIB s/d Selesai</td>
        </tr>
        <tr>
            <td>Tempat</td>
            <td>:</td>
            <td>{{ $seminar->ruangan->ruangan }}</td>
        </tr>
    </table>
    <p class="justify">
        Demikian surat  undangan seminar kerja praktik, terimakasih atas perhatiannya.
    </p>
    <table class="ttd">
        <tr>
            <td>Purbalingga, {{ $seminar->created_at->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Ketua Jurusan Teknik Informatika</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ ($kajur) ? $kajur->dosen->nama : '(belum di set)'}}</td>
        </tr>
        <tr>
            <td>{{ ($kajur) ? $kajur->dosen->nip : '(belum di set)' }}</td>
        </tr>
    </table>
    <p class="tembusan">Rangkap 4 : ( Dosen Pembimbing, di tempel 1 oleh mhs )</p>
</div>
</body>
</html>