<!DOCTYPE html>
<html moznomarginboxes mozdisallowselectionprint>
<head>
    <link rel="stylesheet" media="print" href="{{ asset('css/spkv2.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('css/spkv2.css') }}">
    <title>Surat Perintah Kerja Praktik</title>
</head>
<body>
<?php $settings = json_decode(file_get_contents(storage_path('settings.json')), true); ?>
<!-- Page 1 -->
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase sans size11">Kementerian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase sans size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase sans size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="sans size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="sans size10">Telepon/Faks. : (0281)6596801</td>
            <td></td>
        </tr>
        <tr>
            <td class="sans size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center">
        <br>
        <b>SURAT PERINTAH KERJA PRAKTIK (SPK)
            <br>
            (FKP-01)
            <br>
            Nomor : {{ $spk->nomor }}
        </b>
        <br>
        <br>
    </p>
    <p>Berdasarkan hasil sidang Komisi Komisi Studi Akhir Fakultas Teknik Universitas Jenderal Soedirman, pada:</p>
    <table>
        <tr>
            <td>Tanggal</td>
            <td class="colon"></td>
            <td>: {{ $spk->created_at->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Tempat</td>
            <td class="colon"></td>
            <td>: Fakultas Teknik Purbalingga</td>
        </tr>
    </table>
    <p class="  left newpar">
        Kerja praktik mahasiswa:
    </p>
    <table>
        <tr>
            <td>Nama</td>
            <td class="colon">:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td class="colon">:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td class="colon">:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td>Topik Kerja Praktik</td>
            <td class="colon">:</td>
            <td>{{ $mhskp->judul }}</td>
        </tr>
        <tr>
            <td>Terhitung sejak</td>
            <td class="colon">:</td>
            <td>
                <!-- {{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_selesai->format('d-m-Y') }} -->
                {{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_mulai->addDays($settings['masa_spk'])->format('d-m-Y') }}
            </tr>
    </table>
    <p class="  left newpar">
        dengan pembimbing :
    </p>
    <table>
        <tr>
            <td>Nama</td>
            <td class="colon">:</td>
            <td>{{ $dosbing->dosbing1->nama }}</td>
        </tr>
        <tr>
            <td>NIP</td>
            <td class="colon">:</td>
            <td>{{ $dosbing->dosbing1->nip }}</td>
        </tr>
    </table>
    <p class="  left newpar">
        Surat Perintah Kerja Praktik (SPK) ini diterbitkan untuk maksud dan fungsi sebagai:
    </p>
    <table>
        <tr>
            <td>1. Dasar mahasiswa melaksanakan kerja praktik,</td>
        </tr>
        <tr>
            <td>2. Penugasan untuk pembimbing,</td>
        </tr>
        <tr>
            <td>3. Dasar penerbitan SK pembimbing kerja praktik.</td>
        </tr>
    </table>
    <table class="ttd">
        <tr>
            <td>Purbalingga, {{ $spk->created_at->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Wakil Dekan Bidang Akademik<br>Fakultas Teknik</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ (App\ListDosen::getwda()) ? App\ListDosen::getwda()->nama : 'belum ada wda' }}</td>
        </tr>
        <tr>
            <td>NIP. {{ (App\ListDosen::getwda()->nama) ? App\ListDosen::getwda()->nip : 'belum ada wda' }}</td>
        </tr>
    </table>
</div>
<!-- Page 2 -->
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase sans size11">Kementerian Riset teknologi dan pendidikan tinggi</td>
            <td></td>
            </td>
        </tr>
        <tr>
            <td class="uppercase sans size11">Universitas Jenderal Soedirman</td>
            <td rowspan="4" class="bold size14" style="border:1px solid black;padding:0.2cm">
                FS-KP15<br>
                <p class="size7">
                    Penilaian<br>
                    Pembimbing<br>
                    Teknis Lapangan</p></td>
        </tr>
        <tr>
            <td class="uppercase sans size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="sans size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="sans size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="sans size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center">
        <br>
        <b>LAMPIRAN PENILAIAN PELAKSANAAN
            <br>
            KERJA PRAKTIK
        </b>
        <br>
        <br>
        <br>
        <br>
        .................................................
        <br>
        <br>
    </p>
    <p>
        Telah dilakukan Kerja Praktik atas nama mahasiswa sebagai berikut,
        <br>
    </p>
    <table class="mhs-daftarhadir">
        <tr class="v-top">
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Jurusan</td>
            <td class="v-top">:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
    </table>
    <p>Dengan perincian sebagai berikut :<br></p>
    <table class="penilaian" border="1" cellspacing="0">
        <tr>
            <td class="center">No</td>
            <td>Komponen Penilaian</td>
            <td colspan="5">Nilai</td>
        </tr>
        <tr>
            <td>1</td>
            <td>Kesesuaian dengan rencana kerja</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>2</td>
            <td>Kehadiran di lokasi Kerja Praktik</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>3</td>
            <td>Kedisiplinan,Sikap,Etika dan Tingkah Laku</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>4</td>
            <td>Keaktifan dan kreatifitas</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>5</td>
            <td>Kecermatan</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr>
            <td>6</td>
            <td>Tanggung jawab</td>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="total">
            <td>&nbsp;</td>
            <td>NILAI ANGKA (TOTAL) / NILAI HURUF</td>
            <td class="bold" colspan="3"></td>
            <td class="bold" colspan="2"></td>
        </tr>

    </table>
    <p>Keterangan Nilai Huruf :</p>
    <table class="keterangan-nilai" border="0" cellspacing="0">
        <td>
            A. 80 &le; Nilai<br>
            AB. 79.99 &le; Nilai &lt; 75<br>
            B. 74.99 &le; Nilai &lt; 70<br>
        </td>
        <td>
            BC. 69.99 &le; Nilai &lt; 65<br>
            C. 64.99 &le; Nilai &lt; 60<br>
            CD. 59.99 &le; Nilai &lt; 56<br>
        </td>
        <td>
            D. 55.99 &le; Nilai &lt; 46<br>
            E. Nilai &lt; 46<br>
        </td>
    </table>
    <p>
        Nilai di atas akan sah sebagai nilai matakuliah jika yang bersangkutan telah melengkapi kekurangannya  yang tercantum dalam "Berita Acara Seminar Kerja Praktik" beserta menyerahkan laporan akhir Kerja Praktik yang telah dijilid dan disahkan.<br>
        Ditetapkan di ................................<strong>,</strong> ...........................................
{{--         {{ $spk->created_at->format('l, d F Y') }} Jam {{ $spk->created_at->format('H:i') }} WIB s/d Selesai --}}
    </p>
    <table class="ttd-berita-acara">
        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td>
                Nama <br>
                Pembimbing Teknis<br>
                Lapangan:<br>
            </td>
            <td rowspan="2" class="ttd-area">TTD {{ ($dosbing->pemlap) ? $dosbing->pemlap : '...........................' }} </td>
            <td class="center" style="border:1px black dotted" rowspan="3">
                Cap<br>
                dari Instansi/Perusahaan
            </td>
        </tr>
        <tr><td colspan="3">&nbsp;</td></tr>
    </table>
    <p class="tembusan">[Rangkap 3:dosen pembimbing, ketua komisi, bapendik]
    </p>
</div>
<!-- Page 3 -->
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase sans size11">Kementerian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase sans size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase sans size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="sans size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="sans size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="sans size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="font uppercase bold center">
        Kartu kendali Bimbingan</p>
    <p class="font uppercase bold center nomargin-left">kerja praktik</p>
    <table class="nomargin-left">
        <tr>
            <td class="font">Nama</td>
            <td class="colon">:</td>
            <td class="font ">{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td class="font">NIM</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td class="font">Jurusan</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td class="font">Judul Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font">.......................................................</td>
        </tr>
        <tr>
            <td class="font">No SPK</td>
            <td class="colon">:</td>
            <td class="font">{{ $spk->nomor }}</td>
        </tr>
        <tr>
            <td class="font">Terhitung sejak</td>
            <td class="colon">:</td>
            <td class="font">
                <!-- {{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_selesai->format('d-m-Y') }} -->
                {{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_mulai->addDays($settings['masa_spk'])->format('d-m-Y') }}
            </td>
        </tr>
        <tr>
            <td class="font">Tempat Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->tempat->instansi }} {{ $mhskp->tempat->dimana }}</td>
        </tr>
        <tr>
            <td class="font">Dosen Pembimbing</td>
            <td class="colon">:</td>
            <td class="font">{{ $dosbing->dosbing1->nama }}</td>
        </tr>
    </table>
    <table border="1" cellspacing="0" class="kendali nomargin-left">
        <tr class="kendali-head">
            <td>No</td>
            <td class="two-line">Tanggal dan Waktu Bimbingan</td>
            <td>Uraian</td>
            <td class="two-line">Tanda Tangan</td>
        </tr>
        <tr class="kendali-isi">
            <td></td>
            <td class="two-line"></td>
            <td></td>
            <td class="two-line"></td>
        </tr>
    </table>
    <table class="ttd">
        <tr>
            <td>........................, ................................ </td>
        </tr>
        <tr>
            <td>Dosen Pembimbing</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ $dosbing->dosbing1->nama }}</td>
        </tr>
    </table>
    <p class="tembusan">*) Kartu kendali dapat diperbanyak sendiri</p>
</div>
<!-- Page 4 -->
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase sans size11">Kementerian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase sans size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase sans size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="sans size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="sans size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="sans size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="font uppercase bold center">
        Kartu kendali lapangan</p>
    <p class="font uppercase bold center nomargin-left">kerja praktik</p>
    <table class="nomargin-left">
        <tr>
            <td class="font">Nama</td>
            <td class="colon">:</td>
            <td class="font ">{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td class="font">NIM</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td class="font">Jurusan</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td class="font">Judul Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font">.......................................................</td>
        </tr>
        <tr>
            <td class="font">No SPK</td>
            <td class="colon">:</td>
            <td class="font">{{ $spk->nomor }}</td>
        </tr>
        <tr>
            <td class="font">Terhitung sejak</td>
            <td class="colon">:</td>
            <td>
                <!-- {{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_selesai->format('d-m-Y') }} -->
                {{ $progress->waktu_spk_mulai->format('d-m-Y') }} sampai {{ $progress->waktu_spk_mulai->addDays($settings['masa_spk'])->format('d-m-Y') }}
            </td>
        </tr>
        <tr>
            <td class="font">Tempat Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font">{{ $mhskp->tempat->instansi }} {{ $mhskp->tempat->dimana }}</td>
        </tr>
        <tr>
            <td class="font">Pembimbing Lapangan</td>
            <td class="colon">:</td>
            <td class="font">{{ ($dosbing->pemlap) ? $dosbing->pemlap : '.................................................' }}</td>
        </tr>
    </table>
    <table border="1" cellspacing="0" class="kendali nomargin-left">
        <tr class="kendali-head">
            <td>No</td>
            <td class="two-line">Tanggal dan Waktu Kehadiran</td>
            <td>Uraian</td>
            <td class="two-line">Tanda Tangan</td>
        </tr>
        <tr class="kendali-isi">
            <td></td>
            <td class="two-line"></td>
            <td></td>
            <td class="two-line"></td>
        </tr>
    </table>
    <table class="ttd">
        <tr>
            <td>..................., .................................... </td>
        </tr>
        <tr>
            <td>Pembimbing Lapangan</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>({{ ($dosbing->pemlap) ? $dosbing->pemlap : '........................................................'}})</td>
        </tr>
    </table>
    <p class="tembusan">*) Kartu kendali dapat diperbanyak sendiri</p>
</div>
</body>