<!DOCTYPE html>
<html>
<head>
    <title>SPK Kerja Praktik</title>
    <link rel="stylesheet" media="screen" href="{{ asset('css/spk.css') }}">
    <link rel="stylesheet" media="print" href="{{ asset('css/spk.css') }}">
</head>
<body>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="4" class="logo"><img src="{{ asset('img/unsoed-outline.png') }}" id="logo-unsoed"></td>
            <td class="font uppercase size12">Kementrian Riset teknologi dan perguruan tinggi</td>
        </tr>
        <tr>
            <td class="font uppercase size12">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="font uppercase size12">fakultas teknik</td>
        </tr>
        <tr>
            <td class="font size11">Jalan Mayjend Sungkono KM 5 Blater Purbalingga Telp/Fax (0281)6596801</td>
        </tr>
        <tr>
            <td colspan="2"></td>
        </tr>
    </table>
    <p class="font uppercase size14 bold center">
        form penilaian kerja praktik (spk)
    </p>
    <p class="font size12 left newpar">
        Penilaian kerja praktik mahasiswa atas nama :
    </p>
    <table>
        <tr>
            <td class="font size12">Nama</td>
            <td class="colon">:</td>
            <td class="font size12">{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td class="font size12">NIM</td>
            <td class="colon">:</td>
            <td class="font size12">{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td class="font size12">Jurusan / Program Studi</td>
            <td class="colon">:</td>
            <td class="font size12">{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
        <tr>
            <td class="font size12">Judul Kerja Praktik</td>
            <td class="colon">:</td>
            <td class="font size12">{{ $mhskp->judul }}</td>
        </tr>
        <tr>
            <td class="font size12">No SPK</td>
            <td class="colon">:</td>
            <td class="font size12">{{ $spk->nomor }}</td>
        </tr>
        <tr>
            <td class="font size12">Dimulai sejak tanggal</td>
            <td class="colon">:</td>
            <td class="font size12">{{ $progress->waktu_spk_mulai->format('d F Y') }}</td>
        </tr>
    </table>
    <p class="font size12 left newpar">
        Penilaian kerja praktik mahasiswa atas nama :
    </p>
    <table>
        <tr>
            <td class="font size12">Nilai Pelaksanaan (40%)</td>
            <td class="colon">:</td>
            <td class="font size12" style="border-bottom: 1px solid black; width: 50px;"></td>
        </tr>
        <tr>
            <td class="font size12">Nilai Seminar (30%)</td>
            <td class="colon">:</td>
            <td class="font size12" style="border-bottom: 1px solid black; width: 50px;"></td>
        </tr>
        <tr>
            <td class="font size12">Nilai Laporan (30%)</td>
            <td class="colon">:</td>
            <td class="font size12" style="border-bottom: 1px solid black; width: 50px;"></td>
        </tr>
        <tr>
            <td class="font size12">Nilai Angka</td>
            <td class="colon">:</td>
            <td class="font size12" style="border-bottom: 1px solid black; width: 50px;">{{ $nilai->nilai_akhir }}</td>
        </tr>
        <tr>
            <td class="font size12">Nilai Huruf</td>
            <td class="colon">:</td>
            <td class="font size12" style="border-bottom: 1px solid black; width: 50px;">{{ $nilaiHuruf }}</td>
        </tr>
        <tr>
            <td class="font size12">Tanggal Selesai</td>
            <td class="colon">:</td>
            <td class="font size12" style="border-bottom: 1px solid black; width: 200px;">{{ $progress->waktu_kp_selesai->format('d F Y') }}</td>
        </tr>
    </table>
    <table class="ttd">
        <tr>
            <td>Purbalingga, {{ $progress->waktu_kp_selesai->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>Pembimbing</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ $dosbing->dosbing1->nama }}</td>
        </tr>
        <tr>
            <td>{{ $dosbing->dosbing1->nip }}</td>
        </tr>
    </table>
</div>
</body>
</html>