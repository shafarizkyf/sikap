<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="print" href="{{ asset('css/revisi.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('css/revisi.css') }}">
    <title>Seminar Kerja Praktik</title>
</head>
<body>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">kementerian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center bold">
        <u>FORM PENGAJUAN REVISI ( PEMBIMBING DAN JUDUL)</u>
        <br>
        PROPOSAL KERJA PRAKTIK (KP) DAN TUGAS AKHIR (TA)
        <br>
    </p>
    <br>
    <p class="justify">
        Yth. <br>
        1. Pembimbing I<br>
        2. Pembimbing II<br>
        3. Komisi Studi Akhir<br>
        4. Bapendik Jurusan Teknik<br><br>
        Yang bertanda tangan dibawah ini mahasiswa dengan 	:
    </p>
    <table>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
    </table>
    <p>
        Bermaksud mengajukan permohonan ke Komisi Studi Akhir untuk penggantian<br>
        1.Revisi Judul (Topik,atau Bahasan)<br>
        2.Mengganti Judul (merevisi redaksional judul tanpa merubah topik/bahasan)<br>
        3.Penggantian pembimbing<br>
        Judul Kerja Praktik (KP) atau Tugas Akhir (TA) ke Komisi studi Akhir dengan alasan :<br>
    </p>
    <table class="dots">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br>
    <p class="table-inside">
        - Judul Kerja Praktik (KP) :<br>
    </p>
    <table class="table-inside dots">
        <tr>
            <td>{{ $mhskp->judul }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <p class="table-inside">
        - Pembimbing I semula Nama : ................................................... <br>
        - Pembimbing II semula Nama : .................................................. <br>
    </p>
    <p class="table-inside">
        Di ganti atau di revisi dengan judul :<br>
        - Judul Kerja Praktik (KP) :<br>
    </p>
    <table class="table-inside dots">
        <tr>
            <td>{{ ($mhskp->judul_final) ? $mhskp->judul_final : '(belum ada judul akhir)' }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <p class="table-inside">
        - Pembimbing I semula Nama : {{ $curDosbing->dosbing1->nama }} <br>
        - Pembimbing II semula Nama : {{ ($curDosbing->dosbing2) ? $curDosbing->dosbing2->nama : '..................................................' }} <br>
    </p>
    <table class="ttd-revisi" cellspacing="0">
        <tr>
            <td>Dosen Pembimbing I</td>
            <td>Dosen Pembimbing II</td>
            <td>Purbalingga, 24 Desember 2016</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>Pemohon Mahasiswa</td>
        </tr>
        <tr class="ttd-area">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>{{ $curDosbing->dosbing1->nama }}</td>
            <td>{{ ($curDosbing->dosbing2) ? $curDosbing->dosbing2->nama : ''}}</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIP.{{ $curDosbing->dosbing1->nip }}</td>
            <td>NIP.{{ ($curDosbing->dosbing2) ? $curDosbing->dosbing2->nip : ''}}</td>
            <td>NIM.{{ $mhskp->nim->nim }}</td>
        </tr>
    </table>
    <br>
    <p class="table-inside size10 justify italic">
        <b class="size12"><u>Catatan :</u></b><br>
        - Coret yang tidak perlu<br>
        - Calon pengganti pembimbing di putuskan oleh Sidang Komisi Studi Akhir<br>
        - Mahasiswa wajib melampirkan SPK dan surat Tugas Pembimbing KP dan atau TA<br>
        - Setelah form ini di acc oleh pembimbing 1 dan 2 dan Ketua Komisi Studi Akhir ajukan ke BAPENDIK untuk disidangkan di Komisi Studi Akhir<br>
    </p>
    <table class="kop-surat">
        <tr>
            <td></td>
        </tr>
    </table>
    <p class="tembusan">Lampiran 1 (Form pengajuan penggantian Judul KP/TA dan Pembimbing ke Komisi Studi Akhir)<br>
        Di Copy Rangkap 3:pembimbing,Bependik,Komisi,Mahasiswa</p>
</div>
<div class="pages">
    <table class="kop-surat">
        <tr>
            <td rowspan="6">
                <img src="{{ asset('img/unsoed-outline.png') }}" class="logo-unsoed" alt="Logo Unsoed">
            <td class="uppercase serif size11">kementerian Riset teknologi dan pendidikan tinggi</td>
            </td>
        </tr>
        <tr>
            <td class="uppercase serif size11">Universitas Jenderal Soedirman</td>
        </tr>
        <tr>
            <td class="uppercase serif size11 bold">fakultas teknik</td>
        </tr>
        <tr>
            <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
        </tr>
        <tr>
            <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
        </tr>
        <tr>
            <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
    </table>
    <p class="center bold">
        <u>BERITA ACARA REVISI JUDUL DAN PEMBIMBING</u>
        <br>
        PROPOSAL KERJA PRAKTIK (KP) DAN TUGAS AKHIR (TA)
        <br>
    </p>
    <br>
    <p class="justify">
        Berdasarkan pengajuan mahasiswa dan persyaratan administrasi di BAPENDIK Jurusan Teknik dengan
        menunjukan :<br>
        1.Surat Perintah Kerja (SPK) KP dan atau TA - Wajib di lampirkan<br>
        2.Surat Tugas Pembimbing KP dan atau TA - Wajib di lampirkan<br><br>
        Telah di setujui oleh Bapendik dan Komisi Studi Akhir untuk :<br>
        1.Merevisi judul (Topik atau bahasan)<br>
        2.Mengganti Judul (merevisi redaksional judul tanpa merubah topik/bahasan)<br>
        3.Penggantian pembimbing<br>
        Proposal Kerja Praktik dan atau Tugas Akhir melalui sidang Komisi Tugas Akhir atas nama mahasiswa :<br><br>
    </p>
    <table>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIM</td>
            <td>:</td>
            <td>{{ $mhskp->nim->nim }}</td>
        </tr>
        <tr>
            <td>Program Studi</td>
            <td>:</td>
            <td>{{ $mhskp->nim->jurusan->jurusan }}</td>
        </tr>
    </table>
    <br>
    <p>
        Judul Kerja Praktik (KP) :<br>
    </p>
    <br>
    <table class="info-kp" cellspacing="0">
        <tr class="header">
            <td>Awal atau Lama</td>
            <td>Pengganti atau Baru</td>
        </tr>
        <tr class="isi">
            <td>
                Judul
            </td>
            <td>Judul</td>
        </tr>
        <tr class="isi">
            <td>
                <table class="dots">
                    <tr>
                        <td>{{ $mhskp->judul }}</td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="dots">
                    <tr>
                        <td>{{ ($mhskp->judul_final) ? $mhskp->judul_final : '(belum ada judul final)' }}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="isi">
            <td>
                <table class="dots">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="dots">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="isi">
            <td>
                <table class="dots">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>
                <table class="dots">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="isi">
            <td>Pembimbing I : {{ $curDosbing->dosbinglama->nama }}</td>
            <td>Pembimbing I : {{ $curDosbing->dosbing1->nama }}</td>
        </tr>
        <tr class="isi">
            <td>Pembimbing II : {{ ($curDosbing->dosbinglama2) ? $curDosbing->dosbinglama2->nama : '' }}</td>
            <td>Pembimbing II : {{ ($curDosbing->dosbing2) ? $curDosbing->dosbing2->nama : '' }}</td>
        </tr>
    </table>
    <br>
    <table class="ttd-revisi" cellspacing="0">
        <tr>
            <td>Komisi Studi Akhir</td>
            <td>Purbalingga, {{ $reqDosbing->createdAt->format('d F Y') }}</td>
        </tr>
        <tr>
            <td>a.n Ketua,</td>
            <td>Pemohon Mahasiswa</td>
        </tr>
        <tr>
            <td>Sub Komisi</td>
            <td>&nbsp;</td>
        </tr>
        <tr class="ttd-area">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>{{ ($komisi) ? $komisi->nama : '(komisi belum diset)' }}</td>
            <td>{{ $mhskp->nim->nama }}</td>
        </tr>
        <tr>
            <td>NIP.{{ ($komisi) ? $komisi->nip : '(komisi belum diset)' }}</td>
            <td>NIM.{{ $mhskp->nim->nim }}</td>
        </tr>
    </table>
    <br>
    <p class="table-inside size10 justify italic">
        <b class="size12"><u>Catatan :</u></b><br>
        - Coret yang tidak perlu<br>
        - Calon pengganti pembimbing di putuskan oleh Sidang Komisi Studi Akhir<br>
        - Mahasiswa wajib melampirkan SPK dan surat Tugas Pembimbing KP dan atau TA<br>
        - Setelah form ini di acc oleh pembimbing 1 dan 2 dan Ketua Komisi Studi Akhir ajukan ke BAPENDIK untuk disidangkan di Komisi Studi Akhir<br>
    </p>
    <table class="kop-surat">
        <tr>
            <td></td>
        </tr>
    </table>
    <p class="tembusan">Lampiran 1 (Form pengajuan penggantian Judul KP/TA dan Pembimbing ke Komisi Studi Akhir)<br>
        Di Copy Rangkap 3:pembimbing,Bependik,Komisi,Mahasiswa</p>
</div>
</body>
</html>