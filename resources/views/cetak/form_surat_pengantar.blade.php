<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="print" href="{{ asset('css/form-pengantar.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('css/form-pengantar.css') }}">
    <title>Form Pengajuan Surat Pengantar Kerja Praktik</title>
</head>
<body>
<table id="kop-surat">
    <tr>
        <td rowspan="6">
            <img src="{{ asset('img/unsoed-outline.png') }}" id="logo-unsoed" alt="Logo Unsoed">
        <td class="uppercase serif size12">kementerian Riset teknologi dan pendidikan tinggi</td>
        </td>
    </tr>
    <tr>
        <td class="uppercase serif size12">Universitas Jenderal Soedirman</td>
    </tr>
    <tr>
        <td class="uppercase serif size11 bold">fakultas teknik</td>
    </tr>
    <tr>
        <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
    </tr>
    <tr>
        <td class="serif size10">Telepon/Faks. : (0281)6596801</td>
    </tr>
    <tr>
        <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
    </tr>
</table>
@if($surat)
<div>
    <p class="newpar uppercase sans center bold size12">FORM PERMINTAAN SURAT PENGANTAR KERJA PRAKTIK <br>KE perusahaan/instansi</p>
    <p class="newpar sans size12 justify">Saya yang bertanda tangan di bawah ini : <br>
        Mengajukan permohonan Surat Pengantar Kerja Praktik kepada<br>
        Dekan Fakultas Teknik,</p>
    <table id="table-mhs" border="1px" cellspacing="0" class="newpar">
        <tr>
            <td class="sans center bold fit">NO.</td>
            <td class="sans center bold">NAMA</td>
            <td class="sans center bold fit">NIM</td>
            <td class="sans center bold fit">SKS</td>
            <td class="sans center bold fit">IPK</td>
            <td class="sans center bold fit">JURUSAN</td>
            <td class="sans center bold">PERUSAHAAN <br>(INSTANSI)</td>
        </tr>
        @foreach($surat as $s)
        <tr>
            <td class="center sans">1</td>
            <td class="center sans">{{ $s->nim->nama }}</td>
            <td class="center sans">{{ $s->nim->nim }}</td>
            <td class="center sans">{{ $s->nim->sks }}</td>
            <td class="center sans">{{ $s->nim->ipk }}</td>
            <td class="center sans">{{ $s->nim->jurusan->jurusan }}</td>
            <td class="center sans">{{ $s->instansi }}</td>
        </tr>
        @endforeach
    </table>
    <p class="sans size12 newpar justify">Keterangan : Form diisi oleh mahasiswa dengan lengkap dan diserahkan ke Bapendik untuk mendapatkan surat Pengantar Kerja Praktik.</p>
</div>
<table class="ttd2">
    <tr>
        <td>Mengetahui,</td>
    </tr>
    <tr>
        <td>Dosen Pembimbing Akademik</td>
    </tr>
    <tr>
        <td class="ttd-area"></td>
    </tr>
    <tr>
        <td>{{ $surat->first()->nim->dosenpa->nama }}</td>
    </tr>
    <tr>
        <td>{{ $surat->first()->nim->dosenpa->nip }}</td>
    </tr>
</table>
<table class="ttd">
    <tr>
        <td>Purbalingga, {{ $surat->last()->waktu_ajukan->format('d F Y') }}</td>
    </tr>
    <tr>
        <td>Pemohon</td>
    </tr>
    <tr>
        <td class="ttd-area"></td>
    </tr>
    <tr>
        <td>{{ $surat->first()->nim->nama }}</td>
    </tr>
    <tr>
        <td>{{ $surat->first()->nim->nim }}</td>
    </tr>
</table>
@endif
</body>
</html>