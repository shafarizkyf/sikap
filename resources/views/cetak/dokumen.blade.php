<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" href="{{ asset('less/style.less') }}" />
    <link rel="stylesheet/less" type="text/css" href="{{ asset('css/latofonts.css') }}" />
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <title>Berkas {{ ucfirst($file) }}</title>
</head>
<body>
<object data="{{ route('view_pdf', array($file, $nim)) }}" type="application/pdf" width="100%" height="99%">
    <embed src="{{ route('view_pdf', array($file, $nim)) }}" type="application/pdf">
</object>

</body>
</html>