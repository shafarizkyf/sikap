<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" media="print" href="{{ asset('css/surat-pengantar.css') }}">
    <link rel="stylesheet" media="screen" href="{{ asset('css/surat-pengantar.css') }}">
    <title>Form Pengajuan Surat Pengantar Kerja Praktik</title>
</head>
<body>
<table id="kop-surat">
    <tr>
        <td rowspan="6">
            <img src="{{ asset('img/unsoed-outline.png') }}" id="logo-unsoed" alt="Logo Unsoed">
        <td class="uppercase serif size12">Kementerian Riset teknologi dan pendidikan tinggi</td>
        </td>
    </tr>
    <tr>
        <td class="uppercase serif size12">Universitas Jenderal Soedirman</td>
    </tr>
    <tr>
        <td class="uppercase serif size11 bold">fakultas teknik</td>
    </tr>
    <tr>
        <td class="serif size10">Alamat : Jl. Mayjend Sungkono km 5 Blater, Kalimanah, Purbalingga 53371</td>
    </tr>
    <tr>
        <td class="serif size10">Telepon/Faks. : (0281) 6596801</td>
    </tr>
    <tr>
        <td class="serif size10">E-mail : ft@unsoed.ac.id Laman : ft.unsoed.ac.id</td>
    </tr>
</table>
@if($surat)
<div id="isi-surat">
    <table id="nomor-surat" class="newpar">
        <tr>
            <td>Nomor</td>
            <td>:</td>
            <td>{{ $surat->nomor }}</td>
            <!-- <td rowspan="4" class="right">{{ $surat->waktu_selesai->format('d F Y') }}</td> -->
            <td rowspan="4" class="right">{{ date('d F Y')}}</td>
        </tr>
        <tr>
            <td>Lamp</td>
            <td>:</td>
            <td>-</td>
        </tr>
        <tr>
            <td>Hal</td>
            <td>:</td>
            <td>Ijin Kerja Praktik Lapangan</td>
        </tr>
    </table>
    <p class="newpar sans justify">
        <br>
        <br>
        Yth. {{ $surat->kepada }} 
        <br>
        {{ $surat->instansi }}
        <br>
        di {{ $surat->dimana }}
        <br>
        <br>
        Dengan ini diberitahukan bahwa nama tersebut di bawah ini adalah benar sebagai mahasiswa Jurusan {{ $surat->nim->jurusan->jurusan }} Fakultas Teknik Universitas Jenderal Soedirman pada tahun ajaran {{ $surat->nim->tahun_ajaran }} sedang mengambil mata kuliah Kerja Praktik Lapangan, dan tercatat sebagai mahasiswa aktif.
    </p>
    <table id="table-mhs" border="1px" cellspacing="0" class="newpar">
        <tr>
            <td class="sans center bold fit">NO.</td>
            <td class="sans center bold">NAMA/NIM</td>
            <td class="sans center bold">JURUSAN</td>
        </tr>
        <tr>
            <td class="center sans">1</td>
            <td class="center sans">
                {{ $surat->nim->nama }}
                <br>
                ({{ $surat->nim->nim }})
            </td>
            <td class="center sans">{{ $surat->nim->jurusan->jurusan }}</td>
        </tr>
    </table>
    <p class="sans newpar justify">
        Untuk maksud tersebut kami mohon bantuan, mahasiswa kami untuk diizinkan melakukan Kerja Praktik Lapangan  
        @if($surat->periode_dari and $surat->periode_ke)
            pada {{ $surat->periode_dari->format('d-m-Y') }} hingga {{ $surat->periode_ke->format('d-m-Y') }}.
        @endif
        
        <br>
        Demikian surat dari kami atas perhatiannya kami sampaikan terima kasih.
    </p>
    <table class="ttd">
        <tr>
            <td>Fakultas Teknik</td>
        </tr>
        <tr>
            <td>a.n Dekan</td>
        </tr>
        <tr>
            <td>Wakil Dekan Bidang Akademik</td>
        </tr>
        <tr>
            <td class="ttd-area"></td>
        </tr>
        <tr>
            <td>{{ App\ListDosen::getwda()->nama }}</td>
        </tr>
        <tr>
            <td>NIP. {{ App\ListDosen::getwda()->nip }}</td>
        </tr>
    </table>
    <p class="tembusan">
        Tembusan :
        <br>
        1. Rektor Universitas Jenderal Soedirman Purwokerto
        <br>
        2. Ketua Jurusan {{ $surat->nim->jurusan->jurusan }}
        <br>
        <span class="bold sans size11 italic">Catatan : *( Rangkap 2: Bapendik, Mahasiswa )</span>
    </p>
</div>
@endif
</body>
</html>