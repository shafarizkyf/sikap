@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class="">menu</span>
        &nbsp;
    </li>
    <li class="items @yield('sidebar-link_seminar')">
        <a href="{{ route('home_seminar') }}">
            <span class="ion-ios-mic icon"></span>
            jadwal seminar
        </a>
    </li>
    <li class="items @yield('sidebar-link_surat')">
        <a href="{{ route('home_surat') }}">
            <span class="ion-android-list icon"></span>
            surat kerja praktik
        </a>
    </li>
    <li class="items @yield('sidebar-link_judul')">
        <a href="{{ route('home_judul') }}">
            <span class="ion-android-list icon"></span>
            judul kerja praktik
        </a>
    </li>
    <li class="items">
        <a href="{{ route('login') }}">
            <span class="ion-person icon"></span>
            @yield('login_username')
        </a>
    </li>
@endsection