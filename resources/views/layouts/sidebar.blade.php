<div class="kp-sidebar">
    <a href="#" class="sidebar-logo">
        <img class="image" src="{{ asset('img/unsoed.png') }}"/>
        <p class="text">
            Fakultas Teknik
            <span>Universitas Jenderal Soedirman</span>
        </p>
    </a>
    <div class="toggle" style="position: fixed; left: 250px; top: 0px;transition: 0.5s all;" data-sidebar="open">
        <a href="#" class="button" style="box-shadow: none;padding: 25px 10px;">
            <i id="toggle-icon" class="icon ion-arrow-left-b"></i>
        </a>
    </div>
    <ul class="sidebar-content">
        @yield('sidebar-links')
    </ul>
</div>
