@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class=""></span>
        menu utama
    </li>
    <li class="items @yield('sidebar-link_surat')">
        <a href="{{ route('mahasiswa_menu', 'surat_pengantar') }}">
            <span class="ion-android-mail icon"></span>
            Surat Pengantar KP
        </a>
    </li>
    <li class="items @yield('sidebar-link_usulan')">
        <a href="{{ route('mahasiswa_index') }}">
            <span class="ion-clipboard icon"></span>
            usulan kp
        </a>
    </li>
    <li class="items @yield('sidebar-link_bimbingan')">
        <a href="{{ route('mahasiswa_menu', 'bimbingan') }}">
            <span class="ion-android-bulb icon"></span>
            bimbingan kp
        </a>
    </li>
    <li class="items @yield('sidebar-link_seminar')">
        <a href="{{ route('mahasiswa_menu', 'seminar') }}">
            <span class="ion-speakerphone icon"></span>
            seminar
        </a>
    </li>
    <li class="items @yield('sidebar-link_nilai')">
        <a href="{{ route('mahasiswa_menu', 'nilai') }}">
            <span class="ion-ribbon-a icon"></span>
            nilai
        </a>
    </li>
    <li class="items @yield('sidebar-link_log')">
        <a href="{{ route('mahasiswa_menu', 'aktivitas') }}">
            <span class="ion-android-stopwatch icon"></span>
            log aktvitas
        </a>
    </li>
    @yield('sidebar-add_link1')
    <li class="group-name">
        keamanan
    </li>
    <li class="items @yield('sidebar-link_password')">
        <a href="{{ route('mahasiswa_menu', 'keamanan') }}">
            <span class="ion-locked icon"></span>
            ubah password
        </a>
    </li>
    <li class="items">
        <a href="{{ URL::to('logout') }}">
            <span class="ion-power icon"></span>
            logout
        </a>
    </li>
@endsection