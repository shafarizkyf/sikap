@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class=""></span>
        menu utama
    </li>
    <li class="items @yield('sidebar-link_seminar')">
        <a href="{{ route('mahasiswa_index') }}">
            <span class="ion-speakerphone icon"></span>
            seminar
        </a>
    </li>
    @yield('sidebar-add_link1')
    <li class="group-name">
        keamanan
    </li>
    <li class="items @yield('sidebar-link_password')">
        <a href="{{ route('mahasiswa_menu', 'keamanan') }}">
            <span class="ion-locked icon"></span>
            ubah password
        </a>
    </li>
    <li class="items">
        <a href="{{ URL::to('logout') }}">
            <span class="ion-power icon"></span>
            logout
        </a>
    </li>
@endsection