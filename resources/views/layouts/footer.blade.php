	<script>
		var jquery_path = '{{ asset('js/jquery/jquery.min.js') }}';
		var test_path = '{{ asset('js/test.js') }}';
		function myJQueryCode(){
			$('.toggle').click(function(){
				var status = $(this).attr('data-sidebar');
				if(status == 'open'){
					$('.kp-sidebar').css('margin-left','-250px');
					$('.container').css('padding-left','0px');
					$('.toggle').css('left','0px');
					$('#toggle-icon').removeClass('ion-arrow-left-b');
					$('#toggle-icon').addClass('ion-arrow-right-b');
					$(this).attr('data-sidebar','closed');

				}else{
					$('.kp-sidebar').css('margin-left','0px');
					$('.container').css('padding-left','250px');
					$('.toggle').css('left','250px');
					$('#toggle-icon').removeClass('ion-arrow-right-b');
					$('#toggle-icon').addClass('ion-arrow-left-b');
					$(this).attr('data-sidebar','open');
				}
			});
		}
		window.onload = function() {
			if(typeof jQuery=='undefined') {
			    var headTag = document.getElementsByTagName("head")[0];
			    var jqTag = document.createElement('script');
			    jqTag.type = 'text/javascript';
			    jqTag.src = jquery_path;
			    jqTag.onload = myJQueryCode;
			    headTag.appendChild(jqTag);
			}else{
			    var headTag = document.getElementsByTagName("head")[0];
			    var jqTag = document.createElement('script');
			    jqTag.type = 'text/javascript';
			    jqTag.src = test_path;
			    jqTag.onload = myJQueryCode;
			    headTag.appendChild(jqTag);
			}
		}
	</script>
</body>
</html>