<div class="bar">
    <div class="container">
        <div class="account">
            <p class="name">
                <span class="ion-person icon"></span>
                @yield('bar-username')
            </p>
        </div>
        <div class="info">
            <p class="main">
                @yield('bar-page_title')
                <span class="secondary">@yield('bar-page_subtitle')</span>
            </p>
        </div>
        @yield('bar-menu')
        @yield('bar-content')
    </div>
</div>