@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class=""></span>
        menu utama
    </li>
    <li class="items @yield('sidebar_link_import')">
        <a href="{{ route('import_export') }}">
            <span class="ion-ios-cloud-upload icon"></span>
            export / import
        </a>
    </li>
    <li class="items @yield('sidebar_link_mhs')">
        <a href="{{ route('bapendik_kp_prasyarat') }}">
            <span class="ion-university icon"></span>
            mahasiswa
        </a>
    </li>
    <li class="items @yield('sidebar_link_dosen')">
        <a href="{{ route('bapendik_menu', 'dosen') }}">
            <span class="ion-ios-people icon"></span>
            dosen
        </a>
    </li>
    <li class="items @yield('sidebar_link_seminar')">
        <a href="{{ route('bapendik_menu', 'seminar') }}">
            <span class="ion-ios-mic icon"></span>
            seminar
        </a>
    </li>
    <li class="items @yield('sidebar_link_arsip')">
        <a href="{{ route('bapendik_menu', 'arsip') }}">
            <span class="ion-android-archive icon"></span>
            arsip
        </a>
    </li>
    <li class="group-name">
        keamanan
    </li>
    <li class="items @yield('sidebar_link_keamanan')">
        <a href="{{ route('bapendik_menu', 'keamanan') }}">
            <span class="ion-locked icon"></span>
            keamanan
        </a>
    </li>
    <li class="items">
        <a href="{{ route('logout') }}">
            <span class="ion-power icon"></span>
            logout
        </a>
    </li>
@endsection