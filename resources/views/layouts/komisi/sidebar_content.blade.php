@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class=""></span>
        menu utama
    </li>
    <li class="items @yield('sidebar-link_dosenpa')">
        <a href="{{ route('komisi_menu', 'mhsa') }}">
            <span class="ion-ios-people icon"></span>
            mahasiswa akademik
        </a>
    </li>
    <li class="items @yield('sidebar-link_dosbing')">
        <a href="{{ route('komisi_menu', 'dosbing') }}">
            <span class="ion-university icon"></span>
            bimbingan kerja praktik
        </a>
    </li>
    <li class="items @yield('sidebar-link_seminar')">
        <a href="{{ route('komisi_menu', 'seminar') }}">
            <span class="ion-speakerphone icon"></span>
            jadwal seminar
        </a>
    </li>
    <li class="group-name">
        komisi
    </li>
    <li class="items @yield('sidebar-link_laporan')">
        <a href="{{ route('komisi_menu', 'laporan') }}">
            <span class="ion-email-unread icon"></span>
            Laporan
        </a>
    </li>
    <li class="items @yield('sidebar-link_mhs')">
        <a href="{{ route('komisi_kp_usulan') }}">
            <span class="ion-ios-briefcase icon"></span>
            mahasiswa kp
        </a>
    </li>
    <li class="items @yield('sidebar-link_dosen')">
        <a href="{{ route('komisi_menu', 'dosen') }}">
            <span class="ion-ios-person icon"></span>
            dosen
        </a>
    </li>
    <li class="items @yield('sidebar-link_arsip')">
        <a href="{{ route('komisi_menu', 'arsip') }}">
            <span class="ion-android-archive icon"></span>
            arsip
        </a>
    </li>
    <li class="group-name">
        keamanan
    </li>
    <li class="items @yield('sidebar-link_keamanan')">
        <a href="{{ route('komisi_menu', 'keamanan') }}">
            <span class="ion-locked icon"></span>
            keamanan
        </a>
    </li>
    <li class="items">
        <a href="{{ route('logout') }}">
            <span class="ion-power icon"></span>
            logout
        </a>
    </li>
@endsection