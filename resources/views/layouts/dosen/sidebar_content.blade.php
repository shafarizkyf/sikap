@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class=""></span>
        menu utama
    </li>
    <li class="items @yield('sidebar-link_mhsa')">
        <a href="{{ route('dosen_menu', 'mhsa') }}">
            <span class="ion-ios-people icon"></span>
            bimbingan akademik
        </a>
    </li>
    <li class="items @yield('sidebar-link_mhskp')">
        <a href="{{ route('dosen_kp_bimbingan') }}">
            <span class="ion-university icon"></span>
            bimbingan kerja praktik
        </a>
    </li>
    <li class="items @yield('sidebar-link_seminar')">
        <a href="{{ route('dosen_menu', 'seminar') }}">
            <span class="ion-speakerphone icon"></span>
            seminar
        </a>
    </li>
    <li class="items @yield('sidebar-link_arsip')">
        <a href="{{ route('dosen_menu', 'arsip') }}">
            <span class="ion-android-archive icon"></span>
            arsip
        </a>
    </li>    
    <li class="group-name">
        keamanan
    </li>
    <li class="items @yield('sidebar-link_keamanan')">
        <a href="{{ route('dosen_menu', 'keamanan') }}">
            <span class="ion-locked icon"></span>
            keamanan
        </a>
    </li>
    <li class="items">
        <a href="{{ route('logout') }}">
            <span class="ion-power icon"></span>
            logout
        </a>
    </li>
@endsection