<!DOCTYPE html>
<html>
<meta charset="utf-8">
<meta name="description" content="Sistem Informasi Kerja Praktik Fakultas Teknik Unsoed">
<meta name="keywords" content="sikap,sistem,informasi,kerja,praktik,shafa,aji,H1L014046,H1L014014">
<meta name="author" content="Aji Sulistyo - Shafa Rizky Fandestika">
<link rel="stylesheet/less" type="text/css" href="{{ asset('less/style.less') }}" />
<link rel="stylesheet/less" type="text/css" href="{{ asset('css/latofonts.css') }}" />
<link rel="shortcut icon" type="image/png" href="{{ asset('img/unsoed.png') }}"/>
<script src="{{ asset('js/less/less.min.js') }}"></script>
@yield('head')
<title>Sistem Informasi Kerja Praktik FT Unsoed</title>
</head>
<body>
@yield('header-content1')