@extends('layouts.sidebar')

@section('sidebar-links')
    <li class="group-name">
        <span class="">Menu</span>
        &nbsp;
    </li>
    <li class="items @yield('sidebar-link_dosen')">
        <a href="{{ route('admin_menu', 'dosen') }}">
            <span class="ion-ios-person icon"></span>
            dosen
        </a>
    </li>
    <li class="items @yield('sidebar-link_mhs')">
        <a href="{{ route('admin_menu', 'mahasiswa') }}">
            <span class="ion-ios-person icon"></span>
            mahasiswa
        </a>
    </li>
    <li class="group-name">
        keamanan
    </li>
    <li class="items @yield('sidebar_link_keamanan')">
        <a href="{{ route('admin_menu', 'keamanan') }}">
            <span class="ion-locked icon"></span>
            keamanan
        </a>
    </li>
    <li class="items">
        <a href="{{ route('logout') }}">
            <span class="ion-power icon"></span>
            logout
        </a>
    </li>
@endsection