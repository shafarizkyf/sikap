@extends('layouts.footer')

@section('header-content1')
    <div class="content">
        <div class="panel">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-person icon"></span>
                    Halo, {{ $username }}
                </div>
            </div>
            <div class="login-header">
                <div class="text">
                    Lengkapi Data Anda
                </div>
            </div>
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>'MahasiswaDaftarKpController@updateMhsData']) !!}
                <div class="field">
                    {!! Form::label('email', null, ["class"=>"ion-email"]) !!}
                    {!! Form::text('email', null, ['id' => 'email', "placeholder"=>"Email"]) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('email') : '' }}
                </div>
                <div class="field">
                    {!! Form::label('nohp', null, ['class' => 'ion-ios-telephone']) !!}
                    {!! Form::text('telp', null, ['id'=>'nohp', 'placeholder'=>"Nomor HP"]) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('telp') : '' }}
                </div>
                <div class="field">
                    {!! Form::label("dosenpa", null, ['class' => 'ion-ios-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('dosenpa', [''=>'Dosen Pembimbing Akademik'] + $dosenpa, $mhs->dosenpa_id) !!}
                    </div>
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('dosenpa') : '' }}
                </div>
                <div class="field noborder right">
                    <a class="helper" href="{{ route('logout') }}">
                        keluar
                    </a>
                    {{ Form::submit('Selanjutnya', ['class'=>'button']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.header')