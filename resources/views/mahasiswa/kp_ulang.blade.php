@extends('layouts.footer')

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    kerja praktik
@endsection

@section('bar-page_subtitle')
    kerja praktik anda harus diulang
@endsection

@section('bar-content')
    <div class="content">
        <div class="main">
            <div class="panel">
                <div class="tag">
                    <div class="tag-content">
                        <span class="ion-locked icon"></span> Maaf, {{ explode(' ', $mhs->nama)[0] }}
                    </div>
                </div>
                <div class="login-header">
                    <div class="text">
                        Kerja Praktik Anda harus diulang
                        <p class="secondary">
                            KP Anda diulang karena anda tidak dapat menyelesaikan kp dalam masa spk atau
                            hal lainnya. Silahkan hubungi komisi untuk detailnya.
                        </p>
                    </div>
                </div>
                <div class="field noborder center">
                    {!! Form::open(['method'=>'post', 'action'=>['MahasiswaDaftarKpController@ulang', $mhs->id]]) !!}
                    {!! Form::submit('Mulai Ulang', ['class'=>'button']) !!}
                    {!! Form::close() !!}
                </div>
                <div class="footer"></div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')