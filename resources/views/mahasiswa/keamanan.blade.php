@extends('layouts.footer')


@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    Pengaturan Keamanan
@endsection

@section('bar-page_subtitle')
    Ubah kata sandi
@endsection

@section('sidebar-link_password')
    active
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-locked icon"></span> ubah password
                </div>
            </div>
            <div class="main">
                @if(session()->has('keamanan'))
                    <div class="field noborder error">
                        {{ session()->get('keamanan') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'post', 'action'=>'UserLoginController@gantiPassword']) !!}
                    <div class="field">
                        {!! Form::label('oldpass', null, ['class'=>'ion-locked']) !!}
                        {!! Form::password('oldpass', ['placeholder' => 'Password lama Anda']) !!}
                    </div>
                    <div class="field noborder error">
                        {{ ($errors->all()) ? $errors->first('oldpass') : '' }}
                    </div>
                    <div class="field">
                        {!! Form::label('newpass', null, ['class'=>'ion-locked']) !!}
                        {!! Form::password('newpass', ['placeholder' => 'Password baru Anda']) !!}
                    </div>
                    <div class="field noborder error">
                        {{ ($errors->all()) ? $errors->first('newpass') : '' }}
                    </div>
                    <div class="field">
                        {!! Form::label('renewpass', null, ['class'=>'ion-locked']) !!}
                        {!! Form::password('renewpass', ['placeholder' => 'Konfirmasi password baru anda']) !!}
                    </div>
                    <div class="field noborder error">
                        {{ ($errors->all()) ? $errors->first('renewpass') : '' }}
                    </div>
                    <div class="field noborder center">
                        {!! Form::submit('Simpan', ['class'=>'button']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')