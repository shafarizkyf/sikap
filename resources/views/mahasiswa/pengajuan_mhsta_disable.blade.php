@extends('layouts.footer')


@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    Pengajuan seminar
@endsection

@section('bar-page_subtitle')
    progress pengajuan saat ini
@endsection

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-content')
    <div class="content">
        <div class="main">
            <div class="panel">
                <div class="tag">
                    <div class="tag-content">
                        <span class="ion-locked icon"></span> {{ $msg['tag'] }}
                    </div>
                </div>
                <div class="login-header">
                    <div class="text">
                        {{ $msg['judul'] }}
                        <p class="secondary">
                            {{ $msg['keterangan'] }}
                        </p>
                    </div>
                </div>
                <div class="footer"></div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content_mhsta')

@extends('layouts.header')