@extends('layouts.footer')

@section('header-content1')
    <div class="content">
        <div class="panel">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-person icon"></span>
                    Halo, {{ $username }}
                </div>
            </div>
            <div class="login-header">
                <div class="text">
                    Sedang KP atau TA ?
                </div>
            </div>
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>'MahasiswaDaftarKpController@updateLevel']) !!}
                <div class="field">
                    {!! Form::label('level', null, ['class' => 'ion-ios-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('level', ['kp'=>'Kerja Praktik', 'ta'=>'Tugas Akhir'], null, ['placeholder'=>'Pilih Penggunaan Sistem']) !!}
                    </div>
                </div>
                @if($errors->has('level'))
                <div class="field noborder error">
                    {{ $errors->first('level') }}
                </div>
                @endif
                <div class="field noborder right">
                    <a class="helper" href="{{ route('logout') }}">
                        keluar
                    </a>
                    {{ Form::submit('Selanjutnya', ['class'=>'button']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.header')