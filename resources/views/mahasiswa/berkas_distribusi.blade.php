@extends('layouts.footer')

@section('sidebar-link_distribusi')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    berkas distribusi kp
@endsection

@section('bar-page_subtitle')
    berkas distribusi kerja praktik
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> diajukan pada, {{ $dist->created_at->format('d F Y') }}
                </div>
            </div>
            <div class="main">
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Form Distribusi</h2>
                        <p class="desc">
                            Form Distribusi : <a href="{{ route('download_berkas', $dist->form) }}">Download</a> |
                            <a href="{{ route('view_file', array('formdist', $mhs->nim)) }}" target="_blank">Lihat</a> |
                            <a href="#ex1" aria-nim="{{ $mhs->id }}" aria-file="dist" rel="modal:open" class="form-dist">Edit</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Pengesahan Laporan KP</h2>
                        <p class="desc">
                            Pengesahan Laporan KP : <a href="{{ route('download_berkas', $dist->pengesahan) }}">Download</a> |
                            <a href="{{ route('view_file', array('pengesahan', $mhs->nim)) }}" target="_blank">Lihat</a> |
                            <a href="#ex1" aria-nim="{{ $mhs->id }}" aria-file="pengesahan" rel="modal:open" class="form-dist">Edit</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.form-dist').click(function () {
                var nim = this.getAttribute('aria-nim');
                var file = this.getAttribute('aria-file');
                getFormDistribusi(file, nim);
            });

            function getFormDistribusi(file, nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/dist/' ?>" + file + '/' + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')