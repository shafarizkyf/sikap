@extends('layouts.footer')

@section('header-content1')
    <div class="content">
        <div class="panel">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-person icon"></span>
                    Halo, {{ $username }}
                </div>
            </div>
            <div class="login-header">
                <div class="text">
                    Pertama, Buat Surat Pengantar KP Anda
                </div>
            </div>
            <div class="main">
                {!! Form::model($mhsSurat, ['method'=>'post', 'action'=>['MahasiswaDaftarKpController@updateSuratPengantarKp', $mhsSurat->id]]) !!}
                <?php
                    if($mhsSurat->kepada == 'Pimpinan' || $mhsSurat->kepada == 'Kepala Sekolah'){
                        $kepadaC = null;
                        $kepada = $mhsSurat->kepada;
                    }else{
                        $kepadaC = $mhsSurat->kepada;
                        $kepada = 'lainnya';
                    }
                ?>
                <div class="field">
                    {!! Form::label('instansi', null, ["class"=>"ion-email"]) !!}
                    {!! Form::text('instansi', null, ['id' => 'instansi', "placeholder"=>"Nama Instansi/Perusahaan"]) !!}
                </div>
                @if($errors->first('instansi'))
                    <div class="field noborder error">
                        {{ $errors->first('instansi') }}
                    </div>
                @endif
                <div class="field">
                    {!! Form::label('kepada', null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('kepada', ['Pimpinan'=>'Pimpinan', 'Kepala Sekolah'=>'Kepala Sekolah', 'lainnya'=>'lainnya'], $kepada, ['placeholder'=>'Orang dituju', 'class'=>'kepada']) !!}
                    </div>
                </div>
                @if($errors->first('kepada'))
                    <div class="field noborder error">
                        {{ $errors->first('kepada') }}
                    </div>
                @endif
                <div class="field kepadacustom">
                    {!! Form::label('kepada_custom', null, ["class"=>"ion-person"]) !!}
                    {!! Form::text('kepada_custom', $kepadaC, ['id' => 'kepada_custom', "placeholder"=>"Kepada (contoh. Direktur)"]) !!}
                </div>
                @if($errors->first('kepada_custom'))
                    <div class="field noborder error">
                        {{ $errors->first('kepada_custom') }}
                    </div>
                @endif
                <div class="field">
                    {!! Form::label('dimana', null, ["class"=>"ion-location"]) !!}
                    {!! Form::text('dimana', null, ['id' => 'dimana', "placeholder"=>"Lokasi (contoh. Purwokerto)"]) !!}
                </div>
                @if($errors->first('dimana'))
                    <div class="field noborder error">
                        {{ $errors->first('dimana') }}
                    </div>
                @endif
                <div class="field noborder center">
                    {!! Form::submit('Simpan', ['class' => 'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $kepada = $('.kepada').val();
            if($kepada != 'lainnya') {
                $('.kepadacustom').hide();
            }

            $('.kepada').change(function(){
                var kepada 	= $('.kepada').val();
                if(kepada == 'lainnya'){
                    $('.kepadacustom').fadeIn('normal');
                }else{
                    $('.kepadacustom').fadeOut('normal');
                }
            });
        });
    </script>
@endsection

@extends('layouts.header')