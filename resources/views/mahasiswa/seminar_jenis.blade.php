@extends('layouts.footer')

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    pengajuan seminar
@endsection

@section('bar-page_subtitle')
    jadwal penggunaan ruangan seminar
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    @if(session()->has('errseminar'))
        <div class="kp-status">
            <h3 class="title">{{ session()->get('errseminar') }}</h3>
        </div>
    @endif
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> JENIS SEMINAR
                </div>
            </div>
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>['SeminarController@setJenis', $mhs->nim]]) !!}
                <div class="field">
                    {!! Form::label("jenis", null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('jenis', ['kp'=>'Seminar Kerja Praktik', 'semprop' => 'Seminar Proposal TA', 'semhas' => 'Seminar Hasil TA', 'kolokium'=>'Seminar Kolokium'],
                            null, ['placeholder'=>'Pilih Jenis Seminar']) !!}
                    </div>
                </div>
                @if($errors->has('jenis'))
                <div class="field noborder error">
                    {{ $errors->first('jenis') }}
                </div>
                @endif
                <div class="field noborder center">
                    {{ Form::submit('Selanjutnya', ['class'=>'button']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')