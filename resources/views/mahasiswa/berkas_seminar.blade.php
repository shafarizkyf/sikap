@extends('layouts.footer')

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    progress kerja praktik
@endsection

@section('bar-page_subtitle')
    progress kerja praktik anda
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> diajukan pada, {{ $berkas->created_at->format('d F Y') }}
                </div>
            </div>
            <div class="main">
                <div class="list">
                    <label for="tempatkp" class="ion-android-document"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">KSM</h2>
                        <p class="desc">
                            Kartu Studi Mahasiswa : <a href="{{ route('download_berkas', $berkas->ksm) }}">Download</a> |
                            <a href="{{ route('view_file', array('ksm', $mhs->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-android-document"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Scan TTD Dosen pada Draft Laporan </h2>
                            <p class="desc">
                                Scan  : <a href="{{ route('download_berkas', $berkas->makalah) }}">Download</a> |
                                <a href="{{ route('view_file', array('makalah', $mhs->nim)) }}" target="_blank">Lihat</a>
                            </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-android-document"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Surat Selesai Kerja Praktik</h2>
                        <p class="desc">
                            Surat Selesai Kerja Praktik : <a href="{{ route('download_berkas', $berkas->surat_selesai) }}">Download</a> |
                            <a href="{{ route('view_file', array('suratselesai', $mhs->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-android-document"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Lembar Presensi Kerja Praktik</h2>
                        <p class="desc">
                            Lembar Presensi Kerja Praktik : <a href="{{ route('download_berkas', $berkas->surat_spk) }}">Download</a> |
                            <a href="{{ route('view_file', array('suratspk', $mhs->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-android-document"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Kartu Seminar</h2>
                        <p class="desc">
                            Kartu Seminar : <a href="{{ route('download_berkas', $berkas->kartu_seminar) }}">Download</a> |
                            <a href="{{ route('view_file', array('kartuseminar', $mhs->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
                @if(!$berkas->status_berkas and !$berkas->status_koreksi or $berkas->status_berkas == 'koreksi')
                <div class="kp-status">
                    <h3 class="title">status pengajuan : menunggu</h3>
                    <p class="desc">Sebelum Anda dapat memilih jadwal seminar, Bapendik akan melakukan pengecekan berkas
                    seminar. Setelah berkas dinyatakan lengkap, Anda dapat memilih jadwal seminar</p>
                </div>
                @endif
            </div>
        </div>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')