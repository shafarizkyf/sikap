@extends('layouts.footer')

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    pengajuan seminar
@endsection

@section('bar-page_subtitle')
    jadwal penggunaan ruangan seminar
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <?php
        $semc  = new \App\Http\Controllers\SeminarController();
        $jenis = $semc->descSession(session()->get('seminar'));
    ?>
    <div class="kp-status">
        <p class="desc">Anda memilih jadwal {{ $jenis }}.
            <a href="{{ route('seminar_clear_session') }}">Ganti Seminar</a>
        </p>
    </div>
    @if(session()->has('jadwal'))
    <div class="kp-status">
        <h3 class="title">{{ session()->get('jadwal') }}</h3>
    </div>
    @endif
    <div class="content withmenu tableinside" style="height: calc( 100% - 150px ) !important;">
        <table class="seminar">
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    Jadwal Seminar
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="11">
                    {!! Form::open(['method'=>'get', 'action'=>['MahasiswaDaftarKpController@menu', 'seminar']]) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('ruangan', []+$ruangan, $listRuangan) !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="center">Tanggal</td>
                <td class="center">SHIFT 1<span>(JAM 7-8)</span></td>
                <td class="center">SHIFT 2<span>(JAM 8-9)</span></td>
                <td class="center">SHIFT 3<span>(JAM 9-10)</span></td>
                <td class="center">SHIFT 4<span>(JAM 10-11)</span></td>
                <td class="center">SHIFT 5<span>(JAM 11-12)</span></td>
                <td class="center">SHIFT 6<span>(JAM 12-13)</span></td>
                <td class="center">SHIFT 7<span>(JAM 13-14)</span></td>
                <td class="center">SHIFT 8<span>(JAM 14-15)</span></td>
                <td class="center">SHIFT 9<span>(JAM 15-16)</span></td>
                <td class="center">SHIFT 10<span>(JAM 16-17)</span></td>
            </tr>
            </thead>
            <tbody>
            @for($x=0; $x < $jumlahHari; $x++, $today->addDay())
                <tr>
                @if($today->isSunday())
                    @continue
                @endif

                @for($i=0; $i<=10; $i++)
                    <?php
                        $sc         = new \App\Http\Controllers\SeminarController();
                        $shift      = $sc->shift($i);
                        $j          = $today->format('Y-m-d').' '.$shift;
                        $jadwal     = \App\MhsSeminar::where('jadwal', $j)->where('ruangan_id', $listRuangan)->first();
                        $nama       = null;

                        if($jadwal){
                            $mhsj       = \App\ListMahasiswa::find($jadwal->nim_id);
                            $nama       = $semc->trimString($jadwal->nim->nama, 8);
                            $jurusan    = $semc->trimString($jadwal->nim->jurusan->jurusan, 14);
                        }
                    ?>

                    @if($i==0)
                        <td class="center caption">
                            {{ $today->format('F') }}
                            <span class="big">{{ $today->format('d') }}</span>
                            {{ $today->format('Y') }}
                        </td>
                    @endif

                    @if($i != 0 and $jadwal)
                        <td class="center">
                            @if(!$jadwal->status and ($mhs->nim == $jadwal->nim->nim))
                            <a href="{{ route('seminar_delete', $jadwal->id) }}"><span class="mark ion-close-circled"></span></a>
                            @elseif($jadwal->status)
                            <span class="mark ion-checkmark-circled"></span>
                            @endif
                            {{ strtoupper($jadwal->jenis) }}<br>
                            {{ $jadwal->nim->nim }}<br>
                            {{ $nama }}
                            <span>{{ $jurusan }}</span>
                        </td>

                        @elseif($i != 0 and !$jadwal and ($jumlah < 2 or $seminarUlang) and !$seminarSelesai)
                            @if($jumlahUlang < 2 or $jumlah < 2)
                                <td class="center">
                                    <div class="buttonwrapper">
                                        {!! Form::open(['method'=>'post', 'action'=>'SeminarController@reqJadwalSeminar']) !!}
                                        {!! Form::hidden('jadwal', $today->format('Y-m-d').'/'.$i.'/'.$mhs->id.'/'.$listRuangan) !!}
                                        {!! Form::submit('Ajukan', ['class'=>'button small']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </td>
                            @else
                                <td class="center"></td>
                            @endif
                        @elseif($i != 0 and !$jadwal)
                        <td class="center"></td>
                    @endif
                @endfor
                </tr>
            @endfor
            </tbody>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')