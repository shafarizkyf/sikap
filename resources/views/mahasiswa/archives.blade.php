@extends('layouts.footer')

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    arsip
@endsection

@section('bar-page_subtitle')
    arsip pengajuan kp sebelumnya
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items active">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> diusulkan {{ $mhsUlang->waktu_daftar->format('d M Y') }}
                </div>
            </div>
            <div class="main">
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Tempat Kerja Praktik</h2>
                        <p class="desc">
                            {{ $mhsUlang->tempat }}
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-ios-lightbulb"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Topik Proposal Kerja Praktik</h2>
                        <p class="desc">
                            {{ ($mhsUlang->judul_final) ? $mhsUlang->judul_final : $mhsUlang->judul }}
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-paperclip"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Berkas Proposal</h2>
                        <p class="desc">
                            Outline : <a href="{{ route('download_berkas', $mhsUlang->outline) }}">Download</a> | <a href="{{ route('view_file', array('outline', $mhsUlang->nim->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-paperclip"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Surat Pengantar</h2>
                        <p class="desc">
                            Surat Pengantar : <a href="{{ route('download_berkas', $mhsUlang->surat) }}">Download</a> |     <a href="{{ route('view_file', array('surat', $mhsUlang->nim->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')