@extends('layouts.footer')

@section('header-content1')
    <div class="content">
        <div class="panel">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-person icon"></span>
                    Halo, {{ $username }}
                </div>
            </div>
            <div class="login-header" style="width: 100%">
                <div class="text">
                    Pengajuan pergantian dosen pembimbing
                </div>
            </div>
            <div class="main">
                <div class="kp-status">
                    <h3 class="title">Perhatian</h3>
                    <p class="desc">Sebelum mengajukan pergantian pembimbing, pastikan anda sudah mendapat izin dari komisi studi akhir terlebih dahulu.</p>
                </div>
                {!! Form::model($dosbing, ['method'=>'post', 'action'=>['MahasiswaDaftarKpController@updateDosbing', $dosbing->nim_id]]) !!}
                <div class="field">
                    {!! Form::label('dosen', null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('dosen', $dosen, null, ['placeholder'=>'Pilih Dosen Pembimbing Utama']) !!}
                </div>
                </div>
                @if($errors->first('dosen'))
                    <div class="field noborder error">
                        {{ $errors->first('dosen') }}
                    </div>
                @endif
                <div class="field">
                    {!! Form::label('dosen2', null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('dosen2', $dosen, null, ['placeholder'=>'Pilih Dosen Pembimbing Anggota']) !!}
                    </div>
                </div>
                @if($errors->first('dosen2'))
                    <div class="field noborder error">
                        {{ $errors->first('dosen2') }}
                    </div>
                @endif
                <div class="field textarea">
                    <label for="alasan" class="ion-ios-lightbulb"></label>
                    {!! Form::textarea('alasan', null, ['id'=>'alasan','placeholder'=>'Alasan pengajuan' ]) !!}
                </div>
                @if($errors->first('alasan'))
                    <div class="field noborder error">
                        {{ $errors->first('alasan') }}
                    </div>
                @endif
                <div class="field noborder center">
                    {!! Form::submit('Ajukan', ['class' => 'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.header')