@extends('layouts.footer')

@section('sidebar-link_usulan')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    pengajuan kerja praktik
@endsection

@section('bar-page_subtitle')
    isilah form dengan cermat
@endsection

@if($mhsUlang)
    @section('sidebar-add_link1')
        <li class="items">
            <a href="{{ route('mahasiswa_menu', 'archives') }}">
                <span class="ion-ios-paperplane icon"></span>
                archives
            </a>
        </li>
    @endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> form usulan kp
                </div>
            </div>
            <div class="main">
                @if(count($surat) > 0)
                {!! Form::open(['method'=>'post', 'action'=>'MahasiswaDaftarKpController@simpanPengajuan', 'files'=>true]) !!}
                <div class="field">
                    {!! Form::label('instansi', null, ['class' => 'ion-location']) !!}
                    <div class="styled-select">
                        {!! Form::select('instansi', [''=>'Tempat Kerja Praktik'] + $surat) !!}
                    </div>
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('instansi') : '' }}
                </div>
                <div class="field">
                    {!! Form::label('judul', null, ['class' => 'ion-ios-lightbulb']) !!}
                    {!! Form::text('judul', null, ['placeholder' => 'Topik Proposal Kerja Praktik']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('judul') : '' }}
                </div>
                <div class="field">
                    {!! Form::label("dosbing1", null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('dosbing1', [''=>'Usulan Dosen Pembimbing Utama'] + $dosbing) !!}
                    </div>
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('dosbing1') : '' }}
                </div>
                <div class="field">
                    {!! Form::label("dosbing2", null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('dosbing2', [''=>'Usulan Dosen Pembimbing KP Anggota'] + $dosbing) !!}
                    </div>
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('dosbing2') : '' }}
                </div>
                <div class="field">
                    <label for="outline" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Upload Outline Proposal Kerja Praktik (.pdf)
                    </label>
                    {!! Form::file('outline', ['id'=>'outline']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('outline') : '' }}
                </div>
                <div class="field">
                    <label for="surat" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Scan Surat Penerimaan Instansi
                        dan Transkrip (1 file pdf)
                    </label>
                    {!! Form::file('surat', ['id'=>'surat']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('surat') : '' }}
                </div>
                <div class="field noborder center">
                    {!! Form::submit('AJUKAN USULAN', ['class'=>'button']) !!}
                </div>
                {!! Form::close() !!}
                    @else
                    <div class="login-header">
                        <div class="text">
                            Anda belum membuat Surat Pengantar KP
                            <div class="field noborder center">
                                <a href="{{ route('surat_pengantar') }}" class="button">Buat Surat</a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')