@extends('layouts.footer')


@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    Surat Pengantar Kerja Praktik
@endsection

@section('bar-page_subtitle')
    pengajuan surat pengantar kerja praktik kamu buat
@endsection

@section('sidebar-link_surat')
    active
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span>Surat Pengantar
                </div>
            </div>
            <div class="main">
                @if(count($surat) > 0)
                    @foreach($surat as $s)
                        <div class="list">
                            <label for="tempatkp" class="ion-android-mail"></label>
                            <div class="item" id="tempatkp">
                                <h2 class="title">Surat Pengajuan Instansi</h2>
                                <p class="desc">
                                    {{ $s->instansi }}
                                    <a href="{{ route('cetak_form_surat_pengantar', $mhs->nim) }}" target="_blank">Cetak Form Pengajuan</a> 
                                    {{-- <a href="{{ route('edit_surat', $s->id) }}">Edit</a> --}}
                                </p>
                            </div>
                        </div>
                    @endforeach
                    @foreach($surat as $s)
                        <div class="list">
                            <div class="kp-status">
                                @if($s->status == 'menunggu')
                                    <h3 class="title">status surat {{ $s->instansi }}: {{ $s->status }}</h3>
                                    <p>Diajukan, {{ $s->waktu_ajukan->toFormattedDateString() }}</p>
                                    <p class="desc"> Berkas akan dicetak dan diproses oleh Bapendik </p>
                                @elseif($s->status == 'selesai')
                                    <h3 class="title">Surat pengantar {{ $s->instansi }} dapat diambil pada {{ $s->waktu_selesai->format('d M Y') }}</h3>
                                    <p class="title">Diajukan, {{ $s->waktu_ajukan->format('d M Y') }}</p>
                                @endif
                            </div>
                        </div>
                    @endforeach
                @else
                <div class="login-header">
                    <div class="text">
                        Anda belum membuat Surat Pengantar KP
                    </div>
                </div>
                @endif
                <div class="field center">
                    @if($jSurat < 2 and !$mhskp)
                        <a href="{{ route('surat_pengantar') }}" class="button">Buat Baru</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')