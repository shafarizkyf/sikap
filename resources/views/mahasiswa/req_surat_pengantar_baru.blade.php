@extends('layouts.footer')


@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    Surat Pengantar Kerja Praktik
@endsection

@section('bar-page_subtitle')
    buat surat pengantar kp baru
@endsection

@section('sidebar-link_surat')
    active
@endsection

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> Pengajuan Surat Pengantar
                </div>
            </div>
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>'MahasiswaDaftarKpController@reqSuratPengantarKp']) !!}
                <div class="field">
                    {!! Form::label('instansi', null, ["class"=>"ion-email"]) !!}
                    {!! Form::text('instansi', null, ['id' => 'instansi', "placeholder"=>"Nama Instansi/Perusahaan"]) !!}
                </div>
                @if($errors->first('instansi'))
                    <div class="field noborder error">
                        {{ $errors->first('instansi') }}
                    </div>
                @endif
                <div class="field">
                    {!! Form::label('kepada', null, ['class' => 'ion-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('kepada', ['Pimpinan'=>'Pimpinan', 'Kepala Sekolah'=>'Kepala Sekolah', 'lainnya'=>'lainnya'], null, ['placeholder'=>'Orang dituju', 'class'=>'kepada']) !!}
                    </div>
                </div>
                @if($errors->first('kepada'))
                    <div class="field noborder error">
                        {{ $errors->first('kepada') }}
                    </div>
                @endif
                <div class="field kepadacustom">
                    {!! Form::label('kepada_custom', null, ["class"=>"ion-person"]) !!}
                    {!! Form::text('kepada_custom', null, ['id' => 'kepada_custom', "placeholder"=>"Kepada (contoh. Direktur)"]) !!}
                </div>
                @if($errors->first('kepada_custom'))
                    <div class="field noborder error">
                        {{ $errors->first('kepada_custom') }}
                    </div>
                @endif
                <div class="field">
                    {!! Form::label('dimana', null, ["class"=>"ion-location"]) !!}
                    {!! Form::text('dimana', null, ['id' => 'dimana', "placeholder"=>"Masukkan alamat lengkap (jln, kec, kab, kdpos)"]) !!}
                </div>
                @if($errors->first('dimana'))
                    <div class="field noborder error">
                        {{ $errors->first('dimana') }}
                    </div>
                @endif
                <div>
                    <input type="checkbox" name="periode" id="periode">
                    <span>Tetapkan Periode KP</span>                    
                </div>
                <div class="periode">
                    <div class="field">
                        {!! Form::label('periode_dari', null, ["class"=>"ion-calendar"]) !!}
                        {!! Form::text('periode_dari', null, ['id'=>'periode_dari', 'placeholder'=>"Periode KP (dari)"]) !!}
                    </div>
                    @if($errors->first('periode_dari'))
                        <div class="field noborder error">
                            {{ $errors->first('periode_dari') }}
                        </div>
                    @endif
                    <div class="field">
                        {!! Form::label('periode_ke', null, ["class"=>"ion-calendar"]) !!}
                        {!! Form::text('periode_ke', null, ['id'=>'periode_ke', 'placeholder'=>"Periode KP (hingga)"]) !!}
                    </div>
                    @if($errors->first('periode_ke'))
                        <div class="field noborder error">
                            {{ $errors->first('periode_ke') }}
                        </div>
                    @endif                    
                </div>                
                <div class="field noborder center">
                    {!! Form::submit('Buat Surat', ['class' => 'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.periode').hide();         
            $('#periode').change(function(){
                var periode = $('#periode');
                if(periode.prop('checked')){
                    $('.periode').fadeIn('normal');                    
                }else{
                    $('.periode').fadeOut('normal');                    
                }
            });

            $('.kepadacustom').hide();
            $('.kepada').change(function(){
                var kepada  = $('.kepada').val();
                if(kepada == 'lainnya'){
                    $('.kepadacustom').fadeIn('normal');
                }else{
                    $('.kepadacustom').fadeOut('normal');
                }
            });
            $('#periode_dari').datepicker();
            $('#periode_ke').datepicker();            
        });
    </script>
@endsection

@extends('layouts.header')