@extends('layouts.footer')

@section('sidebar-link_usulan')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    progress kerja praktik
@endsection

@section('bar-page_subtitle')
    progress kerja praktik anda
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> diusulkan {{ $mhskp->waktu_daftar->format('d M Y') }}
                </div>
            </div>
            <div class="main">
                @if($nilai and $progress->status_distribusi)
                <?php $nilaiHuruf = \App\Http\Controllers\NilaiController::convertToHuruf($nilai->nilai_akhir); ?>
                <div class="list">
                    <label for="tempatkp" class="ion-paperclip"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Nilai Kerja Praktik</h2>
                        <p class="desc">
                            Nilai : {{ $nilai->nilai_akhir }} / {{ $nilaiHuruf }}
                        </p>
                    </div>
                </div>
                @endif
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Tempat Kerja Praktik</h2>
                            <p class="desc">
                                {{ $mhskp->tempat->instansi }}
                            </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-ios-lightbulb"></label>
                    <div class="item" id="tempatkp">
                        @if($mhskp->judul_final)
                        <h2 class="title">Judul Proposal Kerja Praktik</h2>
                            <p class="desc">
                                {{ $mhskp->judul_final }}
                            </p>
                        @else
                            <h2 class="title">Topik Proposal Kerja Praktik</h2>
                            <p class="desc">
                                {{ $mhskp->judul }}
                            </p>
                        @endif
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-person"></label>
                    <div class="item" id="tempatkp">
                    @if($progress->status_judul != 'layak')
                        <h2 class="title">Usulan Dosen Pembimbing Kerja Praktik</h2>
                            <p class="desc">
                                {{ $dosbing->dosbing1->nama }} {{ ($dosbing->dosbing2) ? ' & '.$dosbing->dosbing2->nama : '' }}
                            </p>
                    @else
                        <?php $gantiDosbing = \App\MhsGantiDosbing::where('nim_id', $mhskp->nim_id)->first(); ?>
                        <h2 class="title">Dosen Pembimbing Kerja Praktik</h2>
                            <p class="desc">
                                {{ $dosbing->dosbing1->nama }} {{ ($dosbing->dosbing2) ? ' & '.$dosbing->dosbing2->nama : '' }}
                                <br>
                                 @if($gantiDosbing)
                                    @if($gantiDosbing->status == 'diterima')
                                        Pergantian Dosen Diterima
                                    @elseif($gantiDosbing->status == 'ditolak')
                                        Pegantian Dosen Ditolak
                                    @else
                                        Sedang Mengajukan Pergantian Dosen Pembimbing
                                    @endif
                                @else
                                    <a href="{{ route('mhs_edit_dosbing', $dosbing->id) }}">Usulkan Pergantian</a>
                                @endif
                            </p>
                    @endif
                    </div>
                </div>
                @if($dosbing->pemlap)
                <div class="list">
                    <label for="tempatkp" class="ion-person"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Pembimbing Lapangan</h2>
                        <p class="desc">
                            {{ $dosbing->pemlap }}
                        </p>
                    </div>
                </div>
                @endif
                @if($progress->waktu_spk_mulai and ($progress->waktu_spk_mulai != \Carbon\Carbon::now()))
                    <div class="list">
                        <label for="tempatkp" class="ion-android-calendar"></label>
                        <div class="item" id="tempatkp">
                            <h2 class="title">Masa SPK</h2>
                            <p class="desc">
                                {{ $progress->waktu_spk_mulai->format('d M Y') }} s.d {{ $progress->waktu_spk_selesai->format('d M Y') }}
                            </p>
                        </div>
                    </div>
                @endif
                <div class="list">
                    <label for="tempatkp" class="ion-paperclip"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Outline Proposal Kerja Praktik</h2>
                            <p class="desc">
                                Outline : <a href="{{ route('download_berkas', $mhskp->outline) }}">Download</a> | <a href="{{ route('view_file', array('outline', $mhskp->nim->nim)) }}" target="_blank">Lihat</a>
                            </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-paperclip"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Surat Penerimaan Lokasi KP</h2>
                        <p class="desc">
                            Surat Penerimaan : <a href="{{ route('download_berkas', $mhskp->surat) }}">Download</a> |     <a href="{{ route('view_file', array('surat', $mhskp->nim->nim)) }}" target="_blank">Lihat</a>
                        </p>
                    </div>
                </div>
                @if(!$progress->status_judul)
                    @if(!$progress->status_berkas)
                    <div class="kp-status">
                        <h3 class="title">status berkas : {{ ($progress->status_berkas) ? $progress->status_berkas : 'Menunggu' }}</h3>
                        <p class="desc">Berkas akan dicek oleh bapendik terlebih dahulu sebelum dilanjutkan ke komisi</p>
                    </div>
                    @elseif($progress->status_berkas == 'lengkap')
                        <div class="kp-status">
                            <h3 class="title">status berkas : {{ ($progress->status_berkas) ? $progress->status_berkas : 'Menunggu' }}</h3>
                            <p class="desc">Berkas yang lengkap akan dirapatkan oleh komisi jurusan untuk menentukan apakah layak
                                atau perlu direvisi</p>
                        </div>
                    @endif
                @elseif($progress->status_judul == 'layak')
                    <div class="kp-status">
                        <h3 class="title">status judul kp : {{ ($progress->status_judul) ? $progress->status_judul : 'Menunggu' }}</h3>
                        <p class="desc">Selamat komisi menyatakan Anda layak KP. Langkah selanjutnya adalah menunggu SPK. Kemudian
                            silahkan melakukan bimbingan dengan dosen pembimbing dan pembimbing lapangan dan
                            materi bimbingan dicatat dalam sistem secara berkala.</p>
                    </div>
                @endif
                @if($progress->status_berkas == 'koreksi' and $progress->status_koreksi == 'belum')
                    <div class="field noborder center">
                        <a href="#ex1" aria-nim="{{ $mhskp->id }}" rel="modal:open" class="button" id="coba-btn">
                            <span class="ion-eye icon"></span>
                            Koreksi
                        </a>
                    </div>
                @endif
                @if($progress->status_judul == 'revisi')
                    <div class="field noborder center">
                        <a href="#ex1" aria-nim="{{ $mhskp->id }}" rel="modal:open" class="button" id="koreksi-komisi">
                            <span class="ion-eye icon"></span>
                            Koreksi
                        </a>
                    </div>
                @elseif($progress->status_judul == 'tolak')
                    <br>
                    <div class="kp-status">
                        <h3 class="title">status judul : {{ $progress->status_judul }}</h3>
                            <p class="desc">{{ $mhskp->revisi }}</p>
                    </div>
                    {!! Form::open(['method'=>'post', 'action'=>['MahasiswaDaftarKpController@revisi', $mhskp->nim_id]]) !!}
                    <div class="field noborder center">
                        {!! Form::submit('Mulai Ulang', ['class'=>'button']) !!}
                    </div>
                    {!! Form::close() !!}
                @endif
                @if($progress->status_judul == 'layak' and !$dosbing->pemlap)
                    <div class="field noborder center">
                        <a href="#ex2" aria-nim="{{ $mhskp->id }}" rel="modal:open" class="button pemlap">
                            <span class="ion-person icon"></span>
                            Set Pembimbing Lapangan
                        </a>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
    <div id="ex2" class="small" style="display:none;">
        <div class="tag">
            <div class="tag-content">Set Pembimbing Lapangan</div>
        </div>
        <div class="modal-content">
            {!! Form::open(['method'=>'post', 'action'=>['ModalController@setPemlap',$mhskp->nim_id]]) !!}
            <div class="field">
                {!! Form::label('nama', null, ['class' => 'ion-person']) !!}
                {!! Form::text('nama', null, ['id' => 'nama', 'placeholder' => 'Nama Pembimbing Lapangan']) !!}
            </div>
            <div class="field">
                {!! Form::label('telp', null, ['class' => 'ion-person']) !!}
                {!! Form::text('telp', null, ['id' => 'telp', 'placeholder' => 'Nomor Kontak Pembimbing Lapangan']) !!}
            </div>
            <div class="field noborder center">
                {!! Form::submit('simpan', ['class'=>'button']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script src="{{ asset('js/sikap/mahasiswa_progress.js') }}"></script>
@endsection

@extends('layouts.header')