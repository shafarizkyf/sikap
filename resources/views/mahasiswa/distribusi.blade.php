@extends('layouts.footer')

@section('sidebar-link_distribusi')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    distribusi berkas kp
@endsection

@section('bar-page_subtitle')
    unggah berkas distribusi kerja praktik
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> form disribusi laporan
                </div>
            </div>
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>['DistribusiController@save', $mhs->id], 'files'=>true]) !!}
                <div class="field">
                    <label for="formdis" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Form Distribusi (.pdf)
                    </label>
                    {!! Form::file('formdis', ['id'=>'formdis']) !!}
                </div>
                @if($errors->first('formdis'))
                    <div class="field noborder error">
                        {{ $errors->first('formdis') }}
                    </div>
                @endif
                <div class="field">
                    <label for="pengesahan" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Pengesahan Laporan KP (.pdf)
                    </label>
                    {!! Form::file('pengesahan', ['id'=>'pengesahan']) !!}
                </div>
                @if($errors->first('pengesahan'))
                    <div class="field noborder error">
                        {{ $errors->first('pengesahan') }}
                    </div>
                @endif
                <div class="field noborder center">
                    {{ Form::submit('Simpan', ['class'=>'button']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@extends('layouts.header')