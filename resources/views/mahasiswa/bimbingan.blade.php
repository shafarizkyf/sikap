@extends('layouts.footer')

@section('sidebar-link_bimbingan')
    active
@endsection

@section('bar-username')
    {{ $mhskp->nim->nama }}
@endsection

@section('bar-page_title')
    pencatatan bimbingan kp
@endsection

@section('bar-page_subtitle')
    mohon diisi dengan benar
@endsection

@if($mhsUlang)
@section('sidebar-add_link1')
    <li class="items">
        <a href="{{ route('mahasiswa_menu', 'archives') }}">
            <span class="ion-ios-paperplane icon"></span>
            archives
        </a>
    </li>
@endsection
@endif

@section('bar-content')
    @if(!$progress->spk_berkas)
    <div class="kp-status">
        <p class="desc">Meskipun bimbingan Anda sudah di unlock oleh dosen pembimbing,
            namun saat ini berkas SPK Anda masih dalam proses pencetakan oleh bapendik, untuk melakukan bimbingan
            berkas SPK diperlukan sebagai bukti resmi.</p>
    </div>
    @endif
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    Catatan Bimbingan
                </td>
            </tr>
            <tr>
                <td class="fit">NO</td>
                <td class="fit">Tanggal</td>
                <td>Pembahasan</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            @if(count($bimbingan) > 0)
                <?php $no = 1 ?>
                @foreach($bimbingan as $b)
                <tr>
                    <td class="center">
                        <span class="big">{{ $no++ }}</span>
                    </td>
                    <td class="fit">
                        <span class="big">{{ $b->waktu_bimbingan->format('l') }}</span>
                        {{ $b->waktu_bimbingan->format('d F Y') }}
                    </td>
                    <td>{{ $b->bimbingan }}</td>
                    <td class="fit">
                        @if(!$bimbingan->first()->status)
                        <div class="buttonwrapper">
                            <a href="#ex2" aria-nim="{{ $b->id }}" rel="modal:open" class="button small materi">
                                <span class="ion-edit icon"></span>
                                UBAh
                            </a>
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5" class="right">

                    @if(!$status)
                    <a href="#ex1" aria-nim="{{ $mhskp->nim_id }}" rel="modal:open" class="button small" id="bimbingan">
                        <span class="ion-android-add icon"></span>
                        tambah catatan bimbingan
                    </a>
                    @endif

                    @if(count($bimbingan) >= $settings['bimbingan_minimum'] and !$status)
                        <a href="#ex2" aria-nim="{{ $mhskp->nim->nim }}" rel="modal:open" class="button small" id="finalisasi">
                            <span class="ion-checkmark icon"></span>
                            Finalisasi
                        </a>
                    @endif

                    @if($progress->status_bimbingan == 'selesai')
                    <a href="{{ route('cetak_kartu_kendali', $bimbingan->first()->nim->nim) }}" class="button small" target="_blank">
                        <span class="ion-checkmark icon"></span>
                        Cetak Kartu Kendali
                    </a>
                    @endif
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;">
        <div class="tag">
            <div class="tag-content">Catatan Bimbingan</div>
        </div>
        <div class="modal-content">
            {!! Form::open(['method'=>'post', 'action'=>'MahasiswaDaftarKpController@simpanBimbingan']) !!}
            <div class="field">
                {!! Form::label('tanggal', null, ['class' => 'ion-android-calendar']) !!}
                {!! Form::text('tanggal', null, ['placeholder'=>'Tanggal Bimbingan', 'id'=>'tanggal']) !!}
            </div>
            <div class="field textarea">
                <label for="bimbingan" class="ion-ios-lightbulb"></label>
                {!! Form::textarea('bimbingan', null, ['id'=>'bimbingan','placeholder'=>'Materi Bimbingan' ]) !!}
            </div>
            <div class="field noborder center">
                {{ Form::submit('Simpan', ['class'=>'button']) }}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div id="ex2" class="small" style="display:none;"></div>

@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(function () {
                $('#tanggal').datepicker();
            });

            $('#bimbingan').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            $('#finalisasi').click(function () {
                var nim = this.getAttribute('aria-nim');
                setJudulFinal(nim);
            });

            $('.materi').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/bimbingan/' ?>" + nim,
                    success: function (data) {
                        $('#ex2').html(data);
                    }
                });
            }

            function setJudulFinal(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/judul/' ?>" + nim,
                    success: function (data) {
                        $('#ex2').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')