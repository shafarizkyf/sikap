@extends('layouts.footer')

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    progress kerja praktik
@endsection

@section('bar-page_subtitle')
    progress kerja praktik anda
@endsection

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> diajuakn pada, {{ $berkas->created_at->format('d F Y') }}
                </div>
            </div>
            <div class="main">
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Makalah Seminar</h2>
                        <p class="desc">
                            Unduh : <a href="{{ route('download_berkas', $berkas->makalah) }}">Acc Ttd Makalah</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Surat Selesai Kerja Praktik</h2>
                        <p class="desc">
                            Unduh : <a href="{{ route('download_berkas', $berkas->surat_selesai) }}">Surat Selesai KP</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">SPK dan Surat Kerja Pembimbing</h2>
                        <p class="desc">
                            Unduh : <a href="{{ route('download_berkas', $berkas->surat_spk) }}">SPK dan Surat Kerja Pembimbing</a>
                        </p>
                    </div>
                </div>
                <div class="list">
                    <label for="tempatkp" class="ion-location"></label>
                    <div class="item" id="tempatkp">
                        <h2 class="title">Kartu Seminar</h2>
                        <p class="desc">
                            Unduh : <a href="{{ route('download_berkas', $berkas->kartu_seminar) }}">Kartu Seminar</a>
                        </p>
                    </div>
                </div>
                <div class="kp-status">
                    <p class="desc">Berkas akan kembali dicek oleh bapendik. Setelah berkas anda dinyatakan lengkap, anda
                    dapat memilih ruang dan jadwal seminar</p>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content_mhsta')

@extends('layouts.header')