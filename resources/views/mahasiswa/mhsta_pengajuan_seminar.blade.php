@extends('layouts.footer')

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-username')
    {{ $mhs->nama }}
@endsection

@section('bar-page_title')
    pengajuan seminar
@endsection

@section('bar-page_subtitle')
    unggah berkas pengajuan seminar
@endsection

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-paper icon"></span> form usulan seminar
                </div>
            </div>
            <div class="main">
                @if(!$berkas)
                    {!! Form::open(['method'=>'post', 'action'=>'SeminarController@saveBerkasSeminar', 'files'=>true]) !!}
                    @elseif($berkas->status_berkas == 'koreksi')
                    {!! Form::open(['method'=>'post', 'action'=>['SeminarController@updateBerkasSeminar', $mhs->id], 'files'=>true]) !!}
                @endif
                <div class="field">
                    <label for="makalah" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Scan TTD ACC Dosen Pembimbing pada Makalah (.pdf)
                    </label>
                    {!! Form::file('makalah', ['id'=>'makalah']) !!}
                </div>
                @if($errors->first('makalah'))
                <div class="field noborder error">
                    {{ $errors->first('makalah') }}
                </div>
                @endif
                <div class="field">
                    <label for="surat" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Surat Selesai Kerja Praktik dari Instansi/Perusahaan (.pdf)
                    </label>
                    {!! Form::file('surat', ['id'=>'surat']) !!}
                </div>
                @if($errors->first('surat'))
                    <div class="field noborder error">
                        {{ $errors->first('surat') }}
                    </div>
                @endif
                <div class="field">
                    <label for="kartu" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Bukti kehadiran (min. 10x) pada Kartu Seminar (.pdf)
                    </label>
                    {!! Form::file('kartu', ['id'=>'kartu']) !!}
                </div>
                @if($errors->first('kartu'))
                    <div class="field noborder error">
                        {{ $errors->first('kartu') }}
                    </div>
                @endif
                <div class="field">
                    <label for="spk" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        SPK dan Surat Tugas Pembimbing (.pdf)
                    </label>
                    {!! Form::file('spk', ['id'=>'spk']) !!}
                </div>
                @if($errors->first('spk'))
                    <div class="field noborder error">
                        {{ $errors->first('spk') }}
                    </div>
                @endif
                <?php
                $sberkas  = ($berkas) ? $berkas->status_berkas : 'baru';
                $skoreksi = ($berkas) ? $berkas->status_koreksi : 'baru';
                ?>
                @if(!$berkas)
                    <div class="field noborder center">
                        {{ Form::submit('Ajukan', ['class'=>'button']) }}
                    </div>
                @elseif($sberkas != 'lengkap' and $skoreksi != 'sudah')
                    <div class="field noborder error">
                        Pesan bapendik : {{ $berkas->kesimpulan }}
                    </div>
                    <div class="field noborder center">
                        {{ Form::submit('Koreksi', ['class'=>'button']) }}
                    </div>
                @endif
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.mahasiswa.sidebar_content_mhsta')

@extends('layouts.header')