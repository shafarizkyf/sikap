@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    Log aktivitas {{ explode(' ', $mhs->nama)[0] }}
@endsection

@section('bar-page_subtitle')
    rekaman aktivitas kp yang tercatat
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    log aktivitas - {{ $mhs->nama }}
                </td>
            </tr>
            <tr>
                <td>aktivitas</td>
                <td class="fit">waktu</td>
            </tr>
            </thead>
            <tbody>
                @if($mhs->nilai)
                <tr>
                    <td class="fit">Nilai KP Anda {{ $mhs->nilai->nilai_akhir }} / {{$mhs->nilai->nilai_huruf}} </td>
                    <td>{{ $mhs->nilai->created_at->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->berkasDistribusi)
                <tr>
                    <td class="fit">Distribusi KP</td>
                    <td>{{ $mhs->berkasDistribusi->created_at->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->progress and $mhs->progress->waktu_kp_selesai)
                <tr>
                    <td class="fit">KP Selesai, Menunggu nilai keluar</td>
                    <td>{{ $mhs->progress->waktu_kp_selesai->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->seminar)
                <tr>
                    <td class="fit">Mahasiswa Mengajukan Seminar</td>
                    <td>{{ $mhs->seminar->created_at->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->berkasSeminar and $mhs->berkasSeminar->status_berkas)
                <tr>
                    <td class="fit">Bapendik menyatakan Status Berkas Seminar {{ $mhs->berkasSeminar->status_berkas }}</td>
                    <td>{{ $mhs->berkasSeminar->updated_at->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->progress and $mhs->progress->waktu_spk_mulai and $mhs->progress->waktu_spk_selesai)
                <tr>
                    <td class="fit">Penentuan Masa SPK</td>
                    <td>{{ $mhs->progress->waktu_spk_mulai->format('d F Y') }} - {{ $mhs->progress->waktu_spk_selesai->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->progress and $mhs->progress->waktu_spk_jadi)
                <tr>
                    <td class="fit">Bapendik memproses SPK KP </td>
                    <td>{{ $mhs->progress->waktu_spk_jadi->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->progress and $mhs->progress->waktu_judul_layak)
                <tr>
                    <td class="fit">Komisi Menyatakan Kelayakan Usulan KP</td>
                    <td>{{ $mhs->progress->waktu_judul_layak->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->progress and $mhs->progress->waktu_berkas_lengkap)
                <tr>
                    <td class="fit">Bapendik Menyatakan Kelengkapan Berkas Usulan KP</td>
                    <td>{{ $mhs->progress->waktu_berkas_lengkap->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->kp and $mhs->kp->waktu_daftar)
                <tr>
                    <td class="fit">Mahasiswa Mengajukan Usulan KP</td>
                    <td>{{ $mhs->kp->waktu_daftar->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->surat and $mhs->surat->waktu_selesai)
                <tr>
                    <td class="fit">Bapendik Membuat Pengajuan Surat Pengantar KP</td>
                    <td>{{ $mhs->surat->waktu_selesai->format('d F Y') }}</td>
                </tr>
                @endif
                @if($mhs->surat)
                <tr>
                    <td class="fit">Mahasiswa Mengajukan Surat Pengantar KP</td>
                    <td>{{ $mhs->surat->waktu_ajukan->format('d F Y') }}</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@extends('layouts.header')