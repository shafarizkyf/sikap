@extends('layouts.footer')

@section('sidebar-link_dosbing')
    active
@endsection

@section('bar-username')
    {{ $dosen->nama }}
@endsection

@section('bar-page_title')
    kendali bimbingan kp
@endsection

@section('bar-page_subtitle')
    catatan bimbingan kp
@endsection

@section('bar-content')
    @if(count($bimbingan) > 0)
        <div class="content withmenu tableinside">
            <table>
                <thead>
                <tr class="tabletitle">
                    <td colspan="9">
                        {{ $bimbingan->first()->nim->nama }}
                    </td>
                </tr>
                <tr>
                    <td class="fit">NO</td>
                    <td class="fit">Tanggal</td>
                    <td>Pembahasan</td>
                </tr>
                </thead>
                <tbody>
                <?php $no = 1 ?>
                @foreach($bimbingan as $b)
                    <tr>
                        <td class="center">
                            <span class="big">{{ $no++ }}</span>
                        </td>
                        <td class="fit">
                            <span class="big">{{ $b->waktu_bimbingan->format('l') }}</span>
                            {{ $b->waktu_bimbingan->format('d F Y') }}
                        </td>
                        <td>{{ $b->bimbingan }}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="5" class="right">
                        @if($progress->status_bimbingan == 'aktif' and $bimbingan->first()->status == 'finalisasi')
                            <a href="#ex2" aria-nim="{{ $bimbingan->first()->nim->nim }}" rel="modal:open" class="button small" id="finalisasi">
                                <span class="ion-checkmark icon"></span>
                                Bimbingan Selesai
                            </a>
                        @endif
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    @else
        <div class="kp-status">
            <h3 class="title">Belum ada catatan bimbingan</h3>
        </div>
    @endif
    <div id="ex2" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#finalisasi').click(function () {
                var nim = this.getAttribute('aria-nim');
                getJudulFinal(nim);
            });

            function getJudulFinal(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/judulkp/' ?>" + nim,
                    success: function (data) {
                        $('#ex2').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')