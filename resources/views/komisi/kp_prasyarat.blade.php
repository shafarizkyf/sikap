@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    status prasyarat kp
@endsection

@section('bar-page_subtitle')
    mahasiswa {{ $komisi->jurusan->jurusan }}
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('komisi_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_progress') }}">sedang kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_ulang') }}">Ulang KP</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_menu', 'gantidosbing') }}">Penggantian Pembimbing</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    usulan kp mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td class="fit">SKS</td>
                <td class="fit">IPK</td>
                <td class="fit">No. Telp</td>
                <td class="fit">Status</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td class="fit">{{ $m->sks }}</td>
                        <td class="fit">{{ $m->ipk }}</td>
                        <td class="fit">{{ $m->telp }}</td>
                        <td class="fit">{{ $m->status_prasyarat }}</td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@extends('layouts.header')