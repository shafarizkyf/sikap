@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    pengajuan kp {{ $komisi->jurusan->jurusan }}
@endsection

@section('bar-page_subtitle')
    pemberian status proposal kerja praktik
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('komisi_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('komisi_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_progress') }}">sedang kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_ulang') }}">Ulang KP</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_menu', 'gantidosbing') }}">Usulan Penggantian Pembimbing</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    usulan kp mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                    {!! Form::open(['method'=>'get', 'action'=>'KomisiController@kpUsulan']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Judul Proposal</td>
                <td>Instansi</td>
                <td class="fit">Status</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhskp)
                @foreach($mhskp as $m)
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td><a href="{{ route('log_mahasiswa', $m->nim) }}" style="color: #282828">{{ $m->nama }}</a></td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td>{{ $m->judul }}</td>
                        <td>{{ $m->instansi }}</td>
                        <td class="fit">{{ ($m->status_judul) ? $m->status_judul : 'baru' }}</td>
                        <td class="fit">
                            <div class="buttonwrapper">
                                <a href="#ex1" aria-nim="{{ $m->nim_id }}" rel="modal:open" class="button small">
                                    <span class="ion-eye icon"></span>
                                    Detail
                                </a>
                                @if($m->spk_berkas)
                                <a href="{{ route('cetak_spk', $m->nim) }}" class="button small" target="_blank">
                                    <span class="ion-ios-paper icon"></span>
                                    SPK
                                </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhskp->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.button').click(function () {
                var nim= this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/kelayakan/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')