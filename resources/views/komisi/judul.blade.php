@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    finalisasi judul kp
@endsection

@section('bar-page_subtitle')
    finalisasi judul proposal kp
@endsection

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    pengajuan seminar kp teknik informatika
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td>judul 1</td>
                <td>judul 2</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1 ?>
            @foreach($mhs as $m)
                <tr>
                    <td class="fit">{{ $no++ }}</td>
                    <td>{{ $m->nama }}</td>
                    <td class="fit">{{ $m->nim }}</td>
                    <td>{{ $m->judul }}</td>
                    <td>{{ ($m->judul_final) ? $m->judul_final : 'belum ditentukan'}}</td>
                    <td class="fit">
                        <div class="buttonwrapper">
                            <a href="#ex1" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small detail-btn">
                                <span class="ion-eye icon"></span>
                                Tentukan Judul Final
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/judul/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.header')