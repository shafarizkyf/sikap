@extends('layouts.footer')

@section('sidebar-link_laporan')
    active
@endsection

@section('bar-username')
    {{ $dosen->nama }}
@endsection

@section('bar-page_title')
    Laporan Kerja Praktik
@endsection

@section('bar-page_subtitle')
    Export berbagai laporan kerja praktik
@endsection

@section('bar-page_subtitle')
    menyimpan/mendapatkan data terbaru dari sistem
@endsection

@section('bar-content')
    <div class="content withmenu">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-cloud-upload icon"></span> unduh data
                </div>
            </div>
            @if(session()->has('import'))
                <div class="field noborder">
                    <p>{{ session()->get('import') }}</p>
                </div>
            @endif
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>'ExportImportController@export']) !!}
                <div class="field">
                    {!! Form::label("dataExport", null, ['class' => 'ion-android-document']) !!}
                    <div class="styled-select">
                        {!! Form::select('data', [
                            ''                      => 'Pilih', 
                            'selesai_kp'            => 'Laporan Mahasiswa Selesai KP', 
                            'progress_kp'           => 'Laporan Mahasiswa Sedang KP', 
                            'rataan_kp_seminar'     => 'Laporan Rataan Waktu KP-Seminar', 
                            'rataan_bimbingan'      => 'Laporan Rataaan Bimbingan KP' , 
                            'kuota_dosen'           => 'Laporan Beban Bimbingan Dosen', 
                            'seminar'               => 'Laporan Jadwal Seminar'
                        ]) !!}
                    </div>
                </div>
                @if($errors->all())
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('dataExport') : '' }}
                </div>
                @endif
                <div class="field">
                    {!! Form::label('tanggal_dari', null, ['class' => 'ion-android-calendar']) !!}
                    {!! Form::text('tanggal_dari', null, ['id' => 'tanggal_dari', 'placeholder' => 'Tanggal Mulai']) !!}
                </div>
                @if($errors->all())
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('tanggal_dari') : '' }}
                </div>                
                @endif
                <div class="field">
                    {!! Form::label('tanggal_ke', null, ['class' => 'ion-android-calendar']) !!}
                    {!! Form::text('tanggal_ke', null, ['id' => 'tanggal_ke', 'placeholder' => 'Tanggal Akhir']) !!}
                </div>
                @if($errors->all())
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('tanggal_ke') : '' }}
                </div>                
                @endif
                <div class="field noborder center">
                    {{ Form::submit('export', ['class'=>'button']) }}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $(function () {
                $('#tanggal_dari').datepicker();
                $('#tanggal_ke').datepicker();
            });
        });
    </script>
@endsection


@extends('layouts.header')