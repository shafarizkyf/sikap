@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    status prasyarat kp
@endsection

@section('bar-page_subtitle')
    mahasiswa {{ $komisi->jurusan->jurusan }}
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('komisi_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_progress') }}">sedang kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_ulang') }}">Ulang KP</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('komisi_menu', 'gantidosbing') }}">Penggantian Pembimbing</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    penggantian pembimbing
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Pembimbing Saat Ini</td>
                <td>Usulan Pembimbing</td>
                <td>Alasan</td>
                <td class="fit">Status</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($ganti)
                @foreach($ganti as $m)
                    <?php $dosbing = \App\MhsPembimbing::where('nim_id', $m->nim_id)->first(); ?>
                    @if($komisi->jurusan->id == $m->nim->jurusan->id)
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nim->nama }}</td>
                        <td class="fit">{{ $m->nim->nim }}</td>
                        <td class="fit"><p>{{ $dosbing->dosbing1->nama }}</p> <p>{{ ($dosbing->dosbing2) ? $dosbing->dosbing2->nama : '' }}</p></td>
                        <td class="fit"><p>{{ $m->dosbing->nama }}</p> <p>{{ ($m->dosbing2) ? $m->dosbing2->nama : '' }}</p></td>
                        <td>{{ $m->alasan }}</td>
                        <td class="fit">{{ ($m->status) ? $m->status : 'Menunggu' }}</td>
                        <td class="fit">
                            @if(!$m->status)
                            <div class="buttonwrapper">
                                <a href="#ex1" aria-nim="{{ $m->id }}" rel="modal:open" class="button small">
                                    <span class="ion-eye icon"></span>
                                    Detail
                                </a>
                            </div>
                            @endif
                        </td>
                    </tr>
                    @endif
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $ganti->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.button').click(function () {
                var nim= this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/gantidosbing/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')