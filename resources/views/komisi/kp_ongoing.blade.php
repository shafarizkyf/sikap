@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    mahasiswa sedang kp
@endsection

@section('bar-page_subtitle')
    mahasiswa {{ $komisi->jurusan->jurusan }} dalam proses kerja praktik
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('komisi_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('komisi_kp_progress') }}">sedang kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_ulang') }}">Ulang KP</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_menu', 'gantidosbing') }}">Usulan Penggantian Pembimbing</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Mahasiswa sedang KP
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                    {!! Form::open(['method'=>'get', 'action'=>'KomisiController@kpOnGoing']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    {!! Form::text('spk_mulai', null, ['id' => 'spk_mulai', 'placeholder' => 'Tanggal Mulai SPK']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    {!! Form::text('spk_selesai', null, ['id' => 'spk_selesai', 'placeholder' => 'Tanggal Selesai SPK']) !!}
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Pembimbing 1</td>
                <td>Pembimbing 2</td>
                <td class="fit">Sisa Waktu</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhskp)
                @foreach($mhskp as $m)
                    @if($m->nim->cekSeminar())
                        <tr>
                    @else
                        @if($m->getSisaWaktuBulan()<=1)
                            <tr style="background: #ffb6b6">
                        @elseif($m->getSisaWaktuBulan()==2)
                            <tr style="background: #fff1b6">
                        @else
                            <tr>
                        @endif
                    @endif
                        <td class="fit">{{ $no }}</td>
                        <td><a href="{{ route('log_mahasiswa', $m->nim->nim) }}">{{ $m->nim->nama }}</a></td>
                        <td class="fit">{{ $m->nim->nim }}</td>
                        <td>
                            @if(!is_null($m->nim->pembimbing->dosbing1))
                                {{ $m->nim->pembimbing->dosbing1->nama }}
                            @endif
                        </td>
                        <td>
                            @if(!is_null($m->nim->pembimbing->dosbing2))
                                {{ $m->nim->pembimbing->dosbing2->nama }}
                            @endif
                        </td>
                        <td class="fit">
                        @if($m->nim->cekSeminar())
                            Sudah Seminar
                        @else
                            @if($m->getSisaWaktuHari()<=0)
                                Habis
                            @elseif($m->getSisaWaktuBulan()==0 && $m->getSisaWaktuHari() > 0)
                                {{$m->getSisaWaktuHari() }} hari
                            @elseif($m->getSisaWaktuBulan()>0)
                                {{$m->getSisaWaktuBulan() }} bulan
                                <br>
                                ({{$m->getSisaWaktuHari() }} hari)
                            @endif
                        @endif

                        </td>
                        <td class="fit">
                            <div class="buttonwrapper">
                                <a href="#ex1" aria-nim="{{ $m->nim_id }}" rel="modal:open" class="button small">
                                    <span class="ion-eye icon"></span>
                                    Detail
                                </a>
                                @if($m->nim->spk)
                                <a href="{{ route('cetak_spk', $m->nim->nim) }}" class="button small" target="_blank">
                                    <span class="ion-ios-paper icon"></span>
                                    SPK
                                </a>
                                @endif
                                <a href="{{ route('cetak_kartu_kendali', $m->nim->nim) }}" class="button small" target="_blank">
                                    <span class="ion-android-people icon"></span>
                                    Kartu Kendali
                                </a>                                
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhskp->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div> 

@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#spk_mulai').datepicker();
            $('#spk_selesai').datepicker();

            $('.button').click(function () {
                var nim= this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/kelayakan/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')