@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    dosen
@endsection

@section('sidebar-link_dosen')
    active
@endsection

@section('bar-page_subtitle')
    daftar dosen {{ $komisi->jurusan->jurusan }}
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    DAFTAR DOSEN
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Dosen</td>
                <td class="fit">NIP</td>
                <td class="fit">NIDN</td>
                <td class="fit">Kuota</td>
                <td class="fit">Request Kuota</td>
            </tr>
            </thead>
            <tbody>
            @if($dosen)
                <?php $x=1 ?>
                @foreach($dosen as $d)
                    <?php
                        $total  =  count(\App\Http\Controllers\GetDosenDataController::getTotalBimbinganMhs($d->id));
                        $kuota  = \App\KuotaDosen::where('dosen_id', $d->id)->where('status', 'menunggu')->first();
                        $jumlah = \App\KuotaDosen::where('dosen_id', $d->id)->where('status', 'menunggu')->get();
                        $jkuota = 0;
                        foreach ($jumlah as $j){
                            $jkuota += $j->jumlah;
                        }
                    ?>
                    <tr>
                        <td class="fit">{{ $x++ }}</td>
                        <td>{{ $d->nama }}</td>
                        <td class="fit">{{ $d->nip }}</td>
                        <td class="fit">{{ $d->nidn }}</td>
                        <td class="fit"> {{$total}} / {{ $d->kuota }}</td>
                        <td class="fit">
                        @if($kuota)
                            @if($kuota->status == 'menunggu')
                                mengajukan +{{ $jkuota }}
                            @else
                                telah ditambah +{{ $jkuota  }}
                            @endif
                        @else
                            belum ada permintaan
                        @endif
                        </td>

                        <td class="fit">
                            <div class="button-wrapper">
                                <a href="#ex1" aria-nip="{{ $d->nip }}" rel="modal:open" class="button small kuota">
                                    <span class="icon ion-ios-calculator"></span>
                                    tambah kuota
                                </a>
                                <a href="#ex2" aria-nip="{{ $d->nip }}" rel="modal:open" class="button small bimbingan">
                                    <span class="icon ion-ios-calculator"></span>
                                    bimbingan
                                </a>
                                <a href="{{ route('komisi_kuota_refresh', $d->id) }}" class="button small kuota">
                                    <span class="icon ion-refresh"></span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li></li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
    <div id="ex2" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.kuota').click(function () {
                var nip = this.getAttribute('aria-nip');
                getDosenKuota(nip);
            });

            $('.bimbingan').click(function () {
                var nip= this.getAttribute('aria-nip');
                getListMhs(nip);
            });

            function getDosenKuota(nip) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/kuota/' ?>" + nip,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

            function getListMhs(nip) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/mhsbimbingan/' ?>" + nip,
                    success: function (data) {
                        $('#ex2').html(data);
                    }
                });
            }
        });
    </script>
@endsection


@extends('layouts.header')