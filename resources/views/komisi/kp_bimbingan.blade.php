@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    mahasiswa kp
@endsection

@section('bar-page_subtitle')
    mahasiswa kp dalam bimbingan anda
@endsection

@section('sidebar-link_dosbing')
    active
@endsection

@section('bar-content')
    @if(session()->has('warning'))
    <div class="kp-status">
        <h3 class="title">{{ session()->get('warning') }}</h3>
    </div>
    @endif
    @if(session()->has('bimbingan'))
        <div class="kp-status">
            <h3 class="title">{{ session()->get('bimbingan') }}</h3>
        </div>
    @endif
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    mahasiswa dalam bimbingan kp
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td>Judul KP</td>
                <td>Tempat</td>
                <td class="fit">Bimbingan</td>
                <td class="fit">KP</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <?php

                    $judul      = ($m->judul_final) ? $m->judul_final : $m->judul;
                    $nilai      = \App\MhsNilai::where('nim_id', $m->nim_id)->first();
                    $seminar    = \App\MhsSeminar::where('nim_id', $m->nim_id)->first();
                    $berkas = App\MhsBerkasDistribusi::where('nim_id',$m->nim_id)->first();
                    $jurusan = App\ListJurusan::find($m->jurusan_id);
                    ?>
                    <tr>
                        <td class="fit">{{ $no++ }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td>{{ $judul }}</td>
                        <td>{{ $m->instansi }}</td>
                        <td class="fit">{{ $m->status_bimbingan }}</td>
                        <td class="fit">{{ ($m->status_kp) ? $m->status_kp : '-' }}</td>
                        <td class="fit">

                            @if(!$m->status_bimbingan)
                                <div class="buttonwrapper">
                                    {!! Form::open(['method'=>'post', 'action'=>['KomisiController@unlockBimbingan', $m->nim_id]]) !!}
                                    {!! Form::submit('mulai bimbingan', ['class' => 'button small']) !!}
                                    {!! Form::close() !!}
                                </div>

                            @elseif($m->status_bimbingan == 'selesai')
                                <div class="button-wrapper">
                                    <a href="{{ route('komisi_lihat_bimbingan', $m->nim) }}" class="button small">
                                        <span class="ion-eye icon"></span>
                                        bimbingan
                                    </a>
                                    @if($seminar)
                                    <a href="#ex1" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small detail-btn">
                                        <span class="ion-star"></span>
                                        status kp
                                    </a>
                                    @endif
                                    @if($berkas AND $m->status_distribusi == 'ok')
                                        <a href="{{ asset('mahasiswa_berkas/'.$jurusan->jurusan.'/'.$berkas->pengesahan) }}" class="button small">
                                            <span class="ion-arrow-down-a icon"></span>
                                            pengesahan
                                        </a>
                                    @endif
                                </div>

                            @else
                                <a href="{{ route('komisi_lihat_bimbingan', $m->nim) }}" class="button small">
                                    <span class="ion-eye icon"></span>
                                    bimbingan
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {


            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/nilai/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.komisi.sidebar_content')

@extends('layouts.header')