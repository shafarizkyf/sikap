@extends('layouts.footer')

@section('bar-username')
    {{ $komisi->nama }}
@endsection

@section('bar-page_title')
    Rekap Mahasiswa yang sudah KP
@endsection

@section('bar-page_subtitle')
    mahasiswa {{ $komisi->jurusan->jurusan }} sudah kerja praktik
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('komisi_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_kp_progress') }}">sedang kp</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('komisi_kp_rekap') }}">rekap</a>
        </li>
        <li class="items">
            <a href="{{ route('komisi_menu', 'gantidosbing') }}">Usulan Penggantian Pembimbing</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Mahasiswa sudah KP
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Dosen PA</td>
                <td>Pembimbing 1</td>
                <td>Pembimbing 2</td>
                <td class="fit">Status KP</td>
                <td class="fit">Nilai</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhskp)
                @foreach($mhskp as $m)
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nim->nama }}</td>
                        <td class="fit">{{ $m->nim->nim }}</td>
                        <td class="fit">
                            @if(!is_null($m->nim->dosenpa))
                                {{ $m->nim->dosenpa->nama }}
                            @endif
                        </td>
                        <td>
                            @if(!is_null($m->nim->pembimbing->dosbing1))
                                {{ $m->nim->pembimbing->dosbing1->nama }}
                            @endif
                        </td>
                        <td>
                            @if(!is_null($m->nim->pembimbing->dosbing2))
                                {{ $m->nim->pembimbing->dosbing2->nama }}
                            @endif
                        </td>
                        <td class="fit">
                            {{ $m->status_kp }}
                        </td>
                        <td class="fit">
                            @if($m->nim->nilai)
                                {{ $m->nim->nilai->nilai_akhir }} / {{ $m->nim->nilai->nilai_huruf }}
                            @endif                            
                        </td>                        
                        <td class="fit">
                            <div class="buttonwrapper">
                                <a href="#ex1" aria-nim="{{ $m->nim_id }}" rel="modal:open" class="button small">
                                    <span class="ion-eye icon"></span>
                                    Detail
                                </a>
                                @if($m->nim->spk)
                                <a href="{{ route('cetak_spk', $m->nim->nim) }}" class="button small" target="_blank">
                                    <span class="ion-ios-paper icon"></span>
                                    SPK
                                </a>
                                @endif
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhskp->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.komisi.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.button').click(function () {
                var nim= this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/kelayakan/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')