@if($mhskp)
    <div class="tag">
        <div class="tag-content">DETAIL PENGAJUAN KP - {{ $mhskp['nim'] }}</div>
    </div>
    <div class="modal-content">
        <div class="list">
            <label for="tempatkp" class="ion-location"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Tempat Kerja Praktik</h2>
                <p class="desc">
                    {{ $mhskp['instansi'] }}
                </p>
            </div>
        </div>
        <div class="list">
            <label for="tempatkp" class="ion-ios-lightbulb"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Topik Proposal Kerja Praktik</h2>
                <p class="desc">
                    {{ $mhskp['judul'] }}
                </p>
            </div>
        </div>
        <div class="list">
            <label for="tempatkp" class="ion-person"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Usulan Dosen Pembimbing 1</h2>
                <p class="desc">
                    {{ $mhskp['dosbing1'] }}
                </p>
            </div>
        </div>
        @if($mhskp['dosbing2'])
        <div class="list">
            <label for="tempatkp" class="ion-person"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Usulan Dosen Pembimbing 2</h2>
                <p class="desc">
                    {{ $mhskp['dosbing2'] }}
                </p>
            </div>
        </div>
        @endif
        <div class="list">
            <label for="tempatkp" class="ion-paperclip"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Outline Proposal Kerja Praktik</h2>
                <p class="desc">
                    Outline Proposal : <a href="{{ route('download_berkas', $mhskp['outline']) }}">Download</a> |
                    <a href="{{ route('view_file', array('outline', $mhskp['nim'])) }}" target="_blank">Lihat</a>
                </p>
            </div>
        </div>
        <div class="list">
            <label for="tempatkp" class="ion-paperclip"></label>
            <div class="item" id="tempatkp">
                <h2 class="title">Surat Penerimaan Tempat KP</h2>
                <p class="desc">
                    Surat Penerimaan : <a href="{{ route('download_berkas', $mhskp['surat']) }}">Download</a> |
                    <a href="{{ route('view_file', array('surat', $mhskp['nim'])) }}" target="_blank">Lihat</a>
                </p>
            </div>
        </div>
        <div class="kp-status">
            <h3 class="title">status berkas : {{ ($mhskp['status_berkas']) ? $mhskp['status_berkas'] : 'menunggu bapendik' }}</h3>
        </div>
        @if(!$mhskp['status_berkas'] or ($mhskp['status_berkas'] == 'koreksi' and $mhskp['status_koreksi'] == 'sudah'))
        {!! Form::open(['method'=>'post', 'action'=>['BapendikController@setStatusBerkas', $mhskp['nim'] ]]) !!}
            <div class="field">
                <label for="aksi" class="ion-flag"></label>
                <div class="styled-select">
                    {!! Form::select('aksi', ['lengkap'   => 'Berkas Lengkap', 'koreksi'   => 'Koreksi Berkas'], null, ['placeholder'=>'Pilih Aksi', 'id'=>'aksi']) !!}
                </div>
            </div>
            @if($errors->first('aksi'))
            <div class="field noborder error">
                {{ $errors->first('aksi') }}
            </div>
            @endif
            <div class="aksi-wrapper">
                <div class="field textarea">
                    <label for="koreksi" class="ion-ios-lightbulb"></label>
                    {!! Form::textarea('koreksi', null, ['id'=>'koreksi','placeholder'=>'Koreksi yang diperlukan' ]) !!}
                </div>
                @if($errors->all())
                <div class="field noborder error">
                    {{ $errors->first('koreksi') }}
                </div>
                @endif
            </div>
            <div class="field noborder center">
                {!! Form::submit('Simpan', ['class'=>'button']) !!}
            </div>
        {!! Form::close() !!}
        @endif
    </div>
@endif

<script>
    $(document).ready(function() {
        $('.aksi-wrapper').hide();

        $('#aksi').change(function(){
            var status 	= $('#aksi').val();
            if(status == 'koreksi'){
                $('.aksi-wrapper').fadeIn('normal');
            }else{
                $('.aksi-wrapper').fadeOut('normal');
            }
        });
    });
</script>