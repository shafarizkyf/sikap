@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-page_title')
    mahasiswa kp
@endsection

@section('bar-page_subtitle')
    mahasiswa yang sudah memiliki spk
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Spk mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@filterKpMhs']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Awal SPK</td>
                <td class="fit">Akhir SPK</td>
                <td class="fit">Nilai</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($listMhs)
                @foreach($listMhs as $m)
                    <?php
                        $jurusan = \App\ListJurusan::find($m->jurusan_id);
                        $spk_mulai = ($m->progress) ? date('d M Y', strtotime($m->progress->waktu_spk_mulai)) : '-';
                        $spk_selesai = ($m->progress) ? date('d M Y', strtotime($m->progress->waktu_spk_selesai)) : '-';
                    ?>
                    <tr>
                        <td class="fit">{{ $no++ }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td class="fit">{{ $jurusan->jurusan }}</td>
                        <td class="fit">{{ $spk_mulai }}</td>
                        <td class="fit">{{ $spk_selesai }}</td>
                        <td class="fit">{{ ($m->nilai and $m->nilai->nilai_akhir) ? $m->nilai->nilai_akhir : '-'}}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li></li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')