@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('sidebar_link_seminar')
    active
@endsection

@section('bar-page_title')
    seminar mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan seminar mahasiswa teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_menu', 'seminar') }}">jadwal</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_seminar_pengajuan') }}">pengajuan</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_menu', 'distribusi') }}">distribusi</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    distribusi berkas kp
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Waktu Distribusi</td>
                <td class="fit">Status</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $x=1; ?>
            @foreach($dist as $d)
                <?php $status = \App\MhsProgressStatus::where('nim_id', $d->nim_id)->first() ?>
                <tr>
                    <td class="fit">{{ $x++ }}</td>
                    <td>{{ $d->nim->nama }}</td>
                    <td class="fit">{{ $d->nim->nim }}</td>
                    <td class="fit">{{ $d->nim->jurusan->jurusan }}</td>
                    <td class="fit">{{ $d->created_at->format('d M Y') }}</td>
                    <td class="fit">{{ ($status->status_distribusi) ? $status->status_distribusi : 'Baru' }}</td>
                    <td>
                        <div class="buttonwrapper">
                            <a href="#ex1" aria-nim="{{ $d->nim_id }}" rel="modal:open" class="button small detail-btn">
                                <span class="ion-eye icon"></span>
                                Berkas
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $dist->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/dis/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.header')