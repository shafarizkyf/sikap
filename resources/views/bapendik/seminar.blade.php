@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('bar-page_title')
    jadwal seminar
@endsection

@section('sidebar_link_seminar')
    active
@endsection

@section('bar-page_subtitle')
    jadwal seminar mahasiswa teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('bapendik_menu', 'seminar') }}">jadwal</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_seminar_pengajuan') }}">pengajuan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table class="seminar">
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    Jadwal Seminar kerja praktik
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="11">
                    {!! Form::open(['method'=>'get', 'action'=>['BapendikController@menu', 'seminar']]) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('ruangan', []+$ruangan, $listRuangan) !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="center">Tanggal</td>
                <td class="center">SHIFT 1<span>(JAM 7-8)</span></td>
                <td class="center">SHIFT 2<span>(JAM 8-9)</span></td>
                <td class="center">SHIFT 3<span>(JAM 9-10)</span></td>
                <td class="center">SHIFT 4<span>(JAM 10-11)</span></td>
                <td class="center">SHIFT 5<span>(JAM 11-12)</span></td>
                <td class="center">SHIFT 6<span>(JAM 12-13)</span></td>
                <td class="center">SHIFT 7<span>(JAM 13-14)</span></td>
                <td class="center">SHIFT 8<span>(JAM 14-15)</span></td>
                <td class="center">SHIFT 9<span>(JAM 15-16)</span></td>
                <td class="center">SHIFT 10<span>(JAM 16-17)</span></td>
            </tr>
            </thead>
            <tbody>
            @for($x=0; $x < $jumlahHari; $x++, $dateToday->addDay())
                <tr>
                @if($dateToday->isSunday())
                    @continue
                @endif
                @for($i=0; $i<=10; $i++)

                    <?php
                        $sc         = new \App\Http\Controllers\SeminarController();
                        $shift      = $sc->shift($i);
                        $today      = $dateToday->format('Y-m-d').' '.$shift;
                        $jadwal     = \App\MhsSeminar::where('jadwal', $today)->where('ruangan_id', $listRuangan)->first();
                        if($jadwal){
                            $mhs        = \App\ListMahasiswa::find($jadwal->nim_id);
                            $nama       = new \App\Http\Controllers\BapendikController();
                            $nama       = $nama->trimString($jadwal->nim->nama, 6);
                            $jurusan    = $sc->trimString($jadwal->nim->jurusan->jurusan, 10);
                            $dosbing    = \App\MhsPembimbing::where('nim_id', $jadwal->nim_id)->first();
                        }
                    ?>

                    @if($i==0)
                        <td class="center caption">
                            {{ $dateToday->format('l') }}<br>
                            <span class="big">{{ $dateToday->format('d') }}</span>
                            {{ $dateToday->format('F Y') }}
                        </td>
                    @endif

                    @if($i != 0 and $jadwal)
                        <td class="center">
                            @if(!$jadwal->status)
                                <a href="{{ route('seminar_delete', $jadwal->id) }}"><span class="mark ion-close-circled"></span></a>
                            @elseif($jadwal->status == 'ulang')
                                <span class="mark ion-refresh "></span>
                            @elseif($jadwal->status == 'selesai')
                                <span class="mark ion-checkmark-circled "></span>
                            @endif
                            {{ strtoupper($jadwal->jenis) }}<br>
                            {{ $jadwal->nim->nim }}<br>
                            {{ $nama }}
                            <span>{{ $jurusan }}</span>
                            @if(!$jadwal->status and !$dosbing)
                                <a href="{{ route('seminar_selesai', $jadwal->id) }}" class="button small"><span class="ion-checkmark"></span></a>
                                <a href="{{ route('seminar_ulang', $jadwal->id) }}" class="button small"><span class="ion-refresh"></span></a>
                            @elseif($jadwal->status == 'ulang')
                                <span class="mark ion-refresh "></span>
                            @elseif($jadwal->status == 'selesai')
                                <span class="mark ion-checkmark-circled "></span>
                            @endif
                        </td>
                        @elseif($i != 0)
                        <td></td>
                    @endif
                @endfor
                </tr>
            @endfor
            </tbody>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/') ?>" + '/modal/seminar/' + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.header')