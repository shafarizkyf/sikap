@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('bar-page_title')
    Surat Pengantar Kerja Praktik
@endsection

@section('bar-page_subtitle')
    pengajuan surat pengantar kp mahasiswa
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}" class="active">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Pengajuan Surat Pengantar kp
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@kpSurat']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('status', ['semua'=>'Semua Status', 'menunggu'=>'Menunggu', 'selesai'=>'Selesai'], 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">Nim</td>
                <td>instansi</td>
                <td class="fit">waktu pengajuan</td>
                <td class="fit">status</td>
                <td></td>
            </tr>
            </thead>
            <tbody>
                <?php $x=1 ?>
                @foreach($mhs as $m)
                <tr>
                    <td class="fit">{{ $x++ }}</td>
                    <td>{{ $m->nim->nama }}</td>
                    <td class="fit">{{ $m->nim->nim }}</td>
                    <td>
                        {{ $m->instansi }}<br>
                        {{ $m->kepada }}
                    </td>
                    <td class="fit">{{ $m->waktu_ajukan->format('d F Y') }}</td>
                    <td class="fit">{{ $m->status }}</td>
                    <td class="fit">
                        @if(!$m->nomor)
                        <div class="buttonwrapper">
                            <a href="#ex1" aria-nim="{{ $m->id }}" rel="modal:open" class="button small detail-btn" id="coba-btn">
                                <span class="ion-printer icon"></span>
                                Set Surat
                            </a>
                        </div>
                            @else
                            <div class="buttonwrapper">
                                <a class="button small" href="{{ route('cetak_surat_pengantar', $m->id) }}" target="_blank">
                                <span class="ion-printer icon"></span>
                                    Cetak Surat
                                </a>
                            </div>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim= this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });


            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/surat/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection


@extends('layouts.header')