@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-page_title')
    mahasiswa kp
@endsection

@section('bar-page_subtitle')
    mahasiswa yang sudah memiliki spk
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <?php $settings = json_decode(file_get_contents(storage_path('settings.json')), true); ?>
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Spk mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@kpMhs']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Awal SPK</td>
                <td class="fit">Akhir SPK</td>
                <td class="fit">Nilai</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @foreach($mhs as $o)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $o->nim->nama }}</td>
                <td class="fit">{{ $o->nim->nim }}</td>
                <td class="fit">{{ $o->nim->jurusan->jurusan }}</td>
                <td class="fit">{{ $o->nim->progress->waktu_spk_mulai->format('d F Y') }}</td>
                <td class="fit">
                    {{ $o->nim->progress->waktu_spk_selesai->format('d F Y') }}
                    <?php $masaSpk = $o->nim->progress->waktu_spk_mulai->diffInDays($o->nim->progress->waktu_spk_selesai, false); ?>
                    @if( $masaSpk != $settings['masa_spk'])
                    <span style="color:red">telah diperpanjang {{ ($masaSpk - $settings['masa_spk']).'H' }}</span>
                    @endif
                </td>
                <td class="fit">{{ $o->nim->nilai ? $o->nim->nilai->nilai_akhir.' / '.$o->nim->nilai->nilai_huruf : '-' }}</td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')