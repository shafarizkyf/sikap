@extends('layouts.footer')

@section('bar-username')
    bapendik
@endsection

@section('bar-page_title')
    usulan kp mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan kerja praktik mahasiswa
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}" class="active">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Pengajuan kerja praktik mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@filterKpUsulan']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('status', ['semua'=>'Semua Status', 'lengkap'=>'Berkas Lengkap', 'koreksi'=>'Berkas Koreksi'], 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Waktu Usulan</td>
                <td class="fit">Status Berkas</td>
                <td class="fit">Status Judul</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            @if(count($listMhs) > 0)
                <?php $no = 1; ?>
                @foreach($listMhs as $m)
                    <?php
                        $waktu      = ($m->waktu_daftar) ? date('d M Y', strtotime($m->waktu_daftar)) : '-';
                        $jurusan    = \App\ListJurusan::find($m->jurusan_id);
                    ?>
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td class="fit">{{ $jurusan->jurusan }}</td>
                        <td class="fit">{{ $waktu }}</td>
                        <td class="fit">{{ $m->status_berkas }}</td>
                        <td class="fit">{{ $m->status_judul }}</td>
                        <td class="fit">
                            <div class="buttonwrapper">
                                <a href="#ex1" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small detail-btn" id="coba-btn">
                                    <span class="ion-eye icon"></span>
                                    Detail
                                </a>
                                @foreach($spk as $s)
                                    @if($s->nim->nim == $m->nim)
                                        <a href="{{ route('cetak_lembar_permohonan', $m->nim) }}" class="button small" target="_blank">
                                            <span class="ion-printer icon"></span>
                                            FR-KP1
                                        </a>
                                        @if($m->status_judul == 'layak' and !$s->nomor)
                                            <a href="#ex2" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small set-spk">
                                                <span class="ion-eye icon"></span>
                                                Set SPK
                                            </a>

                                        @elseif($m->status_judul == 'layak' and $s->nomor)
                                            <a href="{{ route('cetak_spk', $m->nim) }}" class="button small">
                                                <span class="ion-printer icon"></span>
                                                Cetak SPK
                                            </a>
                                        @endif
                                    @endif
                                @endforeach
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
    <div id="ex2" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
<script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
<script src="{{ asset('js/less/less.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.detail-btn').click(function () {
            var nim= this.getAttribute('aria-nim');
            getDetailMhs(nim);
        });

        $('.set-spk').click(function () {
            var nim= this.getAttribute('aria-nim');
            setSpk(nim);
        });

        function getDetailMhs(nim) {
            $.ajax({
                type: 'get',
                url: "<?php echo \Illuminate\Support\Facades\URL::to('/bapendik/kp/usulan/detail') ?>" + '/' + nim,
                success: function (data) {
                    $('#ex1').html(data);
                }
            });
        }

        function setSpk(nim) {
            $.ajax({
                type: 'get',
                url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/spk/' ?>" + nim,
                success: function (data) {
                    $('#ex2').html(data);
                }
            });
        }
    });
</script>
@endsection


@extends('layouts.header')