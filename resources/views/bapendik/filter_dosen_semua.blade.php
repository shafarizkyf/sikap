@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('bar-page_title')
    semua dosen teknik
@endsection

@section('sidebar_link_dosen')
    active
@endsection

@section('bar-page_subtitle')
    daftar semua dosen teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a  class="active" href="{{ route('bapendik_menu', 'dosen') }}">semua dosen</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_menu', 'dosbing') }}">dosen pembimbing</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    DAFTAR DOSEN
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@filterDosenSemua']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Dosen</td>
                <td class="fit">NIP</td>
                <td class="fit">NIDN</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Kuota</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            @if($listDosen)
                <?php $x=1 ?>
                @foreach($listDosen as $d)
                    <?php
                        $jurusan = \App\ListJurusan::find($d->jurusan_id);
                        $total = count(\App\Http\Controllers\GetDosenDataController::getTotalBimbinganMhs($d->id));
                    ?>
                    <tr>
                        <td class="fit">{{ $x++ }}</td>
                        <td>{{ $d->nama }}</td>
                        <td class="fit">{{ $d->nip }}</td>
                        <td class="fit">{{ $d->nidn }}</td>
                        <td class="fit">{{ $jurusan->jurusan }}</td>
                        <td class="fit">{{ $total }} / {{ $d->kuota }}</td>
                        <td class="fit">
                            <a href="#ex1" aria-nip="{{ $d->nip }}" rel="modal:open" class="button small kuota">
                                <span class="icon ion-ios-calculator"></span>
                                mahasiswa bimbingan
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script src="{{ asset('js/sikap/wda_dosbing_mhs.js') }}"></script>
@endsection

@extends('layouts.header')