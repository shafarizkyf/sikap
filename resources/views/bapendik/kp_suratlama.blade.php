@extends('layouts.footer')

@section('bar-username')
    bapendik
@endsection

@section('bar-page_title')
    usulan kp mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan kerja praktik mahasiswa
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}" class="active">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Arsip Surat
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td>Instansi</td>
                <td class="fit">Nomor Surat</td>
                <td class="fit">Waktu Ganti</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $x=1 ?>
            @foreach($surat as $s)
                <tr>
                    <td class="fit">{{ $x++ }}</td>
                    <td>{{ $s->nim->nama }}</td>
                    <td class="fit">{{ $s->nim->nim }}</td>
                    <td class="fit">{{ $s->nim->jurusan->jurusan }}</td>
                    <td>{{ $s->instansi }}</td>
                    <td class="fit">{{ $s->nomor }}</td>
                    <td class="fit">{{ $s->created_at->format('d F Y') }}</td>
                    <td class="fit">
                        <a href="{{ route('cetak_suratlama', $s->nim) }}" class="button small" target="_blank">
                            <span class="ion-printer icon"></span>
                            Cetak Surat
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $surat->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')