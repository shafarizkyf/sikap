@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('sidebar_link_seminar')
    active
@endsection

@section('bar-page_title')
    seminar mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan seminar mahasiswa teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_menu', 'seminar') }}">jadwal</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_seminar_pengajuan') }}">pengajuan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    pengajuan seminar
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@filterPengajuanSeminar']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('status', ['semua'=>'Semua Status', 'lengkap'=>'Berkas Lengkap', 'koreksi'=>'Berkas Koreksi'], 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Ruangan</td>
                <td class="fit">Tanggal</td>
                <td class="fit">Jam</td>
                <td class="fit">Status</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $x=1 ?>
            @foreach($listMhs as $m)
                <?php
                    if($m->jadwal){
                        $jadwal = date('d M Y', strtotime($m->jadwal));
                        $jadwal_awal = date('H:i', strtotime($m->jadwal_awal));
                        $jadwal_akhir = date('H:i', strtotime($m->jadwal_akhir));
                    }

                    if($m->ruangan_id){
                        $ruangan = \App\ListRuangSeminar::find($m->ruangan_id);
                    }
                ?>
                <tr>
                    <td class="fit">{{ $x++ }}</td>
                    <td class="fit">{{ $m->nama }}</td>
                    <td class="fit">{{ $m->nim }}</td>
                    <td class="fit">{{ ($m->ruangan_id) ? $ruangan->ruangan : '-' }}</td>
                    <td class="fit">{{ ($m->jadwal) ? $jadwal : '-' }}</td>
                    <td class="fit">{{ ($m->jadwal) ? $jadwal_awal : '' }} - {{ ($m->jadwal) ? $jadwal_akhir : '' }}</td>
                    <td class="fit">{{ $m->status_berkas }}</td>
                    <td class="fit">
                        <div class="buttonwrapper">
                            <a href="#ex1" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small detail-btn">
                                <span class="ion-eye icon"></span>
                                Detail
                            </a>
                            <a href="{{ route('cetak_seminar', $m->nim) }}" class="button small" target="_blank">
                                <span class="ion-printer icon"></span>
                                Berkas Seminar
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        {{--<li>{{ $results->appends(\Illuminate\Support\Facades\Input::except('page'))->render() }}</li>--}}
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: '/modal/seminar/'+nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.header')