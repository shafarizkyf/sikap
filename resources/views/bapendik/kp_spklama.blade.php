@extends('layouts.footer')

@section('bar-username')
    bapendik
@endsection

@section('bar-page_title')
    usulan kp mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan kerja praktik mahasiswa
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}" class="active">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Arsip SPK
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Waktu Ganti</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $x=1 ?>
            @foreach($spk as $s)
                <tr>
                    <td class="fit">{{ $x++ }}</td>
                    <td>{{ $s->nim->nama }}</td>
                    <td class="fit">{{ $s->nim->nim }}</td>
                    <td class="fit">{{ $s->nim->jurusan->jurusan }}</td>
                    <td class="fit">{{ $s->created_at->format('d F Y') }}</td>
                    <td class="fit">
                        <div class="button-wrapper">
                            <a href="{{ route('cetak_spklama', $s->nim) }}" class="button small" target="_blank">
                                <span class="ion-printer icon"></span>
                                SPK Lama
                            </a>
                            <a href="{{ route('cetak_gantidosen', $s->nim->nim) }}" class="button small" target="_blank">
                                <span class="ion-printer icon"></span>
                                Cetak Revisi
                            </a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $spk->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')