@extends('layouts.footer')

@section('bar-username')
    bapendik
@endsection

@section('bar-page_title')
    usulan kp mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan kerja praktik mahasiswa
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}" class="active">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Pengajuan kerja praktik mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@kpUsulan']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="cari" class="ion-flag"></label>
                                    {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('status', ['semua'=>'Semua Status', 'lengkap'=>'Berkas Lengkap', 'koreksi'=>'Berkas Koreksi'], 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Waktu Usulan</td>
                <td class="fit">Status Berkas</td>
                <td class="fit">Status Judul</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @foreach($spk as $o)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $o->nim->nama }}</td>
                <td class="fit">{{ $o->nim->nim }}</td>
                <td class="fit">{{ $o->nim->jurusan->jurusan }}</td>
                <td>{{ $o->nim->kp->waktu_daftar->format('d F Y') }}</td>
                <td>
                    {{ $o->status_berkas ? $o->status_berkas : 'menunggu' }}
                    @if($o->status_koreksi == 'sudah')
                    ({{$o->status_koreksi}})
                    @endif
                </td>
                <td>{{ $o->status_judul ? $o->status_judul : 'menunggu' }}</td>
                <td class="fit">
                    <div class="buttonwrapper">
                        <a href="#ex1" aria-nim="{{ $o->nim->nim }}" rel="modal:open" class="button small detail-btn" id="coba-btn">
                            <span class="ion-eye icon"></span>
                            Detail
                        </a>
                        @if($o->status_judul == 'layak' and !$o->spk->nomor)
                        <a href="#ex2" aria-nim="{{ $o->nim->nim }}" rel="modal:open" class="button small set-spk">
                            <span class="ion-eye icon"></span>
                            Set SPK
                        </a>
                        @elseif($o->status_judul == 'layak' and $o->spk)
                        <a href="{{ route('cetak_spk', $o->nim->nim) }}" class="button small" target="_blank">
                            <span class="ion-printer icon"></span>
                            Cetak SPK
                        </a>
                        @endif
                    </div>
                </td>                
            </tr>
            @endforeach;
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $spk->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>            
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
    <div id="ex2" class="small" style="display:none;"></div>

@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
<script src="{{ asset('js/datepicker/datepicker.js') }}"></script>
<script src="{{ asset('js/less/less.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $('.detail-btn').click(function () {
            var nim= this.getAttribute('aria-nim');
            getDetailMhs(nim);
        });

        $('.set-spk').click(function () {
            var nim= this.getAttribute('aria-nim');
            setSpk(nim);
        });

        function getDetailMhs(nim) {
            $.ajax({
                type: 'get',
                url: 'usulan/detail/' + nim,
                success: function (data) {
                    $('#ex1').html(data);
                }
            });
        }

        function setSpk(nim) {
            $.ajax({
                type: 'get',
                url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/spk/' ?>" + nim,
                success: function (data) {
                    $('#ex2').html(data);
                }
            });
        }
    });
</script>
@endsection

@extends('layouts.header')