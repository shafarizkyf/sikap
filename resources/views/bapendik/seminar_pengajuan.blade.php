@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('sidebar_link_seminar')
    active
@endsection

@section('bar-page_title')
    seminar mahasiswa
@endsection

@section('bar-page_subtitle')
    pengajuan seminar mahasiswa teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_menu', 'seminar') }}">jadwal</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_seminar_pengajuan') }}">pengajuan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    pengajuan seminar
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9"></td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td class="fit">Seminar</td>
                <td class="fit">Ruangan</td>
                <td class="fit">Tanggal</td>
                <td class="fit">Jam</td>
                <td class="fit">Status</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $x=1; ?>
            @foreach($seminar as $s)
                <?php
                    //$s = \App\MhsSeminar::where('nim_id', $b->nim_id)->first();
                    $berkas = \App\MhsBerkasSeminar::where('nim_id', $s->nim_id)->first();
                    $curTime = \Jenssegers\Date\Date::now();
                    $statusKoreksi = null;
                    if($s->status_berkas == 'koreksi' && !$s->kesimpulan){
                        $statusKoreksi = 'sudah';
                    }
                ?>
                <tr>
                    <td class="fit">{{ $x++ }}</td>
                    <td>{{ $s->nim->nama }}</td>
                    <td class="fit">{{ $s->nim->nim }}</td>
                    <td class="fit">{{ ($s->jenis) ? $s->jenis : '-' }}</td>
                    <td class="fit">{{ ($s->ruangan) ? $s->ruangan->ruangan : '-' }}</td>
                    <td class="fit">{{ ($s->jadwal && ($s->jadwal != $curTime)) ? $s->jadwal->format('l, d M y') : '-' }}</td>
                    <td class="fit">{{ ($s->jadwal && ($s->jadwal != $curTime)) ? $s->jadwal->format('H:i') : '-' }}</td>
                    <td class="fit">{{ ($berkas) ? $berkas->status_berkas : '-' }} {{ $statusKoreksi }}</td>
                    <td class="fit">
                        <div class="buttonwrapper">
                            @if($berkas)
                                <a href="#ex1" aria-nim="{{ $s->nim->nim }}" rel="modal:open" class="button small detail-btn">
                                    <span class="ion-eye icon"></span>
                                    Detail
                                </a>
                                @if(!$s->nomor and $berkas->status_berkas == 'lengkap' and $s->ruangan)
                                    <a href="#ex2" aria-nim="{{ $s->id }}" rel="modal:open" class="button small setnomor-btn">
                                        <span class="ion-printer icon"></span>
                                        Set Surat
                                    </a>
                                @elseif($s->nomor and $berkas->status_berkas == 'lengkap')
                                    <a href="{{ route('cetak_seminar', $s->nim->nim) }}" class="button small" target="_blank">
                                        <span class="ion-printer icon"></span>
                                        Berkas Seminar
                                    </a>
                                @endif
                            @endif
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $seminar->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
    <div id="ex2" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            $('.setnomor-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                setNomor(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/seminar/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

            function setNomor(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/beritaacara/' ?>" + nim,
                    success: function (data) {
                        $('#ex2').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.header')