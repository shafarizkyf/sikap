@extends('layouts.footer')


@section('bar-username')
    {{ $user->username }}
@endsection

@section('bar-page_title')
    Pengaturan Keamanan
@endsection

@section('bar-page_subtitle')
    Ubah username
@endsection

@section('sidebar_link_keamanan')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_menu', 'keamanan') }}">keamanan</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_menu', 'username') }}">username</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-locked icon"></span> ubah username
                </div>
            </div>
            <div class="main">
                @if(session()->has('keamanan'))
                    <div class="field noborder error">
                        {{ session()->get('keamanan') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'post', 'action'=>'UserLoginController@gantiUsername']) !!}
                <div class="field">
                    {!! Form::label('oldusername', null, ['class'=>'ion-android-person']) !!}
                    {!! Form::text('oldusername', null, ['placeholder' => 'Username Lama Anda']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('oldusername') : '' }}
                </div>
                <div class="field">
                    {!! Form::label('newusername', null, ['class'=>'ion-android-person']) !!}
                    {!! Form::text('newusername', null, ['placeholder' => 'Username Baru Anda']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('newusername') : '' }}
                </div>
                <div class="field">
                    {!! Form::label('renewusername', null, ['class'=>'ion-android-person']) !!}
                    {!! Form::text('renewusername', null, ['placeholder' => 'Konfirmasi Username Baru Anda']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('renewusername') : '' }}
                </div>
                <div class="field noborder center">
                    {!! Form::submit('Simpan', ['class'=>'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')