@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('bar-page_title')
    semua mahasiswa
@endsection

@section('sidebar_link_mhs')
    active
@endsection

@section('bar-page_subtitle')
    status pra syarat kp semua mahasiswa teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a  class="active" href="{{ route('bapendik_kp_prasyarat') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_surat') }}">Pengajuan Surat Pengantar</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_usulan') }}">usulan kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_mhs') }}">mahasiswa kp</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_spklama') }}">SPK Lama</a>
        </li>
        <li class="items">
            <a href="{{ route('bapendik_kp_suratlama') }}">Surat Lama</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Status Pra syarat kerja praktik mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="8">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@filterKpPrasyarat']) !!}
                        <table class="tablefilter">
                            <tr>
                                <td></td>
                                <td class="filter">
                                    <div class="field small">
                                        <label for="cari" class="ion-flag"></label>
                                        {!! Form::text('cari', '', ['placeholder'=>'NIM/Nama']) !!}
                                    </div>
                                </td>
                                <td class="filter">
                                    <div class="field small">
                                        <label for="statuskp" class="ion-flag"></label>
                                        <div class="styled-select">
                                            {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                        </div>
                                    </div>
                                </td>
                                <td class="filter">
                                    <div class="field small">
                                        <label for="statuskp" class="ion-flag"></label>
                                        <div class="styled-select">
                                            {!! Form::select('status', ['semua'=>'Semua Status', 'belum'=>'Belum Memenuhi', 'memenuhi'=>'Memenuhi Prasyarat'], 'semua') !!}
                                        </div>
                                    </div>
                                </td>
                                <td class="filter-btn">
                                        {!! Form::submit('cari', ['class' => 'button small']) !!}
                                </td>
                            </tr>
                        </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td class="fit">Jurusan</td>
                <td class="fit">I P K</td>
                <td class="fit">S K S</td>
                <td class="fit">No. Telp</td>
                <td class="fit">Status KP</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($listMhs)
                @foreach($listMhs as $m)
                <tr>
                    <td class="fit">{{ $no++ }}</td>
                    <td>{{ $m->nama }}</td>
                    <td class="fit">{{ $m->nim }}</td>
                    <td class="fit">{{ $m->jurusan->jurusan }}</td>
                    <td class="fit">{{ $m->ipk }}</td>
                    <td class="fit">{{ $m->sks }}</td>
                    <td class="fit">{{ $m->telp }}</td>
                    <td class="fit">{{ $m->status_prasyarat }}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $listMhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')