@extends('layouts.footer')

@section('bar-username')
    {{ $user->username }}
@endsection

@section('bar-page_title')
    semua dosen teknik
@endsection

@section('sidebar_link_dosen')
    active
@endsection

@section('bar-page_subtitle')
    daftar semua dosen teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('bapendik_menu', 'dosen') }}">semua dosen</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('bapendik_menu', 'dosbing') }}">dosen pembimbing</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    DAFTAR DOSEN
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'action'=>'BapendikController@filterDosbing']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('jurusan', ['semua'=>'Semua Jurusan'] + $listJurusan, 'semua') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Dosen</td>
                <td class="fit">NIP</td>
                <td class="fit">NIDN</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Kuota</td>
            </tr>
            </thead>
            <tbody>
            @if($listDosen)
                <?php $x=1 ?>
                @foreach($listDosen as $d)
                    <?php $dosbing = \App\ListDosen::find($d->dosbing1_id); ?>
                    <tr>
                        <td class="fit">{{ $x++ }}</td>
                        <td class="fit">{{ $dosbing->nama }}</td>
                        <td class="fit">{{ $dosbing->nip }}</td>
                        <td class="fit">{{ $dosbing->nidn }}</td>
                        <td class="fit">{{ $dosbing->jurusan->jurusan }}</td>
                        <td class="fit">{{ $dosbing->kuota }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')