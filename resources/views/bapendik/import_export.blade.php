@extends('layouts.footer')

@section('bar-username')
   bapendik
@endsection

@section('bar-page_title')
    import/export
@endsection

@section('sidebar_link_import')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('import_export') }}">Import</a>
        </li>
        <li class="items">
            <a href="{{ route('export') }}">Export</a>
        </li>
    </ul>
@endsection

@section('bar-page_subtitle')
    menyimpan/mendapatkan data terbaru dari sistem
@endsection

@section('bar-content')
    <div class="content withmenu">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-ios-cloud-upload icon"></span> unggah data
                </div>
            </div>
            @if(session()->has('import'))
            <div class="field noborder">
                <p>{{ session()->get('import') }}</p>
            </div>
            @endif
            <div class="main">
                {!! Form::open(['method'=>'post', 'action'=>'ExportImportController@import', 'files'=>true]) !!}
                <div class="field">
                    {!! Form::label("dataExport", null, ['class' => 'ion-android-document']) !!}
                    <div class="styled-select">
                        {!! Form::select('dataExport', ['' => 'Pilih', 'mahasiswa'=>'Mahasiswa', 'dosen'=>'Dosen', 'bapendik' => 'Bapendik', 'ruangan'=>'Ruangan', 'jurusan'=>'Jurusan'], null, ['id'=>'dataExport']) !!}
                    </div>
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('dataExport') : '' }}
                </div>
                <div class="field">
                    <label for="fileExcel" class="filefield">
                        <span class="ion-paperclip icon"></span>
                        Unggah Data
                    </label>
                    {!! Form::file('fileExcel', ['id'=>'fileExcel']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('fileExcel') : '' }}
                </div>
                <div class="field noborder right">
                    <a class="helper" href="{{ URL::to('download/sample/import') }}">
                        Download Format File
                    </a>
                    {!! Form::submit('Import', ['class'=>'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.bapendik.sidebar_content')

@extends('layouts.header')