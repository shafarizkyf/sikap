@extends('layouts.footer')

@section('bar-username')
    {{ $wda->nama }}
@endsection

@section('bar-page_title')
    Nilai kp mahasiswa
@endsection

@section('bar-page_subtitle')
    informasi nilai kerja praktik mahasiswa
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('wda_menu', 'mahasiswa') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('wda_menu', 'nilai') }}">nilai</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Daftar Mahasiswa
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Judul</td>
                <td>Instansi</td>
                <td class="fit">Nilai</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <?php
                    ?>
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nim->nama }}</td>
                        <td class="fit">{{ $m->nim->nim }}</td>
                        <td>{{ $m->nim->kp->judul_final }}</td>
                        <td>{{ $m->nim->kp->tempat->instansi }}</td>
                        <td class="fit">
                            @if($m->nim->nilai)
                                {{ $m->nim->nilai->nilai_akhir }} / {{ $m->nim->nilai->nilai_huruf }}
                            @else
                                -
                            @endif
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@extends('layouts.header')