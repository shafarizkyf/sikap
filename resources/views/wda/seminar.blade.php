@extends('layouts.footer')

@section('bar-username')
    {{ $dosen->nama }}
@endsection

@section('bar-page_title')
    jadwal seminar
@endsection

@section('sidebar-link_seminar')
    active
@endsection

@section('bar-page_subtitle')
    jadwal seminar mahasiswa teknik
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table class="seminar">
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    Jadwal Seminar kerja praktik
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="11">
                    {!! Form::open(['method'=>'get', 'action'=>['WdaController@menu', 'seminar']]) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="statuskp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('ruangan', []+$ruangan, $listRuangan) !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="center">Tanggal</td>
                <td class="center">SHIFT 1<span>(JAM 7-8)</span></td>
                <td class="center">SHIFT 2<span>(JAM 8-9)</span></td>
                <td class="center">SHIFT 3<span>(JAM 9-10)</span></td>
                <td class="center">SHIFT 4<span>(JAM 10-11)</span></td>
                <td class="center">SHIFT 5<span>(JAM 11-12)</span></td>
                <td class="center">SHIFT 6<span>(JAM 12-13)</span></td>
                <td class="center">SHIFT 7<span>(JAM 13-14)</span></td>
                <td class="center">SHIFT 8<span>(JAM 14-15)</span></td>
                <td class="center">SHIFT 9<span>(JAM 15-16)</span></td>
                <td class="center">SHIFT 10<span>(JAM 16-17)</span></td>
            </tr>
            </thead>
            <tbody>
            @for(; $dateToday <= $dateMonth->endOfMonth(); $dateToday->addDay())
                <tr>
                    @if($dateToday->isSunday())
                        @continue
                    @endif
                    @for($i=0; $i<=10; $i++)

                        <?php
                        $sc         = new \App\Http\Controllers\SeminarController();
                        $shift      = $sc->shift($i);
                        $today      = $dateToday->format('Y-m-d').' '.$shift;
                        $jadwal     = \App\MhsSeminar::where('jadwal', $today)->where('ruangan_id', $listRuangan)->first();
                        if($jadwal){
                            $mhs    = \App\ListMahasiswa::find($jadwal->nim_id);
                            $nama       = $sc->trimString($jadwal->nim->nama, 8);
                            $jurusan    = $sc->trimString($jadwal->nim->jurusan->jurusan, 12);
                            $dosbing    = \App\Http\Controllers\GetDosenDataController::getMhsDosbing($jadwal->nim_id);
                            $dosbing    = ($dosbing) ? $dosbing->dosbing1->id : null;                        
                        }
                        ?>

                        @if($i==0)
                            <td class="center caption">
                                {{ $dateToday->format('F') }}
                                <span class="big">{{ $dateToday->format('d') }}</span>
                                {{ $dateToday->format('Y') }}
                            </td>
                        @endif

                        @if($i != 0 and $jadwal)
                            <td class="center">
                                @if(!$jadwal->status)
                                    <span class="mark ion-help-circled"></span>
                                @endif
                                {{ strtoupper($jadwal->jenis) }}<br>
                                {{ $jadwal->nim->nim }}<br>
                                {{ $nama }}<br>
                                <span>{{ $jurusan }}</span>
                                @if(!$jadwal->status and $dosen->id == $dosbing)
                                    <a href="{{ route('seminar_selesai', $jadwal->id) }}" class="button small"><span class="ion-checkmark"></span></a>
                                    <a href="{{ route('seminar_ulang', $jadwal->id) }}" class="button small"><span class="ion-refresh"></span></a>
                                @elseif($jadwal->status == 'ulang')
                                    <span class="mark ion-refresh "></span>
                                @elseif($jadwal->status == 'selesai')
                                    <span class="mark ion-checkmark-circled "></span>
                                @endif
                            </td>
                        @elseif($i != 0)
                            <td></td>
                        @endif
                    @endfor
                </tr>
            @endfor
            </tbody>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@extends('layouts.header')