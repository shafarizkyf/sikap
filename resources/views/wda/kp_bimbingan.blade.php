@extends('layouts.footer')

@section('bar-username')
    {{ $wda->nama }}
@endsection

@section('bar-page_title')
    mahasiswa kp
@endsection

@section('bar-page_subtitle')
    mahasiswa kp dalam bimbingan anda
@endsection

@section('sidebar-link_dosbing')
    active
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    mahasiswa kp
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">N I M</td>
                <td>Judul KP</td>
                <td>Tempat</td>
                <td class="fit">Status</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <?php
                    $judul      = ($m->judul_final) ? $m->judul_final : $m->judul;
                    $seminar    = \App\MhsSeminar::where('nim_id', $m->nim_id)->first();
                    ?>
                    <tr>
                        <td class="fit">{{ $no++ }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td>{{ $judul }}</td>
                        <td>{{ $m->instansi }}</td>
                        <td class="fit">{{ ($m->status_bimbingan) ? $m->status_bimbingan : '-' }}</td>
                        <td class="fit">
                            @if(!$m->status_bimbingan)
                                <div class="buttonwrapper">
                                    {!! Form::open(['method'=>'post', 'action'=>['WdaController@unlockBimbingan', $m->nim_id]]) !!}
                                    {!! Form::submit('mulai bimbingan', ['class' => 'button small']) !!}
                                    {!! Form::close() !!}
                                </div>

                            @elseif($m->status_bimbingan == 'selesai' and !$m->status_kp)
                                <div class="button-wrapper">
                                    <a href="{{ route('wda_lihat_bimbingan', $m->nim) }}" class="button small">
                                        <span class="ion-eye icon"></span>
                                        bimbingan
                                    </a>
                                    @if($seminar)
                                    <a href="#ex1" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small detail-btn">
                                        <span class="ion-star"></span>
                                        nilai kp
                                    </a>
                                    @endif
                                </div>

                            @elseif($m->status_bimbingan == 'selesai' and $m->status_kp)
                                <div class="button-wrapper">
                                    <a href="{{ route('wda_lihat_bimbingan', $m->nim) }}" class="button small">
                                        <span class="ion-eye icon"></span>
                                        bimbingan
                                    </a>
                                    <a href="#ex1" aria-nim="{{ $m->nim }}" rel="modal:open" class="button small detail-btn">
                                        <span class="ion-star"></span>
                                        nilai kp
                                    </a>
                                    @if($berkas AND $m->status_distribusi == 'ok')
                                        <a href="{{ asset('mahasiswa_berkas/'.$jurusan->jurusan.'/'.$berkas->pengesahan) }}" class="button small">
                                            <span class="ion-arrow-down-a icon"></span>
                                            pengesahan
                                        </a>
                                    @endif
                                </div>
                            @else
                                <a href="{{ route('wda_lihat_bimbingan', $m->nim) }}" class="button small">
                                    <span class="ion-eye icon"></span>
                                    bimbingan
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nim = this.getAttribute('aria-nim');
                getDetailMhs(nim);
            });

            function getDetailMhs(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/nilai/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.wda.sidebar_content')

@extends('layouts.header')