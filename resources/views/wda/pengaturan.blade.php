@extends('layouts.footer')

@section('bar-username')
    Wakil Dekan bd Akademik
@endsection

@section('bar-page_title')
    Pengaturan Sistem
@endsection

@section('bar-page_subtitle')
    Pengaturan Penetapan SKS KP dan Jumlah Bimbingan    
@endsection

@section('sidebar-link_pengaturan')
    active
@endsection

@section('bar-content')
    <div class="content">
        <div class="panel form">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-locked icon"></span> Pengaturan Sistem
                </div>
            </div>
            <div class="main">
                @if(session()->has('keamanan'))
                    <div class="field noborder error">
                        {{ session()->get('keamanan') }}
                    </div>
                @endif
                {!! Form::open(['method'=>'post', 'action'=>'WdaController@updateSettings']) !!}

                <p>Minimum SKS KP</p>
                <div class="field">
                    {!! Form::label('skskp', null, ['class'=>'ion-key']) !!}
                    {!! Form::text('skskp', $settings['sks_minimum'], ['placeholder' => 'Minimum SKS KP']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('skskp') : '' }}
                </div>
                
                <p>Minimum IPK KP</p>
                <div class="field">
                    {!! Form::label('ipkkp', null, ['class'=>'ion-key']) !!}
                    {!! Form::text('ipkkp', $settings['ipk_minimum'], ['placeholder' => 'Minimum IPK KP']) !!}
                </div>
                
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('ipkkp') : '' }}
                </div>
                
                <p>Minimum SKS Masuk Sistem</p>
                <div class="field">
                    {!! Form::label('skssistem', null, ['class'=>'ion-key']) !!}
                    {!! Form::text('skssistem', $settings['sks_login'], ['placeholder' => 'Minimum SKS Login']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('skssistem') : '' }}
                </div>
                <p>Minimum Jumlah Bimbingan KP</p>
                <div class="field">
                    {!! Form::label('bimbinganmin', null, ['class'=>'ion-key']) !!}
                    {!! Form::text('bimbinganmin', $settings['bimbingan_minimum'], ['placeholder' => 'Minimum Bimbingan KP']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('bimbinganmin') : '' }}
                </div>
                <p>Masa SPK (Dalam Hari)</p>
                <div class="field">
                    {!! Form::label('masaspk', null, ['class'=>'ion-key']) !!}
                    {!! Form::text('masaspk', $settings['masa_spk'], ['placeholder' => 'Masa SPK (Dalam Hari)']) !!}
                </div>
                <div class="field noborder error">
                    {{ ($errors->all()) ? $errors->first('masaspk') : '' }}
                </div>
                <div class="field noborder center">
                    {!! Form::submit('Simpan', ['class'=>'button']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@extends('layouts.header')