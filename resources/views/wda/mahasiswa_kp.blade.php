@extends('layouts.footer')

@section('bar-username')
    {{ $wda->nama }}
@endsection

@section('bar-page_title')
    mahasiswa sedang kp
@endsection

@section('bar-page_subtitle')
    informasi mahasiswa dalam progress kp
@endsection

@section('sidebar-link_dosenpa')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('wda_menu', 'mhsa') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('wda_menu', 'mhsakp') }}">dalam proses kp</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Daftar Mahasiswa
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td>Judul</td>
                <td class="fit">Instansi</td>
                <td class="fit">Nilai</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <?php
                    $tempat = \App\MhsSuratPengantarKp::find($m->tempat_id);
                    $nilai  = \App\MhsNilai::where('nim_id', $m->nim_id)->first();
                    $huruf  = ($nilai) ? \App\Http\Controllers\NilaiController::convertToHuruf($nilai->nilai_akhir) : null;
                    ?>
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td>{{ ($m->judul_final) ? $m->judul_final : $m->judul }}</td>
                        <td class="fit">{{ ($tempat) ? $tempat->instansi : 'belum mengajukan' }}</td>
                        <td class="fit">{{ ($nilai) ? $nilai->nilai_akhir.' / ' : 'belum dinilai' }} {{ $huruf }}</td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@extends('layouts.header')