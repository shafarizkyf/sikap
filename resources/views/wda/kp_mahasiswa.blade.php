@extends('layouts.footer')

@section('bar-username')
    {{ $wda->nama }}
@endsection

@section('bar-page_title')
    mahasiswa sedang kp
@endsection

@section('bar-page_subtitle')
    informasi mahasiswa dalam progress kp
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('wda_menu', 'mahasiswa') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('wda_menu', 'nilai') }}">nilai</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <?php $settings = json_decode(file_get_contents(storage_path('settings.json')), true); ?>
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Daftar Mahasiswa
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Masa SPK</td>
                <td class="fit">Sisa Waktu</td>
                <td class="fit"></td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhskp)
                @foreach($mhskp as $m)
                    @if($m->nim->cekSeminar())
                        <tr>
                    @else
                        @if($m->getSisaWaktuBulan()<=1)
                            <tr style="background: #ffb6b6">
                        @elseif($m->getSisaWaktuBulan()==2)
                            <tr style="background: #fff1b6">
                        @else
                            <tr>
                        @endif
                    @endif
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nim->nama }}</td>
                        <td class="fit">{{ $m->nim->nim }}</td>
                        <td class="fit">{{ $m->nim->jurusan->jurusan }}</td>
                        <td class="fit">
                            {{ $m->waktu_spk_mulai->format('d M Y') }} - 
                            {{ $m->waktu_spk_selesai->format('d M Y') }}
                        </td>
                        <td class="fit">
                        @if($m->nim->cekSeminar())
                            Sudah Seminar
                        @else
                            @if($m->getSisaWaktuHari()<=0)
                                Habis
                            @elseif($m->getSisaWaktuBulan()==0 and $m->getSisaWaktuHari() > 0)
                                {{$m->getSisaWaktuHari() }} hari
                            @elseif($m->getSisaWaktuBulan()>0)
                                {{$m->getSisaWaktuBulan() }} bulan
                                ({{$m->getSisaWaktuHari() }} hari)
                            @endif
                        @endif
                        </td>
                        <td>
                            <?php $masaSpk = $m->waktu_spk_selesai->diffInDays($m->waktu_spk_mulai); ?>
                            @if($masaSpk > 180)
                            <div class="buttonwrapper">
                                <span class="button small update-spk" style="background: #f68f1a; cursor:default; box-shadow: none;">
                                    <span class="ion-checkmark"></span>
                                    Diperpanjang {{ ($masaSpk - $settings['masa_spk']).'H' }}
                                </span>
                            </div>
                            @endif
                        </td>
                        <td>
                            <div class="buttonwrapper">
                                <a href="#ex1" aria-nim="{{ $m->nim->nim }}" rel="modal:open" class="button small update-spk">
                                    <span class="ion-eye icon"></span>
                                    Perpanjang SPK
                                </a>
                            </div>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhskp->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            $('.update-spk').click(function () {
                var nim= this.getAttribute('aria-nim');
                setSpk(nim);
            });

            function setSpk(nim) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/addspk/' ?>" + nim,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

        });
    </script>
@endsection

@extends('layouts.header')