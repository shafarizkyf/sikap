@extends('layouts.footer')

@section('bar-username')
    {{ $wda->nama }}
@endsection

@section('bar-page_title')
    mahasiswa bimbingan akademik
@endsection

@section('bar-page_subtitle')
    informasi mahasiswa dalam bimbingan akademik
@endsection

@section('sidebar-link_dosenpa')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('wda_menu', 'mhsa') }}">semua mahasiswa</a>
        </li>
        <li class="items">
            <a href="{{ route('wda_menu', 'mhsakp') }}">dalam proses kp</a>
        </li>
    </ul>
@endsection


@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    Daftar Mahasiswa
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'get', 'route'=>['wda_menu', 'mhsa']]) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="layakkp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('layakkp', [''=>'Syarat KP', '1'=>'Menenuhi Syarat', '0'=>'Belum Memenuhi'], '') !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="kp" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('kp', [''=>'Pelaksanaan KP', '1'=>'Sedang/Sudah KP', '0'=>'Belum KP'], '') !!}
                                    </div>
                                </div>                                
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('cari', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>                                    
            <tr>
                <td class="fit">#</td>
                <td>Nama Mahasiswa</td>
                <td class="fit">NIM</td>
                <td class="fit">IPK</td>
                <td class="fit">SKS</td>
                <td class="fit">Prasyarat KP</td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @if($mhs)
                @foreach($mhs as $m)
                    <tr>
                        <td class="fit">{{ $no }}</td>
                        <td>{{ $m->nama }}</td>
                        <td class="fit">{{ $m->nim }}</td>
                        <td class="fit">{{ $m->ipk }}</td>
                        <td class="fit">{{ $m->sks }}</td>
                        <td class="fit">{{ $m->status_prasyarat }}</td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@extends('layouts.header')