@extends('layouts.footer')

@section('bar-username')
    {{ $wda->nama }}
@endsection

@section('bar-page_title')
    kuota dosen
@endsection

@section('sidebar-link_dosen')
    active
@endsection

@section('bar-page_subtitle')
    permintaah kuota tambahan dosen teknik
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('wda_menu', 'dosen') }}">semua dosen</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('wda_menu', 'kuota') }}">permintaan kuota tambahan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="9">
                    DAFTAR DOSEN
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama Dosen</td>
                <td class="fit">NIP</td>
                <td class="fit">NIDN</td>
                <td class="fit">Jurusan</td>
                <td class="fit">Permintaan Kuota</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            @if($dosen)
                <?php $x=1 ?>
                @foreach($dosen as $d)
                <?php
                    $jurusan = \App\ListJurusan::find($d->jurusan_id);
                ?>
                    <tr>
                        <td class="fit">{{ $x++ }}</td>
                        <td>{{ $d->nama }}</td>
                        <td class="fit">{{ $d->nip }}</td>
                        <td class="fit">{{ $d->nidn }}</td>
                        <td class="fit">{{ $jurusan->jurusan }}</td>
                        <td class="fit">{{ $d->jumlah }}</td>
                        <td class="fit">
                            <a href="{{ route('wda_addkuota', $d->id) }}" class="button small">
                                <span class="ion-person icon"></span>
                                Acc
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $dosen->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.wda.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.button').click(function () {
                var nip= this.getAttribute('aria-nip');
                getListMhs(nip);
            });

            function getListMhs(nip) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/mhsbimbingan/' ?>" +nip,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')