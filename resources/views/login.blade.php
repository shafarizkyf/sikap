@extends('layouts.footer')

@section('header-content1')
    <div class="content">
        <div class="panel">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-locked icon"></span> LOGIN USER
                </div>
            </div>
            <div class="login-header">
                <div class="text">
                    Sistem Informasi Kerja Praktik
                    <p class="secondary">
                        Fakultas Teknik Universitas Jenderal Soedirman
                    </p>
                </div>
            </div>
            <div class="main">
                <div class="main">
                    {!! Form::open(['method'=>'post', 'action'=>'UserLoginController@process']) !!}
                    <div class="field">
                        {!! Form::label('username', null, ['class' => 'ion-person']) !!}
                        {!! Form::text('username', null, ['id' => 'username', 'placeholder' => 'Username']) !!}
                    </div>
                    @if($errors->first('username'))
                    <div class="field noborder error">
                        {{ $errors->first('username') }}
                    </div>
                    @endif
                    <div class="field">
                        {!! Form::label('password', null, ['class' => 'ion-locked']) !!}
                        {!! Form::password('password',  ['id' => 'password', 'placeholder' => 'Password']) !!}
                    </div>
                    @if($errors->first('password'))
                        <div class="field noborder error">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                    <div class="field noborder right">
                        <a class="helper" href="{{ route('home_index') }}">
                            Home
                        </a>
                        {{ Form::submit('Masuk', ['class'=>'button']) }}
                    </div>
                    {!! Form::close() !!}
                    @if(session()->has('login'))
                    <div class="field noborder error">
                        {{ session()->get('login') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@extends('layouts.header')