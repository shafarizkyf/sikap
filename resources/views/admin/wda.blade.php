@extends('layouts.footer')

@section('bar-username')
    {{ $admin->username }}
@endsection

@section('bar-page_title')
    ketua jurusan di teknik
@endsection

@section('bar-page_subtitle')
    perubahan status jabatan ketua jurusan
@endsection

@section('sidebar-link_dosen')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('admin_menu', 'dosen') }}">semua dosen</a>
        </li>
        <li class="items">
            <a href="{{ route('admin_menu', 'kajur') }}">ketua jurusan</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('admin_menu', 'wda') }}">wakil dekan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content">
        <div class="panel">
            <div class="tag">
                <div class="tag-content">
                    <span class="ion-person icon"></span>
                    Set Wakil Dekan Akademik
                </div>
            </div>
            <div class="login-header">
            </div>
            @if(Session::has('wda'))
            <div class="field noborder">
                <p>{{ session()->get('wda') }}</p>
            </div>
            @endif
            {!! Form::open(['method'=>'post', 'action'=>'AdminController@setWda']) !!}
            <div class="main">
                <div class="field">
                    {!! Form::label("dosen", null, ['class' => 'ion-ios-person']) !!}
                    <div class="styled-select">
                        {!! Form::select('dosen', [''=>'Pilih Wakil Dekan Akademik'] + $dosen, $wda->user_id) !!}
                    </div>
                </div>
                <div class="field noborder">
                    {{ Form::submit('Simpan', ['class'=>'button']) }}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.admin.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
@endsection

@extends('layouts.header')