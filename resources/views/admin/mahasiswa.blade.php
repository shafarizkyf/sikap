@extends('layouts.footer')

@section('bar-username')
    {{ $admin->username }}
@endsection

@section('bar-page_title')
    mahasiswa teknik
@endsection

@section('bar-page_subtitle')
    perubahan status dosen pa dan akun
@endsection

@section('sidebar-link_mhs')
    active
@endsection

@section('bar-content')
    @if(session()->has('nim'))
        <div class="kp-status">
            <h3 class="title">{{ session()->get('nim') }}</h3>
        </div>
    @endif
    @if(session()->has('reset'))
        <div class="kp-status">
            <h3 class="title">{{ session()->get('reset') }}</h3>
        </div>
    @endif
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    daftar mahasiswa teknik
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="9">
                    {!! Form::open(['method'=>'post', 'action'=>'AdminController@setDosenpa']) !!}
                    <table class="tablefilter">
                        <tr>
                            <td></td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="dari" class="ion-flag"></label>
                                    {!! Form::text('dari', '', ['placeholder'=>'Dari (contoh. H1L014001)']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="ke" class="ion-flag"></label>
                                    {!! Form::text('ke', '', ['placeholder'=>'Ke (contoh. H1L014050)']) !!}
                                </div>
                            </td>
                            <td class="filter">
                                <div class="field small">
                                    <label for="dosenpa" class="ion-flag"></label>
                                    <div class="styled-select">
                                        {!! Form::select('dosenpa', [] + $dosen, null, ['placeholder'=>'Pilih Dosen PA']) !!}
                                    </div>
                                </div>
                            </td>
                            <td class="filter-btn">
                                {!! Form::submit('Simpan', ['class' => 'button small']) !!}
                            </td>
                        </tr>
                    </table>
                    {!! Form::close() !!}
                </td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama</td>
                <td class="fit">NIM</td>
                <td class="fit">Jurusan</td>
                <td>Dosen PA</td>
                <td class="fit">Telp</td>
                <td class="fit"></td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @foreach($mhs as $m)
                <tr>
                    <td class="fit">{{ $no++ }}</td>
                    <td>{{ $m->nama }}</td>
                    <td class="fit">{{ $m->nim }}</td>
                    <td class="fit">{{ $m->jurusan->jurusan }}</td>
                    <td>{{ ($m->dosenpa) ? $m->dosenpa->nama : '-' }}</td>
                    <td class="fit">{{ ($m->telp) ? $m->telp : '-' }}</td>
                    <td>
                        <div class="buttonwrapper">
                            {!! Form::open(['method'=>'post', 'action'=>['AdminController@resetPasswordMhs', $m->id]]) !!}
                            {!! Form::submit('Reset Password', ['class'=>'button small']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>
                    <td>
                        <div class="buttonwrapper">                            
                            {!! Form::open(['method'=>'post', 'action'=>['AdminController@resetAkunMahasiswa', $m->id]]) !!}
                            {!! Form::submit('Reset Ke Mode KP', ['class'=>'button small']) !!}
                            {!! Form::close() !!}
                        </div>
                    </td>

                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $mhs->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.admin.sidebar_content')

@extends('layouts.header')