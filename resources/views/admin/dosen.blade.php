@extends('layouts.footer')

@section('bar-username')
    {{ $admin->username }}
@endsection

@section('bar-page_title')
    dosen teknik
@endsection

@section('bar-page_subtitle')
    perubahan status jabatan dan penggunaan sistem
@endsection

@section('sidebar-link_dosen')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a class="active" href="{{ route('admin_menu', 'dosen') }}">semua dosen</a>
        </li>
        <li class="items">
            <a href="{{ route('admin_menu', 'kajur') }}">ketua jurusan</a>
        </li>
        <li class="items">
            <a href="{{ route('admin_menu', 'wda') }}">wakil dekan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    daftar dosen teknik
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7"></td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama</td>
                <td class="fit">Jurusan</td>
                <td class="fit">NIP</td>
                <td class="fit">NIDN</td>
                <td class="fit">Level User</td>
                <td class="fit">Akun</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            <?php $no = 1; ?>
            @foreach($dosen as $d)
            <tr>
                <td class="fit">{{ $no++ }}</td>
                <td>{{ $d->nama }}</td>
                <td class="fit">{{ $d->jurusan }}</td>
                <td class="fit">{{ $d->nip }}</td>
                <td class="fit">{{ $d->nidn }}</td>
                <td class="fit">{{ $d->status }}</td>
                <td class="fit">{{ $d->is_active }}</td>
                <td class="fit">
                    <div class="buttonwrapper">
                        <a href="#ex1" aria-nip="{{ $d->nip }}" rel="modal:open" class="button small detail-btn">
                            <span class="ion-eye icon"></span>
                            Edit Data
                        </a>
                        <a href="#ex2" aria-nip="{{ $d->nip }}" rel="modal:open" class="button small akun-btn">
                            <span class="ion-eye icon"></span>
                            Edit Akun
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="7">
                    <ul class="pagination">
                        <li>{{ $dosen->links() }}</li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;"></div>
    <div id="ex2" class="small" style="display:none;"></div>
@endsection

@extends('layouts.bar')

@extends('layouts.admin.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.detail-btn').click(function () {
                var nip= this.getAttribute('aria-nip');
                getDetailDosen(nip);
            });

            $('.akun-btn').click(function () {
                var nip= this.getAttribute('aria-nip');
                getDosenAkun(nip);
            });

            function getDetailDosen(nip) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/dosendetail/' ?>" + nip,
                    success: function (data) {
                        $('#ex1').html(data);
                    }
                });
            }

            function getDosenAkun(nip) {
                $.ajax({
                    type: 'get',
                    url: "<?php echo \Illuminate\Support\Facades\URL::to('/').'/modal/dosenakun/' ?>" + nip,
                    success: function (data) {
                        $('#ex2').html(data);
                    }
                });
            }
        });
    </script>
@endsection

@extends('layouts.header')