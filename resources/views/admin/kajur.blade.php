@extends('layouts.footer')

@section('bar-username')
    {{ $admin->username }}
@endsection

@section('bar-page_title')
    ketua jurusan di teknik
@endsection

@section('bar-page_subtitle')
    perubahan status jabatan ketua jurusan
@endsection

@section('sidebar-link_dosen')
    active
@endsection

@section('bar-menu')
    <ul class="menu">
        <li class="items">
            <a href="{{ route('admin_menu', 'dosen') }}">semua dosen</a>
        </li>
        <li class="items">
            <a class="active" href="{{ route('admin_menu', 'kajur') }}">ketua jurusan</a>
        </li>
        <li class="items">
            <a href="{{ route('admin_menu', 'wda') }}">wakil dekan</a>
        </li>
    </ul>
@endsection

@section('bar-content')
    @if(session()->has('kajur'))
        <div class="kp-status">
            <h3 class="title">{{ session()->get('kajur') }}</h3>
        </div>
    @endif
    <div class="content withmenu tableinside">
        <table>
            <thead>
            <tr class="tabletitle">
                <td colspan="7">
                    daftar ketua jurusan
                </td>
            </tr>
            <tr class="withtablefilter">
                <td colspan="7"></td>
            </tr>
            <tr>
                <td class="fit">#</td>
                <td>Nama</td>
                <td class="fit">Jurusan</td>
                <td class="fit">NIP</td>
                <td class="fit">NIDN</td>
                <td class="fit"></td>
            </tr>
            </thead>
            <tbody>
            @if($kajur)
                <?php $no=1; ?>
                @foreach($kajur as $k)
                    <tr>
                        <td class="fit">{{ $no++ }}</td>
                        <td>{{ $k->dosen->nama }}</td>
                        <td class="fit">{{ $k->jurusan->jurusan }}</td>
                        <td class="fit">{{ $k->dosen->nip }}</td>
                        <td class="fit">{{ $k->dosen->nidn }}</td>
                        <td class="fit">
                            <div class="button-wrapper">
                                <a href="{{ route('admin_hapus_kajur', $k->id) }}" class="button small">
                                    <span class="ion-trash-a icon"></span>
                                    Hapus
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
            <tfoot>
            <tr>
                <td colspan="6" class="right">
                    <a href="#ex1" rel="modal:open" class="button small add-btn">
                        <span class="ion-person icon"></span>
                        Set Ketua Jurusan
                    </a>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div id="ex1" class="small" style="display:none;">
        <div class="tag">
            <div class="tag-content">Set Ketua Jurusan</div>
        </div>
        <div class="modal-content">
            {!! Form::open(['method'=>'post ', 'action'=>'ModalController@setKajur']) !!}
            <div class="field">
                <label for="kajur" class="ion-person"></label>
                <div class="styled-select">
                    {!! Form::select('kajur', [] + $dosen, null, ['placeholder' => 'Pilih Ketua Jurusan']) !!}
                </div>
            </div>
            <div class="field">
                <label for="jurusan" class="ion-person"></label>
                <div class="styled-select">
                    {!! Form::select('jurusan', [] + $jurusan, null, ['placeholder' => 'Pilih Jurusan']) !!}
                </div>
            </div>
            <div class="field noborder center">
                {{ Form::submit('Simpan', ['class'=>'button']) }}
            </div>
        </div>
    </div>
@endsection

@extends('layouts.bar')

@extends('layouts.admin.sidebar_content')

@section('head')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.modal.css') }}" />
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery-modal/jquery.modal.js') }}"></script>
    <script src="{{ asset('js/less/less.min.js') }}"></script>
@endsection

@extends('layouts.header')