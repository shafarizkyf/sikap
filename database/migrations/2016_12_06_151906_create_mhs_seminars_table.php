<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsSeminarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_seminars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id')->index();
            $table->string('nomor');
            $table->integer('ruangan_id');
            $table->timestamp('jadwal')->nullable();
            $table->integer('shift');
            $table->enum('jenis',['kp', 'semprop', 'semhas', 'kolokium'])->nullable();
            $table->enum('status',['selesai', 'ulang'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_seminars');
    }
}
