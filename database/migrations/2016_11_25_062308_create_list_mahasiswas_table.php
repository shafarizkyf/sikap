<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_mahasiswas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nim')->unique();
            $table->integer('jurusan_id')->index()->unsigned();
            $table->integer('dosenpa_id');
            $table->string('nama');
            $table->enum('kelamin',['laki-laki', 'perempuan']);
            $table->integer('sks');
            $table->float('ipk');
            $table->string('email');
            $table->string('telp');
            $table->string('tahun_ajaran');
            $table->enum('status_prasyarat', ['belum', 'memenuhi'])->nullable();
            $table->enum('level', ['kp', 'ta'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('list_mahasiswas');
    }
}
