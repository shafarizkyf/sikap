<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsPembimbingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_pembimbings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->integer('dosbing1_id');
            $table->integer('dosbing2_id');
            $table->string('pemlap');
            $table->string('pemlap_telp');
            $table->integer('dosbinglama_id');
            $table->integer('dosbinglama2_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_pembimbings');
    }
}
