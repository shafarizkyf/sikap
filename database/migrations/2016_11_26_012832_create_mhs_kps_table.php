<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsKpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_kps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->integer('tempat_id');
            $table->string('surat');
            $table->string('outline');
            $table->string('judul');
            $table->string('judul_final');
            $table->string('koreksi');
            $table->string('revisi');
            $table->timestamp('waktu_daftar')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_kps');
    }
}
