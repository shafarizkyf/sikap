<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsGantiDosbingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_ganti_dosbings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->integer('dosbing_id');
            $table->integer('dosbing2_id');
            $table->string('alasan');
            $table->enum('status', ['diterima', 'ditolak'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_ganti_dosbings');
    }
}
