<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsBerkasSeminarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_berkas_seminars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id')->index();
            $table->string('ksm');
            $table->string('makalah');
            $table->string('surat_selesai');
            $table->string('surat_spk');
            $table->string('kartu_seminar');
            $table->string('kesimpulan');
            $table->enum('status_berkas',['lengkap', 'koreksi'])->nullable();
            $table->enum('status_koreksi',['sudah', 'belum'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_berkas_seminars');
    }
}
