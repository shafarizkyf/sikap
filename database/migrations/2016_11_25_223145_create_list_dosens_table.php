<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_dosens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jurusan_id')->index()->unsigned();
            $table->string('nip');
            $table->string('nidn');
            $table->string('nama');
            $table->string('telp');
            $table->integer('kuota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('list_dosens');
    }
}
