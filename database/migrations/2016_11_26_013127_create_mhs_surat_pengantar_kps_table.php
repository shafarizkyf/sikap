<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsSuratPengantarKpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_surat_pengantar_kps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->string('nomor');
            $table->string('instansi');
            $table->string('kepada');
            $table->string('dimana');
            $table->timestamp('waktu_ajukan')->nullable();
            $table->timestamp('waktu_selesai')->nullable();
            $table->enum('status', ['menunggu', 'proses', 'selesai']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_surat_pengantar_kps');
    }
}
