<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsBimbingansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_bimbingans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->text('bimbingan');
            $table->timestamp('waktu_bimbingan')->nullable();
            $table->enum('status', ['finalisasi'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_bimbingans');
    }
}
