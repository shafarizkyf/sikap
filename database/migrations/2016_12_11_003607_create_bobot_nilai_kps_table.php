<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBobotNilaiKpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bobot_nilai_kps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jurusan_id')->index();
            $table->integer('bobot1');
            $table->integer('bobot2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bobot_nilai_kps');
    }
}
