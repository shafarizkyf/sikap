<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsProgressStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_progress_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->enum('status_berkas',['lengkap','koreksi'])->nullable();
            $table->enum('status_koreksi', ['sudah', 'belum'])->nullable();
            $table->enum('status_judul', ['layak', 'revisi', 'tolak'])->nullable();
            $table->enum('status_judul_final', ['ok', 'revisi'])->nullable();
            $table->enum('spk_berkas', ['menunggu', 'jadi'])->nullable();
            $table->enum('status_spk', ['aktif', 'habis'])->nullable();
            $table->timestamp('waktu_berkas_lengkap')->nullable();
            $table->timestamp('waktu_judul_layak')->nullable();
            $table->timestamp('waktu_spk_jadi')->nullable();
            $table->timestamp('waktu_spk_mulai')->nullable();
            $table->timestamp('waktu_spk_selesai')->nullable();
            $table->enum('status_bimbingan', ['aktif', 'selesai'])->nullable();
            $table->enum('status_distribusi', ['ok', 'koreksi'])->nullable();
            $table->enum('status_kp',['selesai','tidak','ulang'])->nullable();
            $table->timestamp('waktu_kp_selesai')->nullable()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_progress_statuses');
    }
}
