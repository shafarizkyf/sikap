<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKuotaDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuota_dosens', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('komisi_id');
            $table->integer('dosen_id');
            $table->integer('jumlah');
            $table->enum('status', ['menunggu', 'terpenuhi']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kuota_dosens');
    }
}
