<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsBerkasDistribusisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_berkas_distribusis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->string('form');
            $table->string('pengesahan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_berkas_distribusis');
    }
}
