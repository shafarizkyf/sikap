<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsKpUlangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_kp_ulangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id');
            $table->string('tempat');
            $table->string('surat');
            $table->string('outline');
            $table->string('judul');
            $table->string('judul_final');
            $table->timestamp('waktu_daftar')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_kp_ulangs');
    }
}
