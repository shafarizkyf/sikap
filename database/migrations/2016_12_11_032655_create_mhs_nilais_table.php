<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMhsNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mhs_nilais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nim_id')->index();
            $table->float('nilai1');
            $table->float('nilai2');
            $table->float('nilai3');
            $table->float('bobot1');
            $table->float('bobot2');
            $table->float('bobot3');
            $table->float('nilai_akhir');
            $table->enum('nilai_huruf', ['A', 'AB', 'B', 'BC', 'C', 'CD', 'D', 'E'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mhs_nilais');
    }
}
